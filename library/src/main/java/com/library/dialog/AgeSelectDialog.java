package com.library.dialog;


import com.library.constant.CommonData;
import com.library.widgets.WheelView;

/**
 * 年龄选择对话框
 * Created by zhangdroid on 2016/6/24.
 */
public class AgeSelectDialog extends OneWheelDialog {
    private OnOneWheelDialogClickListener mOnDialogClickListener;

    public static AgeSelectDialog newInstance(String title, String positive, String negative, boolean isCancelable, OnOneWheelDialogClickListener listener) {
        AgeSelectDialog ageSelectDialog = new AgeSelectDialog();
        ageSelectDialog.mOnDialogClickListener = listener;
        ageSelectDialog.setArguments(getDialogBundle(title, null, positive, negative, isCancelable));
        return ageSelectDialog;
    }

    @Override
    protected void setWheelView(WheelView wheelView) {
        wheelView.setData(CommonData.getAgeList());
        wheelView.setDefaultIndex(6);// 设置默认年龄为24岁
    }

    @Override
    protected OnOneWheelDialogClickListener setOnDialogClickListener() {
        return mOnDialogClickListener;
    }

}
