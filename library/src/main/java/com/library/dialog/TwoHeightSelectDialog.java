package com.library.dialog;


import com.library.constant.CommonData;
import com.library.utils.HeightUtils;
import com.library.widgets.WheelView;

/**
 * 双列年龄选择对话框
 * Created by zhangdroid on 2016/6/25.
 */
public class TwoHeightSelectDialog extends TwoWheelDialog {
    private OnTwoWheelDialogClickListener mOnDialogClickListener;

    public static TwoHeightSelectDialog newInstance(String title, String positive, String negative, boolean isCancelable, OnTwoWheelDialogClickListener listener) {
        TwoHeightSelectDialog twoHeightSelectDialog = new TwoHeightSelectDialog();
        twoHeightSelectDialog.mOnDialogClickListener = listener;
        twoHeightSelectDialog.setArguments(getDialogBundle(title, null, positive, negative, isCancelable));
        return twoHeightSelectDialog;
    }

    @Override
    protected void setWheelView1(WheelView wheelView) {
        wheelView.setData(HeightUtils.getInchCmList());
        wheelView.setDefaultIndex(6);// 设置默认年龄为24岁
    }

    @Override
    protected void setWheelView2(WheelView wheelView) {
        wheelView.setData(HeightUtils.getInchCmList());
        wheelView.setDefaultIndex(6);// 设置默认年龄为24岁
    }

    @Override
    protected OnTwoWheelDialogClickListener setOnDialogClickListener() {
        return mOnDialogClickListener;
    }

}
