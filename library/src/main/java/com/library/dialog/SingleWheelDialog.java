package com.library.dialog;


import com.library.constant.CommonData;
import com.library.widgets.WheelView;

import java.util.List;

/**
 * 年龄选择对话框
 * Created by zhangdroid on 2016/6/24.
 */
public class SingleWheelDialog extends OneWheelDialog {
    private OnOneWheelDialogClickListener mOnDialogClickListener;
    // 数据源
    private List<String> mDataList;
    // 默认选项索引
    private int mDefaultIndex = 0;

    public static SingleWheelDialog newInstance(List<String> dataList, String title, String positive, String negative, boolean isCancelable, OnOneWheelDialogClickListener listener) {
        SingleWheelDialog singleWheelDialog = new SingleWheelDialog();
        singleWheelDialog.mOnDialogClickListener = listener;
        singleWheelDialog.mDataList = dataList;
//        singleWheelDialog.mDefaultIndex = defaultIndex;
        singleWheelDialog.setArguments(getDialogBundle(title, null, positive, negative, isCancelable));
        return singleWheelDialog;
    }

    @Override
    protected void setWheelView(WheelView wheelView) {
        wheelView.setData(mDataList);
        if (mDefaultIndex < 0) {
            mDefaultIndex = 0;
        }
        wheelView.setDefaultIndex(mDefaultIndex);
    }

    @Override
    protected OnOneWheelDialogClickListener setOnDialogClickListener() {
        return mOnDialogClickListener;
    }





//    private OnOneWheelDialogClickListener mOnDialogClickListener;
//    private static List<String> list;
//
//    public static SingleWheelDialog newInstance(List<String> stringList, String title, String positive, String negative, boolean isCancelable, OnOneWheelDialogClickListener listener) {
//        SingleWheelDialog singleWheelDialog = new SingleWheelDialog();
//        singleWheelDialog.mOnDialogClickListener = listener;
//        list=stringList;
//        singleWheelDialog.setArguments(getDialogBundle(title, null, positive, negative, isCancelable));
//        return singleWheelDialog;
//    }
//
//    @Override
//    protected void setWheelView(WheelView wheelView) {
//        wheelView.setData(list);
//        wheelView.setDefaultIndex(1);// 设置默认年龄为24岁
//    }
//
//    @Override
//    protected OnOneWheelDialogClickListener setOnDialogClickListener() {
//        return mOnDialogClickListener;
//    }

}
