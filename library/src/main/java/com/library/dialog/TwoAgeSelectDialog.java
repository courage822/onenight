package com.library.dialog;


import com.library.constant.CommonData;
import com.library.widgets.WheelView;

/**
 * 双列年龄选择对话框
 * Created by zhangdroid on 2016/6/25.
 */
public class TwoAgeSelectDialog extends TwoWheelDialog {
    private OnTwoWheelDialogClickListener mOnDialogClickListener;

    public static TwoAgeSelectDialog newInstance(String title, String positive, String negative, boolean isCancelable, OnTwoWheelDialogClickListener listener) {
        TwoAgeSelectDialog twoAgeSelectDialog = new TwoAgeSelectDialog();
        twoAgeSelectDialog.mOnDialogClickListener = listener;
        twoAgeSelectDialog.setArguments(getDialogBundle(title, null, positive, negative, isCancelable));
        return twoAgeSelectDialog;
    }

    @Override
    protected void setWheelView1(WheelView wheelView) {
        wheelView.setData(CommonData.getAgeList());
        wheelView.setDefaultIndex(6);// 设置默认年龄为24岁
    }

    @Override
    protected void setWheelView2(WheelView wheelView) {
        wheelView.setData(CommonData.getAgeList());
        wheelView.setDefaultIndex(6);// 设置默认年龄为24岁
    }

    @Override
    protected OnTwoWheelDialogClickListener setOnDialogClickListener() {
        return mOnDialogClickListener;
    }

}
