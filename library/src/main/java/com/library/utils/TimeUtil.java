package com.library.utils;

import android.content.Context;
import android.text.TextUtils;

import com.library.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TimeUtil {
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    public static SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

    public static String getCurrentDate() {
        return dateFormat.format(new Date(System.currentTimeMillis()));
    }

    public static String getCurrentTime() {
        return timeFormat.format(new Date(System.currentTimeMillis()));
    }

    /**
     * 显示创建时间
     *
     * @param date   需要显示的日期
     * @param format 需要显示的格式，为空时显示默认格式
     * @return 返回指定格式的时间（默认为yyyy年MM月dd日 HH:mm），当前年不显示年份
     */
    public static String showCreatedTime(Context context, String date, String format) {
        if (TextUtils.isEmpty(format)) {
            format = "yyyy-MM-dd HH:mm";
        }
        String time = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            Date old = simpleDateFormat.parse(date);
            time = new SimpleDateFormat(format, Locale.getDefault()).format(old);
            Date currentDate = new Date();
            if (old.getYear() == currentDate.getYear()) {// 当前年
                long result = currentDate.getTime() - old.getTime();
                if (result < 60 * 1000) {// 一分钟内
                    time = context.getString(R.string.moment_ago);
                } else if (result >= 60 * 1000 && result < 60 * 60 * 1000) {// 一小时内
                    int mins = (int) (result / (60 * 1000));
                    time = mins + context.getResources().getQuantityString(R.plurals.minutes_ago, mins);
                } else if (result >= 60 * 60 * 1000 && result <= 24 * 60 * 60 * 1000) {// 24小时内
                    int hours = (int) (result / (60 * 60 * 1000));
                    time = hours + context.getResources().getQuantityString(R.plurals.hours_ago, hours);
                } else {// 第二日及以后
                    time = pad(old.getMonth() + 1) + "-" + pad(old.getDate()) + " " + time.substring(11);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return time;
    }

    /**
     * 显示距离现在已经过了多长时间
     *
     * @param date 要显示的时间
     * @return
     */
    public static String showTimeAgo(Context context, String date) {
        String time = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            Date old = simpleDateFormat.parse(date);
            time = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault()).format(old);
            Date currentDate = new Date();
            if (old.getYear() == currentDate.getYear()) {// 当前年
                long result = currentDate.getTime() - old.getTime();
                if (result < 60 * 1000) {// 一分钟内
                    time = context.getString(R.string.moment_ago);
                } else if (result >= 60 * 1000 && result < 60 * 60 * 1000) {// 一小时内
                    int mins = (int) (result / (60 * 1000));
                    time = mins + context.getResources().getQuantityString(R.plurals.minutes_ago, mins);
                } else if (result >= 60 * 60 * 1000 && result < 24 * 60 * 60 * 1000) {// 24小时内
                    int hours = (int) (result / (60 * 60 * 1000));
                    time = hours + context.getResources().getQuantityString(R.plurals.hours_ago, hours);
                } else if (result >= 24 * 60 * 60 * 1000 && result < 7 * 24 * 60 * 60 * 1000) {// 一周内
                    int days = (int) (result / (24 * 60 * 60 * 1000));
                    time = days + context.getResources().getQuantityString(R.plurals.days_ago, days);
                } else {// 一周以后
                    time = pad(old.getMonth() + 1) + "-" + pad(old.getDate()) + " " + time.substring(11);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return time;
    }

    /**
     * 日期/时间小于10时格式补0
     */
    public static String pad(int c) {
        if (c >= 10) {
            return String.valueOf(c);
        } else {
            return "0" + String.valueOf(c);
        }
    }
}
