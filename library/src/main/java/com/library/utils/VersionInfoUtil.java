package com.library.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/**
 * 版本信息：packageName、versionCode和versionName
 * Created by zhangdroid on 2016/5/25.
 */
public class VersionInfoUtil {

    private static PackageInfo getPackageInfo(Context context, String packageName) {
        PackageManager packageManager = context.getApplicationContext().getPackageManager();
        try {
            return packageManager.getPackageInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getPackageName(Context context) {
        return context.getPackageName();
    }

    public static int getVersionCode(Context context) {
        return getPackageInfo(context, getPackageName(context)).versionCode;
    }

    public static String getVersionName(Context context) {
        return getPackageInfo(context, getPackageName(context)).versionName;
    }

    /**
     * 检测是否安装了某个应用
     *
     * @param packageName 包名
     * @return
     */
    public static boolean checkAPKExist(Context context, String packageName) {
        PackageInfo packageInfo = getPackageInfo(context, packageName);
        if (packageInfo == null) {
            return false;
        } else {
            return true;
        }
    }

}
