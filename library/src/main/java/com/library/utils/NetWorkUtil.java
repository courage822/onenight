package com.library.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetWorkUtil {
    private ConnectivityManager connectionManager;
    private final int MOBILE = 0, WIFI = 1, WIMAX = 6, ETHERNET = 9,
            BLUETOOTH = 7;

    public NetWorkUtil(Context context) {
        this.connectionManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public NetworkInfo getNetWorkInfo() {
        return connectionManager.getActiveNetworkInfo();
    }

    public String getType() {
        String type = "0";
        switch (getNetWorkInfo().getType()) {
            case MOBILE:
                type = "MOBILE";
                break;
            case WIFI:
                type = "WIFI";
                break;
            case WIMAX:
                type = "WIMAX";
                break;
            case ETHERNET:
                type = "ETHERNET";
                break;
            case BLUETOOTH:
                type = "BLUETOOTH";
                break;
            default:
                type = "MOBILE";
                break;
        }
        return type;
    }

    public String getTypeName() {
        if (getNetWorkInfo() == null) {
            return "未知网络";
        }
        String name = getNetWorkInfo().getTypeName();
        if (name == null) {
            name = "未知网络";
        }
        return name;
    }

    public String getExtraInfo() {
        return getNetWorkInfo().getExtraInfo();
    }

}
