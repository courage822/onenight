package com.library.widgets;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;

import com.library.R;

/**
 * The clearable EditText, you can set on TextWatcherCallback to handle the text
 * change event
 * <p/>
 * Created by zhangdroid on 2016/2/17.
 */
public class ClearableEditText extends EditText implements TextWatcher, OnFocusChangeListener {
    private Drawable mClearDrawable;
    private TextWatcherCallback mTextWatcherCallback = null;
    private boolean mIsFocused;

    /**
     * 设置删除图标
     *
     * @param resId 删除图标id
     */
    public void setClearImageResource(int resId) {
        mClearDrawable = getResources().getDrawable(resId);
    }

    /**
     * 设置TextWatcher
     *
     * @param textWatcherCallback
     */
    public void setTextWatcherCallback(TextWatcherCallback textWatcherCallback) {
        mTextWatcherCallback = textWatcherCallback;
    }

    public ClearableEditText(Context context) {
        this(context, null);
    }

    public ClearableEditText(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.editTextStyle);
    }

    public ClearableEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        mClearDrawable = getCompoundDrawables()[2];
        if (mClearDrawable == null) {
            mClearDrawable = getResources().getDrawable(R.drawable.icon_clear);
        }
        mClearDrawable.setBounds(0, 0, mClearDrawable.getIntrinsicWidth(), mClearDrawable.getIntrinsicHeight());
        setClearDrawableVisible(false);
        addTextChangedListener(this);
        setOnFocusChangeListener(this);
    }

    protected void setClearDrawableVisible(boolean isVisible) {
        setCompoundDrawables(getCompoundDrawables()[0], getCompoundDrawables()[1], isVisible ? mClearDrawable : null,
                getCompoundDrawables()[3]);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        mIsFocused = hasFocus;
        if (hasFocus) {
            setClearDrawableVisible(getText().length() > 0);
        } else {
            setClearDrawableVisible(false);
        }
    }

    /**
     * 监听Touch事件，通过触摸点的位置，来模拟点击事件
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (getCompoundDrawables()[2] != null) {
                int x = (int) event.getX();
                int y = (int) event.getY();
                // 判断触摸点是否在删除图标上
                Rect rect = getCompoundDrawables()[2].getBounds();
                int height = rect.height();
                // 删除图标左、右边缘
                int leftBound = getWidth() - getTotalPaddingRight();
                int rightBound = getWidth() - getPaddingRight();
                boolean isInnerWidth = x > leftBound && x < rightBound;
                // 删除图标上、下边缘
                int distance = (getHeight() - height) / 2;
                boolean isInnerHeight = y > distance && y < (distance + height);
                if (isInnerWidth && isInnerHeight) {
                    this.setText("");
                }
            }
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        if (mIsFocused) {
            setClearDrawableVisible(text.length() > 0);
        }
        if (mTextWatcherCallback != null) {
            mTextWatcherCallback.handleTextChanged(text, start, lengthBefore, lengthAfter);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public interface TextWatcherCallback {
        /**
         * onTextChanged方法的回调
         *
         * @param text
         * @param start
         * @param lengthBefore
         * @param lengthAfter
         */
        public void handleTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        mClearDrawable = null;
    }

}
