package com.library.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.library.utils.DensityUtil;
import com.library.utils.LogUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>模拟波纹扩散效果的view</p>
 * <p>利用半径和透明度随时间变化实现</p>
 * Created by zhangdroid on 2016/7/2.
 */
public class WaveView extends View {
    // 初始波纹半径
    private float mInitRadius;
    // 最大波纹半径
    private float mMaxRadius;
    // 波纹扩散速度（ms）
    private int mSpeed;
    // 每次扩散需要绘制的圆的数目
    private int mCircleCount;
    // 绘制circle的画笔
    private Paint mPaint;
    // 保存所有Circle的List
    private List<Circle> mCircleList;

    private int mDrawCount;

    public WaveView(Context context) {
        this(context, null);
    }

    public WaveView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WaveView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
        // 开始绘制
        mCircleRunTask.run();
    }

    private void init(Context context) {
        mInitRadius = DensityUtil.dip2px(context, 60);
        mMaxRadius = DensityUtil.getScreenWidth(context) / 2;
        mCircleCount = 6;
        LogUtil.e(LogUtil.TAG_ZL, "mInitRadius = " + mInitRadius + "  mMaxRadius = " + mMaxRadius);
        mSpeed = 50;

        mPaint = new Paint();
        mPaint.setAntiAlias(false);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(Color.BLUE);
        createCircleList();
    }

    private void createCircleList() {
        mCircleList = new ArrayList<Circle>();
        float circleInterval = (mMaxRadius - mInitRadius) / mCircleCount;
        for (int i = 0; i < circleInterval; i++) {
            Circle circle = new Circle();
            if (i < mCircleCount / 3) {
                circle.setAlpha(159);
            } else {
                circle.setAlpha((int) (95 - i / (circleInterval / 32)));
            }
            circle.setRadius(mInitRadius);
            mCircleList.add(circle);
        }
    }

    private Runnable mCircleRunTask = new Runnable() {
        @Override
        public void run() {
            // 重绘
            invalidate();
            postDelayed(mCircleRunTask, mSpeed);
        }
    };

    public void stop() {
        if (mCircleRunTask != null) {
            removeCallbacks(mCircleRunTask);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // 测量宽度
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int measureWidth;
        if (widthMode == MeasureSpec.EXACTLY) {// match_parent/固定宽度
            measureWidth = widthSize;
        } else {
            measureWidth = (int) (mMaxRadius * 2);
        }

        // 测量高度
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int measureHeight;
        if (heightMode == MeasureSpec.EXACTLY) {// match_parent/固定宽度
            measureHeight = heightSize;
        } else {
            measureHeight = (int) (mMaxRadius * 2);
        }

        // 设置测量后的宽高
        setMeasuredDimension(measureWidth, measureHeight);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // 所有圆的中心点为View的中心点
        int xCenter = getWidth() / 2;
        int yCenter = getHeight() / 2;
        // 遍历mCircleList，绘制出所有的circle
        if (mCircleList != null) {
            mDrawCount++;
            int length = mCircleList.size();
            if (mDrawCount > length) {
                // 已经绘制完List中所有的圆，重置次数
                mDrawCount = 1;
                // 重置所有半径
                for (int j = 0; j < length; j++) {
                    mCircleList.get(j).setRadius(mInitRadius);
                }
            }
            for (int i = 0; i < mDrawCount; i++) {
                Circle circle = mCircleList.get(i);
                float currentRadius = circle.getRadius();
                // 连续绘制相同透明度的圆
                for (int k = 0; k < mCircleCount; k++) {
                    currentRadius++;// 半径线性增长
                    if (currentRadius > mMaxRadius) {// 当前Circle超出范围
                        circle.setRadius(mInitRadius);// 重置半径
                        continue;
                    } else {
                        circle.setRadius(currentRadius);// 保存新的半径
                    }
                    // 根据透明度和半径绘制Circle
                    mPaint.setAlpha(circle.getAlpha());
                    canvas.drawCircle(xCenter, yCenter, currentRadius, mPaint);
                }
            }
        }
    }

    /**
     * 定义Circle半径和透明度
     */
    private class Circle {
        private int alpha;
        private float radius;

        public void setAlpha(int alpha) {
            this.alpha = alpha;
        }

        public void setRadius(float radius) {
            this.radius = radius;
        }

        public int getAlpha() {
            return alpha;
        }

        public float getRadius() {
            return radius;
        }
    }

}
