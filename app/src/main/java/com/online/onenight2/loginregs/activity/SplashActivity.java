package com.online.onenight2.loginregs.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;

import com.alibaba.fastjson.JSON;
import com.appsflyer.AppsFlyerLib;
import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.library.dialog.OnDoubleDialogClickListener;
import com.library.utils.DensityUtil;
import com.library.utils.DialogUtil;
import com.library.utils.LogUtil;
import com.library.utils.SharedPreferenceUtil;
import com.library.utils.TimeUtil;
import com.library.utils.ToastUtil;
import com.library.utils.VersionInfoUtil;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.online.onenight2.MainActivity;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseActivity;
import com.online.onenight2.base.BaseApplication;
import com.online.onenight2.bean.BaseModel;
import com.online.onenight2.bean.CheckRefresh;
import com.online.onenight2.bean.LocationEntity;
import com.online.onenight2.bean.LocationInfo;
import com.online.onenight2.bean.LoginEntity;
import com.online.onenight2.bean.PlatformInfo;
import com.online.onenight2.callback.JsonCallback;
import com.online.onenight2.constant.IConfigConstant;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.loginregs.presenter.SplashPresenter;
import com.online.onenight2.utils.AppsflyerUtils;
import com.online.onenight2.utils.CommonRequestUtil;
import com.online.onenight2.utils.SharedPreferencesUtils;
import com.online.onenight2.xml.PlatformInfoXml;
import com.online.onenight2.xml.UserInfoXml;

//import org.greenrobot.eventbus.EventBus;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by foxmanman on 2017/4/12.
 * 启动页面
 */

public class SplashActivity extends BaseActivity<SplashPresenter> {
    private String UPDATE_SERVERAPK = "OneNight";
    private String userName;
    private String password;
    private boolean isLogin=false;
    private LocationManager mLocationManager;
    private static final int PERMISSION_CODE_ACCESS_FINE_LOCATION = 0;
    private static final int PERMISSION_CODE_READ_PHONE_STATE = 0;
    private String jingdu;
    private String weidu;
    private boolean isLocation=false;
    private Handler handler=new Handler();
    private boolean isFinish=false;
    private String getCountry;

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    public SplashPresenter newPresenter() {
        return new SplashPresenter();
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        //全屏
        getWindow().setFlags(WindowManager.LayoutParams. FLAG_FULLSCREEN ,
                WindowManager.LayoutParams. FLAG_FULLSCREEN);
        userName = UserInfoXml.getUserName();
        password = UserInfoXml.getPassword();
        // 启动百度push
        PushManager.startWork(getApplicationContext(), PushConstants.LOGIN_TYPE_API_KEY,
               IConfigConstant.BAIDU_PUSH_API_KEY);
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        SharedPreferencesUtils.saveData(BaseApplication.getAppContext(),"removeLocationListener",0);

        // 获得platform信息
        getPlatformInfo();
        // AppsFlyer初始化
        AppsFlyerLib.getInstance().setAndroidIdData(getAndroidId());
        AppsFlyerLib.getInstance().startTracking(this.getApplication(), IConfigConstant.APPFLYER_KEY);
        // 请求定位信息并加载列表
        requestLocation();
    }
    // 获得platform信息
    private void getPlatformInfo() {
        String language = Locale.getDefault().getLanguage();
        LogUtil.v("language===","language==="+language);
        PlatformInfo platformInfo = new PlatformInfo();
        if (SharedPreferenceUtil.getStringValue(SplashActivity.this, PlatformInfoXml.XML_NAME, PlatformInfoXml.KEY_PID, null)==null) {

            String pid= Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            SharedPreferenceUtil.setStringValue(SplashActivity.this, PlatformInfoXml.XML_NAME, PlatformInfoXml.KEY_PID, pid);
            SharedPreferenceUtil.setStringValue(SplashActivity.this, PlatformInfoXml.XML_NAME, PlatformInfoXml.KEY_IMSI, pid);
        }
        if(UserInfoXml.getCountry()!=null){
            platformInfo.setCountry(UserInfoXml.getCountry());
            if(UserInfoXml.getCountry().equals("Taiwan")){
                platformInfo.setLanguage("Traditional");
            }else{
                platformInfo.setLanguage("English");
            }

        }else{
             platformInfo.setCountry(getCountry());
            if(getCountry().equals("Taiwan")){
                platformInfo.setLanguage("Traditional");
            }else{
                platformInfo.setLanguage("English");
            }
        }
        platformInfo.setW(String.valueOf(DensityUtil.getScreenWidth(SplashActivity.this)));
        platformInfo.setH(String.valueOf(DensityUtil.getScreenHeight(SplashActivity.this)));
        platformInfo.setVersion(String.valueOf(VersionInfoUtil.getVersionCode(getApplicationContext())));
        platformInfo.setPhonetype(Build.MODEL);
        platformInfo.setProduct(IConfigConstant.PRODUCT_ID);// 产品号
        platformInfo.setNetType(getNetType());
        platformInfo.setMobileIP(getMobileIP());
        platformInfo.setRelease(TimeUtil.getCurrentTime());
        platformInfo.setPlatform("2");
        platformInfo.setSystemVersion(Build.VERSION.RELEASE);
        platformInfo.setPid(getAndroidId());
//        渠道号
        if (platformInfo.getCountry() != null && platformInfo.getCountry().equals("Taiwan")) {
            platformInfo.setFid(IConfigConstant.F_IDTW);// 台湾渠道号
        } else if (platformInfo.getCountry() != null && platformInfo.getCountry().equals("America")) {
            platformInfo.setFid(IConfigConstant.F_IDAM);// 美国渠道号
        }

        platformInfo.setPid( SharedPreferenceUtil.getStringValue(SplashActivity.this, PlatformInfoXml.XML_NAME, PlatformInfoXml.KEY_PID, null));
        // 保存platform信息
        PlatformInfoXml.setPlatfromInfo(platformInfo);
//        if (isLogin) {
//            init();
//        }
    }
    private void init(final boolean isLocation) {
        if(!isFinish){
            isFinish=true;
        // 初始化数据字典
          CommonRequestUtil.initParamsDict();
        SharedPreferences sp=getSharedPreferences("out_login", Context.MODE_PRIVATE);
        isLogin = sp.getBoolean("login", false);
        if(isLogin){
            Intent intent_login=new Intent(this,LoginAcitivity.class);
            startActivity(intent_login);
            finish();
            overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
        }else{

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                if (!UserInfoXml. getIsClient()) {
//                     activation();
//                }
                    if (UserInfoXml.getUserName() != null && UserInfoXml.getPassword() != null) {
//                        CheckRefresh();
                        login();
                    } else {
//                            Util.gotoActivity(SplashActivity.this, RegisterActivity.class, true,"null",true);
                         Intent intent = new Intent(SplashActivity.this, RegisterActivity.class);
                         if(isLocation){
                            intent.putExtra("cleanCountry",true);
                             SharedPreferencesUtils.saveData(BaseApplication.getAppContext(),"cleanCountry",true);
                          }
                         startActivity(intent);
                         finish();
                         overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
                    }
            }
        }, 500);

        }
        }

    }

    private String getAndroidId() {
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }





    @Override
    public void initData() {

    }
//语言
    public String getLanguage() {
        String language = "Traditional";
        String LG = Locale.getDefault().getLanguage();
        if (LG.equals("zh")) {
            language = "Traditional";
        } else if (LG.equals("en")) {
            language = "English";
        }
        return language;
//        return "English";
    }

    private String getNetType() {
        String type = "";
        ConnectivityManager cm = (ConnectivityManager) SplashActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null) {
            type = "0";
        } else if (info.getType() == ConnectivityManager.TYPE_WIFI) {
            type = "2";
        } else if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
            String extraInfo = info.getExtraInfo();
            if (extraInfo.equals("cmwap")) {
                type = "3";
            } else if (extraInfo.equals("cmnet")) {
                type = "4";
            } else if (extraInfo.equals("ctnet")) {
                type = "5";
            } else if (extraInfo.equals("ctwap")) {
                type = "6";
            } else if (extraInfo.equals("3gwap")) {
                type = "7";
            } else if (extraInfo.equals("3gnet")) {
                type = "8";
            } else if (extraInfo.equals("uniwap")) {
                type = "9";
            } else if (extraInfo.equals("uninet")) {
                type = "10";
            }
        }
        return type;
    }
//MobileIP
    private String getMobileIP() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
//    国家
public String getCountry(){
    String country = "Taiwan";
    String CT = Locale.getDefault().getCountry();
    if (CT.equals("TW")||CT.equals("CN")) {
        country = "Taiwan";
    } else if (CT.equals("US")) {
        country = "America";
    }
    return country;
}


    //    检查更新
    public void CheckRefresh() {
        List<String> list=new ArrayList<>();
        OkGo.post(IUrlConstant.URL_CHECK_UPDATE)
                .headers("token", PlatformInfoXml.getToken())
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .execute(new CheckRefrshCallBack());
    }
    private class CheckRefrshCallBack extends JsonCallback<CheckRefresh> {

        @Override
        public void onSuccess(CheckRefresh checkRefresh, okhttp3.Call call, okhttp3.Response response) {
            LogUtil.e(LogUtil.TAG_LMF,"更新数据 getisUpdate"+checkRefresh.isUpdate);
            if (checkRefresh != null) {
                if (checkRefresh.isUpdate == null || checkRefresh.isUpdate.equals("0")) {
                    login();
                } else {
                }
            }
        }

        @Override
        public void onError(okhttp3.Call call, okhttp3.Response response, Exception e) {
            super.onError(call, response, e);
        }
    }
    //    登录
    public void login() {
        OkGo.post(IUrlConstant.URL_LOGIN)
                .headers("token", PlatformInfoXml.getToken())
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("pushToken", PlatformInfoXml.getPushToken())
                .params("account", UserInfoXml.getUserName())
                .params("password", UserInfoXml.getPassword())
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String response, Call call, Response s) {
                        if(!TextUtils.isEmpty(response)){
                            LoginEntity loginEntity = JSON.parseObject(response, LoginEntity.class);
                            if(loginEntity!=null){
                                String isSucceed=loginEntity.getIsSucceed();
                                if(!TextUtils.isEmpty(isSucceed)&&"1".equals(isSucceed)){
                                    UserInfoXml.setUserInfo(loginEntity.userEnglish);
                                    int likeNum = +loginEntity.getUserEnglish().getSendPraiseCount();
                                    SharedPreferencesUtils.saveData(BaseApplication.getAppContext(),"likeNum",likeNum);
                                    if (!isLogin) {
                                        isFinish=true;
                                        AppsflyerUtils.appsFlyerLogin();
                                        if(loginEntity.getIsShowUploadImg() != null && loginEntity.getIsShowUploadImg().equals("1")){
//                                            Util.gotoActivity(SplashActivity.this,UploadAvatarActivity.class,true);
                                            Intent intent = new Intent(SplashActivity.this, UploadAvatarActivity.class);
                                            startActivity(intent);
                                            finish();
                                            overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
                                        }else if(UserInfoXml.getGender().equals("1") && loginEntity.getIsShowUploadAudioInformation() != null && loginEntity.getIsShowUploadAudioInformation().equals("1")){
//                                            Util.gotoActivity(SplashActivity.this,UploadAudioActivity.class,true);
                                            Intent intent = new Intent(SplashActivity.this, UploadAudioActivity.class);
                                            startActivity(intent);
                                            finish();
                                            overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
                                        }else{
//                                            Util.gotoActivity(SplashActivity.this,MainActivity.class,true);
                                            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                                            startActivity(intent);
                                            finish();
                                            overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
                                        }
                                    }
                                }else{
                                    isFinish=true;
                                    //登陆失败，跳转登录界面
//                                    Util.gotoActivity(SplashActivity.this,LoginAcitivity.class,true);
                                    Intent intent = new Intent(SplashActivity.this, LoginAcitivity.class);
                                    startActivity(intent);
                                    finish();
                                    overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
                                    ToastUtil.showShortToast(SplashActivity.this, String.valueOf(R.string.login_onerror));

                                }
                            }
                        }
                    }
                });
    }
    //激活
    public void activation() {
         showLoadingPress();
        OkGo.post(IUrlConstant.URL_CLENT_ACTIVATION)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .execute(new SplaActivationCallBack());
    }

    private class SplaActivationCallBack extends JsonCallback<BaseModel> {


        @Override
        public void onSuccess(BaseModel baseModel, okhttp3.Call call, okhttp3.Response response) {
            if (baseModel != null) {
                String isSucceed = baseModel.getIsSucceed();
                if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
//                    getView().hideLoadingPress();
                    UserInfoXml.setIsClient();
                    if(UserInfoXml. getIsClient()){
                        AppsflyerUtils.activate();
                    }
                }
            }
        }

        @Override
        public void onError(okhttp3.Call call, okhttp3.Response response, Exception e) {
            super.onError(call, response, e);
        }
    }

    /**
     * 请求定位信息
     */
    private void requestLocation() {
        if (checkGPS()) {// GPS打开
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_CODE_ACCESS_FINE_LOCATION);
                } else {
                    if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {// Network定位
                        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 5, mLocationListener);
//                        /**改**/
//                        Handler handler = new Handler();
//                        handler.postDelayed(
//                                new Runnable(){
//                            @Override
//                            public void run() {
//                                init(false);
//                            }
//                        },5000);

//                        getCountryByLoaction(mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));

                    } else if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// GPS定位
                        getCountryByLoaction(mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
                    }
                    /**改**/
                    Handler handler = new Handler();
                    handler.postDelayed(
                            new Runnable(){
                                @Override
                                public void run() {
                                    init(false);
                                }
                            },5000);
                }
            } else {
                if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {// Network定位
                    mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 5, mLocationListener);
//                    /**改**/
//                    Handler handler = new Handler();
//                    handler.postDelayed(
//                            new Runnable(){
//                                @Override
//                                public void run() {
//                                    init(false);
//                                }
//                            },5000);
//                    getCountryByLoaction(mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));

                } else if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// GPS定位
                    getCountryByLoaction(mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
                }
                /**改**/
                Handler handler = new Handler();
                handler.postDelayed(
                        new Runnable(){
                            @Override
                            public void run() {
                                init(false);
                            }
                        },5000);
            }

//            }
        } else {// GPS关闭
            // 跳转到设置页，打开定位开关
            DialogUtil.showDoubleBtnDialog(getSupportFragmentManager(), null, getString(R.string.dialog_location_tip), getString(R.string.dialog_set), null, true, new OnDoubleDialogClickListener() {
                @Override
                public void onPositiveClick(View view) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(intent, 0);
                }

                @Override
                public void onNegativeClick(View view) {
                    init(false);
                }
            });
        }
    }

    /**
     * 检查GPS定位开关
     *
     * @return
     */
    private boolean checkGPS() {
        if (mLocationManager != null) {
            return (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
//            return (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER));

        }
        return false;
    }

    private LocationListener mLocationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
//            handler.removeMessages();
            int removeListener = SharedPreferencesUtils.getData(mContext, "removeLocationListener", 0);
            if(removeListener!=1){
                getCountryByLoaction(location);
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        requestLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSION_CODE_READ_PHONE_STATE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {// 申请权限成功
                if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {// Network定位
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    Activity-Compat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
//                        getCountryByLoaction(mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));

//                        return;
                    }
                    mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 5, mLocationListener);
                } else if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// GPS定位
                    getCountryByLoaction(mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
                }
            } else {// 申请权限失败，则使用UUID
                String uuid = UUID.randomUUID().toString();
                String imsi = uuid.replaceAll("-", "").substring(0, 15);
                LogUtil.e(LogUtil.TAG_ZL, "uuid = " + uuid + " imsi = " + imsi);
                SharedPreferenceUtil.setStringValue(SplashActivity.this, PlatformInfoXml.XML_NAME, PlatformInfoXml.KEY_PID, uuid.replaceAll("-", ""));
                SharedPreferenceUtil.setStringValue(SplashActivity.this, PlatformInfoXml.XML_NAME, PlatformInfoXml.KEY_IMSI, imsi);
//                init(false);
            }
            /**改**/
            Handler handler = new Handler();
            handler.postDelayed(
                    new Runnable(){
                        @Override
                        public void run() {
                            init(false);
                        }
                    },5000);
            /****/
//
        }
    }
    /**
     * 上传位置信息
     *
     *
     */
    private void uploadLocation(String latitude,String longitude) {
            LocationInfo locationInfo = new LocationInfo();
            locationInfo.setAddrStr("");
            locationInfo.setProvince("");
            locationInfo.setCity("");
            locationInfo.setCityCode("");
            locationInfo.setDistrict("");
            locationInfo.setStreet("");
            locationInfo.setStreetNumber("");
            locationInfo.setLatitude(latitude);
            locationInfo.setLongitude(longitude);
            Map<String, LocationInfo> map = new HashMap<String, LocationInfo>();
            map.put("locationInfo", locationInfo);
            OkGo.post(IUrlConstant.URL_UPLOAD_LOCATION)
                    .headers("token", PlatformInfoXml.getToken())
                    .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                    .params("locationInfo", JSON.toJSONString(map))
                    .execute(new StringCallback() {
                                 @Override
                                 public void onSuccess(String s, Call call, Response response) {

                                     if (!TextUtils.isEmpty(s)) {
                                         BaseModel baseModel = JSON.parseObject(s, BaseModel.class);
                                         if(baseModel!=null){
                                         }
                                     }
                                 }
                             }
                    );

    }
    /**
     * 根据位置信息（经纬度）获得所在国家
     *
     * @param location 定位信息
     */
    private void getCountryByLoaction(Location location) {
        if (location != null &&  !isFinish) {
            LocationInfo locationInfo = new LocationInfo();
//美国
//            locationInfo.setLatitude("31.543114");
//            locationInfo.setLongitude("-97.154185");
//            jingdu=String.valueOf("31.543114");
//            weidu=String.valueOf("-97.154185");
// 台湾
//            locationInfo.setLatitude("25.035883");
//            locationInfo.setLongitude("121.569609");
//            jingdu=String.valueOf("25.035883");
//            weidu=String.valueOf("121.569609");
// 印度
//            locationInfo.setLatitude("28.61345942");
//            locationInfo.setLongitude("77.20092773");
//            jingdu=String.valueOf("28.61345942");
//            weidu=String.valueOf("77.20092773");

            locationInfo.setLatitude(String.valueOf(location.getLatitude()));
            locationInfo.setLongitude(String.valueOf(location.getLongitude()));
            jingdu=String.valueOf(location.getLatitude());
            weidu=String.valueOf(location.getLongitude());
            SharedPreferenceUtil.setStringValue(SplashActivity.this,"LOCATION","getLatitude",jingdu);
            SharedPreferenceUtil.setStringValue(SplashActivity.this,"LOCATION","getLongitude",weidu);


            Map<String, LocationInfo> map = new HashMap<String, LocationInfo>();
            map.put("locationInfo", locationInfo);
            OkGo.post(IUrlConstant.URL_GET_COUNTRY_BY_LOCATION)
                    .params("locationInfo", JSON.toJSONString(map))
                    .execute(new LocationCallBack());
        }
    }
    public class LocationCallBack extends JsonCallback<LocationEntity>  {

        @Override
        public void onError(Call call, Response response, Exception e) {
            super.onError(call, response, e);
            init(false);
        }

        @Override
        public void onSuccess(LocationEntity response, Call call, Response re) {
            String locationCountry;
            if (response != null) {
                String succeed = response.getIsSucceed();
                if (!TextUtils.isEmpty(succeed) && succeed.equals("1")) {
                    locationCountry = response.getCountry();
                    // 香港需要转换
                    if ("Hong Kong-China".equals(response.getCountry())) {
                        locationCountry = "HongKong";
                    }
                    if("United States".equals(response.getCountry())){
                        locationCountry="America";
                    }
                    if (locationCountry.contains(" ")) {
                        locationCountry = locationCountry.replaceAll(" ", "");
                    }
                    // 支持的国家列表
                    List<String> countryList = new ArrayList<String>();
                    // 添加13个英语国家
                    countryList.add("America");
                    countryList.add("Australia");
                    countryList.add("India");
                    countryList.add("Indonesia");
                    countryList.add("UnitedKingdom");
                    countryList.add("Canada");
                    countryList.add("NewZealand");
                    countryList.add("Ireland");
                    countryList.add("SouthAfrica");
                    countryList.add("Singapore");
                    countryList.add("Pakistan");
                    countryList.add("Philippines");
                    countryList.add("HongKong");
                    countryList.add("Taiwan");
                    boolean countryExist=false;
                    for (int i = 0; i < countryList.size(); i++) {
                        if (countryList.get(i).equals(locationCountry)) {
                            countryExist=true;
                            PlatformInfoXml.setCountry(locationCountry);
//                            uploadLocation(jingdu,weidu);
//                          定位成功
                            setCountryAndFid(locationCountry);
                            getCountry=locationCountry;
//                            System.out.println("1111111111111111111111"+locationCountry);
//                            UserInfoXml.setWelComeLocation(true);
//                            if(!UserInfoXml.getIsClient()){
//                                activation();
//                            }
                            isLocation=true;
//                            init(isLocation);
                        }
                    }
                }
            }
            init(isLocation);
        }
    }
    private void setCountryAndFid(String locationCountry) {
        PlatformInfo platformInfo = new PlatformInfo();
        platformInfo.setW(String.valueOf(DensityUtil.getScreenWidth(SplashActivity.this)));
        platformInfo.setH(String.valueOf(DensityUtil.getScreenHeight(SplashActivity.this)));
        platformInfo.setVersion(String.valueOf(VersionInfoUtil.getVersionCode(mContext)));
        platformInfo.setPhonetype(Build.MODEL);
        platformInfo.setNetType(getNetType());
        platformInfo.setMobileIP(getMobileIP());
        platformInfo.setRelease(TimeUtil.getCurrentTime());
        platformInfo.setPlatform("2");
        platformInfo.setSystemVersion(Build.VERSION.RELEASE);
//        platformInfo.setLanguage(getLanguage());
//        platformInfo.setCountry(getCountry());
        platformInfo.setCountry(locationCountry);
        if(locationCountry.equals("Taiwan")){
            platformInfo.setLanguage("Traditional");
        }else{
            platformInfo.setLanguage("English");
        }
        platformInfo.setPid(getAndroidId());
        platformInfo.setImsi(getAndroidId());
        if (locationCountry.equals("America")) {
            platformInfo.setFid(IConfigConstant.F_IDAM);// 美国渠道号
        } else if (locationCountry.equals("India")) {
            platformInfo.setFid(IConfigConstant.F_IDIN);// 印度渠道号
        } else if (locationCountry.equals("Australia")) {
            platformInfo.setFid(IConfigConstant.F_IDNSW);// 澳大利亚
        }else if (locationCountry.equals("Indonesia")) {
            platformInfo.setFid(IConfigConstant.F_IDJT);// 印尼
        }else if (locationCountry.equals("UnitedKingdom")) {
            platformInfo.setFid(IConfigConstant.F_IDGL);// 英国
        }else if (locationCountry.equals("Canada")) {
            platformInfo.setFid(IConfigConstant.F_IDTO);// 加拿大渠道号
        }else if (locationCountry.equals("NewZealand")) {
            platformInfo.setFid(IConfigConstant.F_IDAL);// 新西兰渠道号
        }else if (locationCountry.equals("Ireland")) {
            platformInfo.setFid(IConfigConstant.F_IDDL);// 爱尔兰渠道号
        }else if (locationCountry.equals("SouthAfrica")) {
            platformInfo.setFid(IConfigConstant.F_IDJG);// 南非渠道号
        }else if (locationCountry.equals("Singapore")) {
            platformInfo.setFid(IConfigConstant.F_IDOD);// 新加坡渠道号
        }else if (locationCountry.equals("Pakistan")) {
            platformInfo.setFid(IConfigConstant.F_IDKC);// 巴基斯坦渠道号
        }else if (locationCountry.equals("Philippines")) {
            platformInfo.setFid(IConfigConstant.F_IDMA);// 菲律宾渠道号
        }else if (locationCountry.equals("HongKong")) {
            platformInfo.setFid(IConfigConstant.F_IDWC);// 香港渠道号
        }else if (locationCountry.equals("Taiwan")) {
            platformInfo.setFid(IConfigConstant.F_IDTW);// 台湾渠道号
        }
        platformInfo.setProduct(IConfigConstant.PRODUCT_ID);//产品号
        // 保存platform信息
        PlatformInfoXml.setPlatfromInfo(platformInfo);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreferencesUtils.saveData(BaseApplication.getAppContext(),"removeLocationListener",1);
    }


}
