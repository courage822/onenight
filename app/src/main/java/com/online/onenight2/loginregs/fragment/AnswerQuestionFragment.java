package com.online.onenight2.loginregs.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.library.adapter.recyclerview.CommonRecyclerViewAdapter;
import com.library.adapter.recyclerview.RecyclerViewHolder;
import com.library.widgets.ScrollControlViewPager;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseApplication;
import com.online.onenight2.base.BaseFragment;
import com.online.onenight2.constant.IConfigConstant;
import com.online.onenight2.loginregs.activity.UploadAvatarActivity;
import com.online.onenight2.loginregs.presenter.AnswerQuestionPresenter;
import com.online.onenight2.utils.CommonRequestUtil;
import com.online.onenight2.view.LoadingDialog;
import com.online.onenight2.xml.PlatformInfoXml;
import com.online.onenight2.xml.UserInfoXml;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


/**
 * Created by Administrator on 2017/6/28.
 */

public class AnswerQuestionFragment extends BaseFragment<AnswerQuestionPresenter>{
    @BindView(R.id.iv_system_user_avatar)
    ImageView mIvSystemUserAvatar;
    @BindView(R.id.tv_question)
    TextView mTvQuestion;
    @BindView(R.id.answer_recyclerview)
    RecyclerView mAnswerRecyclerview;
    private int index;
    private AnswerAdapter mAnswerAdapter;
//    // 本地头像
    int[] aq_images_female;
    int[] aq_images_male;
//    private int[] aq_images_female = new int[]{R.mipmap.twwomen1, R.mipmap.twwomen2,
//            R.mipmap.twwomen3, R.mipmap.twwomen4, R.mipmap.twwomen5,R.mipmap.twwomen1, R.mipmap.twwomen2,
//            R.mipmap.twwomen3, R.mipmap.twwomen4, R.mipmap.twwomen5};
//    private int[] aq_images_male = new int[]{R.mipmap.twman1, R.mipmap.twman2,
//            R.mipmap.twman3, R.mipmap.twman4, R.mipmap.twman5,R.mipmap.twman1, R.mipmap.twman2,
//            R.mipmap.twman3, R.mipmap.twman4, R.mipmap.twman5};

    private static final String ARG_QUESTION_INDEX = "arg_QUESTION_INDEX";

    public static AnswerQuestionFragment newInstance(int index) {
        AnswerQuestionFragment answerQuestionFragment = new AnswerQuestionFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_QUESTION_INDEX, index);
        answerQuestionFragment.setArguments(bundle);
        return answerQuestionFragment;
    }
    @Override
    public AnswerQuestionPresenter newPresenter() {
        return null;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_answer_question;
    }

    @Override
    protected void initView() {
        String fid = PlatformInfoXml.getPlatfromInfo().getFid();
        // 本地头像
//        台湾,香港,新加坡
        if(fid.equals(IConfigConstant.F_IDTW )|| fid.equals(IConfigConstant.F_IDWC)|| fid.equals(IConfigConstant.F_IDOD)){
            aq_images_female = new int[]{R.mipmap.twwomen1, R.mipmap.twwomen2, R.mipmap.twwomen3,
                    R.mipmap.twwomen4, R.mipmap.twwomen5};
            aq_images_male = new int[]{R.mipmap.twman1, R.mipmap.twman2,
                    R.mipmap.twman3, R.mipmap.twman4, R.mipmap.twman5};

        }else if(fid.equals(IConfigConstant.F_IDIN)){
//            印度
            aq_images_female = new int[]{R.mipmap.inwomen1, R.mipmap.inwomen2, R.mipmap.inwomen3,
                    R.mipmap.inwomen4, R.mipmap.inwomen5};
           aq_images_male = new int[]{R.mipmap.inman1, R.mipmap.inman2, R.mipmap.inman3,
                    R.mipmap.inman4, R.mipmap.inman5};
//
//        }else if(fid.equals(IConfigConstant.F_IDMA)){
////            菲律宾
//            aq_images_female = new int[]{R.mipmap.philipinwoman1, R.mipmap.philipinwoman2, R.mipmap.philipinwoman3,
//                    R.mipmap.philipinwoman4, R.mipmap.philipinwoman5,R.mipmap.philipinwoman6, R.mipmap.philipinwoman7,
//                    R.mipmap.philipinwoman8, R.mipmap.philipinwoman9, R.mipmap.philipinwoman10};
//            aq_images_male = new int[]{R.mipmap.philipinman1, R.mipmap.philipinman2, R.mipmap.philipinman3,
//                    R.mipmap.philipinman4, R.mipmap.philipinman5,R.mipmap.philipinman6, R.mipmap.philipinman7,
//                    R.mipmap.philipinman8, R.mipmap.philipinman9, R.mipmap.philipinman10};
//
//        }else if(fid.equals(IConfigConstant.F_IDJT)){
////            印尼
//            aq_images_female = new int[]{R.mipmap.indonisiawoman1, R.mipmap.indonisiawoman2, R.mipmap.indonisiawoman3,
//                    R.mipmap.indonisiawoman4, R.mipmap.indonisiawoman5,R.mipmap.indonisiawoman6, R.mipmap.indonisiawoman7,
//                    R.mipmap.indonisiawoman8, R.mipmap.indonisiawoman9, R.mipmap.indonisiawoman10};
//            aq_images_male = new int[]{R.mipmap.indonisiaman1, R.mipmap.indonisiaman2, R.mipmap.indonisiaman3,
//                    R.mipmap.indonisiaman4, R.mipmap.indonisiaman5,R.mipmap.indonisiaman6, R.mipmap.indonisiaman7,
//                    R.mipmap.indonisiaman8, R.mipmap.indonisiaman9, R.mipmap.indonisiaman10};
//
//        }else if(fid.equals(IConfigConstant.F_IDKC)){
////            巴基斯坦
//            aq_images_female = new int[]{R.mipmap.pakistanwomen1, R.mipmap.pakistanwomen2, R.mipmap.pakistanwomen3,
//                    R.mipmap.pakistanwomen4, R.mipmap.pakistanwomen5,R.mipmap.pakistanwomen6, R.mipmap.pakistanwomen7,
//                    R.mipmap.pakistanwomen8, R.mipmap.pakistanwomen9, R.mipmap.pakistanwomen10};
//            aq_images_male = new int[]{R.mipmap.pakistanman1, R.mipmap.pakistanman2, R.mipmap.pakistanman3,
//                    R.mipmap.pakistanman4, R.mipmap.pakistanman5,R.mipmap.pakistanman6, R.mipmap.pakistanman7,
//                    R.mipmap.pakistanman8, R.mipmap.pakistanman8, R.mipmap.pakistanman10};
//
//
//        }else if(fid.equals(IConfigConstant.F_IDJG)){
////            南非
//            aq_images_female = new int[]{R.mipmap.southafricawomen1, R.mipmap.southafricawomen2, R.mipmap.southafricawomen3,
//                    R.mipmap.southafricawomen4, R.mipmap.southafricawomen5,R.mipmap.southafricawomen6, R.mipmap.southafricawomen7,
//                    R.mipmap.southafricawomen8, R.mipmap.southafricawomen9, R.mipmap.southafricawomen10};
//            aq_images_male = new int[]{R.mipmap.southafricaman1, R.mipmap.southafricaman2, R.mipmap.southafricaman3,
//                    R.mipmap.southafricaman4, R.mipmap.southafricaman5,R.mipmap.southafricaman6, R.mipmap.southafricaman7,
//                    R.mipmap.southafricaman8, R.mipmap.southafricaman8, R.mipmap.southafricaman10};
        }else{
            aq_images_female= new int[]{R.mipmap.americawomen1, R.mipmap.americawomen2, R.mipmap.americawomen3,
                    R.mipmap.americawomen4, R.mipmap.americawomen5};
            aq_images_male = new int[]{R.mipmap.americaman1, R.mipmap.americaman2, R.mipmap.americaman3,
                    R.mipmap.americaman4, R.mipmap.americaman5};
        }

        Bundle bundle = getArguments();
        if (bundle != null) {
            index = bundle.getInt(ARG_QUESTION_INDEX);
            mIvSystemUserAvatar.setImageResource(UserInfoXml.isMale() ? aq_images_female[index - 1] : aq_images_male[index - 1]);
            String question = "";
            List<String> answerList = new ArrayList<>();
            switch (index) {
                case 1:// 婚姻状况
                    question = getString(R.string.question1);
                   //answerList = ParamsUtils.getMarriageMapValue();
                    String[] stringArray1 = getResources().getStringArray(R.array.answer1);
                    answerList=getStringList(stringArray1);
                    break;
                case 2:// 教育
                    question = getString(R.string.question2);
//                   answerList = ParamsUtils.getEducationMapValue();
                    String[] stringArray2 =getResources().getStringArray(R.array.answer2);
                    answerList=getStringList(stringArray2);
                    break;
                case 3:// 收入是多少
                    question = getString(R.string.question3);
//                  answerList = ParamsUtils.getIncomeMapValue();
                    String[] stringArray3 =getResources().getStringArray(R.array.answer3);
                    answerList=getStringList(stringArray3);
                    break;
                case 4:// 职业
                    question = getString(R.string.question5);
//                    answerList = ParamsUtils.getWorkMapValue();
                    String[] stringArray5 =getResources().getStringArray(R.array.answer5);
                    answerList=getStringList(stringArray5);
                    break;
                case 5:// 兴趣爱好
                    question = getString(R.string.question4);
//                   answerList = ParamsUtils.getInterestMapValue();
                    String[] stringArray4 =getResources().getStringArray(R.array.answer4);
                    answerList=getStringList(stringArray4);
                    break;
//                case 6:// 婚姻状况
//                    question = getString(R.string.question6);
//                    //answerList = ParamsUtils.getMarriageMapValue();
//                    String[] stringArray6 = getResources().getStringArray(R.array.answer6);
//                    answerList=getStringList(stringArray6);
//                    break;
//                case 7:// 教育
//                    question = getString(R.string.question7);
////                   answerList = ParamsUtils.getEducationMapValue();
//                    String[] stringArray7 =getResources().getStringArray(R.array.answer7);
//                    answerList=getStringList(stringArray7);
//                    break;
//                case 8:// 收入是多少
//                    question = getString(R.string.question8);
////                  answerList = ParamsUtils.getIncomeMapValue();
//                    String[] stringArray8 =getResources().getStringArray(R.array.answer8);
//                    answerList=getStringList(stringArray8);
//                    break;
//                case 9:// 职业
//                    question = getString(R.string.question9);
////                    answerList = ParamsUtils.getWorkMapValue();
//                    String[] stringArray9 =getResources().getStringArray(R.array.answer9);
//                    answerList=getStringList(stringArray9);
//                    break;
//                case 10:// 兴趣爱好
//                    question = getString(R.string.question10);
////                   answerList = ParamsUtils.getInterestMapValue();
//                    String[] stringArray10 =getResources().getStringArray(R.array.answer10);
//                    answerList=getStringList(stringArray10);
//                    break;


            }
            mTvQuestion.setText(question);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            mAnswerRecyclerview.setLayoutManager(linearLayoutManager);
         //在这里设置Adapter
            mAnswerAdapter = new AnswerAdapter(getActivity(), R.layout.item_answer_list, answerList);
            mAnswerRecyclerview.setAdapter(mAnswerAdapter);
        }
    }

    private List<String> getStringList(String[] stringArray1) {
        List<String> list_string=new ArrayList<>();
        for (int i = 0; i < stringArray1.length; i++) {
            list_string.add(stringArray1[i]);
        }
        return list_string;
    }

    @Override
    protected void initData() {
        mAnswerAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                String selectedItem = mAnswerAdapter.getItemByPosition(position);
                switch (index) {
                    case 1:// 婚姻状况
                        //UserInfoXml.setMarriage(ParamsUtils.getMarriageMapKeyByValue(selectedItem));
                        next();
                        break;
                    case 2:// 学历
                       // UserInfoXml.setEducation(ParamsUtils.getEducationMapKeyByValue(selectedItem));
                        next();
                        break;
                    case 3:// 是否想要孩子
                       // UserInfoXml.setIncome(ParamsUtils.getIncomeMapKeyByValue(selectedItem));
                        next();
                        break;
                    case 4:// 兴趣爱好
                       // UserInfoXml.setInterest(ParamsUtils.getInterestMapKeyByValue(selectedItem));
                        next();
                        break;
//                    case 5:// 婚姻状况
//                        //UserInfoXml.setMarriage(ParamsUtils.getMarriageMapKeyByValue(selectedItem));
//                        next();
//                        break;
//                    case 6:// 学历
//                        // UserInfoXml.setEducation(ParamsUtils.getEducationMapKeyByValue(selectedItem));
//                        next();
//                        break;
//                    case 7:// 是否想要孩子
//                        // UserInfoXml.setIncome(ParamsUtils.getIncomeMapKeyByValue(selectedItem));
//                        next();
//                        break;
//                    case 8:// 兴趣爱好
//                        // UserInfoXml.setInterest(ParamsUtils.getInterestMapKeyByValue(selectedItem));
//                        next();
//                        break;
//                    case 9:// 兴趣爱好
//                        // UserInfoXml.setInterest(ParamsUtils.getInterestMapKeyByValue(selectedItem));
//                        next();
//                        break;
                    case 5:// 职业
                       // UserInfoXml.setWork(ParamsUtils.getWorkMapKeyByValue(selectedItem));
                        LoadingDialog.showDialogForLoading(getActivity());
                        CommonRequestUtil.uploadUserInfo(false, new CommonRequestUtil.OnCommonListener() {
                            @Override
                            public void onSuccess() {
                                LoadingDialog.cancelDialogForLoading(mContext);
                                goToActivity();
                            }

                            @Override
                            public void onFail() {
                                LoadingDialog.cancelDialogForLoading(mContext);
                                goToActivity();
                            }
                        });
                        break;
                }
            }
        });
    }
    private void goToActivity(){
        if(getActivity()!=null){
            Intent intent=new Intent(getActivity(), UploadAvatarActivity.class);
            startActivity(intent);
            getActivity().finish();
        }else{
            Intent intent=new Intent(BaseApplication.getAppContext(), UploadAvatarActivity.class);
            startActivity(intent);
//            finish();
        }

    }
    private void next() {
        ScrollControlViewPager viewPager = (ScrollControlViewPager) getActivity().findViewById(R.id.answer_question_viewpager);
        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, false);
    }
    private static class AnswerAdapter extends CommonRecyclerViewAdapter<String> {

        public AnswerAdapter(Context context, int layoutResId) {
            super(context, layoutResId);
        }

        public AnswerAdapter(Context context, int layoutResId, List<String> list) {
            super(context, layoutResId, list);
        }

        @Override
        protected void convert(int position, RecyclerViewHolder viewHolder, String bean) {
            if (!TextUtils.isEmpty(bean)) {
                viewHolder.setText(R.id.item_answer, bean);
            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
