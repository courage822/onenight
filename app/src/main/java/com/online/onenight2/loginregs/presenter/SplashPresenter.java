package com.online.onenight2.loginregs.presenter;

import android.location.LocationManager;
import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.library.utils.LogUtil;
import com.library.utils.Util;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.online.onenight2.MainActivity;
import com.online.onenight2.base.BaseApplication;
import com.online.onenight2.base.BasePresenter;
import com.online.onenight2.bean.CheckRefresh;
import com.online.onenight2.bean.LoginEntity;
import com.online.onenight2.callback.JsonCallback;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.loginregs.activity.LoginAcitivity;
import com.online.onenight2.loginregs.activity.SplashActivity;
import com.online.onenight2.loginregs.activity.UploadAudioActivity;
import com.online.onenight2.loginregs.activity.UploadAvatarActivity;
import com.online.onenight2.utils.SharedPreferencesUtils;
import com.online.onenight2.xml.PlatformInfoXml;
import com.online.onenight2.xml.UserInfoXml;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/4/12.
 */

public class SplashPresenter extends BasePresenter<SplashActivity> {
    private LocationManager mLocationManager;

    //激活
//    public void activation() {
//        getView().showLoadingPress();
//        OkGo.post(IUrlConstant.URL_CLENT_ACTIVATION)
//                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
//                .execute(new SplaActivationCallBack());
//    }
//
//    private class SplaActivationCallBack extends JsonCallback<BaseModel> {
//
//
//        @Override
//        public void onSuccess(BaseModel baseModel, okhttp3.Call call, okhttp3.Response response) {
//            if (baseModel != null) {
//                String isSucceed = baseModel.getIsSucceed();
//                if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
////                    getView().hideLoadingPress();
//                    UserInfoXml.setIsClient();
//                }
//            }
//        }
//
//        @Override
//        public void onError(okhttp3.Call call, okhttp3.Response response, Exception e) {
//            super.onError(call, response, e);
//        }
//    }

    //    检查更新
    public void CheckRefresh() {
        OkGo.post(IUrlConstant.URL_CHECK_UPDATE)
                .headers("token", PlatformInfoXml.getToken())
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .execute(new CheckRefrshCallBack());
    }
    private class CheckRefrshCallBack extends JsonCallback<CheckRefresh>{

        @Override
        public void onSuccess(CheckRefresh checkRefresh, okhttp3.Call call, okhttp3.Response response) {
            LogUtil.e(LogUtil.TAG_LMF,"更新数据 getisUpdate"+checkRefresh.isUpdate);
            if (checkRefresh != null) {
                if (checkRefresh.isUpdate == null || checkRefresh.isUpdate.equals("0")) {
                    login();
                } else {
                   /* DialogUtil.showSingleBtnDialog(getSupportFragmentManager(), "update", response.updateInfo, getString(R.string.sure), false, new OnSingleDialogClickListener() {
                        @Override
                        public void onPositiveClick(View view) {
                            pd = new ProgressDialog(WelcomeActivity.this);
                            pd.setTitle("downloading");
                            pd.setCanceledOnTouchOutside(false);
                            pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                            downFile("http://iospan.ilove.ren:8092/apk/" + getString(R.string.app_name) + ".apk");
                        }
                    });*/
                }
            }
        }

        @Override
        public void onError(okhttp3.Call call, okhttp3.Response response, Exception e) {
            super.onError(call, response, e);
        }
    }
    //    登录
    public void login() {
        OkGo.post(IUrlConstant.URL_LOGIN)
                .headers("token", PlatformInfoXml.getToken())
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("pushToken", PlatformInfoXml.getPushToken())
                .params("account", UserInfoXml.getUserName())
                .params("password", UserInfoXml.getPassword())
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String response, Call call, Response s) {
                        if(!TextUtils.isEmpty(response)){
                            LoginEntity loginEntity = JSON.parseObject(response, LoginEntity.class);
                            if(loginEntity!=null){
                                String isSucceed=loginEntity.getIsSucceed();
                                if(!TextUtils.isEmpty(isSucceed)&&"1".equals(isSucceed)){
                                    UserInfoXml.setUserInfo(loginEntity.userEnglish);
                                    int likeNum = +loginEntity.getUserEnglish().getSendPraiseCount();
                                    SharedPreferencesUtils.saveData(BaseApplication.getAppContext(),"likeNum",likeNum);
                                    LogUtil.e(LogUtil.TAG_LMF,"boolean IsExitLogin "+UserInfoXml.IsExitLogin());
                                    if(UserInfoXml.IsExitLogin()==false){
                                        Util.gotoActivity(getView(),LoginAcitivity.class,false);
                                 }else if(UserInfoXml.IsExitLogin()==true) {
                                        if(loginEntity.getIsShowUploadImg() != null && loginEntity.getIsShowUploadImg().equals("1")){
                                            Util.gotoActivity(getView(),UploadAvatarActivity.class,true);
                                        }else if(loginEntity.getIsShowUploadAudioInformation() != null && loginEntity.getIsShowUploadAudioInformation().equals("1")){
                                            Util.gotoActivity(getView(),UploadAudioActivity.class,true);
                                        }else{
                                            Util.gotoActivity(getView(),MainActivity.class,true);
                                        }
                                    }

                                }
                            }
                        }
                    }
                });
    }
}
