package com.online.onenight2.loginregs.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.TextView;

import com.library.dialog.OnListDoubleDialogClickListener;
import com.library.utils.DialogUtil;
import com.library.utils.ToastUtil;
import com.lzy.okgo.OkGo;
import com.online.onenight2.MainActivity;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseActivity;
import com.online.onenight2.bean.UpImage;
import com.online.onenight2.callback.JsonCallback;
import com.online.onenight2.constant.IConfigConstant;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.profile.presenter.UploadAvatarPresenter;
import com.online.onenight2.utils.FileSizeUtils;
import com.online.onenight2.view.LoadingDialog;
import com.online.onenight2.xml.PlatformInfoXml;
import com.online.onenight2.xml.UserInfoXml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/6/29.
 */

public class UploadAvatarActivity extends BaseActivity<UploadAvatarPresenter>implements View.OnClickListener{
    @BindView(R.id.tv_up_photo)
    TextView tv_up_photo;
    @BindView(R.id.skip)
    TextView skip;
    private List<String> photo_list;
    private static final int UP_PHOTO_LOCAL = 1;
    private static final int UP_PHOTO_CAMERA = 2;
    private static final int UP_PHOTO_CROP = 3;
    private String isMain = "1";
    private static final int PERMISSION_CODE_READ_EXTERNAL_STORAGE = 0;
    @Override
    public UploadAvatarPresenter newPresenter() {
        return null;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_upphoto;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        photo_list=new ArrayList<>();
        photo_list.add(getString(R.string.profile_benditupian));
        photo_list.add(getString(R.string.profile_paizhao));
        photo_list.add(getString(R.string.profile_quxiao));
        skip.setOnClickListener(this);
        tv_up_photo.setOnClickListener(this);
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_up_photo:
                isMain="1";
                diaupphoto();
                break;
            case R.id.skip:
                goToActivity();
                break;
        }
    }

    private void goToActivity() {
//        注册的是女用户，进入录音界面
        if (!UserInfoXml.isMale()) {
            Intent intent=new Intent(UploadAvatarActivity.this, UploadAudioActivity.class);
            startActivity(intent);
            finish();
        }else{
            Intent intent=new Intent(UploadAvatarActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CODE_READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePhoto();
            }
        }
    }
    //选择照片或者直接拍照
    private void diaupphoto() {
        DialogUtil.showListDialog(getSupportFragmentManager(), null, null, null, false, photo_list, new OnListDoubleDialogClickListener() {
            @Override
            public void onPositiveClick(View view) {
            }
            @Override
            public void onNegativeClick(View view) {
            }
            @Override
            public void onItemClick(View view, String item) {
                if (item.equals(photo_list.get(0))) {
                    Intent localIntent = new Intent(Intent.ACTION_PICK);
                    localIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    localIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    localIntent.setType("image/*");
                    startActivityForResult(localIntent, UP_PHOTO_LOCAL);
                } else if (item.equals(photo_list.get(1))) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                            // 请求android.permission.READ_EXTERNAL_STORAGE权限
                            ActivityCompat.requestPermissions(mContext, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_CODE_READ_EXTERNAL_STORAGE);
                            return;
                        }
                    }
                    takePhoto();
                } else if (item.equals(photo_list.get(2))) {
                }
            }
        });
    }
    public void takePhoto(){
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            // 表示对目标应用临时授权该Uri所代表的文件，7.0及以上
            takePhotoIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        //设置照片的临时保存路径
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, getUriFromFile(mContext,new File(IConfigConstant.LOCAL_AVATAR_PATH)));
        startActivityForResult(takePhotoIntent, UP_PHOTO_CAMERA);
    }
    /**
     * 根据file获得Uri（适配7.0及以上）
     *
     * @param file
     */
    private Uri getUriFromFile(Context context, File file) {
        Uri imageUri = null;
        if (context != null && file != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                // 7.0及以上因为安全问题，需要通过Content Provider封装Uri对象
                imageUri = FileProvider.getUriForFile(mContext, "com.online.onenight.provider", file);
            } else {
                imageUri = Uri.fromFile(file);
            }
        }
        return imageUri;
    }
    Uri originalUri1;
    Bitmap upBp;
    int readPgree;
    File file;
    double fileOrFSize;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case UP_PHOTO_LOCAL:
                if (data != null) {
                    Uri localuri = data.getData();
                    cropImage(localuri);
                }
                break;
            case UP_PHOTO_CAMERA:
                if (data != null) {
                    Uri uri = data.getData();
                    if (uri != null) {
                        cropImage(uri);
                    } else {
                        // 拍照后，7.0以上系统读取图片时需要转换路径读取方式
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            cropImage(getUriFromFile(mContext, new File(IConfigConstant.LOCAL_AVATAR_PATH)));
                        } else {
                            cropImage(Uri.fromFile(new File(IConfigConstant.LOCAL_AVATAR_PATH)));
                        }
                    }
                } else {
                    // 拍照后，7.0以上系统读取图片时需要转换路径读取方式
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        cropImage(getUriFromFile(mContext, new File(IConfigConstant.LOCAL_AVATAR_PATH)));
                    } else {
                        cropImage(Uri.fromFile(new File(IConfigConstant.LOCAL_AVATAR_PATH)));
                    }
                }
                break;
            case UP_PHOTO_CROP:
                if (resultCode != mContext.RESULT_CANCELED) {
                    final BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    options.inSampleSize = calculateInSampleSize(options, 480, 800);
                    options.inJustDecodeBounds = false;
                    if (isMain.equals("1")) {
                        upBp = BitmapFactory.decodeFile(IConfigConstant.LOCAL_AVATAR_PATH, options);
                        saveMyBitmap(upBp, 100);
                        fileOrFSize = FileSizeUtils.getFileOrFilesSize(IConfigConstant.LOCAL_AVATAR_PATH,
                                FileSizeUtils.SIZETYPE_KB);
                    }
                    if (fileOrFSize > 200 && fileOrFSize <= 10000) {
                        int compress = (int) (120 / fileOrFSize * 100);
                        if (compress < 20) {
                            compress = 20;
                        }
                        saveMyBitmap(upBp, compress);
                    }
                    if (isMain.equals("1")) {
                        readPgree = readPicDegree(IConfigConstant.LOCAL_AVATAR_PATH);
                    }
                    if (readPgree != 0) {
                        if (fileOrFSize > 200 && fileOrFSize <= 10000) {
                            int compress = (int) (120 / fileOrFSize * 100);
                            if (compress < 20) {
                                compress = 20;
                            }
                            saveMyBitmap(upBp, compress);
                        }
                    }
                    if (isMain.equals("1")) {
                        file = new File(IConfigConstant.LOCAL_AVATAR_PATH);
                    }
                    if (fileOrFSize > 10000) {
                        file = null;
                        ToastUtil.showShortToast(mContext,getString(R.string.profile_bunengshiyong));
                    }
                }
                if (file != null && file.exists()) {
                    LoadingDialog.showDialogForLoading(this);
                    upPhoto(isMain,file);
                }
                break;
            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    //裁剪图片
    private void cropImage(Uri localuri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(localuri, "image/*");
        intent.putExtra("crop", "true");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        // aspectX aspectY 是裁剪框宽高的比例
        intent.putExtra("aspectX", 3);
        intent.putExtra("aspectY", 4);
        if (isMain.equals("1")) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(IConfigConstant.LOCAL_AVATAR_PATH)));
        }
        intent.putExtra("return-data", false);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        startActivityForResult(intent, UP_PHOTO_CROP);
    }
    // 将图片纠正到正确方向
    public static Bitmap rotateBitmap(int degree, Bitmap bitmap) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap bm = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                bitmap.getHeight(), matrix, true);
        return bm;
    }
    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? widthRatio : heightRatio;
        }
        return inSampleSize;
    }

    /**
     * 通过ExifInterface类读取图片文件的被旋转角度
     *
     * @param path ： 图片文件的路径
     * @return 图片文件的被旋转角度
     */
    public static int readPicDegree(String path) {
        int degree = 0;
        // 读取图片文件信息的类ExifInterface
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (exif != null) {
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
            }
        }
        return degree;
    }
    File fs;
    public void saveMyBitmap(Bitmap mBitmap, int compress) {
        if (isMain.equals("1")) {
            fs = new File(IConfigConstant.LOCAL_AVATAR_PATH);
        }
        if(!fs.exists()){
            fs.mkdir();
        }
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(fs);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        mBitmap.compress(Bitmap.CompressFormat.JPEG, compress, fOut);
        try {
            fOut.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //上传图片
    public void upPhoto(String isMain,File file){
        OkGo.post(IUrlConstant.URL_UPLOAD_PHOTO)
                .tag(this)
                .isMultipart(true)
                .connTimeOut(60*1000)
                .readTimeOut(60*1000)
                .writeTimeOut(60*1000)
                .headers("isMain", isMain)
                .headers("productId", PlatformInfoXml.getPlatfromInfo().getProduct())
                .headers("fid", PlatformInfoXml.getPlatfromInfo().getFid())
                .headers("pid", PlatformInfoXml.getPlatfromInfo().getPid())
                .headers("country", PlatformInfoXml.getPlatfromInfo().getCountry())
                .headers("language", PlatformInfoXml.getPlatfromInfo().getLanguage())
                .headers("version", PlatformInfoXml.getPlatfromInfo().getVersion())
                .headers("platform", PlatformInfoXml.getPlatfromInfo().getPlatform())
                .params("file",file)
                .execute(new JsonCallback<UpImage>() {
                    @Override
                    public void onSuccess(final UpImage image, Call call, Response response) {
                        if (image != null) {
                            if (image.getIsSucceed() != null && image.getIsSucceed().equals("1")) {
                                LoadingDialog.cancelDialogForLoading(mContext);
                                goToActivity();
                            }
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        LoadingDialog.cancelDialogForLoading(mContext);
                        goToActivity();
                    }
                });

    }

}
