package com.online.onenight2.loginregs.presenter;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;

import com.lzy.okgo.OkGo;
import com.online.onenight2.R;
import com.online.onenight2.base.BasePresenter;
import com.online.onenight2.base.RxBus;
import com.online.onenight2.bean.Register;
import com.online.onenight2.callback.DialogCallback;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.loginregs.activity.AnswerQuestionActivity;
import com.online.onenight2.loginregs.activity.RegisterActivity;
import com.online.onenight2.utils.AppsflyerUtils;
import com.online.onenight2.utils.CommonRequestUtil;
import com.online.onenight2.utils.ParamsUtils;
import com.online.onenight2.view.LoadingDialog;
import com.online.onenight2.xml.PlatformInfoXml;
import com.online.onenight2.xml.UserInfoXml;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/4/12.
 * 注册
 */

public class RegisterPresenter extends BasePresenter<RegisterActivity> {
    String dender;
    String age;
    Activity context;
    public RegisterPresenter(Activity context){
        this.context=context;
    }
    public RegisterPresenter(String dender,String age){
        this.dender=dender;
        this.age=age;
    }
    public void RegisterNet(String gender, String age){
        //加载进度框
        LoadingDialog.showDialogForLoading(context);
        OkGo.post(IUrlConstant.URL_REGISTER)
                .params("gender", gender)
                .params("pushToken", PlatformInfoXml.getPushToken())
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("age", age)
                .execute(new RegisterCallBack(getView()));
    }
    private class RegisterCallBack extends DialogCallback<Register>{
        public RegisterCallBack(Activity activity) {
            super(activity);
        }

        @Override
        public void onSuccess(Register register, Call call, Response response) {
            LoadingDialog.cancelDialogForLoading(context);
            if(register!=null){
                String isSuccess=register.getIsSucceed();
                if (!TextUtils.isEmpty(register.getMsg())) {
                    //ToastUtil.showShortToast(getView(), register.getMsg());
                }
                if (!TextUtils.isEmpty(isSuccess)&&"1".equals(isSuccess)) {
                    AppsflyerUtils.appsFlyerRegister();
                    UserInfoXml.setUserInfo(register.getUserPandora());
                    PlatformInfoXml.setToken(register.getToken());
                    // 初始化数据字典
                    CommonRequestUtil.initParamsDict();
                    if (ParamsUtils.getParamsInit() != null && ParamsUtils.getMarriageMap() != null) {
                        RxBus.getInstance().post("change_user",true);

                        //这里就是注册成功后，，进入mainactiivty
                        RxBus.getInstance().post("change_user",true);
//                        Util.gotoActivity(context,AnswerQuestionActivity.class,true,"regist","regist_pager");//一处MainActivity变成注册引导Activity
                        Intent intent = new Intent(context, AnswerQuestionActivity.class);//一处MainActivity变成注册引导Activity
                        intent.putExtra("regist","regist_pager");
                        context.startActivity(intent);
                        context.finish();
                        context.overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);

                    } else {
                        CommonRequestUtil.initParamsDict();
//                        SystemClock.sleep(2000);
//                        RegisterNet(dender,age);

                        if (ParamsUtils.getParamsInit() != null && ParamsUtils.getMarriageMap() != null) {
                            Intent intent = new Intent(context, AnswerQuestionActivity.class);//二处MainActivity变成注册引导Activity
                            intent.putExtra("regist","regist_pager");
                            context.startActivity(intent);
                            context.finish();
                            context.overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
//                                        InterPhotoRecord(response);
                        } else {
                            CommonRequestUtil.initParamsDict();
//                            SystemClock.sleep(2000);
//                            RegisterNet(dender,age);

                            Intent intent = new Intent(context, AnswerQuestionActivity.class);//三处MainActivity变成注册引导Activity
                            intent.putExtra("regist","regist_pager");
                            context.startActivity(intent);
                            context.finish();
                            context.overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);

                        }
                    }
                }
            }

        }

        @Override
        public void onError(Call call, Response response, Exception e) {
            super.onError(call, response, e);
            LoadingDialog.cancelDialogForLoading(context);

        }
    }

}
