package com.online.onenight2.loginregs.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.library.widgets.ScrollControlViewPager;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseActivity;
import com.online.onenight2.loginregs.fragment.AnswerQuestionFragment;
import com.online.onenight2.loginregs.presenter.AnswerQuestionActivityPresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Administrator on 2017/6/28.
 */

public class AnswerQuestionActivity extends BaseActivity<AnswerQuestionActivityPresenter>{
    @BindView(R.id.answer_question_viewpager)
    ScrollControlViewPager mAnswerQuestionViewPager;
    @Override
    public AnswerQuestionActivityPresenter newPresenter() {
        return null;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_answer_question;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        List<Fragment> questionList = new ArrayList<>();
        questionList.add(AnswerQuestionFragment.newInstance(1));
        questionList.add(AnswerQuestionFragment.newInstance(2));
        questionList.add(AnswerQuestionFragment.newInstance(3));
        questionList.add(AnswerQuestionFragment.newInstance(4));
        questionList.add(AnswerQuestionFragment.newInstance(5));
//        questionList.add(AnswerQuestionFragment.newInstance(6));
//        questionList.add(AnswerQuestionFragment.newInstance(7));
//        questionList.add(AnswerQuestionFragment.newInstance(8));
//        questionList.add(AnswerQuestionFragment.newInstance(9));
//        questionList.add(AnswerQuestionFragment.newInstance(10));
        mAnswerQuestionViewPager.setAdapter(new AnswerQuestionAdapter(getSupportFragmentManager(), questionList));
        mAnswerQuestionViewPager.setCanScroll(false);
        mAnswerQuestionViewPager.setOffscreenPageLimit(questionList.size() - 1);
        mAnswerQuestionViewPager.setCurrentItem(0);
    }

    @Override
    public void initData() {

    }

    private static class AnswerQuestionAdapter extends FragmentPagerAdapter {
        private List<Fragment> mFragmentList;

        public AnswerQuestionAdapter(FragmentManager fm, List<Fragment> list) {
            super(fm);
            mFragmentList = list;
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList == null ? null : mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList == null ? 0 : mFragmentList.size();
        }
    }

}
