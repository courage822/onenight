package com.online.onenight2.loginregs.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.online.onenight2.R;
import com.online.onenight2.base.BaseActivity;
import com.online.onenight2.constant.IConfigConstant;
import com.online.onenight2.loginregs.presenter.UserAgreePresenter;

import java.util.Locale;

import butterknife.BindView;

/**
 * Created by Administrator on 2017/4/14.
 */

public class UserAgreeActivity extends BaseActivity<UserAgreePresenter>  {

    @BindView(R.id.tv_useragree_back)
    TextView tv_useragree_back;
    @BindView(R.id.wv_user_agree)
    WebView wv_user_agree;
    @Override
    public int getLayoutId() {
        return R.layout.activity_user_agree;
    }

    @Override
    public UserAgreePresenter newPresenter() {
        return new UserAgreePresenter();
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        tv_useragree_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UserAgreeActivity.this,RegisterActivity.class));
            }
        });
        final String LG = Locale.getDefault().getLanguage();
        if (LG.equals("zh")) {
            wv_user_agree.loadUrl("http://www.easyplay.site/datelove/userTerms.do?pid="+ IConfigConstant.F_IDTW);
        }else{
            wv_user_agree.loadUrl("http://www.easyplay.site/userTerms.do?pid=11001");
        }
        wv_user_agree.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
    }

    @Override
    public void initData() {

    }


}
