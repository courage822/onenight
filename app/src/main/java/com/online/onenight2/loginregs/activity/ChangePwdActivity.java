package com.online.onenight2.loginregs.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.library.utils.ToastUtil;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseActivity;
import com.online.onenight2.bean.ChangePwdBean;
import com.online.onenight2.loginregs.presenter.ChagePwdPresenter;
import com.online.onenight2.xml.UserInfoXml;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;

/**
 * Created by foxmanman on 2017/4/13.
 */

public class ChangePwdActivity extends BaseActivity<ChagePwdPresenter> implements View.OnClickListener {
    @BindView(R.id.tv_change_id)
    TextView tv_change_id;
    @BindView(R.id.tv_change_pwd_text)
    TextView tv_change_pwd_text;
    @BindView(R.id.change_currt_pwd_text)
    EditText change_currt_pwd_text;
    @BindView(R.id.change_sure_passwod_text)
    EditText change_sure_passwod_text;
    @BindView(R.id.change_sure)
    Button change_sure;
    @BindView(R.id.tv_change_back)
    TextView tv_change_back;
    String currtPwd;
    String surePwd;
    private String regEx="[A-Za-z0-9]{4}";
    @Override
    public int getLayoutId() {
        return R.layout.activity_changepwd;
    }

    @Override
    public ChagePwdPresenter newPresenter() {
        return new ChagePwdPresenter();
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        tv_change_id.setText(UserInfoXml.getUID());
        tv_change_pwd_text.setText(UserInfoXml.getPassword());
        tv_change_back.setOnClickListener(this);
        change_sure.setOnClickListener(this);
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_change_back:
                     finish();
                    break;
            case R.id.change_sure:
                currtPwd=change_currt_pwd_text.getText().toString();
                surePwd=change_sure_passwod_text.getText().toString();
                //使用正则表达式校验密码
                Pattern pattern = Pattern.compile(regEx);
                if(!TextUtils.isEmpty(currtPwd)&&!TextUtils.isEmpty(surePwd)){
                    if(currtPwd.equals(surePwd)){
                        Matcher matcher = pattern.matcher(surePwd);
                        boolean rs = matcher.matches();
                        if(rs){
                          getPresenter().saveNewPassword(surePwd);
                        }else {
                            Toast.makeText(ChangePwdActivity.this,getString(R.string.xiugaimima_zuhe), Toast.LENGTH_SHORT).show();
                            change_currt_pwd_text.setText("");
                            change_sure_passwod_text.setText("");
                        }
                    }else{
                        Toast.makeText(ChangePwdActivity.this,getString(R.string.change_new_different), Toast.LENGTH_SHORT).show();
                        change_currt_pwd_text.setText("");
                        change_sure_passwod_text.setText("");
                    }
                }else{
                    Toast.makeText(ChangePwdActivity.this,getString(R.string.xiugaimima_not_null), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }



    public void getNetData(ChangePwdBean changePwdBean ){
        if(changePwdBean!=null&&changePwdBean.getIsSucceed()!=null){
            if(changePwdBean.getIsSucceed().equals("1")){
                tv_change_pwd_text.setText(surePwd);
                tv_change_pwd_text.setText(UserInfoXml.getPassword());
                change_currt_pwd_text.setText("");
                change_sure_passwod_text.setText("");
                UserInfoXml.setPassword(surePwd);
                ToastUtil.showShortToast(ChangePwdActivity.this,changePwdBean.getMsg());
                finish();

            }
        }else {
            ToastUtil.showShortToast(ChangePwdActivity.this,changePwdBean.getMsg());
        }
    }
}
