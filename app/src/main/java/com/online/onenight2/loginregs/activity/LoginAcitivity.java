package com.online.onenight2.loginregs.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.online.onenight2.R;
import com.online.onenight2.base.BaseActivity;
import com.online.onenight2.loginregs.presenter.LoginPresenter;
import com.online.onenight2.utils.ProfileSpUtils;
import com.online.onenight2.xml.UserInfoXml;

import butterknife.BindView;


/**
 * Created by foxmanman on 2017/4/11.
 */



public class LoginAcitivity extends BaseActivity<LoginPresenter> implements View.OnClickListener {
//    @BindView(R.id.tv_login_back)
//    TextView tvBack;
    @BindView(R.id.et_username)
    EditText UserName;
    @BindView(R.id.et_passWord)
    EditText PassWord;
    @BindView(R.id.btn_surelogin)
    Button SureButton;
    @BindView(R.id.btn_sureregister)
    Button btn_register;
    String username;
    String password;
    private boolean isCanLogin=true;
    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        if (UserInfoXml.getUserName() != null) {
            UserName.setText(UserInfoXml.getUserName());
        }
        if (UserInfoXml.getPassword() != null) {
            String s=UserInfoXml.getPassword();
            PassWord.setText(UserInfoXml.getPassword());
        }
//        tvBack.setOnClickListener(this);
        SureButton.setOnClickListener(this);
        btn_register.setOnClickListener(this);
    }

    @Override
    public LoginPresenter newPresenter() {
        return new LoginPresenter(mContext);
    }


    @Override
    public void initData() {

    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
//            case R.id.tv_login_back:
//                UserInfoXml.setUsername("");
//                UserInfoXml.setPassword("");
//                Intent intent=new Intent(LoginAcitivity.this,RegisterActivity.class);
//                intent.putExtra("change_user",true);
//                startActivity(intent);
//                break;
            case R.id.btn_surelogin:
                username=UserName.getText().toString().trim();
                password=PassWord.getText().toString().trim();
                if(!TextUtils.isEmpty(username)&&!TextUtils.isEmpty(password)){
                    getPresenter().LoginNet(username,password);
                }
                isCanLogin=false;
                break;
            case R.id.btn_sureregister:
                isCanLogin=false;
                ProfileSpUtils.clearHobit(LoginAcitivity.this);
                Intent intent2=new Intent(LoginAcitivity.this,RegisterActivity.class);
                startActivity(intent2);
                finish();
                overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
                break;
        }
    }
    private long exitTime = 0;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getAction() == KeyEvent.ACTION_DOWN) {
            if ((System.currentTimeMillis() - exitTime) > 2000) {
                //弹出提示，可以有多种方式
                Toast.makeText(getApplicationContext(),getString(R.string.tuichu), Toast.LENGTH_SHORT).show();
                exitTime = System.currentTimeMillis();
            } else {
                finish();
            }
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isCanLogin) {
            setState(true);
        }else{
            setState(false);
        }
    }
    public void setState(boolean state){
        SharedPreferences sp=getSharedPreferences("out_login", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sp.edit();
        edit.putBoolean("login",state);
        edit.commit();
    }
}
