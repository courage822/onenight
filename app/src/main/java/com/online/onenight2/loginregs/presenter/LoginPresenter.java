package com.online.onenight2.loginregs.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;

import com.library.utils.DensityUtil;
import com.library.utils.SharedPreferenceUtil;
import com.library.utils.TimeUtil;
import com.library.utils.ToastUtil;
import com.library.utils.VersionInfoUtil;
import com.lzy.okgo.OkGo;
import com.online.onenight2.MainActivity;
import com.online.onenight2.R;
import com.online.onenight2.base.BasePresenter;
import com.online.onenight2.base.RxBus;
import com.online.onenight2.bean.LoginEntity;
import com.online.onenight2.bean.PlatformInfo;
import com.online.onenight2.bean.User;
import com.online.onenight2.bean.UserBase;
import com.online.onenight2.callback.DialogCallback;
import com.online.onenight2.constant.IConfigConstant;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.loginregs.activity.LoginAcitivity;
import com.online.onenight2.utils.CountryFidUtils;
import com.online.onenight2.xml.PlatformInfoXml;
import com.online.onenight2.xml.UserInfoXml;

import okhttp3.Call;
import okhttp3.Response;

import static com.online.onenight2.utils.TimeUtils.mContext;

/**
 * Created by foxmanman on 2017/4/12.
 * 登录
 */

public class LoginPresenter extends BasePresenter<LoginAcitivity> {
    Activity context;
    public LoginPresenter(Activity context){
        this.context=context;
    }
    public void LoginNet(String account,String password){
        OkGo.post(IUrlConstant.URL_LOGIN)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("pushToken", PlatformInfoXml.getPushToken())
                .params("account", account)
                .params("password", password)
                .execute(new LoginCallBack(getView()));
    }
    private class LoginCallBack extends DialogCallback<LoginEntity>{
        public LoginCallBack(Activity activity) {
            super(activity);
        }

        @Override
        public void onSuccess(LoginEntity loginEntity, Call call, Response response) {
            if (loginEntity != null) {
                String isSucceed = loginEntity.getIsSucceed();
                if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                    setUserInfo(loginEntity.userEnglish);
                    UserInfoXml.setUserInfo(loginEntity.userEnglish);
                    UserInfoXml.setIsExitLogin(true);
                    RxBus.getInstance().post("login_change","login");
                    SharedPreferences sp=getView().getSharedPreferences("out_login", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = sp.edit();
                    edit.putBoolean("login",false);
                    edit.commit();
//                    Util.gotoActivity(getView(),MainActivity.class,true,"regist","regist_pager");
                    clearCusotomSp();
//                    getView().finish();
                    Intent intent = new Intent(context, MainActivity.class);
                    intent.putExtra("regist","regist_pager");
                    context.startActivity(intent);
                    context.finish();
                    context.overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
                    if (!TextUtils.isEmpty(loginEntity.getMsg())) {
                        ToastUtil.showShortToast(getView(), getView().getString(R.string.login_success));
                    }
                }else{
                    ToastUtil.showShortToast(getView(),getView().getString(R.string.login_onerror));
                }

            }
        }

        @Override
        public void onError(Call call, Response response, Exception e) {
            ToastUtil.showShortToast(getView(),getView().getString(R.string.login_onerror));
        }
    }

    private void setUserInfo(User userEnglish) {
        UserBase userBaseEnglish = userEnglish.getUserBaseEnglish();
        String country = userBaseEnglish.getCountry();
        String language = userBaseEnglish.getLanguage();

        PlatformInfo platformInfo = new PlatformInfo();
        if (country.equals("Taiwan")) {
            platformInfo.setLanguage("Traditional");
        } else {
            platformInfo.setLanguage("English");
        }
        platformInfo.setCountry(country);
        platformInfo.setFid(CountryFidUtils.getCountryFidMap().get(country));
        if (SharedPreferenceUtil.getStringValue(context, PlatformInfoXml.XML_NAME, PlatformInfoXml.KEY_PID, null) == null) {

            String pid = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            SharedPreferenceUtil.setStringValue(context, PlatformInfoXml.XML_NAME, PlatformInfoXml.KEY_PID, pid);
            SharedPreferenceUtil.setStringValue(context, PlatformInfoXml.XML_NAME, PlatformInfoXml.KEY_IMSI, pid);
        }
        platformInfo.setW(String.valueOf(DensityUtil.getScreenWidth(context)));
        platformInfo.setH(String.valueOf(DensityUtil.getScreenHeight(context)));
        platformInfo.setVersion(String.valueOf(VersionInfoUtil.getVersionCode(context)));
        platformInfo.setPhonetype(Build.MODEL);

        platformInfo.setProduct(IConfigConstant.PRODUCT_ID);// 产品号
//        platformInfo.setNetType(getNetType());
//        platformInfo.setMobileIP(getMobileIP());
        platformInfo.setRelease(TimeUtil.getCurrentTime());
        platformInfo.setPlatform("2");
        platformInfo.setSystemVersion(Build.VERSION.RELEASE);
        platformInfo.setPid(getAndroidId());
//                                    platformInfo.setPid( SharedPreferenceUtil.getStringValue(RegisterActivity.this, PlatformInfoXml.XML_NAME, PlatformInfoXml.KEY_PID, null));
        PlatformInfoXml.setPlatfromInfo(platformInfo);

    }


    private void clearCusotomSp() {
        SharedPreferenceUtil.setStringValue(mContext, "PHOTO_URL", "url", "");
        SharedPreferenceUtil.setStringValue(mContext,"PHOTO_NUM","num","0");
        SharedPreferences sp=mContext.getSharedPreferences("save_progress", Context.MODE_PRIVATE);
        SharedPreferences sp3=mContext.getSharedPreferences("photo_num", Context.MODE_APPEND);
        SharedPreferences.Editor edit = sp.edit();
        SharedPreferences.Editor edit3= sp3.edit();
        edit.clear();
        edit3.clear();
        edit.commit();
        edit3.commit();
        UserInfoXml.setAvatarUrl(null);
    }
    private String getAndroidId() {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

}
