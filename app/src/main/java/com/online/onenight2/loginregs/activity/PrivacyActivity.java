package com.online.onenight2.loginregs.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.online.onenight2.R;
import com.online.onenight2.base.BaseActivity;
import com.online.onenight2.constant.IConfigConstant;
import com.online.onenight2.loginregs.presenter.PrivacyPresenter;

import java.util.Locale;

import butterknife.BindView;

/**
 * Created by Administrator on 2017/4/14.
 */

public class PrivacyActivity extends BaseActivity<PrivacyPresenter>  {

    @BindView(R.id.tv_privacy_back)
    TextView tv_privacy_back;
    @BindView(R.id.wv_privacy)
    WebView wv_privacy;
    @Override
    public int getLayoutId() {
        return R.layout.activity_privacy;
    }

    @Override
    public PrivacyPresenter newPresenter() {
        return new PrivacyPresenter();
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        tv_privacy_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PrivacyActivity.this,RegisterActivity.class));
            }
        });
        final String LG = Locale.getDefault().getLanguage();
        if (LG.equals("zh")) {
            wv_privacy.loadUrl("http://www.easyplay.site/datelove/privacypolicy.do?pid"+ IConfigConstant.F_IDTW);
        }else{
            wv_privacy.loadUrl("http://www.easyplay.site/privacypolicy.do?pid=11001");
        }
        wv_privacy.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
    }

    @Override
    public void initData() {

    }


}
