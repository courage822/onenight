package com.online.onenight2.loginregs.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.library.dialog.OneWheelDialog;
import com.library.utils.DensityUtil;
import com.library.utils.DialogUtil;
import com.library.utils.SharedPreferenceUtil;
import com.library.utils.TimeUtil;
import com.library.utils.ToastUtil;
import com.library.utils.VersionInfoUtil;
import com.lzy.okgo.OkGo;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseActivity;
import com.online.onenight2.bean.BaseModel;
import com.online.onenight2.bean.PlatformInfo;
import com.online.onenight2.callback.JsonCallback;
import com.online.onenight2.constant.CommonData;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.constant.IConfigConstant;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.loginregs.presenter.RegisterPresenter;
import com.online.onenight2.utils.AppsflyerUtils;
import com.online.onenight2.utils.CountryFidUtils;
import com.online.onenight2.utils.SharedPreferencesUtils;
import com.online.onenight2.view.LoadingDialog;
import com.online.onenight2.view.WheelView;
import com.online.onenight2.xml.PlatformInfoXml;
import com.online.onenight2.xml.UserInfoXml;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.graphics.Color.rgb;
import static com.online.onenight2.R.id.cb_register_agree;

/**
 * Created by foxmanman on 2017/4/11.
 */

public class RegisterActivity extends BaseActivity<RegisterPresenter> implements View.OnClickListener {
    @BindView(R.id.tv_register_tologin)
    TextView Tologin;
//    @BindView(R.id.iv_regisrer_male)
//    ImageView male;//男
//    @BindView(R.id.iv_register_female)
//    ImageView female;//女
    @BindView(cb_register_agree)
    CheckBox agree;//用户协议
    @BindView(R.id.cb_register_privacy)
    CheckBox cb_register_privacy;//隱私連接
//    @BindView(R.id.wv_age)
//    WheelView wv_age;
//    String age = "24";
    @BindView(R.id.tv_country)
    TextView tvCountry;
    @BindView(R.id.rl_country)
    RelativeLayout llCountry;

    @BindView(R.id.tv_sex)
    TextView tvSex;
    @BindView(R.id.tv_age)
    TextView tvAge;
    @BindView(R.id.btn_register)
    Button btnRegister;
    private boolean change_user;
    private List<String> mAgeList = new ArrayList<>();


    @Override
    public int getLayoutId() {
        return R.layout.activity_register;
    }

    @Override
    public void initView(Bundle savedInstanceState) {


        Tologin.setOnClickListener(this);
        tvAge.setOnClickListener(this);
        tvSex.setOnClickListener(this);
        agree.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        tvCountry.setOnClickListener(this);
        cb_register_privacy.setOnClickListener(this);
        agree.setSelected(true);
//        wv_age.setOffset(1);
//        wv_age.setItems(getAgeList());
//        wv_age.setSeletion(6);
//        wv_age.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
//            @Override
//            public void onSelected(int selectedIndex, String item) {
//                age = item;
//            }
//        });
        new RegisterPresenter(RegisterActivity.this);

    }

//    public static List<String> getAgeList() {
//        List<String> ageList = new ArrayList<String>();
//        for (int i = 0; i < 45; i++) {
//            ageList.add(String.valueOf(i + 18));
//        }
//        return ageList;
//    }

    @Override
    public RegisterPresenter newPresenter() {

        return new RegisterPresenter(mContext);
    }

    @Override
    public void initData() {
        Intent intent = getIntent();
        if (null != intent) {
            change_user = intent.getBooleanExtra("change_user", false);
        }
        agree.setTextColor(rgb(31, 115, 242));
        cb_register_privacy.setTextColor(rgb(31, 115, 242));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_register_tologin:
                startActivity(new Intent(RegisterActivity.this, LoginAcitivity.class));
                finish();
                overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
                break;
            case R.id.btn_register:
                //清空自定义的存储
                clearCusotomSp();
                if (llCountry.getVisibility() != View.GONE) {
                    if (TextUtils.isEmpty(tvCountry.getText().toString()) || tvCountry.getText().toString().equals(getString(R.string.country))) {
                        ToastUtil.showShortToast(this, getString(R.string.select_country));
                        return;
                    }
                }
                String age = (String) tvAge.getText();
                String sex = (String) tvSex.getText();
                if(sex.equals(getString(R.string.female))){
                    if (!UserInfoXml. getIsClient()) {
                        activation();
                    }
                    getPresenter().RegisterNet("1", age);
                }else{
                    if (!UserInfoXml. getIsClient()) {
                        activation();
                    }
                    getPresenter().RegisterNet("0", age);
                }
                break;
            case R.id.tv_age:
                showAgeDialog();
                break;
            case R.id.tv_sex:
                showSexDialog();
                break;
            case cb_register_agree:
                startActivity(new Intent(RegisterActivity.this, UserAgreeActivity.class));
                break;
            case R.id.cb_register_privacy:
                startActivity(new Intent(RegisterActivity.this, PrivacyActivity.class));
                break;
            case R.id.tv_country:
                final List<String> countryList;
                final String LG = Locale.getDefault().getLanguage();
                if (LG.equals("zh")) {
                    countryList = CommonData.getCountryComplexList();
                } else {
                    countryList = CommonData.getCountryList();
                }
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), countryList,
                        getString(R.string.country), getString(R.string.positive), getString(R.string.negative), true, new OneWheelDialog.OnOneWheelDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view, String selectedText) {
                                if (!TextUtils.isEmpty(selectedText)) {
                                    tvCountry.setText(selectedText);
                                    if (LG.equals("zh")) {
                                        for (int i = 0; i < countryList.size(); i++) {
                                            if (countryList.get(i).equals(selectedText)) {
                                                selectedText = CommonData.getCountryList().get(i);
                                            }
                                        }
                                    }
                                    if (selectedText.contains(" ")) {
                                        selectedText = selectedText.replaceAll(" ", "");
                                    }
                                    if ("UnitedStates".equals(selectedText)) {
                                        selectedText = "America";
                                    }
                                    PlatformInfo platformInfo = new PlatformInfo();
                                    if (selectedText.equals("Taiwan")) {
                                        platformInfo.setLanguage("Traditional");
                                    } else {
                                        platformInfo.setLanguage("English");
                                    }
                                    platformInfo.setCountry(selectedText);
                                    platformInfo.setFid(CountryFidUtils.getCountryFidMap().get(selectedText));
                                    if (SharedPreferenceUtil.getStringValue(RegisterActivity.this, PlatformInfoXml.XML_NAME, PlatformInfoXml.KEY_PID, null) == null) {

                                        String pid = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                                        SharedPreferenceUtil.setStringValue(RegisterActivity.this, PlatformInfoXml.XML_NAME, PlatformInfoXml.KEY_PID, pid);
                                        SharedPreferenceUtil.setStringValue(RegisterActivity.this, PlatformInfoXml.XML_NAME, PlatformInfoXml.KEY_IMSI, pid);
                                    }
                                    platformInfo.setW(String.valueOf(DensityUtil.getScreenWidth(RegisterActivity.this)));
                                    platformInfo.setH(String.valueOf(DensityUtil.getScreenHeight(RegisterActivity.this)));
                                    platformInfo.setVersion(String.valueOf(VersionInfoUtil.getVersionCode(getApplicationContext())));
                                    platformInfo.setPhonetype(Build.MODEL);

                                    platformInfo.setProduct(IConfigConstant.PRODUCT_ID);// 产品号
                                    platformInfo.setNetType(getNetType());
                                    platformInfo.setMobileIP(getMobileIP());
                                    platformInfo.setRelease(TimeUtil.getCurrentTime());
                                    platformInfo.setPlatform("2");
                                    platformInfo.setSystemVersion(Build.VERSION.RELEASE);
                                    platformInfo.setPid(getAndroidId());
//                                    platformInfo.setPid("2d19d48a08189be6");
//                                    platformInfo.setPid( SharedPreferenceUtil.getStringValue(RegisterActivity.this, PlatformInfoXml.XML_NAME, PlatformInfoXml.KEY_PID, null));
                                    PlatformInfoXml.setPlatfromInfo(platformInfo);
                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        });
                break;


        }
    }

    private void clearCusotomSp() {
        SharedPreferenceUtil.setStringValue(mContext,"PHOTO_NUM","num","0");
        SharedPreferenceUtil.setStringValue(mContext, "PHOTO_URL", "url", "");
        SharedPreferences sp = getSharedPreferences("save_progress", Context.MODE_PRIVATE);
        SharedPreferences sp3 = mContext.getSharedPreferences("photo_num", Context.MODE_APPEND);
        SharedPreferences.Editor edit = sp.edit();
        SharedPreferences.Editor edit3 = sp3.edit();
        edit.clear();
        edit3.clear();
        edit.commit();
        edit3.commit();
        UserInfoXml.setAvatarUrl(null);
    }

    private long exitTime = 0;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getAction() == KeyEvent.ACTION_DOWN) {
            if ((System.currentTimeMillis() - exitTime) > 2000) {
                //弹出提示，可以有多种方式
                Toast.makeText(getApplicationContext(), getString(R.string.tuichu), Toast.LENGTH_SHORT).show();
                exitTime = System.currentTimeMillis();
            } else {
                finish();
            }
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
//        EventBus.getDefault().register(this);
        Intent intent = getIntent();
        boolean country = intent.getBooleanExtra("cleanCountry", false);
        boolean data = SharedPreferencesUtils.getData(mContext, ConfigConstant.CLEANCOUNTRY, false);
        if (country || data) {
            llCountry.setVisibility(View.GONE);

        } else {
            llCountry.setVisibility(View.VISIBLE);
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        LoadingDialog.cancelDialogForLoading(this);
//        EventBus.getDefault().unregister(this);
    }

    private String getNetType() {
        String type = "";
        ConnectivityManager cm = (ConnectivityManager) RegisterActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null) {
            type = "0";
        } else if (info.getType() == ConnectivityManager.TYPE_WIFI) {
            type = "2";
        } else if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
            String extraInfo = info.getExtraInfo();
            if (extraInfo.equals("cmwap")) {
                type = "3";
            } else if (extraInfo.equals("cmnet")) {
                type = "4";
            } else if (extraInfo.equals("ctnet")) {
                type = "5";
            } else if (extraInfo.equals("ctwap")) {
                type = "6";
            } else if (extraInfo.equals("3gwap")) {
                type = "7";
            } else if (extraInfo.equals("3gnet")) {
                type = "8";
            } else if (extraInfo.equals("uniwap")) {
                type = "9";
            } else if (extraInfo.equals("uninet")) {
                type = "10";
            }
        }
        return type;
    }

    private String getMobileIP() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private String getAndroidId() {
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }
    private void showAgeDialog() {
        View view = getLayoutInflater().inflate(R.layout.dialog_select_age,
                null);
        final Dialog dialog = new Dialog(this, R.style.transparentFrameWindowStyle);
        dialog.setContentView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        Window window = dialog.getWindow();


        final WheelView wheelView = (WheelView) view.findViewById(R.id.dialog_age_wheelview);
        for (int i = 18; i < 66; i++) {
            mAgeList.add(String.valueOf(i));
        }
        wheelView.setData(mAgeList);
        if (!TextUtils.isEmpty(tvAge.getText().toString())) {
            wheelView.setDefaultIndex(mAgeList.indexOf(tvAge.getText().toString()));
        }
        Button btnSure = (Button) view.findViewById(R.id.dialog_age_sure);
        btnSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvAge.setText(wheelView.getSelectedText());
                dialog.dismiss();
            }
        });


        // 设置显示动画
        window.setWindowAnimations(R.style.main_menu_animstyle);
        WindowManager.LayoutParams wl = window.getAttributes();
        wl.x = 0;
        wl.y = getWindowManager().getDefaultDisplay().getHeight();
        // 以下这两句是为了保证按钮可以水平满屏
        wl.width = ViewGroup.LayoutParams.MATCH_PARENT;
        wl.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        // 设置显示位置
        dialog.onWindowAttributesChanged(wl);
        // 设置点击外围解散
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }
    private void showSexDialog() {
        View view = getLayoutInflater().inflate(R.layout.sex_dialog,
                null);
        TextView tvMan = (TextView) view.findViewById(R.id.tv_man);
        TextView tvWoman = (TextView) view.findViewById(R.id.tv_woman);
        Button btnCancel = (Button) view.findViewById(R.id.btn_cancel);
        final Dialog dialog = new Dialog(this, R.style.transparentFrameWindowStyle);
        dialog.setContentView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        Window window = dialog.getWindow();
        // 设置显示动画
        window.setWindowAnimations(R.style.main_menu_animstyle);
        WindowManager.LayoutParams wl = window.getAttributes();
        wl.x = 0;
        wl.y = getWindowManager().getDefaultDisplay().getHeight();
        // 以下这两句是为了保证按钮可以水平满屏
        wl.width = ViewGroup.LayoutParams.MATCH_PARENT;
        wl.height = ViewGroup.LayoutParams.WRAP_CONTENT;

        // 设置显示位置
        dialog.onWindowAttributesChanged(wl);
        // 设置点击外围解散
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
        tvMan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvSex.setText(getString(R.string.male));
                dialog.dismiss();
            }
        });
        tvWoman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvSex.setText(getString(R.string.female));
                dialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }
    //激活
    public void activation() {
        showLoadingPress();
        OkGo.post(IUrlConstant.URL_CLENT_ACTIVATION)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .execute(new SplaActivationCallBack());
    }

    private class SplaActivationCallBack extends JsonCallback<BaseModel> {


        @Override
        public void onSuccess(BaseModel baseModel, okhttp3.Call call, okhttp3.Response response) {
            if (baseModel != null) {
                String isSucceed = baseModel.getIsSucceed();
                if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
//                    getView().hideLoadingPress();
                    UserInfoXml.setIsClient();
                    if(UserInfoXml. getIsClient()){
                        AppsflyerUtils.activate();
                    }
                }
            }
        }

        @Override
        public void onError(okhttp3.Call call, okhttp3.Response response, Exception e) {
            super.onError(call, response, e);
        }
    }
}
