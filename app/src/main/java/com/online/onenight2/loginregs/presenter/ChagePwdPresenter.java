package com.online.onenight2.loginregs.presenter;

import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.online.onenight2.base.BasePresenter;
import com.online.onenight2.bean.ChangePwdBean;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.loginregs.activity.ChangePwdActivity;
import com.online.onenight2.xml.PlatformInfoXml;
import com.online.onenight2.xml.UserInfoXml;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/4/13.
 */

public class ChagePwdPresenter extends BasePresenter<ChangePwdActivity> {
    public void saveNewPassword(String updatePwd){
        OkGo.post(IUrlConstant.URL_MODIFY_PWD)
                .headers("token", PlatformInfoXml.getToken())
                .params("password", UserInfoXml.getPassword())
                .params("newPassword",updatePwd)
                .params("platformInfo",PlatformInfoXml.getPlatformJsonString())
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String response, Call call, Response s) {
                        if(!TextUtils.isEmpty(response)){
                            ChangePwdBean changePwdBean = JSON.parseObject(response, ChangePwdBean.class);
                            if(changePwdBean!=null){
                                getView().getNetData(changePwdBean);
                            }
                        }
                    }
                });
    }

}
