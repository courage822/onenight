package com.online.onenight2.loginregs.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.library.utils.FileUtil;
import com.library.utils.LogUtil;
import com.library.utils.ToastUtil;
import com.lzy.okgo.OkGo;
import com.online.onenight2.MainActivity;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseActivity;
import com.online.onenight2.bean.BaseModel;
import com.online.onenight2.callback.JsonCallback;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.utils.RecordUtil;
import com.online.onenight2.utils.Utils;
import com.online.onenight2.xml.PlatformInfoXml;
import com.online.onenight2.xml.UserInfoXml;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Administrator on 2017/1/13.
 */
public class UploadAudioActivity extends BaseActivity {
    @BindView(R.id.record)
    Button mBtnRecord;
    @BindView(R.id.audition)
    Button mBtnAudition;
    @BindView(R.id.upLoad)
    Button mBtnUpload;
    @BindView(R.id.skip)
    TextView skip;
    @BindView(R.id.profile_activity_rl_back)
    RelativeLayout profileActivityRlBack;
    @BindView(R.id.iv_receive_voice_pic)
    ImageView ivReceiveVoicePic;
    @BindView(R.id.rl_receive_voice_viewbg)
    RelativeLayout rlReceiveVoiceViewbg;
    @BindView(R.id.tv_receive_voice_duration)
    TextView tvReceiveVoiceDuration;

    // 录音框
    private PopupWindow mRecordPopupWindow;
    private static final int PERMISSION_CODE_RECORD_AUDIO = 0;
    private static final int PERMISSION_CODE_WRITE_EXTERNAL_STORAGE = 1;
    private static final int MSG_TYPE_TIMER = 1;

    // 录音文件路径
    private String mOutputPath;
    // 录音文件名
    private String mRecordFileName;
    // 是否正在录音
    private boolean mIsRecording;
    // 录音是否被取消
    private boolean mIsRecordCanceled;
    // 录音时长（毫秒）
    private long mRecordDuration;
    // 录音计时刷新间隔（毫秒）
    private final long mRefreshInterval = 250;
    // 计算录音时长的Handler
    private Handler mTimerHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_TYPE_TIMER:
                    if (mIsRecording) {
                        // 计时
                        mRecordDuration += mRefreshInterval;
                        // 录音超过1分钟自动发送
                        if (mRecordDuration > 60 * 1000) {
                            stopRecordAndSend();
                        } else {
                            mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_TIMER, mRefreshInterval);
                        }
                    }
                    break;
            }
        }
    };
    private String audioSecond;
    private boolean needRequestPermission = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_upload_audio;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Util.gotoActivity(UploadAudioActivity.this, MainActivity.class, true);
                Intent intent = new Intent(UploadAudioActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
            }
        });
        // 检查权限
        if (!needRequestPermission) {// 请求权限时不录音
            checkPermissions();
        }
        mBtnRecord.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                    // 检查权限
                    if (!checkPermissions()) {// 请求权限时不录音
//                        checkPermissions();
//                    } else {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:// 按下时开始录音
                        mBtnRecord.setText(getString(R.string.loose_to_end));
                        showRecordPopupWindow(STATE.RECORDING);
                        startRecord();
                        mIsRecordCanceled = false;
                        LogUtil.e(LogUtil.TAG_ZL, "ACTION_DOWN start()");
                        break;

                    case MotionEvent.ACTION_MOVE:
                        if (event.getY() < -100) { // 上滑取消发送
                            mIsRecordCanceled = true;
                            showRecordPopupWindow(STATE.CANCELED);
                        } else {
                            mIsRecordCanceled = false;
                            showRecordPopupWindow(STATE.RECORDING);
                        }
                        LogUtil.e(LogUtil.TAG_ZL, "ACTION_MOVE " + event.getY() + " mIsRecordCanceled = " + mIsRecordCanceled);
                        break;

                    case MotionEvent.ACTION_UP:// 松开时停止录音并发送
                        mBtnRecord.setText(getString(R.string.press_to_speak));
                        if (mIsRecordCanceled) {
                            cancelRecord();
                            LogUtil.e(LogUtil.TAG_ZL, "ACTION_UP canceld()");
                        } else {
                            stopRecordAndSend();
                            LogUtil.e(LogUtil.TAG_ZL, "ACTION_UP stop()");
                        }
                        break;
                }
                    }
                return true;
            }
        });

        mBtnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRecordDuration < 1 * 1000) {
                    ToastUtil.showShortToast(UploadAudioActivity.this, UploadAudioActivity.this.getString(R.string.record_short));
                } else {
                    uploadAudio();
                }

            }
        });
        //开始设置监听
        final ImageView ieaLlSinger = ivReceiveVoicePic;
        mBtnAudition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOutputPath != null) {
                    File file = new File(mOutputPath);
                    if (file != null && file.exists()) {
                final AnimationDrawable animationDrawable = (AnimationDrawable) ieaLlSinger.getBackground();
                        //获取语音信号栏，开始播放动画
                        animationDrawable.start();
                        mBtnAudition.setText(getResources().getString(R.string.pause));
                if( RecordUtil.getInstance().mPlayerIsNull()){
                    RecordUtil.getInstance().play(mOutputPath, new RecordUtil.OnPlayerListener() {
                        @Override
                        public void onCompleted() {
                            mBtnAudition.setText(getResources().getString(R.string.audition));
                            animationDrawable.stop();
                            rlReceiveVoiceViewbg.setVisibility(View.GONE);
                        }

                        @Override
                        public void onPaused() {

                        }
                    });
                    rlReceiveVoiceViewbg.setVisibility(View.VISIBLE);
                }else if( RecordUtil.getInstance().isPlaying()){
                    RecordUtil.getInstance().pause();
                    mBtnAudition.setText(getResources().getString(R.string.go_on));
                    animationDrawable.stop();
                    rlReceiveVoiceViewbg.setVisibility(View.GONE);
                }else{
                    RecordUtil.getInstance().startPaly(new RecordUtil.OnPlayerListener() {
                        @Override
                        public void onCompleted() {
                            mBtnAudition.setText(getResources().getString(R.string.audition));
                            animationDrawable.stop();
                            rlReceiveVoiceViewbg.setVisibility(View.GONE);
                        }

                        @Override
                        public void onPaused() {

                        }
                    });
                    rlReceiveVoiceViewbg.setVisibility(View.VISIBLE);
                    animationDrawable.start();
                    mBtnAudition.setText(getResources().getString(R.string.pause));
                }

//                i++;
//                if (mOutputPath != null) {
//                    File file = new File(mOutputPath);
//                    if (file != null && file.exists()) {
//                        if (i % 2 != 0) {
//                        mBtnAudition.setText("暂停");
//                        rlReceiveVoiceViewbg.setVisibility(View.VISIBLE);
//                        final AnimationDrawable animationDrawable = (AnimationDrawable) ieaLlSinger.getBackground();
//                        //获取语音信号栏，开始播放动画
//                        animationDrawable.start();
//                        //播放前重置。
////                        MediaManager.release();
//                        MediaManager.playSound(mOutputPath, new MediaPlayer.OnCompletionListener() {
//                            @Override
//                            public void onCompletion(MediaPlayer mp) {
//                                animationDrawable.selectDrawable(0);//显示动画第一帧
//                                animationDrawable.stop();
//                            }
//                        });
//                            RecordUtil.getInstance().preparePlayer(mOutputPath);
//
//                    }else{
//                            // 播放/暂停语音
//                            RecordUtil.getInstance().toggle(mOutputPath, new RecordUtil.OnPlayerListener() {
//
//                                @Override
//                                public void onCompleted() {
//                                    mBtnAudition.setText(getResources().getString(R.string.audition));
//                                    rlReceiveVoiceViewbg.setVisibility(View.GONE);
//                                }
//
//                                @Override
//                                public void onPaused() {
//
//                                    if (mBtnAudition.getText().equals("暂停")) {
//                                        mBtnAudition.setText("继续");
//                                        rlReceiveVoiceViewbg.setVisibility(View.GONE);
//                                    } else {
//                                        mBtnAudition.setText("暂停");
//                                        rlReceiveVoiceViewbg.setVisibility(View.VISIBLE);
//                                    }
//
//                                }
//                            });
//
//                        }
                    } else {
                        ToastUtil.showShortToast(UploadAudioActivity.this, getResources().getString(R.string.not_exist));
                    }
                } else {
                    ToastUtil.showShortToast(UploadAudioActivity.this, getResources().getString(R.string.not_exist));
                }
            }
        });

//        rlReceiveVoiceViewbg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });
    }

    @Override
    public void initData() {

    }

    @Override
    public Object newPresenter() {
        return null;
    }


    // 录音状态常量
    private enum STATE {
        /**
         * 默认值
         */
        IDLE,
        /**
         * 录音中
         */
        RECORDING,
        /**
         * 取消
         */
        CANCELED,
        /**
         * 录音时间太短
         */
        TOO_SHORT
    }

    @Override
    public void onBackPressed() {
        // 屏蔽返回键
    }

    private boolean checkPermissions() {
        needRequestPermission = false;
        if (ContextCompat.checkSelfPermission(UploadAudioActivity.this.getApplicationContext(),
                Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_DENIED) {
            needRequestPermission = true;
            // 请求android.permission.RECORD_AUDIO权限
            ActivityCompat.requestPermissions(UploadAudioActivity.this, new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSION_CODE_RECORD_AUDIO);
        }
        if (ContextCompat.checkSelfPermission(UploadAudioActivity.this.getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
            needRequestPermission = true;
            // 请求android.permission.WRITE_EXTERNAL_STORAGE权限
            ActivityCompat.requestPermissions(UploadAudioActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_CODE_WRITE_EXTERNAL_STORAGE);
        }
        return needRequestPermission;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * 开始录音
     */
    private void startRecord() {
        // 录音开始前震动一下
        Vibrator vibrator = (Vibrator) UploadAudioActivity.this.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(88);
        // 根据系统时间生成文件名
        mRecordFileName = "Onenight" + ".mp3";
        // 录音文件保存路径: 根目录/Amor/record/用户id/xxx.mp3
        mOutputPath = FileUtil.RECORD_DIRECTORY_PATH + File.separator + UserInfoXml.getUID() + File.separator + mRecordFileName;
        LogUtil.e(LogUtil.TAG_ZL, "录音存放路径：" + mOutputPath);
        // 开始录音
        RecordUtil.getInstance().startRecord(mOutputPath);
        mIsRecording = true;
        mRecordDuration = 0;
        // 计时，间隔250毫秒刷新
        mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_TIMER, mRefreshInterval);
    }

    /**
     * 停止录音并发送
     */
    private void stopRecordAndSend() {
        if (mIsRecording) {
            mIsRecording = false;
            LogUtil.e(LogUtil.TAG_ZL, "录音时长（毫秒）：" + mRecordDuration);
            if (mRecordDuration < 1 * 1000) {// 录音时长小于1秒的不发送
                showRecordPopupWindow(STATE.TOO_SHORT);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // 延时500毫秒调用MediaRecorder#stop()，防止出现start()之后立马调用stop()的异常
                        RecordUtil.getInstance().stopRecord();
                        // 删除小于1秒的文件
                        FileUtil.deleteFile(mOutputPath);
                    }
                }, 500);
            } else {// 发送语音
                RecordUtil.getInstance().stopRecord();
            }
            // 隐藏录音框
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mRecordPopupWindow != null && mRecordPopupWindow.isShowing()) {
                        mRecordPopupWindow.dismiss();
                    }
                }
            }, 500);
        }
    }

    /**
     * 录音取消，删除已经录制的文件
     */
    private void cancelRecord() {
        if (mIsRecordCanceled) {
            RecordUtil.getInstance().stopRecord();
            mIsRecordCanceled = false;
            FileUtil.deleteFile(mOutputPath);

            // 隐藏录音框
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mRecordPopupWindow != null && mRecordPopupWindow.isShowing()) {
                        mRecordPopupWindow.dismiss();
                    }
                }
            }, 500);
        }
    }

    /**
     * 弹出录音框
     */
    private void showRecordPopupWindow(STATE state) {
        if (mRecordPopupWindow == null) {
            View contentView = LayoutInflater.from(UploadAudioActivity.this).inflate(R.layout.popup_chat_record, null);
            mRecordPopupWindow = new PopupWindow(contentView, WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT, true);
            mRecordPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        View view = mRecordPopupWindow.getContentView();
        if (view != null) {
            RelativeLayout rlRecording = (RelativeLayout) view.findViewById(R.id.popup_recording_container);
            ImageView ivRecording = (ImageView) view.findViewById(R.id.popup_record_anim);
            // 设置动画
            List<Drawable> drawableList = new ArrayList<Drawable>();
            drawableList.add(getResources().getDrawable(R.drawable.voice1));
            drawableList.add(getResources().getDrawable(R.drawable.voice2));
            drawableList.add(getResources().getDrawable(R.drawable.voice3));
            drawableList.add(getResources().getDrawable(R.drawable.voice4));
            drawableList.add(getResources().getDrawable(R.drawable.voice5));
            drawableList.add(getResources().getDrawable(R.drawable.voice6));
            AnimationDrawable animationDrawable = Utils.getFrameAnim(drawableList, true, 150);
            ivRecording.setImageDrawable(animationDrawable);

            ImageView ivCancel = (ImageView) view.findViewById(R.id.popup_cancel_record);
            ImageView ivTooShort = (ImageView) view.findViewById(R.id.popup_record_too_short);
            TextView tvState = (TextView) view.findViewById(R.id.popup_record_state);
            switch (state) {
                case RECORDING: // 正在录音
                    rlRecording.setVisibility(View.VISIBLE);
                    // 播放动画
                    animationDrawable.start();
                    ivCancel.setVisibility(View.GONE);
                    ivTooShort.setVisibility(View.GONE);
                    tvState.setText(getString(R.string.slideup_to_cancel));
                    break;

                case CANCELED: // 取消录音
                    rlRecording.setVisibility(View.GONE);
                    // 停止动画
                    if (animationDrawable.isRunning()) {
                        animationDrawable.stop();
                    }
                    ivCancel.setVisibility(View.VISIBLE);
                    ivTooShort.setVisibility(View.GONE);
                    tvState.setText(getString(R.string.loose_to_cancel));
                    break;

                case TOO_SHORT:// 录音时间太短
                    rlRecording.setVisibility(View.GONE);
                    // 停止动画
                    if (animationDrawable.isRunning()) {
                        animationDrawable.stop();
                    }
                    ivCancel.setVisibility(View.GONE);
                    ivTooShort.setVisibility(View.VISIBLE);
                    tvState.setText(getString(R.string.record_too_short));
                    break;
            }
        }
        mRecordPopupWindow.showAtLocation(mBtnRecord, Gravity.CENTER, 0, -300);
    }

    /**
     * 上传语音
     */
    private void uploadAudio() {

        showLoadingPress();

        OkGo.post(IUrlConstant.URL_UPLOAD_INTRODUCE_VOICE)
                .headers("token", PlatformInfoXml.getToken())
                .headers("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .headers("audioSecond", String.valueOf(Math.round(mRecordDuration / 1000)))
                .params("request", new File(mOutputPath))
                .execute(new JsonCallback<BaseModel>() {
                    @Override
                    public void onSuccess(BaseModel response, Call call, Response re) {
                        if (response != null) {
                            String isSucceed = response.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                String message = response.getMsg();
                                if (!TextUtils.isEmpty(message)) {
                                    ToastUtil.showShortToast(UploadAudioActivity.this, message);
                                }
                                hideLoadingPress();
//                                Util.gotoActivity(UploadAudioActivity.this, MainActivity.class, true);
                                Intent intent = new Intent(UploadAudioActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
                            } else {
                                hideLoadingPress();
                                ToastUtil.showShortToast(UploadAudioActivity.this, getString(R.string.upload_audio_fail));
                            }
                        } else {
                            hideLoadingPress();
                            ToastUtil.showShortToast(UploadAudioActivity.this, getString(R.string.upload_audio_fail));
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        hideLoadingPress();
                        ToastUtil.showShortToast(UploadAudioActivity.this, getString(R.string.upload_audio_fail));
                    }
                });
    }

}
