package com.online.onenight2.recordmanager;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.online.onenight2.utils.FileUtil;

/**
 * 描述：
 * Created by qyh on 2017/3/22.
 */

public class AudioRecordHeple {
    //设置是否允许录音
    private boolean canRecord = false;
    private boolean mPersion=false;
    private volatile static AudioRecordHeple mAudioRecordHeple = null;
    //三个对话框的状态常量
    private static final int STATE_NORMAL = 1;
    private static final int STATE_RECORDING = 2;
    private static final int STATE_WANT_TO_CANCEL = 3; // 正在录音标记
    //取消录音的状态值
    private static final int MSG_VOICE_STOP = 4;
    private boolean isRecording = false;
    //提醒倒计时
    private int mRemainedTime = 5;
    private final Context mContext;
    //核心录音类
    private AudioManager mAudioManager;
    //当前录音时长
    private float mTime = 0;
    // 是否触发了onlongclick，准备好了
    private boolean mReady;
    //标记是否强制终止
    private boolean isOverTime = false;
    //最大录音时长（单位:s）
    private int mMaxRecordTime = 15;

    private AudioRecordHeple(Context context) {
        this.mContext = context;
    }
    public static AudioRecordHeple getInstance(Context context) {
        //先检查实例是否存在，如果不存在才进入下面的同步块
        if (mAudioRecordHeple == null) {
            //同步块，线程安全的创建实例
            synchronized (AudioRecordHeple.class) {
                //再次检查实例是否存在，如果不存在才真正的创建实例
                mAudioRecordHeple = new AudioRecordHeple(context);
            }
        }
        return mAudioRecordHeple;
    }
    public void getConnection(){
        //获取录音保存位置
        String dir = FileUtil.getAppRecordDir(mContext).toString();
        //实例化录音核心类
        mAudioManager = AudioManager.getInstance(dir);
                if (isCanRecord()&&mPersion==true) {
                    mReady = true;
                    mAudioManager.prepareAudio();
                }
        }

    // 获取音量大小的runnable
    private Runnable mGetVoiceLevelRunnable = new Runnable() {

        @Override
        public void run() {
            while (isRecording) {
                try {
                    //最长mMaxRecordTimes
                    if (mTime > mMaxRecordTime) {
                        mStateHandler.sendEmptyMessage(MSG_VOICE_STOP);
                        return;
                    }
                    Thread.sleep(100);
                    mTime += 0.1f;
                    mStateHandler.sendEmptyMessage(MSG_VOICE_CHANGE);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };
    // 三个状态
    private static final int MSG_AUDIO_PREPARED = 0X110;
    private static final int MSG_VOICE_CHANGE = 0X111;
    private static final int MSG_DIALOG_DIMISS = 0X112;
    private Handler mStateHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_AUDIO_PREPARED:
                    isRecording = true;
                    new Thread(mGetVoiceLevelRunnable).start();

                    // 需要开启一个线程来变换音量
                    break;
                case MSG_VOICE_CHANGE:
                    //剩余10s
                    showRemainedTime();
                    break;
                case MSG_DIALOG_DIMISS:

                    break;
                case MSG_VOICE_STOP:
                    isOverTime = true;//超时
                    mAudioManager.release();// release释放一个mediarecorder
                    System.out.println("录音完成==============");
                    reset();// 恢复标志位
                    break;

            }
        }
    };
    boolean isShcok;
    /**
     * 回复标志位以及状态
     */
    private void reset() {
        isRecording = false;
        mReady = false;
        mTime = 0;

        isOverTime = false;
        isShcok = false;
    }
    private void showRemainedTime() {
        //倒计时
        int remainTime = (int) (mMaxRecordTime - mTime);
        if (remainTime < mRemainedTime) {
            if (!isShcok) {
                isShcok = true;
               // doShock();
            }

        }

    }
    public boolean isCanRecord() {
        return canRecord;
    }

    public void setCanRecord(boolean canRecord) {
        this.canRecord = canRecord;
    }
    public void setHavePersion(boolean persion){
        this.mPersion=persion;
    }
}
