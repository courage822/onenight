package com.online.onenight2.hot.adapter;


import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;

import com.library.utils.HeightUtils;
import com.library.utils.ToastUtil;
import com.lzy.okgo.OkGo;
import com.online.onenight2.R;
import com.online.onenight2.base.RxManager;
import com.online.onenight2.base.adapter.BaseQuickAdapter;
import com.online.onenight2.base.adapter.BaseViewHolder;
import com.online.onenight2.bean.NearByUser;
import com.online.onenight2.bean.SayHello;
import com.online.onenight2.bean.UserBase;
import com.online.onenight2.callback.JsonCallback;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.hot.activity.UserInfoActivity;
import com.online.onenight2.utils.ImageLoaderUtils;
import com.online.onenight2.xml.PlatformInfoXml;
import com.sackcentury.shinebuttonlib.ShineButton;

import java.util.List;

import okhttp3.Call;
import okhttp3.Response;
import rx.functions.Action1;

/**
 * 描述：
 * Created by qyh on 2017/3/25.
 */

public class RecommendListAdapter extends BaseQuickAdapter {
    Activity mCotext;
    private boolean isDoubleClick=false;

    // private FateUser userData;
    public RecommendListAdapter(int layoutResId, List data) {
        super(layoutResId, data);

    }

    @Override
    protected void convert(BaseViewHolder helper, Object item, final int position) {
        final NearByUser userData = (NearByUser) item;
        final UserBase userBaseEnglish = userData.getUserBaseEnglish();
        final String uId = userBaseEnglish.getId();
        //设置昵称
        helper.setText(R.id.tv_recomm_nickname, userBaseEnglish.getNickName());
        //身高
        helper.setText(R.id.tv_recomm_height, HeightUtils.getInchCmByCm(userBaseEnglish.getHeightCm()));
        //年龄
        helper.setText(R.id.tv_recomm_age, userBaseEnglish.getAge()+" ");
        //距离
        if(userData.getDistance()!=null){
            String s= userData.getDistance();
//            s=s.replace("miles","km");
            s=s.replace("km","miles");
            helper.setText(R.id.tv_distance,s);
        }

       final ImageView iv_big= helper.getView(R.id.iv_recomm_avar_big);
        ImageLoaderUtils.display(mContext,iv_big
                , userBaseEnglish.getImage().getThumbnailUrlM());
        ImageLoaderUtils.display(mContext,(ImageView) helper.getView(R.id.iv_recomm_avar)
                , userBaseEnglish.getImage().getThumbnailUrl());

        final ShineButton like = helper.getView(R.id.iv_recomm_like);
        if(userData.getIsSayHello().equals("1")){
            like.setChecked(true);
            like.setEnabled(false);
        }else{
            like.setChecked(false);
            like.setEnabled(true);
        }
        RxManager manager=new RxManager();
        manager.on(ConfigConstant.SAYHI, new Action1<String>() {
            @Override
            public void call(String id) {
                if(userBaseEnglish.getId().equals(id)){
                    NearByUser fateUser = userData;
                    fateUser.setIsSayHello("1");
                    like.setChecked(true);
                    like.setEnabled(false);
                }
            }
        });
        ImageView view =helper.getView(R.id.iv_recomm_avar_big);
        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sayHi(userData,uId,like);
            }
        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext, UserInfoActivity.class);
                intent.putExtra(ConfigConstant.USERID, userBaseEnglish.getId());
                intent.putExtra("ditance",userData.getDistance());
                intent.putExtra(ConfigConstant.ISRECORD,"0");
                intent.putExtra(ConfigConstant.SOURCETAG,"1");
                intent.putExtra("image",userBaseEnglish.getImage().getImageUrl());
                if(!isDoubleClick){
                    isDoubleClick=true;
                    mContext.startActivity(intent);
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        isDoubleClick=false;
                    }
                },1500);
//                mContext.overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
            }
        });

    }

    private void sayHi(final NearByUser userData, String uId, final ShineButton like) {
        OkGo.post(IUrlConstant.URL_SAY_HELLO)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("uid",uId)
                .params("sayHelloType","9")
                .execute(new JsonCallback<SayHello>() {
                    @Override
                    public void onSuccess(SayHello sayHello, Call call, Response response) {
                        if(null!=sayHello&& sayHello.getIsSucceed().equals("1")){
                            ToastUtil.showShortToast(mContext,mContext.getString(R.string.say_hello_success));
                            like.setChecked(true);
                            like.setEnabled(false);
                            userData.setIsSayHello("1");
                        }
                    }
                });
            }
}
