package com.online.onenight2.hot.adapter;

import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.online.onenight2.R;
import com.online.onenight2.base.adapter.BaseQuickAdapter;
import com.online.onenight2.base.adapter.BaseViewHolder;
import com.online.onenight2.bean.SearchUser;
import com.online.onenight2.bean.UserBase;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.hot.activity.UserInfoActivity;
import com.online.onenight2.utils.ImageLoaderUtils;

import java.util.List;

/**
 * 描述：
 * Created by qyh on 2017/4/11.
 */

public class NearListAdapter extends BaseQuickAdapter {
    private boolean isDoubleClick=false;

    public NearListAdapter(int layoutResId, List data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Object item, int position) {
        SearchUser listData=(SearchUser)item;
        final String neardistance=listData.getDistance();
//        LogUtil.e(LogUtil.TAG_LMF,"NearListAdapter neardistance"+neardistance);
       final UserBase userBaseEnglish = listData.getUserBaseEnglish();
        //设置昵称
        helper.setText(R.id.tv_near_nickname,userBaseEnglish.getNickName());
        ImageView iv=helper.getView(R.id.iv_near_avar);
        if(userBaseEnglish.getImage()!=null){
            ImageLoaderUtils.display(mContext,(ImageView) iv,userBaseEnglish.getImage().getThumbnailUrlM());
        }
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext, UserInfoActivity.class);
                intent.putExtra(ConfigConstant.USERID, userBaseEnglish.getId());
                intent.putExtra(ConfigConstant.ISRECORD,"0");
                intent.putExtra(ConfigConstant.SOURCETAG,"1");
                intent.putExtra("image",userBaseEnglish.getImage().getImageUrl());
                if(!TextUtils.isEmpty(neardistance)){
                    intent.putExtra(ConfigConstant.NEARDISTANCE,neardistance);
                }
                if(!isDoubleClick){
                    isDoubleClick=true;
                    mContext.startActivity(intent);
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        isDoubleClick=false;
                    }
                },1500);
            }
        });
    }
}
