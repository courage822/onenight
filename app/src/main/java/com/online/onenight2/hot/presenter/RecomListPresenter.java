package com.online.onenight2.hot.presenter;

import com.library.utils.ToastUtil;
import com.lzy.okgo.OkGo;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseApplication;
import com.online.onenight2.base.BasePresenter;
import com.online.onenight2.bean.NearBy;
import com.online.onenight2.callback.JsonCallback;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.hot.fragment.NearListFragment;
import com.online.onenight2.hot.fragment.RecommendListFragment;
import com.online.onenight2.xml.PlatformInfoXml;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 描述：推荐Presenter
 * Created by qyh on 2017/3/29.
 */

public class RecomListPresenter extends BasePresenter<RecommendListFragment> {


    public  void getHotData(String pageNum, String pageSize, final int type,String distance){


        OkGo.post(IUrlConstant.URL_NEARBY)
                .params("pageNum",pageNum)
                .params("pageSize",pageSize)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("distance", distance)
                .execute(new JsonCallback<NearBy>() {
                    @Override
                    public void onSuccess(NearBy fate, Call call, Response response) {
                        if(null!=fate){
                            if(fate.getListUser()!=null&&fate.getListUser().size()>0){
                                    getView().hideLoadingPress();
                                switch (type) {
                                    case RecommendListFragment.NORMAL:
                                        getView().showData(fate);
                                        break;
                                    case NearListFragment.REFRESH:
                                        getView().refresh(fate);
                                        break;
                                    case NearListFragment.LOADMORE:
                                        getView().loadMore(fate);
                                        break;
                                }
                            }else{
                                getView().showNoData();
                                getView().hideLoadingPress();
                            }
                        }
                    }
                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        if(getView()!=null){
                            getView().hideLoadingPress();
                        }
                        ToastUtil.showShortToast(BaseApplication.getContext(),BaseApplication.getContext().getString(R.string.net_fail));

                    }
                });


           }

}
