package com.online.onenight2.hot.fragment;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.online.onenight2.R;
import com.online.onenight2.base.BaseFragment;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.hot.activity.FilterConditionActivity;
import com.online.onenight2.hot.adapter.TabPagerAdapter;
import com.online.onenight2.utils.SharedPreferencesUtils;

import butterknife.BindView;
import rx.functions.Action1;

/**
 * 描述：首页界面，盛放两个viewpager
 * Created by qyh on 2017/4/11.
 */

public class HomeFragment extends BaseFragment implements View.OnClickListener {
    @BindView(R.id.tb_home_layout)
    TabLayout tbHomeLayout;
    @BindView(R.id.tv_home_km)
    TextView tvHomeKm;
    @BindView(R.id.vp_home_pager)
    ViewPager vpHomePager;
    private boolean is_filter_sure=false;
    private final int tabNum=2;
    private TabPagerAdapter tabAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initView() {
        //挤在一起显示
        tbHomeLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        setAdapter();
        tbHomeLayout.addTab(tbHomeLayout.newTab().setText(getString(R.string.near)));
        tbHomeLayout.addTab(tbHomeLayout.newTab().setText(getString(R.string.hot)));
        tbHomeLayout.setupWithViewPager(vpHomePager);
        tvHomeKm.setOnClickListener(this);
        initKM();
    }

    private void initKM() {
        String data = SharedPreferencesUtils.getData(mContext, ConfigConstant.DISTANCE, 10 + "miles");
        if(TextUtils.isEmpty(data)){
            tvHomeKm.setText("10KM");
        }else {
            tvHomeKm.setText(data);
        }
    }

    private void setAdapter() {
        tabAdapter = new TabPagerAdapter(getChildFragmentManager(),tabNum);
        tabAdapter.addTabFragment(RecommendListFragment.newInstance(),getString(R.string.near));
        tabAdapter.addTabFragment(NearListFragment.newInstance(),getString(R.string.hot));
        vpHomePager.setAdapter(tabAdapter);
    }

    @Override
    protected void initData() {
        mRxManager.on(ConfigConstant.FILTER_SURE, new Action1<Boolean>() {
            @Override
            public void call(Boolean aBoolean) {
                if(aBoolean){
                    //从条件筛选界面回来，进入附近界面
                    vpHomePager.setCurrentItem(0);
                    initKM();
                }
            }
        });

        if(!is_filter_sure){
            RecommendListFragment.newInstance();
        }
    }

    @Override
    public Object newPresenter() {

        return null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_home_km:
                Intent intent=new Intent(mContext, FilterConditionActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        is_filter_sure=false;
    }


}
