package com.online.onenight2.hot.fragment;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.online.onenight2.R;
import com.online.onenight2.base.BaseFragment;
import com.online.onenight2.base.RxManager;
import com.online.onenight2.base.adapter.BaseQuickAdapter;
import com.online.onenight2.base.adapter.OnItemClickListener;
import com.online.onenight2.bean.MatcherInfo;
import com.online.onenight2.bean.SearchUser;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.hot.adapter.NearListAdapter;
import com.online.onenight2.hot.presenter.NearListPresenter;
import com.online.onenight2.utils.SharedPreferencesUtils;
import com.online.onenight2.view.refresh.NormalRefreshViewHolder;
import com.online.onenight2.view.refresh.RefreshLayout;

import java.util.List;

import butterknife.BindView;
import rx.functions.Action1;

/**
 * 描述：附近界面
 * Created by qyh on 2017/4/11.
 */

public class NearListFragment extends BaseFragment<NearListPresenter> implements RefreshLayout.RefreshLayoutDelegate, BaseQuickAdapter.RequestLoadMoreListener{
    @BindView(R.id.rc_hot_content)
    RecyclerView rcHotContent;
    @BindView(R.id.refresh)
    RefreshLayout refresh;
    private NearListAdapter mNearAdapter;
    private int pageNum = 1;
    //距离",以千米为单位
    private String distance = "10";

    // 正常请求
    public static final int NORMAL = 1;
    // 刷新请求
    public static final int REFRESH = 2;
    // 加载更多请求
    public static final int LOADMORE = 3;
    private View notLoadingView;
    //是否已经展示了无数据布局
    private boolean isShowNoDataView = false;
    private boolean  isCanLoad=true;
    @Override
    public int getLayoutId() {
        return R.layout.fragmentlist_recom_near;
    }
    public static NearListFragment newInstance(){
        NearListFragment nearListFragment=new NearListFragment();
        return nearListFragment;
    }

    @Override
    public NearListPresenter newPresenter() {
        return new NearListPresenter();
    }

    @Override
    protected void initView() {
        mNearAdapter = new NearListAdapter(R.layout.item_nearlist_image, null);
        rcHotContent.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        rcHotContent.setHasFixedSize(true);
        //设置适配器加载动画
        mNearAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        rcHotContent.setAdapter(mNearAdapter);
        //设置适配器可以上拉加载
        mNearAdapter.setOnLoadMoreListener(this);
        //设置下拉、上拉
        refresh.setDelegate(this);
        refresh.setRefreshViewHolder(new NormalRefreshViewHolder(mContext, true));
        //条目的点击事件
        rcHotContent.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void SimpleOnItemClick(BaseQuickAdapter adapter, View view, int position) {

            }
        });
        RxManager manager=new RxManager();
        manager.on("info", new Action1<MatcherInfo>() {
            @Override
            public void call(MatcherInfo matcherInfo) {
                if(matcherInfo!=null){
                    String distanceKm = SharedPreferencesUtils.getData(mContext, ConfigConstant.DISTANCE, 10 + "");
                    distance=distanceKm;
//                    getPresenter().getNearListData(pageNum, distanceKm,NORMAL);

                }
            }
        });
        initData();
    }

    @Override
    protected void initData() {
      //  showLoadingPress();
        getPresenter().getNearListData(pageNum,distance,NORMAL);
    }

    public void showData(List<SearchUser> list){
        mNearAdapter.setNewData(list);

    }
    public void refresh(List<SearchUser> list){
        //移除底部布局
        if (null != notLoadingView) {
            mNearAdapter.removeFooterView(notLoadingView);
        }
        mNearAdapter.setNewData(list);
        refresh.endRefreshing();

    }
    public void loadMore(List<SearchUser> list){
        mNearAdapter.addData(list);
        refresh.endLoadingMore();
    }
    public void showNoData() {
        refresh.endLoadingMore();
        //加载没有更多提示布局
        mNearAdapter.loadComplete();
        if (notLoadingView == null) {
            notLoadingView = mContext.getLayoutInflater().inflate(R.layout.not_loading,
                    (ViewGroup) rcHotContent.getParent(), false);
        }

        if (!isShowNoDataView) {
            //加载脚布局
            mNearAdapter.addFooterView(notLoadingView);
            isShowNoDataView = true;
        }
    }
    @Override
    public void onLoadMoreRequested() {
    }

    @Override
    public void onRefreshLayoutBeginRefreshing(RefreshLayout refreshLayout) {
        pageNum=1;
        getPresenter().getNearListData(pageNum,distance,REFRESH);
    }

    @Override
    public boolean onRefreshLayoutBeginLoadingMore(RefreshLayout refreshLayout) {
        pageNum++;
        getPresenter().getNearListData(pageNum,distance,LOADMORE);
        return true;
    }
    @Override
    public void onResume() {
        super.onResume();
        RxManager rxManager=new RxManager();
        rxManager.on("login_change", new Action1<String>() {
            @Override
            public void call(String s) {
                if (isCanLoad) {
                    getPresenter().getNearListData(pageNum,distance,NORMAL);
                    isCanLoad=false;
                }
            }
        });
    }
}
