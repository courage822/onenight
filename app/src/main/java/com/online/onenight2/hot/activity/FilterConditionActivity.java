package com.online.onenight2.hot.activity;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.bigkoo.pickerview.OptionsPickerView;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseActivity;
import com.online.onenight2.base.RxBus;
import com.online.onenight2.bean.MatcherInfo;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.profile.activity.PayActivity;
import com.online.onenight2.utils.ParamsUtils;
import com.online.onenight2.utils.SharedPreferencesUtils;
import com.online.onenight2.utils.TextContentUtils;
import com.online.onenight2.xml.UserInfoXml;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 描述：条件筛选界面
 * Created by qyh on 2017/4/15.
 */

public class FilterConditionActivity extends BaseActivity {
    @BindView(R.id.tv_appbar_back)
    TextView tvAppbarBack;
    @BindView(R.id.tv_appbar_title)
    TextView tvAppbarTitle;
    @BindView(R.id.tv_appbar_sure)
    TextView tvAappbarSure;
    @BindView(R.id.tv_filter_km)
    TextView tvFilterKm;
    @BindView(R.id.tv_filter_age)
    TextView tvFilterAge;
    @BindView(R.id.tv_filter_height)
    TextView tvFilterHeight;
    @BindView(R.id.tv_filter_education)
    TextView tvFilterEducation;
    @BindView(R.id.tv_filter_job)
    TextView tvFilterJob;
    private MatcherInfo info=new MatcherInfo();
    private ArrayList<String> kmData;
    private boolean isVip;

    @Override
    public int getLayoutId() {
        return R.layout.activity_filter_condition;
    }

    @Override
    public Object newPresenter() {
        return null;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        tvAppbarTitle.setText(getResources().getString(R.string.screen));
        tvAppbarBack.setText(getResources().getString(R.string.profile_quxiao));
        tvAappbarSure.setText(getResources().getString(R.string.positive));
        String data = SharedPreferencesUtils.getData(mContext, ConfigConstant.DISTANCE, 10 + "");
        if(TextUtils.isEmpty(data)){
            tvFilterKm.setText("10KM");
        }else {
            tvFilterKm.setText(data);
        }
    }

    @Override
    public void initData() {
        if (UserInfoXml.isBeanUser()|| UserInfoXml.isVip() || UserInfoXml.isMonthly()
                ||UserInfoXml.getGender().equals("1")) {
            isVip = true;
        }
    }
    @OnClick({R.id.tv_appbar_sure,R.id.tv_appbar_back,R.id.tv_filter_age,R.id.tv_filter_height
            ,R.id.tv_filter_education,R.id.tv_filter_job,R.id.tv_filter_km})
    public void Click(TextView view){
        switch (view.getId()){
            case R.id.tv_appbar_sure:
                RxBus.getInstance().post(ConfigConstant.FILTER_SURE,true);
                RxBus.getInstance().post("info",info);

                finish();
                break;
            case R.id.tv_appbar_back:
                    finish();
                break;
            case R.id.tv_filter_age:
                if(isVip) {
                    OptionsPickerView ageView = new OptionsPickerView.Builder(this, new OptionsPickerView.OnOptionsSelectListener() {
                        @Override
                        public void onOptionsSelect(int options1, int options2, int options3, View v) {
                            String minAge = TextContentUtils.getAgeLeft().get(options1);
                            String maxAge = TextContentUtils.getAgeRight().get(options1).get(options2);
                            tvFilterAge.setText(minAge + "~" + maxAge);
                            info.setMinAge(Integer.parseInt(minAge));
                            info.setMaxAge(Integer.parseInt(maxAge));
                        }
                    })
                            .setSubmitText(getString(R.string.change_sure))
                            .setCancelText(getString(R.string.change_cancel))
                            .setSubmitColor(0xFFe71623)
                            .setCancelColor(0xFFe71623)
                            .build();
                    ageView.setPicker(TextContentUtils.getAgeLeft(), TextContentUtils.getAgeRight());
                    ageView.show();
                }else {
                    goPay();
                }
                break;
            case R.id.tv_filter_height:
                if(isVip) {
                    OptionsPickerView optionsPickerView = new OptionsPickerView.Builder(this, new OptionsPickerView.OnOptionsSelectListener() {
                        @Override
                        public void onOptionsSelect(int options1, int options2, int options3, View v) {
                            String minCm = TextContentUtils.getHeightLeft().get(options1);
                            String maxCm = TextContentUtils.getHeightRight().get(options1).get(options2);
                            tvFilterHeight.setText(minCm + "~" + maxCm);
                            String minString = minCm.substring(0, 3);
                            String maxString = maxCm.substring(0, 3);
                            info.setMinHeightCm(Integer.parseInt(minString));
                            info.setMaxHeightCm(Integer.parseInt(maxString));
                        }
                    })
                            .setSubmitText(getString(R.string.change_sure))
                            .setCancelText(getString(R.string.change_cancel))
                            .setSubmitColor(0xFFe71623)
                            .setCancelColor(0xFFe71623)
                            .build();

                    optionsPickerView.setPicker(TextContentUtils.getHeightLeft(), TextContentUtils.getHeightRight());
                    optionsPickerView.show();
                }else {
                    goPay();
                }
                break;
            case R.id.tv_filter_education:
                if(isVip) {
                    OptionsPickerView oeducationView = new OptionsPickerView.Builder(this, new OptionsPickerView.OnOptionsSelectListener() {
                        @Override
                        public void onOptionsSelect(int options1, int options2, int options3, View v) {
                            String s = ParamsUtils.getEducationMapValue().get(options1);
                            tvFilterEducation.setText(s);
                            info.setMinimumEducation(Integer.valueOf(ParamsUtils.getEducationMapKeyByValue(s)));
                        }
                    })
                            .setSubmitText(getString(R.string.change_sure))
                            .setCancelText(getString(R.string.change_cancel))
                            .setSubmitColor(0xFFe71623)
                            .setCancelColor(0xFFe71623)
                            .build();

                    oeducationView.setPicker(ParamsUtils.getEducationMapValue());
                    oeducationView.show();
                }else {
                    goPay();
                }
                break;
            case R.id.tv_filter_job:
                if(isVip) {
                    OptionsPickerView jobView = new OptionsPickerView.Builder(this, new OptionsPickerView.OnOptionsSelectListener() {
                        @Override
                        public void onOptionsSelect(int options1, int options2, int options3, View v) {
                            String s = ParamsUtils.getWorkMapValue().get(options1);
                            tvFilterJob.setText(s);
                            info.setOccupation(Integer.valueOf(ParamsUtils.getWorkMapKeyByValue(s)));
                        }
                    })
                            .setSubmitText(getString(R.string.change_sure))
                            .setCancelText(getString(R.string.change_cancel))
                            .setSubmitColor(0xFFe71623)
                            .setCancelColor(0xFFe71623)
                            .build();

                    jobView.setPicker(ParamsUtils.getWorkMapValue());
                    jobView.show();
                }else {
                    goPay();
                }
                break;
            case R.id.tv_filter_km:
                kmData = TextContentUtils.getKMData();
                OptionsPickerView KMView=new OptionsPickerView.Builder(this, new OptionsPickerView.OnOptionsSelectListener() {
                    @Override
                    public void onOptionsSelect(int options1, int options2, int options3, View v) {
                        String distance = TextContentUtils.getKMData().get(options1);
                        tvFilterKm.setText(distance);
                        SharedPreferencesUtils.saveData(mContext,ConfigConstant.DISTANCE,distance);
                    }
                })
                        .setSubmitText(getString(R.string.change_sure))
                        .setCancelText(getString(R.string.change_cancel))
                        .setSubmitColor(0xFFe71623)
                        .setCancelColor(0xFFe71623)
                        .build();

                KMView.setPicker(kmData);
                KMView.show();
                break;
        }
    }
    public void goPay(){
        Intent intent=new Intent(mContext, PayActivity.class);
        startActivityForResult(intent,100);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==100){
            if (UserInfoXml.isBeanUser()|| UserInfoXml.isVip() || UserInfoXml.isMonthly()
                    ||UserInfoXml.getGender().equals("1")) {
                isVip = true;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(null!=kmData){
            kmData.clear();
            kmData=null;
        }
    }
}
