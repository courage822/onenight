package com.online.onenight2.hot.presenter;

import com.google.gson.Gson;
import com.lzy.okgo.OkGo;
import com.online.onenight2.base.BasePresenter;
import com.online.onenight2.bean.MatcherInfo;
import com.online.onenight2.bean.Search;
import com.online.onenight2.bean.SearchUser;
import com.online.onenight2.callback.JsonCallback;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.hot.fragment.NearListFragment;
import com.online.onenight2.xml.PlatformInfoXml;
import com.online.onenight2.xml.SearchXml;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 描述：
 * Created by qyh on 2017/4/11.
 */

public class NearListPresenter extends BasePresenter<NearListFragment> {

    private Gson gson;

    public void getNearListData(int pageNum, String distance, final int type){
        Map<String, MatcherInfo> map = new HashMap<>();
        map.put("matcherInfo", SearchXml.getMatcherInfo());
        gson = new Gson();
        String mapString = gson.toJson(map);
        OkGo.post(IUrlConstant.URL_SEARCH)
            .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
            .params("pageNum",String.valueOf(pageNum))
            .params("randomNum",String.valueOf(0))
             .params("matcherInfo", mapString)
             .params("distance",distance)
            .execute(new JsonCallback<Search>() {
                @Override
                public void onSuccess(Search search, Call call, Response response) {
                  getView().hideLoadingPress();
                    if(null!=search){
                        List<SearchUser> listUser = search.getListSearch();
                        if(null!=listUser&&listUser.size()>0){
                            getView().hideLoadingPress();
                            switch (type) {
                                case NearListFragment.NORMAL:
                                    getView().showData(listUser);
                                    break;
                                case NearListFragment.REFRESH:
                                    getView().refresh(listUser);
                                    break;
                                case NearListFragment.LOADMORE:
                                    getView().loadMore(listUser);
                                    break;
                            }
                        }else {
                            getView().showNoData();
                        }
                    }
                }

                @Override
                public void onError(Call call, Response response, Exception e) {
                    super.onError(call, response, e);

                }
            });
    }
}
