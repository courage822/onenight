package com.online.onenight2.hot.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.library.utils.HeightUtils;
import com.library.utils.ToastUtil;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseActivity;
import com.online.onenight2.base.RxBus;
import com.online.onenight2.bean.BaseModel;
import com.online.onenight2.bean.Image;
import com.online.onenight2.bean.MyInfo;
import com.online.onenight2.bean.NextUsers;
import com.online.onenight2.bean.SayHello;
import com.online.onenight2.bean.User;
import com.online.onenight2.bean.UserBase;
import com.online.onenight2.bean.UserDetail;
import com.online.onenight2.callback.JsonCallback;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.good.ActionSheetDialog;
import com.online.onenight2.good.OnOperItemClickL;
import com.online.onenight2.hot.presenter.UserInfoPresenter;
import com.online.onenight2.message.activity.ChatMegActivity;
import com.online.onenight2.profile.activity.PictureLookActivity;
import com.online.onenight2.profile.adapter.PhotoWallAdapter;
import com.online.onenight2.utils.ImageLoaderUtils;
import com.online.onenight2.utils.MathDataUtils;
import com.online.onenight2.utils.ParamsUtils;
import com.online.onenight2.utils.YeMeiShow;
import com.online.onenight2.view.LoadingDialog;
import com.online.onenight2.view.dialog.ReportDialog;
import com.online.onenight2.xml.PlatformInfoXml;
import com.online.onenight2.xml.UserInfoXml;
import com.online.onenight2.yemei.model.MyPrestener;
import com.online.onenight2.yemei.view.IVContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Response;

/**
 * 描述：对方空间页
 * Created by wangyong on 2017/4/12.
 */

public class UserInfoActivity extends BaseActivity<UserInfoPresenter> implements View.OnClickListener, IVContract {
    @BindView(R.id.userinfor_collapsing_toolbar)
    CollapsingToolbarLayout collapsing_toolbar;
    @BindView(R.id.userinfor_iv_Image)
    ImageView userinfor_iv;
    @BindView(R.id.userinfor_toolbar)
    Toolbar toolbar;
    @BindView(R.id.userinfor_rl)
    RecyclerView rl;
    @BindView(R.id.userinfor_tv_msg)
    TextView tv_msg;
    //我的标签
    @BindView(R.id.userinfo_tv_tag1)
    TextView tv_tag1;
    @BindView(R.id.userinfo_tv_tag2)
    TextView tv_tag2;
    @BindView(R.id.userinfo_tv_tag3)
    TextView tv_tag3;
    @BindView(R.id.userinfo_tv_tag4)
    TextView tv_tag4;
    //我的介绍
    @BindView(R.id.userinfo_tv_jieshao)
    TextView tv_jieshao;
    //基本资料
    @BindView(R.id.userinfo_tv_xueli)
    TextView tv_xueli;
    @BindView(R.id.userinfo_tv_zhiye)
    TextView tv_zhiye;
    @BindView(R.id.userinfo_tv_shouru)
    TextView tv_shouru;
    @BindView(R.id.userinfo_tv_hunyin)
    TextView tv_hunyin;
    @BindView(R.id.userinfo_tv_xingzuo)
    TextView tv_xingzuo;
    @BindView(R.id.userinfo_tv_xingquaihao)
    TextView tv_xingquaihao;
    @BindView(R.id.userinfo_tv_yundong)
    TextView tv_yundong;
    @BindView(R.id.userinfo_view_xian)
    View xian;
    @BindView(R.id.userinfo_xingqu_linear)
    LinearLayout xingqu_linear;
    @BindView(R.id.userinfo_rl_say_hello)
    RelativeLayout rl_sayHello;
    @BindView(R.id.home_activity_linear)
    LinearLayout biaoqian_ll;
    @BindView(R.id.userinfo_rl_say_hello_tv)
    TextView tv_sayhello;
    @BindView(R.id.userinfo_rl_write)
    RelativeLayout rl_write;

    // 常住地
    @BindView(R.id.userinfo_tv_changzhudi)
    TextView userinfo_tv_changzhudi;
    @BindView(R.id.activity_home_userinfo_rl_biaoqian)
    RelativeLayout rl_biaoqian;
    @BindView(R.id.activity_home_userinfo_cd_biaoqian)
    CardView cd_biaoqian;
    @BindView(R.id.userDetail_fab_next)
    FloatingActionButton userDetailFabNext;
    private String uid;
    private String sourceTag;
    private String isRecord;
    private PhotoWallAdapter photoWalladapter;
    private List<Image> listImagedate = new ArrayList<>();
    private boolean first_lahei = true;
    private int isLahei = 0;
    private String nickName;
    private String isPraised;
    private User userEnglish;
    private String ditance;
    private UserBase user = new UserBase();
    private MyPrestener myPrestener;
    private int pageNum=0;
    private boolean isNextButton=false;

    @Override
    public int getLayoutId() {
        return R.layout.activity_home_userinfo;
    }

    @Override
    public UserInfoPresenter newPresenter() {
        return new UserInfoPresenter();
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        collapsing_toolbar.setCollapsedTitleGravity(Gravity.CENTER);
        setSupportActionBar(toolbar);
        //设置是否有返回箭头
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        userinfor_iv.setOnClickListener(this);
        rl_sayHello.setOnClickListener(this);
        rl_write.setOnClickListener(this);
        userDetailFabNext.setOnClickListener(this);
        initIntent();
        //加载进度框
        LoadingDialog.showDialogForLoading(mContext);
        photoWalladapter = new PhotoWallAdapter(this, listImagedate);
        rl.setAdapter(photoWalladapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rl.setLayoutManager(linearLayoutManager);
        rl.setHasFixedSize(true);
        photoWalladapter.setOnItemClickListener(mOnItemClickListener);
        myPrestener = new MyPrestener(this);
    }


    private void initIntent() {
        //进入对方空间页，不推送 谁看过我 消息
        RxBus.getInstance().post(ConfigConstant.ISUSERINFO, true);
        Intent intent = getIntent();
        if (null != intent) {
            uid = intent.getStringExtra(ConfigConstant.USERID);
            sourceTag = intent.getStringExtra(ConfigConstant.SOURCETAG);
            isRecord = intent.getStringExtra(ConfigConstant.ISRECORD);
            ditance = intent.getStringExtra("ditance");
            String image = intent.getStringExtra("image");
            // ImageLoaderUtils.display(mContext,userinfor_iv,image);
        }
        getUserInfo(uid, sourceTag, isRecord);
        getUerInfo();
    }

    //获取下载完成的数据
    public void getData(User userEnglish,boolean isNextButton) {
//        userEnglish = userDetail.getUserEnglish();
        if (userEnglish != null) {
            //是否拉黑
            isLahei = userEnglish.getIsBlackList();
            user = userEnglish.getUserBaseEnglish();
            //距离，年龄 身高/体重
            setUserBaseData(userEnglish,isNextButton);
            UserBase userBaseEnglish = userEnglish.getUserBaseEnglish();
            if (userBaseEnglish != null) {
                nickName = userBaseEnglish.getNickName();
                collapsing_toolbar.setTitle(userBaseEnglish.getNickName());
                if (ParamsUtils.getIncomeMap() != null) {
                String textMsg = ParamsUtils.getIncomeMap().get(String.valueOf(userBaseEnglish.getIncome()));
                if (!TextUtils.isEmpty(textMsg)) {
                    if (!TextUtils.isEmpty(ditance)) {
                        if (ditance.contains("km")) {
                            ditance = ditance.replace("km", "miles");
                        }
//                        tv_msg.setText(ditance + " " + userBaseEnglish.getAge() + "years " + userBaseEnglish.getHeightCm() + "CM/" + textMsg);
                        tv_msg.setText(ditance + " " + userBaseEnglish.getAge() + "  " + HeightUtils.getInchCmByCm(userBaseEnglish.getHeightCm()));

                    } else {
//                        tv_msg.setText(userBaseEnglish.getAge() + "years " + userBaseEnglish.getHeightCm() + "CM/" + textMsg);
                        tv_msg.setText(userBaseEnglish.getAge() + "  " + HeightUtils.getInchCmByCm(userBaseEnglish.getHeightCm()));
                    }
                } else {
                    if (!TextUtils.isEmpty(ditance)) {
                        if (ditance.contains("km")) {
                            ditance = ditance.replace("km", "miles");
                        }
                        tv_msg.setText(ditance + " " + userBaseEnglish.getAge() + "  " + HeightUtils.getInchCmByCm(userBaseEnglish.getHeightCm()));
                    } else {
                        tv_msg.setText(userBaseEnglish.getAge() + "  " + HeightUtils.getInchCmByCm(userBaseEnglish.getHeightCm()));

                    }
                }
            }
                //设置基本数据
                setBaseData(userBaseEnglish);
            }
            Image image = userEnglish.getListImage().get(0);

            if (image != null) {
                listImagedate.clear();
                listImagedate.addAll(userEnglish.getListImage());
                photoWalladapter.setData(userEnglish.getListImage());
                ImageLoaderUtils.display(mContext, userinfor_iv, userEnglish.getListImage().get(0).getImageUrl());
                //取消进度框
                LoadingDialog.cancelDialogForLoading(mContext);
            }
        }
    }

    public void isCanWrite(MyInfo info) {
        if (info != null) {
            User userEnglish = info.getUserEnglish();
            if (userEnglish != null) {
                //判断是否包月和开通Vip
                if (userEnglish.getIsMonthly() == 1 || userEnglish.getIsVip() == 1) {
                    rl_sayHello.setVisibility(View.GONE);
                    rl_write.setVisibility(View.VISIBLE);
                    //男用户啊
                } else if (UserInfoXml.getGender().equals("0")) {
                    rl_sayHello.setVisibility(View.VISIBLE);
                    rl_write.setVisibility(View.GONE);
                } else {
                    rl_sayHello.setVisibility(View.GONE);
                    rl_write.setVisibility(View.VISIBLE);
                }
            }
        }

    }

    private void setUserBaseData(User userEnglish,boolean isNextButton) {
        //判断是否已经打招呼了
        int isSayHello = userEnglish.getIsSayHello();
        if (isSayHello == 1 && !isNextButton) {
            rl_sayHello.setBackgroundResource(R.mipmap.say_hello_height);
            tv_sayhello.setText(getString(R.string.say_helloed));
            rl_sayHello.setEnabled(false);

        }else{
            rl_sayHello.setBackgroundResource(R.mipmap.say_hello);
            tv_sayhello.setText(getString(R.string.say_hello));
            rl_sayHello.setEnabled(true);
        }
        //兴趣爱好
        String listHobby = userEnglish.getListHobby();
        if (!TextUtils.isEmpty(listHobby)) {
            String[] hobbyArray = userEnglish.getListHobby().split("\\|");
            int length = hobbyArray.length;
            StringBuilder hobby = new StringBuilder();
            for (int i = 0; i < length; i++) {
                if(ParamsUtils.getInterestMap()!=null){
                    String itemStr = ParamsUtils.getInterestMap().get(hobbyArray[i]);
                    if (!TextUtils.isEmpty(itemStr)) {
                        hobby.append(itemStr);
                        if (i < length - 1) {
                            hobby.append(",");
                        }
                    }
                }

            }
            if (!TextUtils.isEmpty(listHobby)) {
                tv_xingquaihao.setText(hobby.toString());
                //设置标签
                String s = hobby.toString();
                String[] split = s.split(",");
                if (split.length > 0) {
                    if (split.length == 1) {
                        tv_tag1.setText(split[0]);
                        tv_tag2.setVisibility(View.GONE);
                        tv_tag3.setVisibility(View.GONE);
                        tv_tag4.setVisibility(View.GONE);
                    } else if (split.length == 2) {
                        tv_tag1.setText(split[0]);
                        tv_tag2.setText(split[1]);
                        tv_tag3.setVisibility(View.GONE);
                        tv_tag4.setVisibility(View.GONE);
                    } else if (split.length == 3) {
                        tv_tag1.setText(split[0]);
                        tv_tag2.setText(split[1]);
                        tv_tag3.setText(split[2]);
                        tv_tag4.setVisibility(View.GONE);
                    } else {
                        tv_tag1.setText(split[0]);
                        tv_tag2.setText(split[1]);
                        tv_tag3.setText(split[2]);
                        tv_tag4.setText(split[3]);
                    }
                }
            }
        } else {
            xingqu_linear.setVisibility(View.GONE);
            xian.setVisibility(View.GONE);
            tv_tag1.setVisibility(View.GONE);
            tv_tag2.setVisibility(View.GONE);
            tv_tag3.setVisibility(View.GONE);
            rl_biaoqian.setVisibility(View.GONE);
            cd_biaoqian.setVisibility(View.GONE);
        }
/**改**/
        //运动
//        if(!TextUtils.isEmpty(userEnglish.getSports())) {
//            String sports = userEnglish.getSports();
//            if (null != sports) {
//                String[] sportsList = sports.split("\\|");
//                int length = sportsList.length;
//                if(length>=4){
//                    tv_tag1.setVisibility(View.GONE);
//                    tv_tag2.setVisibility(View.GONE);
//                    tv_tag3.setVisibility(View.GONE);
//                }
//                StringBuilder sport = new StringBuilder();
//                for (int i = 0; i < length-1; i++) {
//                    String s = ParamsUtils.getSportMap().get(sportsList[i]);
//                    if (!TextUtils.isEmpty(s)) {
//                        sport.append(s);
//                            if (i < length - 1) {
//                                sport.append(",");
//                            }
//                    }
//                }
//                if(!TextUtils.isEmpty(sport.toString())){
//                    tv_yundong.setText(sport.toString());
//                    tv_tag4.setText(sport.toString());
//                }else{
////                    tv_yundong.setVisibility(View.GONE);
//                    tv_tag4.setVisibility(View.GONE);
//                }
//            }
//        }else{
//            tv_yundong.setVisibility(View.GONE);
//            tv_tag4.setVisibility(View.GONE);
//        }
    }

    private void setBaseData(UserBase userBaseEnglish) {
        if (ParamsUtils.getEducationMap()!=null && ParamsUtils.getEducationMap().get(String.valueOf(userBaseEnglish.getEducation())) != null) {
            tv_xueli.setText(ParamsUtils.getEducationMap().get(String.valueOf(userBaseEnglish.getEducation())));
        } else {
            tv_xueli.setText(getString(R.string.profile_shouru_baomu));
        }
        if (ParamsUtils.getWorkMap()!=null && ParamsUtils.getWorkMap().get(String.valueOf(userBaseEnglish.getOccupation())) != null) {
            tv_zhiye.setText(ParamsUtils.getWorkMap().get(String.valueOf(userBaseEnglish.getOccupation())));
        } else {
            tv_zhiye.setText(getString(R.string.profile_shouru_baomu));
        }
        if (String.valueOf(userBaseEnglish.getIncome()) != null && ParamsUtils.getIncomeMap()!=null) {
            if (ParamsUtils.getIncomeMap().get(String.valueOf(userBaseEnglish.getIncome())) != null) {
                tv_shouru.setText(ParamsUtils.getIncomeMap().get(String.valueOf(userBaseEnglish.getIncome())));
            } else {
                tv_shouru.setText(getString(R.string.profile_shouru_baomu));
            }
        } else {
            tv_shouru.setText(getString(R.string.profile_shouru_baomu));
        }
        if (ParamsUtils.getMarriageMap()!=null && ParamsUtils.getMarriageMap().get(String.valueOf(userBaseEnglish.getMaritalStatus())) != null) {
            tv_hunyin.setText(ParamsUtils.getMarriageMap().get(String.valueOf(userBaseEnglish.getMaritalStatus())));
        } else {
            tv_hunyin.setText(getString(R.string.profile_shouru_baomu));
        }
        if (!TextUtils.isEmpty(userBaseEnglish.getArea().getProvinceName())) {
            userinfo_tv_changzhudi.setText(userBaseEnglish.getArea().getProvinceName());
        } else {
            userinfo_tv_changzhudi.setText(getString(R.string.profile_shouru_baomu));
        }
        if (userBaseEnglish.getSign() == 0) {
            tv_xingzuo.setText("Leo");
        } else {
            if(ParamsUtils.getConstellationMap()!=null){
                String s = ParamsUtils.getConstellationMap().get(String.valueOf(userBaseEnglish.getSign()));
                tv_xingzuo.setText(s);
            }

        }
        //个人介绍
        tv_jieshao.setText(userBaseEnglish.getMonologue());
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_setting, menu);
        return true;
    }

    @Override
    public void initData() {
//public void main void main String void pubp ni dehsuo de shi shenem publivodi
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                actionSheetDialogNoTitle();
                break;
            //重写ToolBar返回按钮的行为，防止重新打开父Activity重走生命周期方法
            case android.R.id.home:
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    //照片墙
    private final PhotoWallAdapter.OnItemClickListener mOnItemClickListener = new PhotoWallAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(View view, int position) {
            PictureLookActivity.toPictureLookActivity(mContext, position, listImagedate);
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.userinfor_iv_Image:
                PictureLookActivity.toPictureLookActivity(mContext, 0, listImagedate);
                break;
            case R.id.userinfo_rl_say_hello:
                rl_sayHello.setBackgroundResource(R.mipmap.say_hello_height);
                tv_sayhello.setText(getString(R.string.say_helloed));
                sayHello(uid, rl_sayHello);
                ToastUtil.showShortToast(mContext, mContext.getString(R.string.say_hello_success));
                break;
            case R.id.userinfo_rl_write:
                if (user != null) {
                    Intent messgeIntent = new Intent(mContext, ChatMegActivity.class);
                    messgeIntent.putExtra(ConfigConstant.USERID, String.valueOf(user.getId()));
                    messgeIntent.putExtra(ConfigConstant.NICKNAME, user.getNickName());
                    Image image = user.getImage();
                    if (image != null) {
                        if (!TextUtils.isEmpty(image.getThumbnailUrl())) {
                            messgeIntent.putExtra(ConfigConstant.USERAVATAR, user.getImage().getThumbnailUrl());
                        }
                    }
                    startActivity(messgeIntent);
                }
                break;
            case R.id.userDetail_fab_next:
                loadNextUser();
                break;
        }
    }

    //自定义弹出对话框
    private void actionSheetDialogNoTitle() {
        final ActionSheetDialog dialog;
        isPraised = userEnglish.getIsPraised();
        String praised = isPraised.equals("0") ? getString(R.string.like) : getString(R.string.liked);
        if (isLahei == 1) {
            final String[] stringItems2 = {praised, getString(R.string.profile_quxiaolahei), getString(R.string.profile_jubo)};
            dialog = new ActionSheetDialog(UserInfoActivity.this, stringItems2, null);
        } else {
            final String[] stringItems1 = {praised, getString(R.string.profile_lahei), getString(R.string.profile_jubo)};
            dialog = new ActionSheetDialog(UserInfoActivity.this, stringItems1, null);
        }
        dialog.isTitleShow(false).show();
        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        setPraised();
                        break;
                    case 1:
                        if (isLahei == 0) {
                            userLaHei(uid, IUrlConstant.URL_PULL_TO_BLACK, 0);
                            isLahei = 1;
                        } else {
                            userLaHei(uid, IUrlConstant.URL_CANCEL_BLACK, 1);
                            isLahei = 0;
                        }
                        break;
                    case 2:
                        ReportDialog.newInstance(getString(R.string.report_title) + nickName, uid).show(getSupportFragmentManager(), "report");
                        break;
                }
                dialog.dismiss();
            }
        });
    }

    //喜歡/已喜歡
    private void setPraised() {
        if (userEnglish.getIsPraised().equals("0")) {
            MathDataUtils.sendPraise(userEnglish.getUserBaseEnglish().getId(), new MathDataUtils.onRequestDataListener() {
                @Override
                public void onSuccess() {
                    userEnglish.setIsPraised("1");
                    ToastUtil.showShortToast(mContext, getString(R.string.like_success));
                    RxBus.getInstance().post("like_unlike", 1);
                }

                @Override
                public void onFailed() {
                    ToastUtil.showShortToast(mContext, getString(R.string.like_fail));
                }
            });
        } else {
            MathDataUtils.canclePraise(userEnglish.getUserBaseEnglish().getId(), new MathDataUtils.onRequestDataListener() {
                @Override
                public void onSuccess() {
                    userEnglish.setIsPraised("0");
                    ToastUtil.showShortToast(mContext, getString(R.string.liked_success));
                    RxBus.getInstance().post("like_unlike", -1);
                }

                @Override
                public void onFailed() {
                    ToastUtil.showShortToast(mContext, getString(R.string.liked_fail));
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        myPrestener.isCanSee(true);
        myPrestener.loadData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxBus.getInstance().post(ConfigConstant.ISUSERINFO, false);
    }

    public void getUserInfo(String use_uid, String isRecord, String sourceTag) {
        OkGo.post(IUrlConstant.URL_GET_USER_INFO)
                .headers("token", PlatformInfoXml.getToken())
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("uid", TextUtils.isEmpty(use_uid) ? "" : use_uid)
                .params("isRecord", TextUtils.isEmpty(isRecord) ? "0" : isRecord)
                .params("sourceTag", TextUtils.isEmpty(sourceTag) ? "1" : sourceTag)
                .execute(new JsonCallback<UserDetail>() {
                    @Override
                    public void onSuccess(UserDetail userDetail, Call call, Response response) {
                        if (userDetail != null) {
                            userEnglish = userDetail.getUserEnglish();
                            getData(userEnglish,false);
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                    }
                });
    }

    public void sayHello(final String uId, final RelativeLayout rl) {
        OkGo.post(IUrlConstant.URL_SAY_HELLO)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("uid", uId)
                .params("sayHelloType", "3")
                .execute(new JsonCallback<SayHello>() {

                    @Override
                    public void onSuccess(SayHello sayHello, Call call, Response response) {
                        if (null != sayHello && sayHello.getIsSucceed().equals("1")) {
//                            ToastUtil.showShortToast(mContext, mContext.getString(R.string.say_hello_success));
                            rl.setBackgroundResource(R.mipmap.say_hello_height);
                            rl.setEnabled(false);
                            RxBus.getInstance().post(ConfigConstant.SAYHI, uId);
                        }
                    }
                });
    }
    /**加载下一位用户**/
    private void loadNextUser() {
        //加载进度框
        LoadingDialog.showDialogForLoading(mContext);
        pageNum++;
        OkGo.post(IUrlConstant.URL_NEXT_USER)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("pageNum",pageNum)
                .params("pageSize","1")
                .execute(new JsonCallback<NextUsers>() {
                    @Override
                    public void onSuccess(NextUsers nextUsers, Call call, Response response) {
//                        getView().hideLoadingPress();
                        if(nextUsers!=null){
                            List<User> listUser = nextUsers.getListUser();
                             userEnglish = listUser.get(0);
                            isNextButton=true;
                            getData(userEnglish,isNextButton);
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                    }
                });
    }
    //拉黑
    public void userLaHei(String uid, String urlPullToBlack, final int pos) {
        OkGo.post(urlPullToBlack)
                .headers("token", PlatformInfoXml.getToken())
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("uid", uid)
                .execute(new JsonCallback<BaseModel>() {
                    @Override
                    public void onSuccess(BaseModel baseModel, Call call, Response response) {
                        if (baseModel != null) {
                            String isSucceed = baseModel.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                if (pos == 0) {
                                    Toast.makeText(UserInfoActivity.this, getString(R.string.profile_laheisuccess), Toast.LENGTH_SHORT).show();
                                } else if (pos == 1) {
                                    Toast.makeText(UserInfoActivity.this, getString(R.string.profile_laheicancle), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                    }
                });
    }

    public void getUerInfo() {
        OkGo.post(IUrlConstant.URL_USER_INFO)
                .headers("token", PlatformInfoXml.getToken())
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String myInfo, Call call, Response response) {
                        if (!TextUtils.isEmpty(myInfo)) {
                            MyInfo myInfo1 = JSON.parseObject(myInfo, MyInfo.class);
                            if (myInfo1 != null) {
                                isCanWrite(myInfo1);
                            }
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                    }
                });
    }

    @Override
    public Context getContext() {
        return UserInfoActivity.this;
    }

    @Override
    public void setYeMeiData(String thumbnailUrl, int userId, String nickName, int age, int sex, String height, String msg, int type, int noticeType) {
        YeMeiShow.initNotification(UserInfoActivity.this, getSupportFragmentManager(), thumbnailUrl, userId, nickName, age, sex, height, msg, type, noticeType);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        myPrestener.isCanSee(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        myPrestener.isCanSee(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
