package com.online.onenight2.hot.presenter;

import com.online.onenight2.base.BasePresenter;
import com.online.onenight2.hot.activity.UserInfoActivity;

/**
 * Created by wangyong on 2017/4/19.
 */

public class UserInfoPresenter extends BasePresenter<UserInfoActivity> {
//    public void getUserInfo(String use_uid,String isRecord,String  sourceTag){
//                OkGo.post(IUrlConstant.URL_GET_USER_INFO)
//                .headers("token", PlatformInfoXml.getToken())
//                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
//                .params("uid", TextUtils.isEmpty(use_uid) ? "" : use_uid)
//                .params("isRecord", TextUtils.isEmpty(isRecord) ? "0" : isRecord)
//                .params("sourceTag", TextUtils.isEmpty(sourceTag) ? "1" : sourceTag)
//                .execute(new JsonCallback<UserDetail>() {
//                    @Override
//                    public void onSuccess(UserDetail userDetail, Call call, Response response) {
//                             if(userDetail!=null){
//                                 getView().getData(userDetail);
//                             }
//                    }
//
//                    @Override
//                    public void onError(Call call, Response response, Exception e) {
//                        super.onError(call, response, e);
//                    }
//                });
//    }
//    public void sayHello(final String uId, final RelativeLayout rl) {
//        OkGo.post(IUrlConstant.URL_SAY_HELLO)
//                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
//                .params("uid",uId)
//                .params("sayHelloType","3")
//                .execute(new JsonCallback<SayHello>() {
//
//                    @Override
//                    public void onSuccess(SayHello sayHello, Call call, Response response) {
//                        if(null!=sayHello&& sayHello.getIsSucceed().equals("1")){
//                            ToastUtil.showShortToast(mContext,mContext.getString(R.string.say_hello_success));
//                             rl.setBackgroundResource(R.mipmap.say_hello_height);
//                             rl.setEnabled(false);
//                             RxBus.getInstance().post(ConfigConstant.SAYHI, uId);
//                        }
//                    }
//                });
//    }
//    //拉黑
//    public void userLaHei(String uid, String urlPullToBlack,final int pos) {
//        OkGo.post(urlPullToBlack)
//                .headers("token", PlatformInfoXml.getToken())
//                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
//                .params("uid", uid)
//                .execute(new JsonCallback<BaseModel>() {
//                    @Override
//                    public void onSuccess(BaseModel baseModel, Call call, Response response) {
//                        if(baseModel!=null){
//                            String isSucceed = baseModel.getIsSucceed();
//                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
//                                if(pos==0){
//                                    Toast.makeText(getView(), getView().getString(R.string.profile_laheisuccess), Toast.LENGTH_SHORT).show();
//                                }else  if(pos==1){
//                                    Toast.makeText(getView(),getView().getString(R.string.profile_laheicancle), Toast.LENGTH_SHORT).show();
//                                }
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onError(Call call, Response response, Exception e) {
//                        super.onError(call, response, e);
//                    }
//                });
//    }
//    public  void getUerInfo() {
//        OkGo.post(IUrlConstant.URL_USER_INFO)
//                .headers("token", PlatformInfoXml.getToken())
//                .params("platformInfo",PlatformInfoXml.getPlatformJsonString())
//                .execute(new StringCallback() {
//                    @Override
//                    public void onSuccess(String myInfo, Call call, Response response) {
//                        if (!TextUtils.isEmpty(myInfo)) {
//                            MyInfo myInfo1 = JSON.parseObject(myInfo, MyInfo.class);
//                            if(myInfo1!=null){
//                                 getView().isCanWrite(myInfo1);
//                            }
//                        }
//                    }
//                    @Override
//                    public void onError(Call call, Response response, Exception e) {
//                        super.onError(call, response, e);
//                    }
//                });
//    }
}
