package com.online.onenight2.hot.presenter;

import com.online.onenight2.MainActivity;
import com.online.onenight2.base.BasePresenter;

/**
 * 描述：
 * Created by qyh on 2017/4/20.
 */

public class MainPresenter extends BasePresenter<MainActivity> {
//    //用户是否打开用户空间页面的标记，打开用户空间页面不推送 谁看过我的消息
//    private boolean isUserinfo = false;
//    //用户是否打开聊天页面的标记，打开聊天页面不推送 来信消息
//    private boolean isChat = false;
//    private RxManager rxManager;
//
//    /**
//     * 获取消息列表
//     * @param pageNum
//     * @param pageSize
//     * @param type
//     */
//    public void getMsgList(int pageNum, String pageSize, int type){
//        OkGo.post(IUrlConstant.URL_GET_ALL_CHAT_LIST)
//                .tag(getView())
//                .params("pageNum",String.valueOf(pageNum))
//                .params("pageSize",pageSize)
//                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
//                .execute(new JsonCallback<ChatListBean>() {
//
//                    @Override
//                    public void onSuccess(ChatListBean chatListBean, Call call, Response response) {
//                        if(null!=chatListBean){
//                            getView().hideLoadingPress();
//                            if(chatListBean.getIsSucceed()!=null&chatListBean.getIsSucceed().equals("1")){
//                                if(chatListBean.getListChat().size()>0){
//                                    getView().ShowMsgListData(chatListBean);
//                                }
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onError(Call call, Response response, Exception e) {
//                        super.onError(call, response, e);
//                        getView().hideLoadingPress();
//                    }
//                });
//        }
//
//    /**
//     * 获取好友推荐
//     */
//    public void getRecommendUser(){
//
//        OkGo.post(IUrlConstant.URL_GET_RECOMMENDUSER)
//                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
//                .params("type", String.valueOf(0))
//                .params("cycle", String.valueOf(20000))
//                .execute(new JsonCallback<RecommendUser>() {
//
//                    @Override
//                    public void onSuccess(RecommendUser recommendUser, Call call, Response response) {
//                        if(null!=recommendUser){
//                            if(!TextUtils.isEmpty(recommendUser.getIsSucceed())&&recommendUser.getIsSucceed().equals("1")){
//                                getView().showRecommendUser(recommendUser);
//                            }
//                        }
//                    }
//                });
//            }
//
//    /**
//     * 去看看，付费用户--->聊天界面；未付费用户--->对方空间页
//     * @param userBaseEnglish
//     * @param mContext
//     */
//    public void goLook(UserBase userBaseEnglish, Context mContext){
//        //付费用户跳转到聊天界面
//        Bundle bundle = new Bundle();
//        if (UserInfoXml.isBeanUser()|| UserInfoXml.isVip() || UserInfoXml.isMonthly()||UserInfoXml.getGender().equals("1")) {
//            bundle.putString(ConfigConstant.USERID, userBaseEnglish.getId());
//            bundle.putString(ConfigConstant.NICKNAME, userBaseEnglish.getNickName());
//            Intent intent = new Intent(mContext, ChatMegActivity.class);
//            intent.putExtras(bundle);
//            if (null != userBaseEnglish.getImage()) {
//                bundle.putString(ConfigConstant.USERAVATAR, userBaseEnglish.getImage().getThumbnailUrl());
//            }
//            mContext.startActivity(intent);
//        } else {
//            bundle.putString(ConfigConstant.USERID, userBaseEnglish.getId());
//            bundle.putString(ConfigConstant.NICKNAME, userBaseEnglish.getNickName());
//            bundle.putString(ConfigConstant.ISRECORD, "0");
//            bundle.putString(ConfigConstant.SOURCETAG, "1");
//            Intent intent = new Intent(mContext, UserInfoActivity.class);
//            intent.putExtras(bundle);
//            mContext.startActivity(intent);
//        }
//    }
//    private String lastMsgTime= "";
//    /**
//     * 获取页眉消息
//     */
//    public void getNotificationMessages(){
//        rxManager = new RxManager();
//        rxManager.on(ConfigConstant.ISUSERINFO, new Action1<Boolean>() {
//
//            @Override
//            public void call(Boolean aBoolean) {
//                isUserinfo = aBoolean;
//            }
//        });
//        rxManager.on(ConfigConstant.ISCHAT, new Action1<Boolean>() {
//
//            @Override
//            public void call(Boolean aBoolean) {
//                isChat = aBoolean;
//            }
//        });
//      //  System.out.println("PlatformInfoXml=="+PlatformInfoXml.getPlatformJsonString());
//        OkGo.post(IUrlConstant.URL_GET_HEADNOTICENEW)
//                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
//                .params("lastMsgTime", lastMsgTime)
//                .execute(new JsonCallback<NotificationNew>() {
//                    @Override
//                    public void onSuccess(NotificationNew notificationNew, Call call, Response response) {
//                        if(null!=notificationNew){
//                            if(!TextUtils.isEmpty(notificationNew.getIsSucceed())&&notificationNew.getIsSucceed().equals("1")){
//                                if (null != notificationNew.getUnreadMsgBoxList()) {
//                                //未讀信息
//                                //System.out.println("NoticeType======"+notificationNew.getNoticeType());
//                            if (notificationNew.getNoticeType() == 1 && !isChat) {
//                                if (null != notificationNew.getUnreadMsgBoxList()) {
//                                    int size = notificationNew.getUnreadMsgBoxList().size();
//                                   // System.out.println("size========" + size);
//                                    //	for (int i = 0; i < size; i++) {
//                                    notificationNew.getUnreadMsgBoxList().size();
//                                    NotificationNew.UnreadMsgBoxList unreadMsgBoxList = notificationNew.getUnreadMsgBoxList().get(size - 1);
//                                    String msg = unreadMsgBoxList.getMsg();
//                                    int type = unreadMsgBoxList.getType();
//                                    int noticeType = notificationNew.getNoticeType();
//                                    lastMsgTime = notificationNew.getLastMsgTime();
//                                    NotificationNew.UnreadMsgBoxList.UserBaseEnglish userBaseEnglish =unreadMsgBoxList.getUserBaseEnglish();
//                                    if(userBaseEnglish!=null){
//
//                                        String  height = userBaseEnglish.getHeightCm();
//                                        String nickName = userBaseEnglish.getNickName();
//                                        int  age = userBaseEnglish.getAge();
//                                        int  userId = userBaseEnglish.getId();
//                                        int  sex = userBaseEnglish.getSex();
//                                        NotificationNew.UnreadMsgBoxList.UserBaseEnglish.Image image = userBaseEnglish.getImage();
//                                        if(image!=null){
//                                            String thumbnailUrl = userBaseEnglish.getImage().getThumbnailUrl();
//                                            getView().initNotification(thumbnailUrl, userId, nickName, age, sex, height, msg, type, noticeType);
//                                        }
//                                    }
//                                }
//                            } else if (notificationNew.getNoticeType() == 2 && !isUserinfo) {
//                                NotificationNew.RemoteYuanfenUserBase.UserBaseEnglish userBaseEnglish = notificationNew.getRemoteYuanfenUserBase().getUserBaseEnglish();
//                                String  nickName = userBaseEnglish.getNickName();
//                                int  userId = userBaseEnglish.getId();
//                                int age = userBaseEnglish.getAge();
//                                String  thumbnailUrl = userBaseEnglish.image.thumbnailUrl;
//                              getView().initNotification(thumbnailUrl, userId, nickName, age, 0, "", "", 0, 2);
//                            }
//                        }
//                    }
//                }
//            }
//            @Override
//            public void onError(Call call, Response response, Exception e) {
//                super.onError(call, response, e);
//                e.printStackTrace();
//            }
//        });
//    }
}
