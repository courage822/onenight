package com.online.onenight2.hot.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.library.utils.HeightUtils;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseApplication;
import com.online.onenight2.bean.FateUser;
import com.online.onenight2.bean.Image;
import com.online.onenight2.bean.UserBase;
import com.online.onenight2.utils.ImageLoaderUtils;

import java.util.List;

/**
 * Created by Administrator on 2017/6/23.
 */

public class PopuWindowsGvAdapter extends BaseAdapter{
    private List<FateUser> list;
    private Context mContext;
    private boolean isSelect=true;
    public PopuWindowsGvAdapter(List<FateUser> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if(convertView==null){
            viewHolder=new ViewHolder();
            convertView= LayoutInflater.from(mContext).inflate(R.layout.popu_gv_item,parent,false);
            viewHolder.iv_avatar= (ImageView) convertView.findViewById(R.id.popu_layout_gv_item_avatar);
            viewHolder.tv_age= (TextView) convertView.findViewById(R.id.popu_layout_gv_item_tv_age);
            viewHolder.rl_root= (RelativeLayout) convertView.findViewById(R.id.popu_layout_gv_item_rl_root);
            viewHolder.iv_select= (CheckBox) convertView.findViewById(R.id.popu_layout_gv_item_iv_select);
            convertView.setTag(viewHolder);
        } else{
            viewHolder= (ViewHolder) convertView.getTag();
        }
        FateUser fateUser = list.get(position);
        if(fateUser!=null){
            UserBase userBaseEnglish = fateUser.getUserBaseEnglish();
            if(userBaseEnglish!=null){
                Image image = userBaseEnglish.getImage();
                if(image!=null){
                    ImageLoaderUtils.display(BaseApplication.getAppContext(),viewHolder.iv_avatar,image.thumbnailUrl);
                }
//                viewHolder.tv_age.setText(userBaseEnglish.getAge()+"yrs "+userBaseEnglish.getHeightCm()+"cm");
                viewHolder.tv_age.setText(userBaseEnglish.getAge()+"  "+ HeightUtils.getInchCmByCm(userBaseEnglish.getHeightCm()));

            }
        }

        return convertView;
    }
    private class ViewHolder{
        private ImageView iv_avatar;
        private TextView tv_age;
        private RelativeLayout rl_root;
        private CheckBox iv_select;
    }
}
