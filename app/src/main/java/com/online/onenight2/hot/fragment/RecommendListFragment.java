package com.online.onenight2.hot.fragment;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.online.onenight2.R;
import com.online.onenight2.base.BaseFragment;
import com.online.onenight2.base.RxManager;
import com.online.onenight2.base.adapter.BaseQuickAdapter;
import com.online.onenight2.bean.MatcherInfo;
import com.online.onenight2.bean.NearBy;
import com.online.onenight2.bean.NearByUser;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.hot.adapter.RecommendListAdapter;
import com.online.onenight2.hot.presenter.RecomListPresenter;
import com.online.onenight2.utils.SharedPreferencesUtils;
import com.online.onenight2.view.refresh.NormalRefreshViewHolder;
import com.online.onenight2.view.refresh.RefreshLayout;

import java.util.List;

import butterknife.BindView;
import rx.functions.Action1;

/**
 * 描述：推荐界面
 * Created by qyh on 2017/4/11.
 */

public class RecommendListFragment extends BaseFragment<RecomListPresenter> implements RefreshLayout.RefreshLayoutDelegate, BaseQuickAdapter.RequestLoadMoreListener{

    @BindView(R.id.rc_hot_content)
    RecyclerView rcHotContent;
    @BindView(R.id.refresh)
    RefreshLayout refresh;
    private RecommendListAdapter mRecommendAdapter;
    private int pageNum = 1;
    private static final String PAGESIZE = "30";
    private List<NearByUser> listUser;
    // 正常请求
    public static final int NORMAL = 1;
    // 刷新请求
    public static final int REFRESH = 2;
    // 加载更多请求
    public static final int LOADMORE = 3;
    private View notLoadingView;
    //是否已经展示了无数据布局
    private boolean isShowNoDataView = false;
    //距离",以千米为单位
    private String distance = "10";
   private static RecommendListFragment recommendListFragment;
    private boolean isCanLoad=true;
    public static  RecommendListFragment newInstance(){
        if(recommendListFragment==null) {
            recommendListFragment = new RecommendListFragment();
        }
        return recommendListFragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragmentlist_recom_near;
    }

    @Override
    public RecomListPresenter newPresenter() {
        return new RecomListPresenter();
    }

    @Override
    protected void initView() {
        mRecommendAdapter = new RecommendListAdapter(R.layout.item_recomlist_image, null);
        rcHotContent.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        rcHotContent.setHasFixedSize(true);
        //设置适配器加载动画
        mRecommendAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        rcHotContent.setAdapter(mRecommendAdapter);
        //设置适配器可以上拉加载
        mRecommendAdapter.setOnLoadMoreListener(this);
        //设置下拉、上拉
        refresh.setDelegate(this);
        refresh.setRefreshViewHolder(new NormalRefreshViewHolder(mContext, true));
        RxManager manager=new RxManager();
        manager.on("info", new Action1<MatcherInfo>() {
            @Override
            public void call(MatcherInfo matcherInfo) {
                if(matcherInfo!=null){
                    String distanceKm = SharedPreferencesUtils.getData(mContext, ConfigConstant.DISTANCE, 10 + "");
                  if(distanceKm.contains("KM")){
//                      distanceKm=distanceKm.r(distanceKm.length()-2,2);
                      distanceKm=distanceKm.substring(0,distanceKm.length()-2);
                  }else if(distanceKm.contains("miles")){
//                      distanceKm=distanceKm.r(distanceKm.length()-2,2);
                      distanceKm=distanceKm.substring(0,distanceKm.length()-5);
                  }
                    distance=distanceKm;
                    getPresenter().getHotData(String.valueOf(pageNum), PAGESIZE,NORMAL,distance);

                }
            }
        });
    }

    @Override
    protected void initData() {
        //更换账号后，重新获取数据
        mRxManager.on("change_user", new Action1<Boolean>() {
            @Override
            public void call(Boolean aBoolean) {
                if(aBoolean){
                    getPresenter().getHotData(String.valueOf(pageNum), PAGESIZE, NORMAL,distance);
                }
            }
        });
        getPresenter().getHotData(String.valueOf(pageNum), PAGESIZE, NORMAL,distance);
        showLoadingPress();
    }
    //获取数据成功后回调
    public void showData(NearBy fate) {
        listUser = fate.getListUser();
        mRecommendAdapter.setNewData(listUser);
        mRecommendAdapter.notifyDataSetChanged();
    }

    public void refresh(NearBy fate) {
        //移除底部布局
        if (null != notLoadingView) {
            mRecommendAdapter.removeFooterView(notLoadingView);
        }
        listUser = fate.getListUser();
        mRecommendAdapter.setNewData(listUser);
        refresh.endRefreshing();
    }

    public void loadMore(NearBy fate) {
        listUser=  fate.getListUser();
        mRecommendAdapter.addData(listUser);
        refresh.endLoadingMore();
    }

    public void showNoData() {
        refresh.endLoadingMore();
        //加载没有更多提示布局
        mRecommendAdapter.loadComplete();
        if (notLoadingView == null) {
            notLoadingView = mContext.getLayoutInflater().inflate(R.layout.not_loading,
                    (ViewGroup) rcHotContent.getParent(), false);
        }

        if (!isShowNoDataView) {
            //加载脚布局
            mRecommendAdapter.addFooterView(notLoadingView);
            isShowNoDataView = true;
        }
    }

    @Override
    public void onRefreshLayoutBeginRefreshing(RefreshLayout refreshLayout) {
        pageNum=1;
        getPresenter().getHotData(String.valueOf(pageNum), PAGESIZE, REFRESH,distance);
    }

    @Override
    public boolean onRefreshLayoutBeginLoadingMore(RefreshLayout refreshLayout) {
        pageNum++;
        getPresenter().getHotData(String.valueOf(pageNum), PAGESIZE, LOADMORE,distance);
        return true;
    }

    @Override
    public void onLoadMoreRequested() {
        //这个方法没有实际用的，只是为了展示加载更多的view
    }
    @Override
    public void onResume() {
        super.onResume();
        RxManager rxManager=new RxManager();
        rxManager.on("login_change", new Action1<String>() {
            @Override
            public void call(String s) {
                if (isCanLoad) {
                    getPresenter().getHotData(String.valueOf(pageNum), PAGESIZE, NORMAL,distance);
                  isCanLoad=false;
                }
            }
        });
    }
}
