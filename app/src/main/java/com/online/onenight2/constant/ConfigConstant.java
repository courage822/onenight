package com.online.onenight2.constant;

/**
 * 描述：全局参数，统一配置
 * Created by qyh on 2017/2/17.
 */

public class ConfigConstant {
//    /**
//     * Google pay app key
//     * 后期修改
//     */
//    //com.jz.jiaoyou
   // public static String GOOGLE_APP_KEY="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAj71PJybbOsuWo6HdFovigM8X6XmyPK1qxezwnQZ8haleUxW7lmdR+xtldS9wUBATzgKEZHOqVBakvDkuC+yc5CqqQU9sQbLSfiMQjlmZeAZT8135UAWc4GEdo7+e7FU40WeDFjza+6wyCbpOpZZrtEl9EYxGOiW05phTSDrkFCTUXipLtGYih6cEUwUBGGHgkEOjGyhD9kp1/MyVG15BhdyKJDkn00fywueSEMXbho7QHhA0nNZJp1dpez/VYjHDP2RuktIrdSp7/aDSV+5G1pU2HFkgPtysoa8D+YmxrLhcQVzGymfrSaPS7R/aIjjxe/yJKFwEQTwnkI9gA9+i6wIDAQAB";
//
//    // 美国  com.juz.flirt
//   public static String GOOGLE_APP_KEY="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnEpod5vrI7IfG7Qpf+KATMwwTnKPQarJAJvylmIo5ld7yhCBuUbJpZiXENTCLZYd5UaqSpUP+S8HXAP9bCoK+5/5j4njN0f+tgC49TexgT1JXmjj/KqdjJn5QUKu9+T59ciqGvJWw3KDUquYU4U9vNMf40a/yaB0ebpRdMJRCPDMdmz02nyxivNYSKF6bWw0TSw0PDSrRv6e+fiz0UhJJ7T94l/feuVtgHsbUudQD0O0wuKVhoXVh3zUkDQy2VfdimLr4xYGFovnJpAEo5dOGF7DS+49qwKgUcAoet/vYv0J6cUiAxxCFQ4yNBkKiv5GWS80Jh6zOrhD5+ygo/oYMQIDAQAB";

    // Google支付SKU
    public static String SKU_STANDARD1 = "1month";
    public static String SKU_STANDARD3 = "3months";
    public static String SKU_STANDARD12 = "1year";

    public static String SKU_VIP1 = "1vip";
    public static String SKU_VIP3 = "3vip";
    public static String SKU_VIP12 = "12vip";


    //
    public static String USERID="uid";//用户ID
    public static String ISRECORD="isRecord";//是否统计，1-统计使用，不需要返回user对象，0->正常获取用户信息,
    public static String PLATFORMINFO="platformInfo";//平台信息对象
    public static String SOURCETAG="sourceTag";
    public static String NICKNAME="nickname";//昵称
    public static String USERAVATAR="useravatar";//用户头像
    public static String pay_success="pay_success";//支付成功
    public static String mefg_into_fate="mefg_into_fate";//从我的页面到缘分页面跳转
    public static String MESEETAG="MeSeeTag";//访客
    public static String ISUSERINFO="isuserinfo";//用户空间页标记
    public static String ISCHAT="ischat";//聊天间页标记
    public static String TOTALUNREAD="totalUnread";//未读消息总数
    public static String FILTER_SURE="sure";//筛选条件确定
    public static String DISTANCE="distance";//是否筛选了距离
    public static String LISTSIZA="listSize";//喜欢列表数量
    public static String SAYHI="sayHi";//打招呼标记
 public static String NEARDISTANCE="near_distance";//是否筛选了附近距离
    public static String CLEANCOUNTRY="cleanCountry";//是否筛选了附近距离
    public static int INTOMEFRAGMENT=00000;//从聊天界面跳转到个人信息界面
    public static int Up=00000;//从聊天界面跳转到个人信息界面


}
