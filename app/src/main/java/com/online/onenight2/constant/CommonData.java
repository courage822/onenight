package com.online.onenight2.constant;

import com.library.utils.TimeUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>公用数据，用于滚轮选择器数据源，格式统一为List<String>.</p>
 * <p/>
 * <p>1、年龄选择列表</p>
 * <p>2、日期选择列表</p>
 * <p>3、省份/城市选择列表</p>
 * Created by zhangdroid on 2016/6/24.
 */
public class CommonData {

    /**
     * 获得年龄列表
     */
    public static List<String> getAgeList() {
        List<String> ageList = new ArrayList<String>();
        for (int i = 0; i < 45; i++) {
            ageList.add(String.valueOf(i + 18));
        }
        return ageList;
    }

    /**
     * 获得年份列表
     *
     * @return
     */
    public static List<String> getYearList() {
        List<String> yearList = new ArrayList<String>();
        for (int i = 1997; i >= 1950; i--) {
            yearList.add(String.valueOf(i));
        }
        return yearList;
    }

    /**
     * 获得月份列表
     *
     * @return
     */
    public static List<String> getMonthList() {
        List<String> monthList = new ArrayList<String>();
        for (int i = 1; i <= 12; i++) {
            monthList.add(TimeUtil.pad(i));
        }
        return monthList;
    }

    /**
     * 获得天数列表
     *
     * @param maxDays 该月的最大天数
     * @return
     */
    public static List<String> getDayList(int maxDays) {
        List<String> dayList = new ArrayList<String>();
        for (int i = 1; i <= maxDays; i++) {
            dayList.add(TimeUtil.pad(i));
        }
        return dayList;
    }

    /**
     * 获得省份列表
     *
     * @return
     */
//    public static List<String> getProvinceList() {
//        return ParamsUtils.getProvinceMapValue();
//    }

    /**
     * 获得国家列表
     *
     * @return
     */
    public static List<String> getCountryList() {
        List<String> list = new ArrayList<String>();
        list.add("United States");
        list.add("Australia");
        list.add("India");
        list.add("Indonesia");
        list.add("United Kingdom");
        list.add("Canada");
        list.add("New Zealand");
        list.add("Ireland");
        list.add("South Africa");
        list.add("Singapore");
        list.add("Pakistan");
        list.add("Philippines");
        list.add("HongKong");
        list.add("Taiwan");
//        list.add(String.valueOf(R.string.united_states));
//        list.add(String.valueOf(R.string.australia));
//        list.add(String.valueOf(R.string.india));
//        list.add(String.valueOf(R.string.indonesia));
//        list.add(String.valueOf(R.string.united_kingdom));
//        list.add(String.valueOf(R.string.canada));
//        list.add(String.valueOf(R.string.new_ealand));
//        list.add(String.valueOf(R.string.ireland));
//        list.add(String.valueOf(R.string.south_africa));
//        list.add(String.valueOf(R.string.singapore));
//        list.add(String.valueOf(R.string.pakistan));
//        list.add(String.valueOf(R.string.philippines));
//        list.add(String.valueOf(R.string.hong_kong));
//        list.add(String.valueOf(R.string.tai_wan));
        return list;
    }

    /**
     * 获得繁体字的国家列表
     *
     * @return
     */
    public static List<String> getCountryComplexList() {
        List<String> list = new ArrayList<String>();
        list.add("美國");
        list.add("澳大利亞");
        list.add("印度");
        list.add("印尼");
        list.add("英國");
        list.add("加拿大");
        list.add("新西蘭");
        list.add("愛爾蘭");
        list.add("南非");
        list.add("新加坡");
        list.add("巴基斯坦");
        list.add("菲律賓");
        list.add("香港");
        list.add("臺灣");
//        list.add(String.valueOf(R.string.united_states));
//        list.add(String.valueOf(R.string.australia));
//        list.add(String.valueOf(R.string.india));
//        list.add(String.valueOf(R.string.indonesia));
//        list.add(String.valueOf(R.string.united_kingdom));
//        list.add(String.valueOf(R.string.canada));
//        list.add(String.valueOf(R.string.new_ealand));
//        list.add(String.valueOf(R.string.ireland));
//        list.add(String.valueOf(R.string.south_africa));
//        list.add(String.valueOf(R.string.singapore));
//        list.add(String.valueOf(R.string.pakistan));
//        list.add(String.valueOf(R.string.philippines));
//        list.add(String.valueOf(R.string.hong_kong));
//        list.add(String.valueOf(R.string.tai_wan));
        return list;
    }

    /**
     * @return 国家语言缩写对照表
     */
    public static Map<String, String> getCountryLanguageMap() {
        Map<String, String> countryLanguageMap = new HashMap<String, String>();
        // 语言
        countryLanguageMap.put("EN", "English");
        countryLanguageMap.put("SP", "Spanish");
        countryLanguageMap.put("TR", "Traditional");
        // 添加13个英语国家
        countryLanguageMap.put("US", "America");
        countryLanguageMap.put("AU", "Australia");
        countryLanguageMap.put("IN", "India");
        countryLanguageMap.put("ID", "Indonesia");
        countryLanguageMap.put("GB", "UnitedKingdom");
        countryLanguageMap.put("CA", "Canada");
        countryLanguageMap.put("NZ", "NewZealand");
        countryLanguageMap.put("IE", "Ireland");
        countryLanguageMap.put("ZA", "SouthAfrica");
        countryLanguageMap.put("SG", "Singapore");
        countryLanguageMap.put("PK", "Pakistan");
        countryLanguageMap.put("PH", "Philippines");
        countryLanguageMap.put("HK", "HongKong");
        return countryLanguageMap;
    }

}
