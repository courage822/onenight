package com.online.onenight2.constant;

import android.os.Environment;


import com.online.onenight2.base.BaseApplication;

import java.io.File;

/**
 * Created by Administrator on 2017/2/11.
 */

public interface IConfigConstant {

    //11008
//      String BAIDU_PUSH_API_KEY = "9UiRirw20K8bstDFPLBeZiNn";
//    更改包名后的百度推送
    String BAIDU_PUSH_API_KEY = "5K5AoHCgoR1CfLvoLuX0tE87";


    /**
     * 渠道号
     * 美国
     */
    String F_IDAM = "11001";
    // 台湾
    String F_IDTW = "11008";

    /**
     * 渠道号 印度
     */
    String F_IDIN = "11012";
    /*
    * 澳大利亚
    * */
    String F_IDNSW="11011";
    /*
    * 印尼
    * */
    String F_IDJT="11013";
    /*
    * 英国
    * */
    String F_IDGL="11014";
    /*
    * 加拿大
    * */
    String F_IDTO="11015";
    /*
    * 新西兰
    * */
    String F_IDAL="11016";
    /*
    * 爱尔兰
    * */
    String F_IDDL="11017";
    /*
    * 南非
    * */
    String F_IDJG="11018";
    /*
    * 新加坡
    * */
    String F_IDOD="11020";
    /*
    * 巴基斯坦
    * */
    String F_IDKC="11024";
    /*
    * 菲律宾
    * */
    String F_IDMA="11025";
    /*
    * 香港
    * */
    String F_IDWC="11033";

    /**
     * 产品号
     */
    String PRODUCT_ID = "10";
    /**
     * 本地图片保存路径（Android/data/包名/files/Pictures）
     */
    String LOCAL_PIC_PATH = BaseApplication.getAppContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            + File.separator + "photo.jpg";
    /**
     * 本地头像保存路径（Android/data/包名/files/Pictures）
     */
    String LOCAL_AVATAR_PATH =  BaseApplication.getAppContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath()
            + File.separator + String.valueOf(System.currentTimeMillis() + ".jpg");//String.valueOf(System.currentTimeMillis() + ".jpg")
//     BaseApplication.getGlobalContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
//            + File.separator + "avatar.jpg";

    /**
     * 默认图片
     */
    //int DEFALUT_PIC= R.drawable.unload;

    String Mail="jzzxchat@outlook.com";
    String APPSFLYER_DEV_KEY = "7EhHiNjd6ef8TGUyZSGzF3";
    //谷歌支付使用的key 11008
//    String GOOGLE_APP_KEY="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhfGzERVVqRpAB/O3W4gs+WtlNQfpNHpFQ2Gr8635AhM0TPNf9uAzqq/WoncbF2C8IH1NgLPhM9vRltbg1sTmn5zV1jiT1kg2d7i/edZXdsi7Y277S4FPXT6fODWG2E4dsGpbPzBxRhLUYstsdV8BPmMeforJMcS7YT2NfQFR6Dd0sGFuEEoqfHraoioLKzICcWuBO/5FC/vD6zKeT6vTRezG9JcxzKOljUMNZZHTN+WhBdgC79/TYrUxykjmxJyCOt/u4Dz1DR5tcqI73ResiaBKfUTQ2wWX6+U6GFyZhpylQPaiGU+b96FchxIgOsKnRfqOabEuQ8lq8+N8lZsJkQIDAQAB";
//    更换包名后的key
String GOOGLE_APP_KEY="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArq+3pu/xZhuZLywRDT8amtjZl/4vbgi+pCGNvYiOnnX7Ovb/V3AVVFywRuWDA+8mxdTIuhZpAsyWRGitZEpQzmgfWafxmVKEEPGCdX3fRg9urY/gYLzdGHB80NbrKWPfFrke2LNf+YGEXIb7Kf3VjDINexOGqiVipKafM/+x+22eFzgyaNH8Dg56iq4YQa6DC/v7m+XX+5ibiPbWQKQVj8XDzvCmUpgvrucKJHNpFYn2DOo0rbHeRy8rhRgD2ucMrqdT1UDPKGdUeCrA7OcMv7Z3Z6vtSrtzWh3jCe8lpiIfSPrMGkUbOue6iJdGb6Sc2UVszuSOd3mYfzZIqfAfmwIDAQAB";

    String APPFLYER_KEY="7EhHiNjd6ef8TGUyZSGzF3";
}
