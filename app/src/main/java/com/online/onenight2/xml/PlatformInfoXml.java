package com.online.onenight2.xml;

import android.content.Context;


import com.google.gson.Gson;
import com.library.utils.SharedPreferenceUtil;
import com.online.onenight2.base.BaseApplication;
import com.online.onenight2.bean.PlatformInfo;
import com.online.onenight2.utils.CountryFidUtils;


import java.util.HashMap;
import java.util.Map;

/**
 * Platfrom信息
 * Created by zhangdroid on 2016/6/17.
 */
public class PlatformInfoXml {

    public static final String XML_NAME = "platform_info";
    public static final String KEY_PID = "pid";//手机串号
    public static final String KEY_IMSI = "imsi";//sim卡imsi号
    private static final String KEY_WIDTH = "width";//分辨率宽
    private static final String KEY_HEIGHT = "height";//分辨率高
    private static final String KEY_VERSION = "version";//版本号
    private static final String KEY_PHONE_TYPE = "phonetype";//手机型号
    private static final String KEY_FID = "fid";//渠道号
    private static final String KEY_PRODUCT_ID = "productId"; //产品号
    private static final String KEY_NET_TYPE = "netType";//联网类型(0->无网络,2->wifi,3->cmwap,4->cmnet,5->ctnet,6->ctwap,7->3gwap,8->3gnet,9->uniwap,10->uninet)
    private static final String KEY_MOBILE_IP = "mobileIP";//手机ip
    private static final String KEY_RELEASE_TIME = "releaseTime";//发版时间，例如：201501170945
    private static final String KEY_PLATFORM = "platform";//平台(1->IOS,2->Android)
    private static final String KEY_SYSTEM_VERSION = "systemVersion";//系统版本
    public static final String KEY_LANGUAGE = "language";// 语言
    public static final String KEY_COUNTRY = "country";//国家

    private static final String KEY_TOKEN = "token";// token值
    private static final String KEY_PUSH_TOKEN = "push_token";// 百度推送token值
    private static final String KEY_AD_UID = "ad_uid";// pid

    private static Context mContext = BaseApplication.getAppContext();
    private static Gson gson;

    /**
     * 保存platform信息
     *
     * @param platformInfo
     */
    public static void setPlatfromInfo(PlatformInfo platformInfo) {
        SharedPreferenceUtil.setStringValue(mContext, XML_NAME, KEY_IMSI, platformInfo.getImsi());
        SharedPreferenceUtil.setStringValue(mContext, XML_NAME, KEY_WIDTH, platformInfo.getW());
        SharedPreferenceUtil.setStringValue(mContext, XML_NAME, KEY_HEIGHT, platformInfo.getH());
        SharedPreferenceUtil.setStringValue(mContext, XML_NAME, KEY_VERSION, platformInfo.getVersion());
        SharedPreferenceUtil.setStringValue(mContext, XML_NAME, KEY_PHONE_TYPE, platformInfo.getPhonetype());
        SharedPreferenceUtil.setStringValue(mContext, XML_NAME, KEY_FID, platformInfo.getFid());
        SharedPreferenceUtil.setStringValue(mContext, XML_NAME, KEY_PRODUCT_ID, platformInfo.getProduct());
        SharedPreferenceUtil.setStringValue(mContext, XML_NAME, KEY_NET_TYPE, platformInfo.getNetType());
        SharedPreferenceUtil.setStringValue(mContext, XML_NAME, KEY_MOBILE_IP, platformInfo.getMobileIP());
        SharedPreferenceUtil.setStringValue(mContext, XML_NAME, KEY_RELEASE_TIME, platformInfo.getRelease());
        SharedPreferenceUtil.setStringValue(mContext, XML_NAME, KEY_PLATFORM, platformInfo.getPlatform());
        SharedPreferenceUtil.setStringValue(mContext, XML_NAME, KEY_SYSTEM_VERSION, platformInfo.getSystemVersion());
        SharedPreferenceUtil.setStringValue(mContext, XML_NAME, KEY_LANGUAGE, platformInfo.getLanguage());
        SharedPreferenceUtil.setStringValue(mContext, XML_NAME, KEY_COUNTRY, platformInfo.getCountry());
        SharedPreferenceUtil.setStringValue(mContext, XML_NAME, KEY_AD_UID, platformInfo.getPid());
    }

    /**
     * 获得PlatformInfo
     *
     * @return
     */
    public static PlatformInfo getPlatfromInfo() {
        PlatformInfo platformInfo = new PlatformInfo();
        platformInfo.setPid(SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_PID, null));
        platformInfo.setImsi(SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_IMSI, null));
        platformInfo.setW(SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_WIDTH, null));
        platformInfo.setH(SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_HEIGHT, null));
        platformInfo.setVersion(SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_VERSION, null));
        platformInfo.setPhonetype(SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_PHONE_TYPE, null));
        platformInfo.setProduct(SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_PRODUCT_ID, null));
        platformInfo.setNetType(SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_NET_TYPE, null));
        platformInfo.setMobileIP(SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_MOBILE_IP, null));
        platformInfo.setRelease(SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_RELEASE_TIME, null));
        platformInfo.setPlatform(SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_PLATFORM, null));
        platformInfo.setSystemVersion(SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_SYSTEM_VERSION, null));
        platformInfo.setLanguage(SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_LANGUAGE, null));
        platformInfo.setCountry(SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_COUNTRY, null));
        platformInfo.setPid(SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_AD_UID, null));
        platformInfo.setFid(SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_FID, null)==null?
                CountryFidUtils.getCountryFidMap().get(platformInfo.getCountry()):SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_FID, null));
        return platformInfo;
    }


    /**
     * 获得Platform JSON字符串
     */
    public static String getPlatformJsonString() {
        gson = new Gson();
        // 创建以platformInfo为Key的JSON字符串
        Map<String, PlatformInfo> map = new HashMap<>();
        map.put("platformInfo", getPlatfromInfo());
        return gson.toJson(map);
    }

    public static void setToken(String token) {
        SharedPreferenceUtil.setStringValue(mContext, XML_NAME, KEY_TOKEN, token);
    }

    /**
     * 获得服务器返回的token值
     */
    public static String getToken() {
        return SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_TOKEN, null);
    }

    public static void setPushToken(String token) {
        SharedPreferenceUtil.setStringValue(mContext, XML_NAME, KEY_PUSH_TOKEN, token);
    }

    /**
     * 获得百度推送token值
     */
    public static String getPushToken() {
        return SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_PUSH_TOKEN, null);
    }

    /**
     * 获得接口请求公用参数
     *
     * @return Map<String, String> platformInfo和token
     */
    public static Map<String, String> getCommonParams() {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("platformInfo", getPlatformJsonString());
        paramsMap.put("", getToken());
        return paramsMap;
    }
    public static String getCountry(){
        return SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_COUNTRY, null);
    }
    public static void setCountry(String country){
       SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_COUNTRY, country);
    }


    public static String getLanguage(){
        return SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_LANGUAGE, null);
    }
    public static void setAdPid(String adPid){
       // LogUtil.e(LogUtil.TAG_LMF,"Xml adpid"+adPid);
        SharedPreferenceUtil.setStringValue(mContext, XML_NAME, KEY_AD_UID, adPid);
    }
    public static String getAdPid(){
        return SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_AD_UID, null);
    }
    public static void setFID(String value) {
        SharedPreferenceUtil.setStringValue(mContext, XML_NAME, KEY_FID, value);
    }
    public static String getFID() {
        return SharedPreferenceUtil.getStringValue(mContext, XML_NAME, KEY_FID, null);
    }

}
