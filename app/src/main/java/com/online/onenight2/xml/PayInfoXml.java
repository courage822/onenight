package com.online.onenight2.xml;

import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.library.utils.SharedPreferenceUtil;
import com.online.onenight2.payutil.Purchase;
import com.online.onenight2.utils.Utils;

/**
 * 保存支付成功后商品信息
 * Created by zhangdroid on 2017/4/18.
 */
public class PayInfoXml {
    public static final String XML_NAME = "pay_info";
    private static final String KEY_PURCHASE = "purchase";// Purchase信息
    private static final String KEY_SERVICE_ID = "service_id";// serviceId
    private static final String KEY_PAY_TYPE = "pay_type";// payType
    private static final String KEY_PAY_RESULT = "pay_result";// 支付接口回调结果：默认为false

    public static void setPurchase(Purchase purchase) {
        if (purchase != null) {
            SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, KEY_PURCHASE, JSON.toJSONString(purchase));
        }
    }

    public static Purchase getPurchase() {
        String purchase = SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PURCHASE, null);
        if (!TextUtils.isEmpty(purchase)) {
            return JSON.parseObject(purchase, Purchase.class);
        }
        return null;
    }

    public static void setServiceId(String serviceId) {
        SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, KEY_SERVICE_ID, serviceId);
    }

    public static String getServiceId() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_SERVICE_ID, null);
    }

    public static void setPayType(String payType) {
        SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, KEY_PAY_TYPE, payType);
    }

    public static String getPayType() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PAY_TYPE, null);
    }

    public static void setPayResult(boolean result) {
        SharedPreferenceUtil.setBooleanValue(Utils.getContext(), XML_NAME, KEY_PAY_RESULT, result);
    }

    public static boolean getPayResult() {
        return SharedPreferenceUtil.getBooleanValue(Utils.getContext(), XML_NAME, KEY_PAY_RESULT, false);
    }

}
