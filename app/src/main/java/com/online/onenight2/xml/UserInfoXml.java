package com.online.onenight2.xml;

import android.text.TextUtils;

import com.library.utils.SharedPreferenceUtil;
import com.library.utils.Util;
import com.online.onenight2.bean.Area;
import com.online.onenight2.bean.Image;
import com.online.onenight2.bean.MatcherInfo;
import com.online.onenight2.bean.User;
import com.online.onenight2.bean.UserBase;
import com.online.onenight2.utils.Utils;

import java.util.List;


/**
 * 登陆用户信息
 * Created by zhangdroid on 2016/6/24.
 */
public class UserInfoXml {
    public static final String XML_NAME = "user_info";
    // 基本信息
    private static final String KEY_USER_NAME = "username";// 账号
    private static final String KEY_PASSWORD = "password";// 密码
    private static final String KEY_UID = "uid";// id
    private static final String KEY_GENDER = "gender";// 性别
    private static final String KEY_AGE = "age";// 年龄
    private static final String KEY_MONOLOGUE = "monologue";// 内心独白
    private static final String KEY_NICKNAME = "nickname";// 昵称
    private static final String KEY_BIRTHDAY = "birthday";// 生日
    private static final String KEY_CONSTELLATION = "constellation";// 星座
    private static final String KEY_IS_BEAN_USER = "isBeanUser";//是否豆币用户
    private static final String KEY_IS_MONTHLY = "isMonthly";// 是否包月用户
    private static final String KEY_IS_VIP = "isVip";// 是否VIP用户
    private static final String KEY_BEAN_COUNT = "beanCount";// 豆币数量
    private static final String KEY_LIKE_COUNT = "likeCount";// like数量
    private static final String KEY_AVATAR_URL = "avatarUrl";// 头像原图url
    private static final String KEY_AVATAR_URL_C = "avatarUrlC";// 头像缩略url
    private static final String KEY_AVATAR_STATUS = "avatarStatus";// 头像审核状态
    // 详细资料项
    private static final String KEY_PHOTO_COUNT = "photoCount";// 图片总数
    private static final String KEY_INFO_LEVEL = "infoLevel";// 资料完成度
    private static final String KEY_LEVEL = "level";// 诚信等级
    private static final String KEY_COUNTRY = "country";// 国家
    private static final String KEY_LANGUAGE = "language";// 国家对应的语言
    private static final String KEY_FID = "fid";// 渠道号
    private static final String KEY_PROVINCE_NAME = "provinceName";// 地区
    private static final String KEY_ETHNICITY = "ethnicity";// 种族
    private static final String KEY_SPOKEN_LANGUAGE = "spoken_language";// 会说的语言
    private static final String KEY_INCOME = "income";// 收入
    private static final String KEY_HEIGHT = "height";// 身高
    private static final String KEY_WEIGHT = "weight";// 体重
    private static final String KEY_WORK = "work";// 职业
    private static final String KEY_EDUCATION = "education";// 学历
    private static final String KEY_MARRIAGE = "marriage";// 婚姻状况
    private static final String KEY_HAVE_KIDS = "have_kids";// 是否有孩子
    private static final String KEY_WANT_BABY = "wantBaby";// 是否想要孩子
    private static final String KEY_SMOKE = "smoke";// 是否吸烟
    private static final String KEY_DRINK = "drink";// 是否饮酒
    private static final String KEY_FAITH = "faith";// 信仰
    private static final String KEY_PETS = "pets";// 宠物
    private static final String KEY_EXERCISE_HABITS = "exercise_habits";// 锻炼习惯
    private static final String KEY_PERSONAL = "personal";// 个性
    private static final String KEY_INTEREST = "interest";// 兴趣爱好
    private static final String KEY_SPORT = "sport";// 喜欢的运动
    private static final String KEY_SEXY_BODY_PART = "sexy_body_part";// 魅力部位
    private static final String KEY_HOUSE_CONDITION = "house_condition";// 住房条件
    private static final String KEY_LONG_DISTANCE_LOVE = "long_distance_love";// 异地恋
    private static final String KEY_LOVE_TYPE = "love_type";// 喜欢的类型
    private static final String KEY_INTIMATE_BEHAVIOR = "intimate_behavior";// 亲密行为
    private static final String KEY_LIVE_WITH_PARENTS = "live_with_parents";// 与父母同住
    private static final String KEY_POLITICAL_VIEW = "political_view";// 政治观点
    private static final String KEY_BOOK_CARTOON = "book_cartoon";// 喜欢的书和漫画
    private static final String KEY_TRAVEL = "travel";// 旅行
    public static final String KEY_QUESTION_USER = "question_user";// 省地区
    public static final String KRY_ISEXITLOGIN="isexitlogin";//是否登录退出
    public static final String KEY_WELCOME_LOCATION = "WELCOME_LOCATION";

    // 其它信息
    private static final String KEY_IS_CLIENT_ACTIVATE = "isClientActivate";// 是否激活
    private static final String KEY_PARAMS_INIT = "paramsInit";// 初始化数据
    public static final String KEY_IS_CLIENT = "isLogin";// 是否激活
    public static final String KEY_PAY_RESIDENCE = "Residence";// 支付失败信息

    public static final String KEY_MATCH_INFO = "matchInfo";
    /*添加*/
    public static final String KEY_PAY_FAIL = "payFail";// 支付失败信息
    public static final String KEY_PAY_HAIRCOLOR = "hairColor";// 头发颜色
    public static final String KEY_PAY_HOWMANYKIDS = "howManyKids";// 有多少小孩
    public static final String KEY_PAY_EYECOLOR = "eyeColor";// 眼睛颜色

    public static final String KEY_PAY_BODYTYPE = "bodytype";// 体格
    public static final String KEY_PAY_POLITICALVIEWS = "politicalViews";// 政治信仰
    public static final String KEY_PAY_HEADICONSEX = "headiconsex";// 保存头像

    public static final String KEY_PAY_CHILDSTATUS = "childStatus";// 是否要小孩
    //征友条件
    //地区
    public static final String KEY_PAY_ZYPLACE = "place";// 地区
    //年龄
    public static final String KEY_PAY_ZYMINAGE = "minAge";// 最小年龄
    public static final String KEY_PAY_ZYMAXAGE = "maxAge";// 最大年龄
    //身高
    public static final String KEY_PAY_ZYMINHEIGHT = "minHeight";// 最低身高（英寸）
    public static final String KEY_PAY_ZYMAXHEIGHT = "maxHeight";// 最高身高（英寸）
    //教育
    public static final String KEY_PAY_ZYMINEDUCATIONENGLISH = "minEducationEnglish";// 最低身高（英寸）
    //收入
    public static final String KEY_PAY_ZYINCOMEENGLISH = "incomeEnglish";// 收入
    private static final String KEY_LOCAL_INFO_LEVEL = "localInfoLevel";// 资料完成度

    private static void saveIfExists(String key, String value) {
        if (!TextUtils.isEmpty(value) && !"".equals(value)) {
            SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, key, value);
        }
    }

    /**
     * 保存用户数据
     */
    public static void setUserInfo(User user) {
        if (user != null) {
            // 保存征友条件信息
            SearchXml.setMatcherInfo(user.getMatcherInfo());
            setBeanCount(user.getBeanCurrencyCount());
            setLikeCount(user.getLikeValueCurrentCount());
            setUsername(user.getAccount());
            setPassword(user.getPassword());
            List<Image> imageList = user.getListImage();
            setPhotoCount(Util.isListEmpty(imageList) ? "0" : String.valueOf(imageList.size()));
            setInfoLevel(String.valueOf(user.getInfoLevel()));
            setLevel(String.valueOf(user.getLevel()));
            setBirthday(user.getBirthday());
            setEthnicity(user.getEthnicity());
            setWeight(String.valueOf(user.getWeight()));
            setHaveKids(String.valueOf(user.getHaveKids()));
            setSmoke(String.valueOf(user.getSmoke()));
            setDrink(String.valueOf(user.getDrink()));
            setFaith(String.valueOf(user.getFaith()));
            setSmoke(String.valueOf(user.getSmoke()));
            setWantBaby(String.valueOf(user.getWantsKids()));
            setChildstatus(user.getChildStatus());
            setSport(user.getSports());
            setBookCartoon(user.getBooks());
            setTravel(user.getTravel());
            setPets(user.getPets());
            setExerciseHabits(String.valueOf(user.getExerciseHabits()));
            setInterest(user.getListHobby());
            setPersonal(user.getCharacteristics());
//            添加
            setHairColor(user.getHairColor());
            setHowManyKids(user.getHowManyKids());
            setEyeColor(user.getEyeColor());
            setBodyType(user.getBodyType());
            setPoliticalViews(user.getPoliticalViews());
//            征友添加

            MatcherInfo matcherInfo = user.getMatcherInfo();
            if (matcherInfo != null) {
                Area area = matcherInfo.getArea();
                if (area != null) {
                    setZyPlace(area.getProvinceName());
                }
                setZyMinage(String.valueOf(matcherInfo.getMinAge()));
                setZyMaxage(String.valueOf(matcherInfo.getMaxAge()));
                setZyMinHeight(matcherInfo.getMinHeight());
                setZyMaxHeight(matcherInfo.getMaxHeight());
                setZyMinEducationEnglish(String.valueOf(matcherInfo.getMinimumEducation()));
                setZyIncomeEnglish(String.valueOf(matcherInfo.getIncome()));

            }


            UserBase userBase = user.getUserBaseEnglish();
            if (userBase != null) {
                Image image = userBase.getImage();
                if (image != null) {
                    // 保存头像原图
                    saveIfExists(KEY_AVATAR_URL, image.getImageUrl());
                    // 保存头像缩略图
                    saveIfExists(KEY_AVATAR_URL_C, image.getThumbnailUrl());
                    // 保存头像审核状态
                    saveIfExists(KEY_AVATAR_STATUS, image.getStatus());
                }
                setUID(userBase.getId());
                setGender(String.valueOf(userBase.getSex()));
                setAge(String.valueOf(userBase.getAge()));
                setIsVip(String.valueOf(userBase.getIsVip()));
                setIsMonthly(String.valueOf(userBase.getIsMonthly()));
                setIsBeanUser(String.valueOf(userBase.getIsBeanUser()));
                setMonologue(userBase.getMonologue());
                setNickName(userBase.getNickName());
                setConstellation(String.valueOf(userBase.getSign()));
                setCountry(userBase.getCountry());
                setLanguage(userBase.getLanguage());
//                setFid(userBase.get);
                Area area = userBase.getArea();
                if (area != null) {
                    setProvinceName(area.getProvinceName());
                }
                setSpokenLanguage(userBase.getSpokenLanguage());
                setHeight(userBase.getHeightCm());
//                setBodyType(userBase.getBodyType());
                setEducation(String.valueOf(userBase.getEducation()));
                setWork(String.valueOf(userBase.getOccupation()));
                setIncome(String.valueOf(userBase.getIncome()));
//                setMarriage(String.valueOf(userBase.getMaritalStatus()));
            }
        }
    }

    // ***********************************************  公用方法，获取用户基本信息  ***********************************************

    /**
     * @return 是否VIP用户
     */
    public static boolean isVip() {
        return "1".equals(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_IS_VIP, "0"));
    }
    /**
     * 是否登陆
     */
    public static boolean isWelComeLocation() {
        return SharedPreferenceUtil.getBooleanValue(Utils.getContext(), XML_NAME, KEY_WELCOME_LOCATION, false);
    }
    /**
     * 是否登陆
     */
    public static void setWelComeLocation(Boolean lacation) {
        SharedPreferenceUtil.setBooleanValue(Utils.getContext(), XML_NAME, KEY_WELCOME_LOCATION, true);
    }


    /**
     * 存储是否VIP用户
     */
    public static void setIsVip(String value) {
        saveIfExists(KEY_IS_VIP, value);
    }

    /**
     * @return 是否包月用户
     */
    public static boolean isMonthly() {
        return "1".equals(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_IS_MONTHLY, "0"));
    }

    /**
     * 存储是否包月用户
     */
    public static void setIsMonthly(String value) {
        saveIfExists(KEY_IS_MONTHLY, value);
    }

    /**
     * @return 是否豆币用户
     */
    public static boolean isBeanUser() {
        return "1".equals(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_IS_BEAN_USER, "0"));
    }

    /**
     * 存储是否豆币用户
     */
    public static void setIsBeanUser(String value) {
        saveIfExists(KEY_IS_BEAN_USER, value);
    }

    /**
     * @return 豆币数量
     */
    public static int getBeanCount() {
        return SharedPreferenceUtil.getIntValue(Utils.getContext(), XML_NAME, KEY_BEAN_COUNT, 0);
    }

    /**
     * 存储豆币值
     */
    public static void setBeanCount(int value) {
        SharedPreferenceUtil.setIntValue(Utils.getContext(), XML_NAME, KEY_BEAN_COUNT, value);
    }

    /**
     * @return like数量
     */
    public static int getLikeCount() {
        return SharedPreferenceUtil.getIntValue(Utils.getContext(), XML_NAME, KEY_LIKE_COUNT, 0);
    }

    /**
     * 存储like值
     */
    public static void setLikeCount(int value) {
        SharedPreferenceUtil.setIntValue(Utils.getContext(), XML_NAME, KEY_LIKE_COUNT, value);
    }

    /**
     * @return 登陆账号
     */
    public static String getUserName() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_USER_NAME, null);
    }

    /**
     * 获得是否激活
     */
    public static boolean getIsClient() {
        return SharedPreferenceUtil.getBooleanValue(Utils.getContext(), XML_NAME, KEY_IS_CLIENT, false);
    }

    public static void setIsClient() {
        SharedPreferenceUtil.setBooleanValue(Utils.getContext(), XML_NAME, KEY_IS_CLIENT, true);
    }

    /**
     * 存储账号
     */
    public static void setUsername(String value) {
        saveIfExists(KEY_USER_NAME, value);
    }

    /**
     * @return 登陆密码
     */
    public static String getPassword() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PASSWORD, null);
    }

    /**
     * 存储密码
     */
    public static void setPassword(String value) {
        saveIfExists(KEY_PASSWORD, value);
    }

    /**
     * @return 用户ID
     */
    public static String getUID() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_UID, null);
    }

    /**
     * 存储用户ID
     */
    public static void setUID(String value) {
        saveIfExists(KEY_UID, value);
    }

    /**
     * @return true is male, else female
     */
    public static boolean isMale() {
        return "0".equals(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_GENDER, "0"));
    }

    /**
     * @return 用户性别 "0"：男性，"1"：女性
     */
    public static String getGender() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_GENDER, "0");
    }

    /**
     * 存储性别
     */
    public static void setGender(String value) {
        saveIfExists(KEY_GENDER, value);
    }

    /**
     * @return 用户年龄
     */
    public static String getAge() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_AGE, null);
    }

    /**
     * 存储年龄
     */
    public static void setAge(String value) {
        saveIfExists(KEY_AGE, value);
    }

    /**
     * @return 内心独白
     */
    public static String getMonologue() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_MONOLOGUE, null);
    }

    /**
     * 存储内心独白
     */
    public static void setMonologue(String value) {
        saveIfExists(KEY_MONOLOGUE, value);
    }

    /**
     * @return 昵称
     */
    public static String getNickName() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_NICKNAME, null);
    }

    /**
     * 设置昵称
     */
    public static void setNickName(String value) {
        saveIfExists(KEY_NICKNAME, value);
    }

    /**
     * @return 生日
     */
    public static String getBirthday() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_BIRTHDAY, null);
    }

    /**
     * 存储生日
     */
    public static void setBirthday(String value) {
        saveIfExists(KEY_BIRTHDAY, value);
    }

    /**
     * @return 星座
     */
    public static String getConstellation() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_CONSTELLATION, null);
    }

    /**
     * 存储星座
     */
    public static void setConstellation(String value) {
        saveIfExists(KEY_CONSTELLATION, value);
    }

    /**
     * @return 地区
     */
    public static String getResidence() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PAY_RESIDENCE, null);
    }

    /**
     * 存储地区
     */
    public static void setResidence(String value) {
        saveIfExists(KEY_PAY_RESIDENCE, value);
    }


    // ***********************************************  公用方法，获取用户图片相关信息  ***********************************************

    /**
     * @return 头像缩略图Url
     */
    public static String getAvatarThumbnailUrl() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_AVATAR_URL_C, null);
    }

    /**
     * @return 头像原图Url
     */
    public static String getAvatarUrl() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_AVATAR_URL, null);
    }

    public static void setAvatarUrl(String avatarUrl) {
        SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, KEY_AVATAR_URL, avatarUrl);
    }

    /**
     * @return 头像审核状况
     */
    public static String getAvatarStatus() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_AVATAR_STATUS, null);
    }

    // ***********************************************  公用方法，返回用户资料项信息  ***********************************************

    /**
     * @return 图片总数
     */
    public static String getPhotoCount() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PHOTO_COUNT, null);
    }

    /**
     * 存储图片总数
     */
    public static void setPhotoCount(String value) {
        saveIfExists(KEY_PHOTO_COUNT, value);
    }

    /**
     * @return 资料完成度
     */
    public static String getInfoLevel() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_INFO_LEVEL, null);
    }

    /**
     * 存储资料完成度
     */
    public static void setInfoLevel(String value) {
        saveIfExists(KEY_INFO_LEVEL, value);
    }

    /**
     * @return 资料完成度2 本地
     */
    public static String getLocleInfoLevel() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_LOCAL_INFO_LEVEL, null);
    }

    /**
     * 存储资料完成度2 本地
     */
    public static void setLocleInfoLevel(String value) {
        SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, KEY_LOCAL_INFO_LEVEL, value);
    }

    /**
     * @return 诚信等级
     */
    public static String getLevel() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_LEVEL, null);
    }

    /**
     * 存储诚信等级
     */
    public static void setLevel(String value) {
        saveIfExists(KEY_LEVEL, value);
    }

    /**
     * @return 国家
     */
    public static String getCountry() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_COUNTRY, null);
    }

    /**
     * 存储国家
     */
    public static void setCountry(String value) {
        saveIfExists(KEY_COUNTRY, value);
    }

    /**
     * @return 语言
     */
    public static String getLanguage() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_LANGUAGE, null);
    }

    /**
     * 存储语言
     */
    public static void setLanguage(String value) {
        saveIfExists(KEY_LANGUAGE, value);
    }
    /**
     * @return 渠道号
     */
    public static String getFid() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_FID, null);
    }

    /**
     * 存储渠道号
     */
    public static void setFid(String value) {
        saveIfExists(KEY_FID, value);
    }

    /**
     * @return 地区
     */
    public static String getProvinceName() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PROVINCE_NAME, null);
    }

    /**
     * 存储地区
     */
    public static void setProvinceName(String value) {
        saveIfExists(KEY_PROVINCE_NAME, value);
    }

    /**
     * @return 种族
     */
    public static String getEthnicity() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_ETHNICITY, null);
    }

    /**
     * 存储种族
     */
    public static void setEthnicity(String value) {
        saveIfExists(KEY_ETHNICITY, value);
    }

    /**
     * @return 用户会说的语言
     */
    public static String getSpokenLanguage() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_SPOKEN_LANGUAGE, null);
    }

    /**
     * 存储用户会说的语言
     */
    public static void setSpokenLanguage(String value) {
        saveIfExists(KEY_SPOKEN_LANGUAGE, value);
    }

    /**
     * @return 收入
     */
    public static String getIncome() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_INCOME, null);
    }

    /**
     * 存储收入
     */
    public static void setIncome(String value) {
        saveIfExists(KEY_INCOME, value);
    }

    /**
     * @return 身高
     */
    public static String getHeight() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_HEIGHT, null);
    }

    /**
     * 存储身高
     */
    public static void setHeight(String value) {
        saveIfExists(KEY_HEIGHT, value);
    }

    /**
     * @return 体重
     */
    public static String getWeight() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_WEIGHT, null);
    }

    /**
     * 存储体重
     */
    public static void setWeight(String value) {
        if (!"0".equals(value)) {
            saveIfExists(KEY_WEIGHT, value);
        }
    }

    /**
     * @return 职业
     */
    public static String getWork() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_WORK, null);
    }

    /**
     * 存储职业
     */
    public static void setWork(String value) {
        saveIfExists(KEY_WORK, value);
    }

    /**
     * @return 学历
     */
    public static String getEducation() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_EDUCATION, null);
    }

    /**
     * 存储学历
     */
    public static void setEducation(String value) {
        saveIfExists(KEY_EDUCATION, value);
    }

    /**
     * @return 婚姻状况
     */
    public static String getMarriage() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_MARRIAGE, null);
    }

    /**
     * 存储婚姻状况
     */
    public static void setMarriage(String value) {
        saveIfExists(KEY_MARRIAGE, value);
    }

    /**
     * @return 是否有孩子
     */
    public static String getHaveKids() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_HAVE_KIDS, null);
    }

    /**
     * 存储是否有孩子
     */
    public static void setHaveKids(String value) {
        saveIfExists(KEY_HAVE_KIDS, value);
    }

    /**
     * @return 是否想要小孩
     */
    public static String getWantBaby() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_WANT_BABY, null);
    }

    /**
     * 存储是否想要小孩
     */
    public static void setWantBaby(String value) {
        saveIfExists(KEY_WANT_BABY, value);
    }

    /**
     * @return 是否吸烟
     */
    public static String getSmoke() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_SMOKE, null);
    }

    /**
     * 存储吸烟
     */
    public static void setSmoke(String value) {
        saveIfExists(KEY_SMOKE, value);
    }

    /**
     * @return 是否饮酒
     */
    public static String getDrink() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_DRINK, null);
    }

    /**
     * 存储饮酒
     */
    public static void setDrink(String value) {
        saveIfExists(KEY_DRINK, value);
    }

    /**
     * @return 信仰
     */
    public static String getFaith() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_FAITH, null);
    }

    /**
     * 存储信仰
     */
    public static void setFaith(String value) {
        saveIfExists(KEY_FAITH, value);
    }

    /**
     * @return 宠物
     */
    public static String getPets() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PETS, null);
    }

    /**
     * 存储宠物
     */
    public static void setPets(String value) {
        saveIfExists(KEY_PETS, value);
    }

    /**
     * @return 锻炼习惯
     */
    public static String getExerciseHabits() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_EXERCISE_HABITS, null);
    }

    /**
     * 存储锻炼习惯
     */
    public static void setExerciseHabits(String value) {
        saveIfExists(KEY_EXERCISE_HABITS, value);
    }

    /**
     * @return 头发颜色
     */
    public static String getHairColor() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PAY_HAIRCOLOR, null);
    }

    /**
     * 存储头发颜色
     */
    public static void setHairColor(String value) {
        saveIfExists(KEY_PAY_HAIRCOLOR, value);
    }

    /**
     * @return 有多少小孩
     */
    public static String getHowManyKids() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PAY_HOWMANYKIDS, null);
    }

    /**
     * 存储有多少小孩
     */
    public static void setHowManyKids(String value) {
        saveIfExists(KEY_PAY_HOWMANYKIDS, value);
    }

    /**
     * @return 眼睛颜色
     */
    public static String getEyeColor() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PAY_EYECOLOR, null);
    }

    /**
     * 存储眼睛颜色
     */
    public static void setEyeColor(String value) {
        saveIfExists(KEY_PAY_EYECOLOR, value);
    }

    /**
     * @return 喜欢的运动
     */
    public static String getSport() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_SPORT, null);
    }

    /**
     * 存储喜欢的运动
     */
    public static void setSport(String value) {
        saveIfExists(KEY_SPORT, value);
    }

    /**
     * @return 个性
     */
    public static String getPersonal() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PERSONAL, null);
    }

    /**
     * 存储个性
     */
    public static void setPersonal(String value) {
        saveIfExists(KEY_PERSONAL, value);
    }

    /**
     * @return 兴趣爱好
     */
    public static String getInterest() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_INTEREST, null);
    }

    /**
     * 存储兴趣爱好
     */
    public static void setInterest(String value) {
        saveIfExists(KEY_INTEREST, value);
    }

    /**
     * @return 魅力部位
     */
    public static String getSexyBodyPart() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_SEXY_BODY_PART, null);
    }

    /**
     * 存储魅力部位
     */
    public static void setSexyBodyPart(String value) {
        saveIfExists(KEY_SEXY_BODY_PART, value);
    }

    /**
     * @return 喜欢的书、漫画
     */
    public static String getBookCartoon() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_BOOK_CARTOON, null);
    }

    /**
     * 存储喜欢的书、漫画
     */
    public static void setBookCartoon(String value) {
        saveIfExists(KEY_BOOK_CARTOON, value);
    }

    /**
     * @return 旅行
     */
    public static String getTravel() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_TRAVEL, null);
    }

    /**
     * 存储旅行
     */
    public static void setTravel(String value) {
        saveIfExists(KEY_TRAVEL, value);
    }


    // ***********************************************  公用方法，保存其它用户相关信息  ***********************************************

    /**
     * @return 是否激活
     */
    public static boolean isClientActivate() {
        return SharedPreferenceUtil.getBooleanValue(Utils.getContext(), XML_NAME, KEY_IS_CLIENT_ACTIVATE, false);
    }

    /**
     * 设置已经激活过
     */
    public static void setClientActivate() {
        SharedPreferenceUtil.setBooleanValue(Utils.getContext(), XML_NAME, KEY_IS_CLIENT_ACTIVATE, true);
    }

    /**
     * @return 初始化数据
     */
    public static String getParamsInit() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PARAMS_INIT, null);
    }

    /**
     * 保存数据字典json串
     */
    public static void setParamsInit(String paramsInit) {
        saveIfExists(KEY_PARAMS_INIT, paramsInit);
    }

    public static void clearUserInfoXml() {
        SharedPreferenceUtil.clearXml(Utils.getContext(), XML_NAME);
    }

    /**
     * 保存questionUser
     */
    public static String getQuestionUser() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_QUESTION_USER, null);
    }

    public static void setQuestionUser(String payFail) {
        SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, KEY_QUESTION_USER, payFail);
    }

    public static void clearPayFail() {
        SharedPreferenceUtil.removeXml(Utils.getContext(), XML_NAME, KEY_PAY_FAIL);
    }

    /**
     * 保存失败信息
     */
    public static String getPayFail() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PAY_FAIL, null);
    }

    public static void setPayFail(String payFail) {
        SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, KEY_PAY_FAIL, payFail);
    }

    //    保存头像
    public static String getHeadIconSex() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PAY_HEADICONSEX, null);
    }

    public static void setHeadIconSex(String headIconSex) {
        saveIfExists(KEY_PAY_HEADICONSEX, headIconSex);

    }

    //    体格
    public static String getBodyType() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PAY_BODYTYPE, null);
    }

    public static void setBodyType(String bodyType) {
        saveIfExists(KEY_PAY_BODYTYPE, bodyType);
    }

    //    政治观点
    public static String getPoliticalViews() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PAY_POLITICALVIEWS, null);
    }

    public static void setPoliticalViews(String politicalViews) {
        saveIfExists(KEY_PAY_POLITICALVIEWS, politicalViews);
    }

    //    是否想要小孩
    public static String getChildstatus() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PAY_CHILDSTATUS, null);
    }

    public static void setChildstatus(String politicalViews) {
        saveIfExists(KEY_PAY_CHILDSTATUS, politicalViews);
    }

    //    征友
// 地区
    public static String getZyPlace() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PAY_ZYPLACE, null);
    }

    public static void setZyPlace(String politicalViews) {
        saveIfExists(KEY_PAY_ZYPLACE, politicalViews);
    }

    //  最小年龄
    public static String getZyMinage() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PAY_ZYMINAGE, null);
    }

    public static void setZyMinage(String politicalViews) {
        saveIfExists(KEY_PAY_ZYMINAGE, politicalViews);
    }

    //  最大年龄
    public static String getZyMaxage() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PAY_ZYMAXAGE, null);
    }

    public static void setZyMaxage(String politicalViews) {
        saveIfExists(KEY_PAY_ZYMAXAGE, politicalViews);
    }

    //  最小身高
    public static String getZyMinHeight() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PAY_ZYMINHEIGHT, null);
    }

    public static void setZyMinHeight(String politicalViews) {
        saveIfExists(KEY_PAY_ZYMINHEIGHT, politicalViews);
    }

    //  最大身高
    public static String getZyMaxHeight() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PAY_ZYMAXHEIGHT, null);
    }

    public static void setZyMaxHeight(String politicalViews) {
        saveIfExists(KEY_PAY_ZYMAXHEIGHT, politicalViews);
    }

    //  最低身高（英寸）
    public static String getZyMinEducationEnglish() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PAY_ZYMINEDUCATIONENGLISH, null);
    }

    public static void setZyMinEducationEnglish(String politicalViews) {
        saveIfExists(KEY_PAY_ZYMINEDUCATIONENGLISH, politicalViews);

    }

    //  收入
    public static String getZyIncomeEnglish() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_PAY_ZYINCOMEENGLISH, null);
    }

    public static void setZyIncomeEnglish(String politicalViews) {
        saveIfExists(KEY_PAY_ZYINCOMEENGLISH, politicalViews);

    }

    /**
     * @return 征友條件
     */
    public static String getMatchInfo() {
        return SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_MATCH_INFO, null);
    }

    /**
     * @return 征友條件
     */
    public static void setMatchInfo(String value) {
        SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, KEY_MATCH_INFO, value);
    }
//    是否退出登录
    public static boolean IsExitLogin(){
        return SharedPreferenceUtil.getBooleanValue(Utils.getContext(), XML_NAME, KRY_ISEXITLOGIN, false);
    }
    public static void setIsExitLogin(boolean value){
        SharedPreferenceUtil.setBooleanValue(Utils.getContext(), XML_NAME, KRY_ISEXITLOGIN, value);
    }
}
