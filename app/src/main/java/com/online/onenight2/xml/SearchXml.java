package com.online.onenight2.xml;

import android.text.TextUtils;

import com.library.utils.SharedPreferenceUtil;
import com.online.onenight2.bean.Area;
import com.online.onenight2.bean.MatcherInfo;
import com.online.onenight2.utils.Utils;


/**
 * 保存用户设置的搜索条件
 * Created by zhangdroid on 2016/8/19.
 */
public class SearchXml {
    public static final String XML_NAME = "search_matchinfo";
    private static final String KEY_LOCATION = "key_LOCATION";
    private static final String KEY_MIN_AGE = "key_MIN_AGE";
    private static final String KEY_MAX_AGE = "key_MAX_AGE";
    private static final String KEY_MIN_HEIGHT = "key_MIN_HEIGHT";
    private static final String KEY_MAX_HEIGHT = "key_MAX_HEIGHT";
    private static final String KEY_OCCUPATION = "key_OCCUPATION";
    private static final String KEY_EDUCATION = "key_EDUCATION";
    private static final String KEY_CONSTELLATION = "key_CONSTELLATION";
    private static final String KEY_INCOME = "key_INCOME";
    private static final String KEY_MARRIAGE = "key_MARRIAGE";
    private static final String KEY_WANT_KIDS = "key_WANT_KIDS";

    /**
     * 设置搜索条件
     */
    public static void setMatcherInfo(MatcherInfo matcherInfo) {
        if (matcherInfo != null) {
            Area area = matcherInfo.getArea();
            SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, KEY_LOCATION, area != null ? area.getProvinceName() : "all");
            SharedPreferenceUtil.setIntValue(Utils.getContext(), XML_NAME, KEY_MIN_AGE, matcherInfo.getMinAge() == 0 ? 18 : matcherInfo.getMinAge());
            SharedPreferenceUtil.setIntValue(Utils.getContext(), XML_NAME, KEY_MAX_AGE, matcherInfo.getMaxAge() == 0 ? 65 : matcherInfo.getMaxAge());
            SharedPreferenceUtil.setIntValue(Utils.getContext(), XML_NAME, KEY_MIN_HEIGHT, matcherInfo.getMinHeightCm() == 0 ? 160 : matcherInfo.getMinHeightCm());
            SharedPreferenceUtil.setIntValue(Utils.getContext(), XML_NAME, KEY_MAX_HEIGHT, matcherInfo.getMaxHeightCm() == 0 ? 180 : matcherInfo.getMaxHeightCm());
            SharedPreferenceUtil.setIntValue(Utils.getContext(), XML_NAME, KEY_OCCUPATION, matcherInfo.getOccupation());
            SharedPreferenceUtil.setIntValue(Utils.getContext(), XML_NAME, KEY_EDUCATION, matcherInfo.getMinimumEducation());
            SharedPreferenceUtil.setIntValue(Utils.getContext(), XML_NAME, KEY_CONSTELLATION, matcherInfo.getSign());
            SharedPreferenceUtil.setStringValue(Utils.getContext(), XML_NAME, KEY_INCOME, matcherInfo.getIncomeEnglish());
            SharedPreferenceUtil.setIntValue(Utils.getContext(), XML_NAME, KEY_MARRIAGE, matcherInfo.getRelationShip());
            SharedPreferenceUtil.setIntValue(Utils.getContext(), XML_NAME, KEY_WANT_KIDS, matcherInfo.getWantKids());
        }
    }

    /**
     * @return 获得设置的搜索条件
     */
    public static MatcherInfo getMatcherInfo() {
        MatcherInfo matcherInfo = new MatcherInfo();
        Area area = new Area();
        // 默认值为用户注册所在地区，若为空，则默认值为全选
        String defaultLocation = "all";
        String provinceName = UserInfoXml.getProvinceName();
        if (!TextUtils.isEmpty(provinceName) && !"".equals(provinceName)) {
            defaultLocation = provinceName;
        }
        area.setProvinceName(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_LOCATION, defaultLocation));
        matcherInfo.setArea(area);
        matcherInfo.setMinAge(SharedPreferenceUtil.getIntValue(Utils.getContext(), XML_NAME, KEY_MIN_AGE, 18));
        matcherInfo.setMaxAge(SharedPreferenceUtil.getIntValue(Utils.getContext(), XML_NAME, KEY_MAX_AGE, 65));
        matcherInfo.setMinHeightCm(SharedPreferenceUtil.getIntValue(Utils.getContext(), XML_NAME, KEY_MIN_HEIGHT, 160));
        matcherInfo.setMaxHeightCm(SharedPreferenceUtil.getIntValue(Utils.getContext(), XML_NAME, KEY_MAX_HEIGHT, 180));
        matcherInfo.setOccupation(SharedPreferenceUtil.getIntValue(Utils.getContext(), XML_NAME, KEY_OCCUPATION, -1));
        matcherInfo.setMinimumEducation(SharedPreferenceUtil.getIntValue(Utils.getContext(), XML_NAME, KEY_EDUCATION, -1));
        matcherInfo.setSign(SharedPreferenceUtil.getIntValue(Utils.getContext(), XML_NAME, KEY_CONSTELLATION, -1));
        matcherInfo.setIncomeEnglish(SharedPreferenceUtil.getStringValue(Utils.getContext(), XML_NAME, KEY_INCOME, null));
        matcherInfo.setRelationShip(SharedPreferenceUtil.getIntValue(Utils.getContext(), XML_NAME, KEY_MARRIAGE, -1));
        matcherInfo.setWantKids(SharedPreferenceUtil.getIntValue(Utils.getContext(), XML_NAME, KEY_WANT_KIDS, -1));
        return matcherInfo;
    }

}
