package com.online.onenight2.math.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.online.onenight2.R;
import com.online.onenight2.base.BaseActivity;
import com.online.onenight2.bean.PraiseUser;
import com.online.onenight2.bean.UserBaseEnglish;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.hot.activity.UserInfoActivity;
import com.online.onenight2.math.adapter.PraisedListAdapter;
import com.online.onenight2.math.presenter.PraisedUserListPresenter;
import com.online.onenight2.view.MyDecoration;
import com.online.onenight2.view.refresh.NormalRefreshViewHolder;
import com.online.onenight2.view.refresh.RefreshLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 描述：点赞/喜欢 好友列表
 * Created by qyh on 2017/4/27.
 */

public class PraisedUserListActivity extends BaseActivity<PraisedUserListPresenter> implements RefreshLayout.RefreshLayoutDelegate {
    // 正常请求
    public static final int NORMAL = 1;
    // 刷新请求
    public static final int REFRESH = 2;
    // 加载更多请求
    public static final int LOADMORE = 3;
    private static int PAGE = 1;
    //获取数据多少
    private static final int PAGESIZE = 60;
    @BindView(R.id.rc_hot_content)
    RecyclerView rcHotContent;
    @BindView(R.id.refresh)
    RefreshLayout refresh;
    @BindView(R.id.tv_appbar_back)
    TextView tvAppbarBack;
    @BindView(R.id.tv_appbar_title)
    TextView tvAppbarTitle;
    private LinearLayoutManager mLinearLayoutManager;
    private PraisedListAdapter mAdapter;
    private List<PraiseUser.PraisedList> mData;
    private String likeNum;
    private Integer likeNumInt;

    @Override
    public int getLayoutId() {
        return R.layout.activity_praised_list;
    }

    @Override
    public PraisedUserListPresenter newPresenter() {
        return new PraisedUserListPresenter();
    }

    @Override
    public void initView(Bundle savedInstanceState) {

        tvAppbarBack.setText(R.string.back);
        tvAppbarTitle.setText(R.string.like_list);
        mAdapter = new PraisedListAdapter();
        mLinearLayoutManager = new LinearLayoutManager(mContext);
        rcHotContent.setLayoutManager(mLinearLayoutManager);
        rcHotContent.addItemDecoration(new MyDecoration(mContext, MyDecoration.VERTICAL_LIST));
        rcHotContent.setHasFixedSize(true);
        rcHotContent.setAdapter(mAdapter);
        //设置下拉、上拉
        refresh.setDelegate(this);
        refresh.setRefreshViewHolder(new NormalRefreshViewHolder(mContext, true));
        rcHotContent.addOnScrollListener(mOnScrollListener);
        mAdapter.setOnItemClickListener(new PraisedListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                UserBaseEnglish userBaseEnglish = mData.get(position).getUserBaseEnglish();
                Intent intent=new Intent(mContext, UserInfoActivity.class);
                intent.putExtra(ConfigConstant.USERID, String.valueOf(userBaseEnglish.getId()));
                intent.putExtra(ConfigConstant.ISRECORD,"0");
                intent.putExtra(ConfigConstant.SOURCETAG,"1");
                intent.putExtra("image",userBaseEnglish.getImage().getImageUrl());
                mContext.startActivity(intent);
                overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
            }
        });
    }

    public void showData(List<PraiseUser.PraisedList> praisedList) {
        mData = praisedList;
        mAdapter.setData(praisedList,likeNumInt);
    }

    public void refreshData(List<PraiseUser.PraisedList> praisedList) {
        mAdapter.setData(praisedList,praisedList.size());
        refresh.endRefreshing();
    }

    public void loadMoreData(List<PraiseUser.PraisedList> praisedList) {
        mData.addAll(praisedList);
        mAdapter.setData(mData,likeNumInt);
    }

    @Override
    public void initData() {
        getPresenter().getPraisedUserList(1, PAGESIZE, NORMAL);
        Intent intent = getIntent();
        if(null!=intent){
            likeNum = intent.getStringExtra("likeNum");
            likeNumInt = Integer.valueOf(likeNum);
        }
    }

    // 监听适配器的滑动，完成上拉加载的功能
    public RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
        //最后一个角标位置
        private int lastVisibleItemPosition;

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            lastVisibleItemPosition = mLinearLayoutManager.findLastVisibleItemPosition();
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mAdapter.getItemCount()) {
                PAGE++;
                getPresenter().getPraisedUserList(PAGE, PAGESIZE, LOADMORE);
            }
        }
    };

    @Override
    public void onRefreshLayoutBeginRefreshing(RefreshLayout refreshLayout) {
        PAGE=1;
        getPresenter().getPraisedUserList(PAGE, PAGESIZE, REFRESH);
    }

    @Override
    public boolean onRefreshLayoutBeginLoadingMore(RefreshLayout refreshLayout) {
        return false;
    }

    @OnClick({R.id.tv_appbar_back})
    public void  click(TextView v){
        switch (v.getId()){
            case R.id.tv_appbar_back:
                finish();
                break;
        }
    }
}
