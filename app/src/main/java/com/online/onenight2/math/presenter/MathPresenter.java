package com.online.onenight2.math.presenter;

import com.lzy.okgo.OkGo;
import com.online.onenight2.base.BasePresenter;
import com.online.onenight2.bean.NextUsers;
import com.online.onenight2.bean.User;
import com.online.onenight2.callback.JsonCallback;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.math.fragment.MathFragment;
import com.online.onenight2.xml.PlatformInfoXml;

import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 描述：
 * Created by qyh on 2017/4/13.
 */

public class MathPresenter extends BasePresenter<MathFragment> {

    public void getUserData(int page, int pageSize, final int type){
        System.out.println("platformInfo=="+PlatformInfoXml.getPlatformJsonString());

        OkGo.post(IUrlConstant.URL_NEXT_USER)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("pageNum",page)
                .params("pageSize",pageSize)
                .execute(new JsonCallback<NextUsers>() {
                    @Override
                    public void onSuccess(NextUsers nextUsers, Call call, Response response) {
                        getView().hideLoadingPress();
                        List<User> listUser = nextUsers.getListUser();
                        if(type==0){
                            getView().getMathData(listUser);
                        }else {
                            getView().loadMoreData(listUser);
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                    }
                });
             }


}
