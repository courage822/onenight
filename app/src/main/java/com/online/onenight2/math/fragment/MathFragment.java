package com.online.onenight2.math.fragment;


import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.online.onenight2.R;
import com.online.onenight2.base.BaseFragment;
import com.online.onenight2.base.RxManager;
import com.online.onenight2.bean.User;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.math.activity.PraisedUserListActivity;
import com.online.onenight2.math.adapter.MathAdapter;
import com.online.onenight2.math.presenter.MathPresenter;
import com.online.onenight2.utils.SharedPreferencesUtils;
import com.online.onenight2.view.CleverRecyclerView.CleverRecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import rx.functions.Action1;

/**
 * 描述： 心动
 * Created by qyh on 2017/3/24.
 */

public class MathFragment extends BaseFragment<MathPresenter> {

    @BindView(R.id.iv_math_heart)
    ImageView ivMathHeart;
    @BindView(R.id.tv_math_likenum)
    TextView tvMathLikenum;
    @BindView(R.id.rl_math_likenum)
    RelativeLayout rlLikenumView;
    @BindView(R.id.tv_math_title)
    TextView tvMathTitle;
    @BindView(R.id.rv_math_pager)
    CleverRecyclerView rvMathPager;
    private static int PAGE=1;
    //获取数据多少
    private static final int PAGESIZE=10;
    //点赞数量
    private int likeNum;
    //用户数据
    private List<User> userData;
    private MathAdapter mMathAdapter;
    private RxManager mRxManager;
    private boolean isCanLoad=true;
    @Override
    public int getLayoutId() {
        return R.layout.fragment_math;
    }
    @Override
    protected void initView(){
        likeNum = SharedPreferencesUtils.getData(mContext, "likeNum", 0);
        mMathAdapter = new MathAdapter(R.layout.item_math_user,null);
        rvMathPager.setHasFixedSize(true);
        rvMathPager.setAdapter(mMathAdapter);
        rvMathPager.setOnPageChangedListener(new CleverRecyclerView.OnPageChangedListener() {
            @Override
            public void onPageChanged(int currentPosition) {
                if(currentPosition%5==0){
                    PAGE++;
                    getPresenter().getUserData(PAGE,PAGESIZE,1);
                }
            }
        });
        mRxManager = new RxManager();
        mRxManager.on("like_unlike", new Action1<Integer>() {

            @Override
            public void call(Integer integer) {
                switch (integer){
                    case 1:
                        setLikeNum(1);
                        break;
                    case -1:
                        setLikeNum(-1);
                        break;
                }
            }
        });
    }
    @OnClick({R.id.rl_math_likenum})
    public  void click(RelativeLayout v){
        Intent intent=new Intent(mContext, PraisedUserListActivity.class);
        intent.putExtra("likeNum",String.valueOf(likeNum));
        startActivityForResult(intent,100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==100){
            likeNum= SharedPreferencesUtils.getData(mContext, ConfigConstant.LISTSIZA, 0);
            if(likeNum>0){
                rlLikenumView.setVisibility(View.VISIBLE);
                tvMathLikenum.setText(String.valueOf(likeNum));
            }else{
                rlLikenumView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void initData() {
        showLoadingPress();
        getPresenter().getUserData(PAGE,PAGESIZE,0);
    }


    @Override
    public MathPresenter newPresenter() {
        return new MathPresenter();
    }
    public void getMathData(List<User> list){
        rvMathPager.setVisibility(View.VISIBLE);
        userData = list;
        mMathAdapter.setNewData(userData);
        setLikeNum(0);
    }

    public void loadMoreData(List<User> list){
        mMathAdapter.addData(list);
        mMathAdapter.notifyDataSetChanged();
        setLikeNum(0);
    }
    private void setLikeNum(int type) {

        if(likeNum>0&&type==0){
            rlLikenumView.setVisibility(View.VISIBLE);
            tvMathLikenum.setText(String.valueOf(likeNum));
        }else if(type==1){
            likeNum++;
                rlLikenumView.setVisibility(View.VISIBLE);
                tvMathLikenum.setText(String.valueOf(likeNum));

        }else if (type==-1){
                if(likeNum>0){
                    likeNum--;
                    rlLikenumView.setVisibility(View.VISIBLE);
                    tvMathLikenum.setText(String.valueOf(likeNum));
                }else{
                    rlLikenumView.setVisibility(View.GONE);
                }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        RxManager rxManager=new RxManager();
        rxManager.on("login_change", new Action1<String>() {
            @Override
            public void call(String s) {
                if(isCanLoad){
                    getPresenter().getUserData(PAGE,PAGESIZE,0);
                    isCanLoad=false;
                }
            }
        });
    }
}
