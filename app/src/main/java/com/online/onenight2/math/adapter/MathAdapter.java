package com.online.onenight2.math.adapter;

import android.content.Intent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.library.utils.HeightUtils;
import com.online.onenight2.R;
import com.online.onenight2.base.RxBus;
import com.online.onenight2.base.RxManager;
import com.online.onenight2.base.adapter.BaseQuickAdapter;
import com.online.onenight2.base.adapter.BaseViewHolder;
import com.online.onenight2.bean.User;
import com.online.onenight2.bean.UserBase;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.hot.activity.UserInfoActivity;
import com.online.onenight2.utils.ImageLoaderUtils;
import com.online.onenight2.utils.MathDataUtils;
import com.sackcentury.shinebuttonlib.ShineButton;

import java.util.List;

import rx.functions.Action1;

/**
 * 描述：
 * Created by qyh on 2017/4/13.
 */

public class MathAdapter extends BaseQuickAdapter {

    private RxManager manager;
    public MathAdapter(int layoutResId, List data ) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(final BaseViewHolder helper, Object item, final int position) {
        final User userData = (User) item;
        final UserBase userBaseEnglish = userData.getUserBaseEnglish();
        int sendPraiseCount = userData.getSendPraiseCount();
        helper.setText(R.id.tv_math_nickname,userBaseEnglish.getNickName());
        //身高
        helper.setText(R.id.tv_math_height, HeightUtils.getInchCmByCm(userBaseEnglish.getHeightCm()));
        //年龄
        helper.setText(R.id.tv_math_age,userBaseEnglish.getAge()+" ");

        ImageLoaderUtils.display(mContext,(ImageView) helper.getView(R.id.iv_math_avar_big)
                ,userBaseEnglish.getImage().getThumbnailUrlM());
        ImageLoaderUtils.display(mContext,(ImageView) helper.getView(R.id.iv_math_avar)
                ,userBaseEnglish.getImage().getThumbnailUrl());
        final ShineButton like = helper.getView(R.id.iv_shinebutton);
        if(userData.getIsPraised().equals("0")){
            like.setChecked(false);
            like.setEnabled(true);
        }else{
            like.setChecked(true);
            like.setEnabled(false);
        }
        manager = new RxManager();
        //从点赞列表 取消点赞后，改变对应按钮的状态
        manager.on("likeState", new Action1<Integer>() {
            @Override
            public void call(Integer integer) {
                if(String.valueOf(integer).equals(userBaseEnglish.getId())){
                    like.setChecked(true);
                    like.setEnabled(false);
                    userData.setIsPraised("0");
                    RxBus.getInstance().post("like_unlike",-1);
                }
            }
        });

        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    sendPraised(userBaseEnglish, helper, userData);
                    like.setChecked(true);
                    like.setEnabled(false);
            }
        });
        //跳转详情页面
        FrameLayout linearLayout=helper.getView(R.id.math_user_viewpager_linear);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext, UserInfoActivity.class);
                intent.putExtra(ConfigConstant.USERID, userBaseEnglish.getId());
                intent.putExtra(ConfigConstant.ISRECORD,"0");
                intent.putExtra(ConfigConstant.SOURCETAG,"1");
                intent.putExtra("image",userBaseEnglish.getImage().getImageUrl());
                mContext.startActivity(intent);
            }
        });
    }

    private void canclePraised(UserBase userBaseEnglish, final BaseViewHolder helper, final User userData) {
        MathDataUtils.canclePraise(userBaseEnglish.getId(), new MathDataUtils.onRequestDataListener() {
            @Override
            public void onSuccess() {
                userData.setIsPraised("0");
                RxBus.getInstance().post("like_unlike",-1);
            }
            @Override
            public void onFailed() {

            }
        });
    }

    private void sendPraised(UserBase userBaseEnglish, final BaseViewHolder helper, final User userData) {
        MathDataUtils.sendPraise(userBaseEnglish.getId(), new MathDataUtils.onRequestDataListener() {
            @Override
            public void onSuccess() {
               // helper.setBackgroundRes(R.id.rl_math_like,R.drawable.shape_math_liked);
                userData.setIsPraised("1");
                RxBus.getInstance().post("like_unlike",1);
            }
            @Override
            public void onFailed() {
            }
        });
    }
}
