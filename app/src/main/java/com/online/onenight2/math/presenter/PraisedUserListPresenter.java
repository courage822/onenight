package com.online.onenight2.math.presenter;

import com.library.utils.LogUtil;
import com.lzy.okgo.OkGo;
import com.online.onenight2.base.BasePresenter;
import com.online.onenight2.bean.PraiseUser;
import com.online.onenight2.callback.JsonCallback;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.math.activity.PraisedUserListActivity;
import com.online.onenight2.xml.PlatformInfoXml;

import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 描述：
 * Created by qyh on 2017/4/27.
 */

public class PraisedUserListPresenter extends BasePresenter<PraisedUserListActivity> {
    /**
     * 获取心动列表
     * @param page
     * @param pageSize
     * @param type
     */
    public void getPraisedUserList(int page, int pageSize, final int type){
        OkGo.post(IUrlConstant.PRAISED_USERLIST)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("pageNum",page)
                .params("pageSize",pageSize)
                .execute(new JsonCallback<PraiseUser>() {

                    @Override
                    public void onSuccess(PraiseUser praiseUser, Call call, Response response) {
                        if(null!=praiseUser&&praiseUser.getIsSucceed().equals("1")){
                            List<PraiseUser.PraisedList> praisedList = praiseUser.getPraisedList();
                            if(praisedList.size()>0){
                                System.out.println();
                                LogUtil.e(LogUtil.TAG_LMF,"数据大小===="+praiseUser.getPraisedList().size());
                                LogUtil.e(LogUtil.TAG_LMF,"数据大小 数据"+praiseUser.getPraisedList().get(0));
//                                for(int i=0;i<=praiseUser.getPraisedList().size();i++){
//                                    PraiseUser.PraisedList praisedList1 = praiseUser.getPraisedList().get(i);
//                                    LogUtil.e(LogUtil.TAG_LMF,praisedList1.toString());
//                                }
                                switch (type){
                                    case PraisedUserListActivity.NORMAL:
                                        getView().showData(praisedList);
                                        break;
                                    case PraisedUserListActivity.REFRESH:
                                        getView().refreshData(praisedList);
                                        break;
                                    case PraisedUserListActivity.LOADMORE:
                                        getView().loadMoreData(praisedList);
                                        break;
                                }
                            }else{
                                System.out.println("无数据======");
                                return;
                            }
                        }
                    }

                });
         }
}
