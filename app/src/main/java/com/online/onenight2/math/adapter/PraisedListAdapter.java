package com.online.onenight2.math.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.library.utils.HeightUtils;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseApplication;
import com.online.onenight2.base.RxBus;
import com.online.onenight2.bean.PraiseUser;
import com.online.onenight2.bean.UserBaseEnglish;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.utils.ImageLoaderUtils;
import com.online.onenight2.utils.MathDataUtils;
import com.online.onenight2.utils.SharedPreferencesUtils;

import java.util.List;

/**
 * 描述：
 * Created by qyh on 2017/4/27.
 */

public class PraisedListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<PraiseUser.PraisedList> mData;
    private OnItemClickListener mOnItemClickListener;
    private boolean isDelete;
    private Integer likeNumInt;

    public void setData(List<PraiseUser.PraisedList> praisedList,int likeNum){
        this.mData=praisedList;
        this.likeNumInt=likeNum;
        notifyDataSetChanged();
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View news = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_praisedlist_list, parent, false);
        ItemViewHolder vh = new ItemViewHolder(news);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final UserBaseEnglish mUserData = mData.get(position).getUserBaseEnglish();
        ((ItemViewHolder)holder).tv_meessage_item_nickname.setText(mUserData.getNickName());
        ((ItemViewHolder)holder).tv_message_item_height.setText(HeightUtils.getInchCmByCm(mUserData.getHeightCm()));
        ((ItemViewHolder)holder).tv_message_item_age.setText(mUserData.getAge()+" ");
        if(null!=mUserData.getImage()) {
            ImageLoaderUtils.display(BaseApplication.getAppContext(), ((ItemViewHolder) holder).iv_message_avar,mUserData.getImage().getThumbnailUrl());
        }
        ((ItemViewHolder)holder).iv_item_delete.setVisibility(View.VISIBLE);
        ((ItemViewHolder)holder).iv_item_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MathDataUtils.canclePraise(String.valueOf(mUserData.getId()), new MathDataUtils.onRequestDataListener() {

                    @Override
                    public void onSuccess() {
                        System.out.println("position======"+position);
                        mData.remove(position);
                        RxBus.getInstance().post("likeState",mUserData.getId());
                        likeNumInt--;
                        System.out.println("likeNumInt======="+likeNumInt);
                        SharedPreferencesUtils.saveData(BaseApplication.getAppContext(),
                                ConfigConstant.LISTSIZA, likeNumInt);
                        isDelete=true;
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onFailed() {

                    }
                });
            }
        });
        if(!isDelete){
            SharedPreferencesUtils.saveData(BaseApplication.getAppContext(),
                    ConfigConstant.LISTSIZA, likeNumInt);
        }
    }
    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private final TextView tv_meessage_item_nickname;
        private final TextView tv_message_item_height;
        private final TextView tv_message_item_age;
        private final ImageView iv_item_delete;
        private final ImageView iv_message_avar;

        public ItemViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tv_meessage_item_nickname = (TextView) itemView.findViewById(R.id.tv_meessage_item_nickname);
            tv_message_item_height = (TextView) itemView.findViewById(R.id.tv_message_item_height);
            tv_message_item_age = (TextView) itemView.findViewById(R.id.tv_message_item_age);
            iv_message_avar = (ImageView) itemView.findViewById(R.id.iv_message_avar);
            iv_item_delete = (ImageView) itemView.findViewById(R.id.iv_item_delete);
        }

        @Override
        public void onClick(View v) {
            if(null!=mOnItemClickListener){
                mOnItemClickListener.onItemClick(getPosition());
            }
        }
    }

    @Override
    public int getItemCount() {
        return mData==null?0:mData.size();
    }

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        this.mOnItemClickListener=listener;
    }
}
