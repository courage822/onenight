package com.online.onenight2.callback;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.annotation.Nullable;

import com.lzy.okgo.request.BaseRequest;
import com.online.onenight2.view.LoadingDialog;

public abstract class DialogCallback<T> extends JsonCallback<T> {

    private ProgressDialog dialog;
    private final Activity mContext;

    private void initDialog(Activity activity) {
//        dialog = new ProgressDialog(activity);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCanceledOnTouchOutside(false);
//        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        dialog.setMessage("请求网络中...");
    }

    public DialogCallback(Activity activity) {
        super();
        initDialog(activity);
        mContext = activity;
    }

    @Override
    public void onBefore(BaseRequest request) {
        super.onBefore(request);
//        //网络请求前显示对话框
//        if (dialog != null && !dialog.isShowing()) {
//            dialog.show();
//        }
        LoadingDialog.showDialogForLoading(mContext);
    }

    @Override
    public void onAfter(@Nullable T t, @Nullable Exception e) {
        super.onAfter(t, e);
//        //网络请求结束后关闭对话框
//        if (dialog != null && dialog.isShowing()) {
//            dialog.dismiss();
//        }
        LoadingDialog.cancelDialogForLoading(mContext);
    }
}
