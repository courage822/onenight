package com.online.onenight2.message.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.lzy.okgo.OkGo;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseActivity;
import com.online.onenight2.bean.PeiDuiBean;
import com.online.onenight2.bean.UserBaseEnglish;
import com.online.onenight2.callback.JsonCallback;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.hot.activity.UserInfoActivity;
import com.online.onenight2.math.adapter.PraisedListAdapter;
import com.online.onenight2.message.adapter.PeiDuiAdapter;
import com.online.onenight2.message.presenter.PeiDuiPresenter;
import com.online.onenight2.view.MyDecoration;
import com.online.onenight2.view.refresh.NormalRefreshViewHolder;
import com.online.onenight2.view.refresh.RefreshLayout;
import com.online.onenight2.xml.PlatformInfoXml;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Response;

/**
 * 描述：互相喜欢界面，，第一次进入界面的数据是从上个界面传递过来的，刷新和加载数据在本界面完成
 * Created by qyh on 2017/5/2.
 */

public class PeiDuiActivity extends BaseActivity<PeiDuiPresenter> implements RefreshLayout.RefreshLayoutDelegate {
    @BindView(R.id.tv_appbar_back)
    TextView tvAppbarBack;
    @BindView(R.id.tv_appbar_title)
    TextView tvAppbarTitle;
    @BindView(R.id.tv_appbar_sure)
    TextView tvAppbarSure;
    @BindView(R.id.rc_hot_content)
    RecyclerView rcHotContent;
    @BindView(R.id.refresh)
    RefreshLayout refresh;
    private static int PAGE = 1;
    // 刷新请求
    public static final int REFRESH = 2;
    // 加载更多请求
    public static final int LOADMORE = 3;
    //获取数据多少
    private static final String PAGESIZE = "10";
    private LinearLayoutManager mLinearLayoutManager;
    private PeiDuiAdapter mAdapter;
    private List<PeiDuiBean.MatcherList> mDataMatcherList;

    @Override
    public int getLayoutId() {
        return R.layout.activity_praised_list;
    }

    @Override
    public PeiDuiPresenter newPresenter() {
        return new PeiDuiPresenter();
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        tvAppbarBack.setText(R.string.back);
        tvAppbarTitle.setText(R.string.peidui);
        mAdapter = new PeiDuiAdapter(mContext);
        mLinearLayoutManager = new LinearLayoutManager(mContext);
        rcHotContent.setLayoutManager(mLinearLayoutManager);
        rcHotContent.addItemDecoration(new MyDecoration(mContext, MyDecoration.VERTICAL_LIST));
        rcHotContent.setHasFixedSize(true);
        rcHotContent.setAdapter(mAdapter);
        //设置下拉、上拉
        refresh.setDelegate(this);
        refresh.setRefreshViewHolder(new NormalRefreshViewHolder(mContext, true));
        rcHotContent.addOnScrollListener(mOnScrollListener);
        mAdapter.setOnItemClickListener(new PraisedListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                UserBaseEnglish userBaseEnglish = mDataMatcherList.get(position).getUserBaseEnglish();
                Intent intent=new Intent(mContext, UserInfoActivity.class);
                intent.putExtra(ConfigConstant.USERID, String.valueOf(userBaseEnglish.getId()));
                intent.putExtra(ConfigConstant.ISRECORD,"0");
                intent.putExtra(ConfigConstant.SOURCETAG,"1");
                intent.putExtra("image",userBaseEnglish.getImage().getImageUrl());
                mContext.startActivity(intent);
                overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
            }
        });
    }
    @OnClick(R.id.tv_appbar_back)
    public void onClick(TextView view){
        switch (view.getId()){
            case R.id.tv_appbar_back:
                finish();
                break;
        }
    }
    @Override
    public void initData() {
        Intent intent = getIntent();
        if(null!=intent){
            PeiDuiBean  mData = (PeiDuiBean) intent.getSerializableExtra("peidui");
            mData.setUnreadCount(0);
            mDataMatcherList = mData.getMatcherList();
            mAdapter.setData(mDataMatcherList);
        }
    }

    public void refreshPeiDuiList(List<PeiDuiBean.MatcherList> matcherList) {
        mDataMatcherList = matcherList;
        refresh.endRefreshing();
        mAdapter.setData(mDataMatcherList);
    }
    public void loadMoreList(List<PeiDuiBean.MatcherList> matcherList){
        mDataMatcherList.addAll(matcherList);
        mAdapter.setData(mDataMatcherList);
    }
    @Override
    public void onRefreshLayoutBeginRefreshing(RefreshLayout refreshLayout) {
        PAGE=1;
       getMathers(PAGE, PAGESIZE,REFRESH);
    }
    // 监听适配器的滑动，完成上拉加载的功能
    public RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
        //最后一个角标位置
        private int lastVisibleItemPosition;

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            lastVisibleItemPosition = mLinearLayoutManager.findLastVisibleItemPosition();
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mAdapter.getItemCount()) {
                PAGE++;
                getMathers(PAGE, PAGESIZE,LOADMORE);
            }
        }
    };
    @Override
    public boolean onRefreshLayoutBeginLoadingMore(RefreshLayout refreshLayout) {
        return false;
    }
    /**
     * 互相喜歡
     * @param pageNum
     * @param pageSize
     */
    public void getMathers(int pageNum, String pageSize, final int type){
        OkGo.post(IUrlConstant.URL_GET_MATCHER_LIST)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("pageNum",String.valueOf(pageNum))
                .params("pageSize",pageSize)
                .execute(new JsonCallback<PeiDuiBean>() {
                    @Override
                    public void onSuccess(PeiDuiBean peiDuiBean, Call call, Response response) {
                        if(null!=peiDuiBean){
                            if(peiDuiBean.getIsSucceed().equals("1")){
                                List<PeiDuiBean.MatcherList> matcherList = peiDuiBean.getMatcherList();
                                switch (type){
                                    case PeiDuiActivity.REFRESH:
                                         refreshPeiDuiList(matcherList);
                                        break;
                                    case PeiDuiActivity.LOADMORE:
                                       loadMoreList(matcherList);
                                        break;
                                }
                            }
                        }
                    }
                });
    }
}
