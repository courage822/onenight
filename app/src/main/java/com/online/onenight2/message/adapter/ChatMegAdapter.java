package com.online.onenight2.message.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.online.onenight2.R;
import com.online.onenight2.bean.Image;
import com.online.onenight2.bean.Message;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.hot.activity.UserInfoActivity;
import com.online.onenight2.profile.activity.PictureLookActivity;
import com.online.onenight2.recordmanager.MediaManager;
import com.online.onenight2.utils.ImageLoaderUtils;
import com.online.onenight2.utils.TimeUtils;
import com.online.onenight2.utils.Utils;
import com.online.onenight2.view.BubbleImageView;
import com.online.onenight2.xml.UserInfoXml;

//import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;

import static com.online.onenight2.utils.TimeUtils.mContext;

/**
 * Created by foxmanman on 2017/4/15.
 */

public class ChatMegAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Message> messagesDate;
    private String userHeadImage;
    private long serverTime=0;
    private Context mContent;
    //文本消息的类型
    private final static String CHAT_MEG_TYPE_TEXT = "4";
    //图片消息类型
    private static final String MSG_TYPE_PHOTO="10";
    //语音消息类型
    private static final String CHAT_TYPE_VOICE="7";
    //我方消息
    private final static int CHAT_SEND_TEXT = 1;
    // 对方消息
    private final static int CHAT_OTHER_TEXT = -1;
    //自己发送的语音
    private static final int CHAT_SEND_VOICE=3;
    //对方发送的语音
    private static final int CHAT_RECIVE_VOICE=-3;
    //自己发送的图片
    private static final int MSG_SEND_PHOTO=2;
    //对方发过来的图片
    private static final int MSG_RECEIVE_PHOTO=-2;
    //当前聊天列表所有的图片集合
    private List<Image> imageList;
    //存储图片position  map
    private HashMap<Integer, Integer> mImageposition;
    public ChatMegAdapter(Context mContent) {
        this.mContent = mContent;
    }


    public void setDate(Long serveTime, String userHeadImage, List<Message> list, HashMap<Integer, Integer> imagePosition, List<Image> imageList) {
        this.serverTime = serveTime;
        this.userHeadImage = userHeadImage;
        this.messagesDate = list;
        this.mImageposition=imagePosition;
        this.imageList=imageList;
        this.notifyItemChanged(list.size()-1);
    }

    @Override
    public int getItemViewType(int position) {
        System.out.println("4444444444444444:::"+position);
        String uid = messagesDate.get(position).getUid();
        if (!TextUtils.isEmpty(uid)) {
            String messageType = messagesDate.get(position).getMsgType();
            if (uid.equals(UserInfoXml.getUID())) {
                // 发送文本消息
                if (!TextUtils.isEmpty(CHAT_MEG_TYPE_TEXT) && CHAT_MEG_TYPE_TEXT.equals(messageType)) {
                    return CHAT_SEND_TEXT;
                }else if(!TextUtils.isEmpty(messageType)&& MSG_TYPE_PHOTO.equals(messageType)){
                    return MSG_SEND_PHOTO;
                }
                else if(!TextUtils.isEmpty(CHAT_TYPE_VOICE) && CHAT_TYPE_VOICE.equals(messageType)){
                    return CHAT_SEND_VOICE;
                }
            } else {
                if (!TextUtils.isEmpty(CHAT_MEG_TYPE_TEXT) && CHAT_MEG_TYPE_TEXT.equals(messageType)) {
                    return CHAT_OTHER_TEXT;
                }else if(!TextUtils.isEmpty(messageType)&& MSG_TYPE_PHOTO.equals(messageType)){
                    return MSG_RECEIVE_PHOTO;
                }
                else if(!TextUtils.isEmpty(CHAT_TYPE_VOICE) && CHAT_TYPE_VOICE.equals(messageType)){
                    return CHAT_RECIVE_VOICE;
                }
            }
        }
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case CHAT_SEND_TEXT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_megtext_right, parent, false);
                viewHolder = new SendTextHolder(view);
                break;
            case CHAT_OTHER_TEXT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_megtext_left, parent, false);
                viewHolder = new OtherTextHolder(view);
                break;
            case CHAT_RECIVE_VOICE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_voice_receive, parent, false);
                viewHolder=new ReceiveVoiceHolder(view);
                break;
            case CHAT_SEND_VOICE:
                view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_voice_send,parent,false);
                viewHolder=new SendVoiceHolder(view);
                break;
            case MSG_RECEIVE_PHOTO:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_receive,parent,false);
                viewHolder=new ReceivePhotoHolder(view);
                break;
            case  MSG_SEND_PHOTO:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_send, parent, false);
                viewHolder=new SendPhotoHolder(view);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Message message = messagesDate.get(position);
        switch (getItemViewType(position)) {
            case CHAT_SEND_TEXT:
                sendTextMessAge((SendTextHolder) holder, message,position);
                break;
            case CHAT_OTHER_TEXT:
                otherTextMessage((OtherTextHolder) holder, message, position);
                break;
            case CHAT_RECIVE_VOICE:
                setVoiceReceiveLayout((ReceiveVoiceHolder)holder,message,position);
                break;
            case CHAT_SEND_VOICE:
                setVoiceSendLayout((SendVoiceHolder)holder,message);
                break;
            case MSG_RECEIVE_PHOTO:
                setPhotoReceiveLayout((ReceivePhotoHolder)holder,message,position);
                break;
            case MSG_SEND_PHOTO:
                setPhotoSendeLayout((SendPhotoHolder)holder,message,messagesDate,position);
                break;
        }
    }
    //对方消息
    private void otherTextMessage(OtherTextHolder holder, final Message message, final int position) {
        final String context = message.getContent();
        holder.other_context.setText(context);
        ImageLoaderUtils.display(mContent, holder.other_headImage, userHeadImage);
        //     时间
        long timedate = TimeUtils.getLongTime(message.getCreateDate());
        String locledate = TimeUtils.getLocalTime(serverTime, timedate);
        holder.other_time.setText(TimeUtils.getLocalTime(serverTime, timedate));
        holder.other_headImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContent, UserInfoActivity.class);
                intent.putExtra(ConfigConstant.USERID,messagesDate.get(position).getUid());
                intent.putExtra(ConfigConstant.ISRECORD,"0");
                intent.putExtra(ConfigConstant.SOURCETAG,"4");
                mContent.startActivity(intent);
            }
        });
    }
    //我方消息
    private void sendTextMessAge(SendTextHolder holder, final Message message, final int position) {
        String context = message.getContent();
        holder.send_context.setText(context);
        ImageLoaderUtils.display(mContent, holder.send_headImage, UserInfoXml.getAvatarUrl());
        holder.send_headImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                android.os.Message msg=new android.os.Message();
//                msg.what= ConfigConstant.INTOMEFRAGMENT;
//                EventBus.getDefault().post(msg);
            }
        });
        //     时间
        long timedate = TimeUtils.getLongTime(message.getCreateDate());
        String locledate = TimeUtils.getLocalTime(serverTime, timedate);
        holder.send_time.setText(TimeUtils.getLocalTime(serverTime, timedate));
    }
    /**
     * 设置自己发送的语音消息
     * @param holder
     * @param messageData
     */
    private void setVoiceSendLayout(SendVoiceHolder holder, final Message messageData) {
        ImageLoaderUtils.display(mContext,holder.iv_send_voice_avatar,UserInfoXml.getAvatarUrl());
//        holder.iv_send_voice_avatar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent(mContext, UserInfoActivity.class);
//                intent.putExtra(ConfigConstant.USERID, messageData.getId());
//                intent.putExtra(ConfigConstant.ISRECORD,"0");
//                intent.putExtra(ConfigConstant.SOURCETAG,"1");
//                intent.putExtra("image",userHeadImage);
//                mContext.startActivity(intent);
//            }
//        });
        String audioTime = messageData.getAudioTime();
        holder.tv_send_voice_duration.setText(audioTime+"''");
        //获取手机的当前时间
        long voiceTime = TimeUtils.getLongTime(messageData.getCreateDate());
        String localTime =  TimeUtils.getLocalTime(serverTime, voiceTime);
        holder.tv_send_voice_time.setText(localTime);
        //更改并显示录音条长度
        ViewGroup.LayoutParams ps =  holder.rl_send_voice_viewbg.getLayoutParams();
        ps.width = Utils.getVoiceLineWight(mContext, Integer.valueOf(audioTime));
        holder.rl_send_voice_viewbg.setLayoutParams(ps); //更改语音长条长度
        //开始设置监听
        final ImageView ieaLlSinger = holder.iv_send_voice_pic;
        //语音播放
        holder.rl_send_voice_viewbg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AnimationDrawable animationDrawable = (AnimationDrawable) ieaLlSinger.getBackground();
                //获取语音信号栏，开始播放动画
                animationDrawable.start();
                //播放前重置。
                MediaManager.release();
                //语音播放
                MediaManager.playSound(messageData.getAudioUrl(), new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        animationDrawable.selectDrawable(0);//显示动画第一帧
                        animationDrawable.stop();
                    }
                });
            }
        });
    }

    /**
     * 设置对方发送过来的语音消息
     * @param holder
     * @param messageData
     */
    private void setVoiceReceiveLayout(ReceiveVoiceHolder holder, final Message messageData,final int position) {
        ImageLoaderUtils.display(mContext,holder.iv_receive_voice_avatar,userHeadImage);
        holder.iv_receive_voice_avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContent, UserInfoActivity.class);
                intent.putExtra(ConfigConstant.USERID,messagesDate.get(position).getUid());
                intent.putExtra(ConfigConstant.ISRECORD,"0");
                intent.putExtra(ConfigConstant.SOURCETAG,"4");
                mContent.startActivity(intent);
            }
        });
        String audioTime = messageData.getAudioTime();
        //左边显示的时间长度
        holder.tv_receive_voice_duration.setText(audioTime+"''");
        //获取手机的当前时间
        long voiceTime = TimeUtils.getLongTime(messageData.getCreateDate());
        String localTime =  TimeUtils.getLocalTime(serverTime, voiceTime);
        //上面显示的时间
        holder.tv_receive_voice_time.setText(localTime);
        //更改并显示录音条长度
        ViewGroup.LayoutParams ps =  holder.rl_receive_voice_viewbg.getLayoutParams();
        ps.width = Utils.getVoiceLineWight(mContext, Integer.valueOf(audioTime));
        holder.rl_receive_voice_viewbg.setLayoutParams(ps); //更改语音长条长度
        //开始设置监听
        final ImageView ieaLlSinger = holder.iv_receive_voice_pic;
        //语音播放
        holder.rl_receive_voice_viewbg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AnimationDrawable animationDrawable = (AnimationDrawable) ieaLlSinger.getBackground();
                //获取语音信号栏，开始播放动画
                animationDrawable.start();
                //播放前重置。
                MediaManager.release();
                MediaManager.playSound(messageData.getAudioUrl(), new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        animationDrawable.selectDrawable(0);//显示动画第一帧
                        animationDrawable.stop();
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return messagesDate == null ? 0 : messagesDate.size();
    }
    //    对方发送消息
    private class OtherTextHolder extends RecyclerView.ViewHolder {
        private ImageView other_headImage;
        private TextView other_time;
        private TextView other_context;
        public OtherTextHolder(View itemView) {
            super(itemView);
            other_headImage = (ImageView) itemView.findViewById(R.id.ci_meg_left_headimage);
            other_time = (TextView) itemView.findViewById(R.id.tv_meg_left_item);
            other_context = (TextView) itemView.findViewById(R.id.tv_meg_left_context);
        }
    }
    /**
     * 设置自己发送的图片信息
     * @param holder
     * @param messageData
     * @param mData
     * @param position
     */
    private void setPhotoSendeLayout(SendPhotoHolder holder, final Message messageData, final List<Message> mData, final int position) {
        if(mData.get(position).isHavePhoto()){
            // Long createDate = messageData.getCreateDate();
            //获取手机的当前时间
            long voiceTime = TimeUtils.getLongTime(messageData.getCreateDate());
            String localTime =  TimeUtils.getLocalTime(serverTime, voiceTime);
            holder.tv_send_image_time.setText(localTime);
            ImageLoaderUtils.display(mContext,holder.biv_send_image_pic,mData.get(position).getPhotoUrl());

            ImageLoaderUtils.display(mContext,holder.iv_send_image_avatar,UserInfoXml.getAvatarUrl());
//            holder.iv_send_image_avatar.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent=new Intent(mContext, UserInfoActivity.class);
//                    intent.putExtra(ConfigConstant.USERID, messageData.getId());
//                    intent.putExtra(ConfigConstant.ISRECORD,"0");
//                    intent.putExtra(ConfigConstant.SOURCETAG,"1");
//                    intent.putExtra("image",userHeadImage);
//                    mContext.startActivity(intent);
//                }
//            });
            //查看大图
            holder.biv_send_image_pic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PictureLookActivity.toPictureLookActivity(mContext,mImageposition.get(position),imageList);
                }
            });
        }
    }

    /**
     * 设置对方发过来的图片信息
     * @param holder
     * @param messageData
     * @param position
     */
    private void setPhotoReceiveLayout(ReceivePhotoHolder holder, final Message messageData, final int position) {
        if(messagesDate.get(position).isHavePhoto()){
            //Long createDate = messageData.getCreateDate();
            //获取手机的当前时间
            long voiceTime = TimeUtils.getLongTime(messageData.getCreateDate());
            String localTime =  TimeUtils.getLocalTime(serverTime, voiceTime);
            holder.tv_receive_image_time.setText(localTime);
            ImageLoaderUtils.display(mContext,holder.biv_receive_image_pic,messagesDate.get(position).getPhotoUrl());
            ImageLoaderUtils.display(mContext,holder.iv_receive_image_avatar,userHeadImage);

//            holder.iv_receive_image_avatar.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent=new Intent(mContext, UserInfoActivity.class);
//                    intent.setExtrasClassLoader(UserInfoActivity.class.getClassLoader());
//                    intent.putExtra(ConfigConstant.USERID, messageData.getId());
//                    intent.putExtra(ConfigConstant.ISRECORD,"0");
//                    intent.putExtra(ConfigConstant.SOURCETAG,"1");
//                    intent.putExtra("image",userHeadImage);
//                    mContext.startActivity(intent);
//                }
//            });
            holder.iv_receive_image_avatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(mContent, UserInfoActivity.class);
                    intent.putExtra(ConfigConstant.USERID,messagesDate.get(position).getUid());
                    intent.putExtra(ConfigConstant.ISRECORD,"0");
                    intent.putExtra(ConfigConstant.SOURCETAG,"4");
                    mContent.startActivity(intent);
                }
            });
            //查看大图
            holder.biv_receive_image_pic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if( messagesDate.get(position).isHavePhoto()) {
                       PictureLookActivity.toPictureLookActivity(mContext,mImageposition.get(position),imageList);
                    }
                }
            });
        }
    }

    //    我方发送消息
//    对方发送消息
    private   class SendTextHolder extends RecyclerView.ViewHolder{
        private ImageView send_headImage;
        private TextView send_time;
        private TextView send_context;
        public SendTextHolder(View itemView) {
            super(itemView);
            send_headImage=(ImageView)itemView.findViewById(R.id.cri_meg_right_headImage);
            send_time=(TextView)itemView.findViewById(R.id.tv_meg_right_time);
            send_context= (TextView) itemView.findViewById(R.id.tv_meg_right_context);
        }
    }
    /**
     * 对方发过来的语音Holder
     */
    private class ReceiveVoiceHolder extends RecyclerView.ViewHolder{
        private final ImageView iv_receive_voice_avatar;
        private final ImageView iv_receive_voice_pic;
        private final TextView tv_receive_voice_duration;
        private final TextView tv_receive_voice_time;
        private final RelativeLayout rl_receive_voice_viewbg;
        public ReceiveVoiceHolder(View itemView) {
            super(itemView);
            rl_receive_voice_viewbg = (RelativeLayout) itemView.findViewById(R.id.rl_receive_voice_viewbg);
            tv_receive_voice_duration = (TextView) itemView.findViewById(R.id.tv_receive_voice_duration);
            iv_receive_voice_avatar = (ImageView) itemView.findViewById(R.id.iv_receive_voice_avatar);
            tv_receive_voice_time = (TextView) itemView.findViewById(R.id.tv_receive_voice_time);
            iv_receive_voice_pic = (ImageView)itemView.findViewById(R.id.iv_receive_voice_pic);
        }
    }
    /**
     * 自己发过去的语音Holder
     */
    private class SendVoiceHolder extends RecyclerView.ViewHolder{
        private final ImageView iv_send_voice_avatar;
        private final ImageView iv_send_voice_pic;
        private final TextView tv_send_voice_time;
        private final RelativeLayout rl_send_voice_viewbg;
        private final TextView tv_send_voice_duration;
        public SendVoiceHolder(View itemView) {
            super(itemView);
            rl_send_voice_viewbg = (RelativeLayout) itemView.findViewById(R.id.rl_send_voice_viewbg);
            tv_send_voice_duration = (TextView) itemView.findViewById(R.id.tv_send_voice_duration);
            iv_send_voice_avatar = (ImageView) itemView.findViewById(R.id.iv_send_voice_avatar);
            tv_send_voice_time = (TextView) itemView.findViewById(R.id.tv_send_voice_time);
            iv_send_voice_pic = (ImageView) itemView.findViewById(R.id.iv_send_voice_pic);
        }
    }
    /**
     * 我方发送的图片消息Holder
     */
    private class SendPhotoHolder extends RecyclerView.ViewHolder{

        private final TextView tv_send_image_time;
        private final BubbleImageView biv_send_image_pic;
        private final ImageView iv_send_image_avatar;

        public SendPhotoHolder(View itemView) {
            super(itemView);
            tv_send_image_time = (TextView) itemView.findViewById(R.id.tv_send_image_time);
            biv_send_image_pic = (BubbleImageView) itemView.findViewById(R.id.biv_send_image_pic);
            iv_send_image_avatar = (ImageView) itemView.findViewById(R.id.iv_send_image_avatar);
        }
    }
    /**
     * 对方发送的图片消息Holder
     */
    private class ReceivePhotoHolder extends RecyclerView.ViewHolder{

        private final TextView tv_receive_image_time;
        private final BubbleImageView biv_receive_image_pic;
        private final ImageView iv_receive_image_avatar;

        public ReceivePhotoHolder(View itemView) {
            super(itemView);
            tv_receive_image_time = (TextView) itemView.findViewById(R.id.tv_receive_image_time);
            biv_receive_image_pic = (BubbleImageView) itemView.findViewById(R.id.biv_receive_image_pic);
            iv_receive_image_avatar = (ImageView) itemView.findViewById(R.id.iv_receive_image_avatar);
        }
    }

}
