package com.online.onenight2.message.adapter;

import android.widget.ImageView;

import com.library.utils.HeightUtils;
import com.online.onenight2.R;
import com.online.onenight2.base.adapter.BaseQuickAdapter;
import com.online.onenight2.base.adapter.BaseViewHolder;
import com.online.onenight2.bean.Chat;
import com.online.onenight2.bean.ChatListBean;
import com.online.onenight2.bean.UserBaseEnglish;
import com.online.onenight2.utils.ImageLoaderUtils;

import java.util.List;

/**
 * 描述：
 * Created by qyh on 2017/4/14.
 */

public class MessageAdapter extends BaseQuickAdapter {


    private Chat chat;
    private ChatListBean.ListChat msgData;
    private String substring;
    private String substring1;
    private String substring2;
    private String substring3;

    public MessageAdapter(int layoutResId, List data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Object item, final int position) {
        msgData = (ChatListBean.ListChat) item;
        final UserBaseEnglish userBaseEnglish = msgData.getUserBaseEnglish();
        String lastTime = msgData.getChat().getLastTime();
        if(null!=lastTime){
            substring = lastTime.substring(5, 16);
             substring1 = lastTime.substring(4, 7);
             substring2 = lastTime.substring(8, 10);
             substring3 = lastTime.substring(11, 16);
        }
        //昵称
        helper.setText(R.id.tv_meessage_item_nickname, userBaseEnglish.getNickName());
        //身高
        helper.setText(R.id.tv_message_item_height, HeightUtils.getInchCmByCm(userBaseEnglish.getHeightCm()));
        //年龄
        helper.setText(R.id.tv_message_item_age,userBaseEnglish.getAge()+" ");
        //消息时间
        helper.setText(R.id.tv_message_item_time,substring3+" "+substring2+substring1);
        chat = msgData.getChat();
        //控制 “最新回复”是否显示
        int unreadCount = chat.getUnreadCount();
        if(unreadCount>0){
//            helper.setVisible(R.id.tv_new_answer, true);
            helper.setVisible(R.id.tv_message_item_msgnum, true);
            String id = chat.getId();
//            SharedPreferenceUtil.setBooleanValue(Utils.getContext(), chat.getId(), "READED", true);
        }else{
//            helper.setVisible(R.id.tv_new_answer, false);
            helper.setVisible(R.id.tv_message_item_msgnum, false);
//            SharedPreferenceUtil.setBooleanValue(Utils.getContext(), chat.getId(), "READED", false);
        }
        //未读数
        helper.setText(R.id.tv_message_item_msgnum,String.valueOf(unreadCount));

        if(null!=userBaseEnglish.getImage()) {
            ImageLoaderUtils.display(mContext, (ImageView) helper.getView(R.id.iv_message_avar)
                    , userBaseEnglish.getImage().getThumbnailUrl());
        }
    }
}
