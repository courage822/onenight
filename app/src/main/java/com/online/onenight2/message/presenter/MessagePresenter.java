package com.online.onenight2.message.presenter;

import com.online.onenight2.base.BasePresenter;
import com.online.onenight2.message.fragment.MessageFragment;

/**
 * 描述：
 * Created by qyh on 2017/4/13.
 */

public class MessagePresenter extends BasePresenter<MessageFragment> {
//    /**
//     * 获取消息列表
//     * @param pageNum
//     * @param pageSize
//     * @param type
//     */
//    public void getMsgList(int pageNum, String pageSize, final int type){
//        OkGo.post(IUrlConstant.URL_GET_ALL_CHAT_LIST)
//                .tag(getView())
//                .params("pageNum",String.valueOf(pageNum))
//                .params("pageSize",pageSize)
//                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
//                .execute(new JsonCallback<ChatListBean>() {
//
//                    @Override
//                    public void onSuccess(ChatListBean chatListBean, Call call, Response response) {
//                        if(null!=chatListBean){
//                            getView().hideLoading();
//                            if(null!=chatListBean&&chatListBean.getIsSucceed().equals("1")) {
//                                if (chatListBean.getListChat().size() > 0) {
//                                    getView().hideLoadingPress();
//                                    switch (type) {
//                                        case RecommendListFragment.NORMAL:
//                                            getView().ShowMsgListData(chatListBean);
//                                            break;
//                                        case NearListFragment.REFRESH:
//                                            getView().ShowMsgListData(chatListBean);
//                                            break;
//                                        case NearListFragment.LOADMORE:
//                                            getView().addMsgListData(chatListBean);
//                                            break;
//                                    }
//                                } else {
//                                    //getView().showNoData();
//                                    getView().hideLoadingPress();
//                                }
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onError(Call call, Response response, Exception e) {
//                        super.onError(call, response, e);
//                        getView().hideLoadingPress();
//                    }
//                });
//         }
//
//    /**
//     * 获取最近访客列表
//     *
//     * @param pageNum
//     * @param pageSize
//     */
//    public void getVisitor(int pageNum,String pageSize){
//        OkGo.post(IUrlConstant.URL_GET_GUEST_LIST)
//              .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
//                .params("pageNum",String.valueOf(pageNum))
//                .params("pageSize",pageSize)
//                .execute(new JsonCallback<SeeMeList>() {
//
//                    @Override
//                    public void onSuccess(SeeMeList seeMeList, Call call, Response response) {
//                        if(null!=seeMeList){
//                            if(seeMeList.getTotalCount()>0) {
//                                getView().showVisitorList(seeMeList);
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onError(Call call, Response response, Exception e) {
//                        super.onError(call, response, e);
//                    }
//                });
//             }
//
//    /**
//     * 互相喜歡
//     * @param pageNum
//     * @param pageSize
//     */
//    public void getMathers(int pageNum,String pageSize){
//        OkGo.post(IUrlConstant.URL_GET_MATCHER_LIST)
//                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
//                .params("pageNum",String.valueOf(pageNum))
//                .params("pageSize",pageSize)
//                .execute(new JsonCallback<PeiDuiBean>() {
//                    @Override
//                    public void onSuccess(PeiDuiBean peiDuiBean, Call call, Response response) {
//                     if(null!=peiDuiBean){
//                         if(peiDuiBean.getIsSucceed().equals("1")){
//                             List<PeiDuiBean.MatcherList> matcherList = peiDuiBean.getMatcherList();
//                                 if(matcherList.size()>0){
//                                      getView().showPeiDuiList(peiDuiBean);
//                                 }
//                             }
//                         }
//                    }
//                });
//             }
}
