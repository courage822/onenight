package com.online.onenight2.message.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.library.dialog.OnDoubleDialogClickListener;
import com.library.utils.DialogUtil;
import com.library.utils.FileUtil;
import com.library.utils.ToastUtil;
import com.library.utils.VersionInfoUtil;
import com.lzy.okgo.OkGo;
import com.online.onenight2.R;
import com.online.onenight2.base.RxBus;
import com.online.onenight2.base.RxManager;
import com.online.onenight2.bean.BaseModel;
import com.online.onenight2.bean.Image;
import com.online.onenight2.bean.Message;
import com.online.onenight2.bean.MessageList;
import com.online.onenight2.bean.SendPhoto;
import com.online.onenight2.bean.WriteMsg;
import com.online.onenight2.callback.DialogCallback;
import com.online.onenight2.callback.JsonCallback;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.constant.IConfigConstant;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.good.ActionSheetDialog;
import com.online.onenight2.good.OnOperItemClickL;
import com.online.onenight2.message.adapter.ChatMegAdapter;
import com.online.onenight2.profile.activity.PayActivity;
import com.online.onenight2.utils.KeyBoardUtils;
import com.online.onenight2.utils.PermissionHelper;
import com.online.onenight2.utils.RecordUtil;
import com.online.onenight2.utils.Utils;
import com.online.onenight2.view.LoadingDialog;
import com.online.onenight2.xml.PlatformInfoXml;
import com.online.onenight2.xml.UserInfoXml;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Response;
import rx.functions.Action1;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;

import static com.online.onenight2.utils.TimeUtils.mContext;

/**
 * Created by foxmanman on 20174/14.
 */

public class ChatMegActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.tv_message_back)
    TextView tv_message_back;
    @BindView(R.id.iv_report)
    ImageView iv_report;//举报按钮
    @BindView(R.id.tv_message_username)
    TextView tv_message_username;//用户名
    @BindView(R.id.sr_chatmeg_refresh)
    SwipeRefreshLayout refresh_layout;//刷新
    @BindView(R.id.rl_chatmeg_content)//刷新的列表
    RecyclerView rl;
    //支付拦截
    @BindView(R.id.ll_comment)
    RelativeLayout ll_intercept;//正常发消息
    //发送语音的按钮
    @BindView(R.id.iv_chatmeg_change)
    ImageView iv_record;//文字 语音切换按钮
    //点击有更多的功能
    @BindView(R.id.iv_chatmeg_add)
    ImageView iv_add;//多功能切换按钮
    //拦截按钮
    @BindView(R.id.btn_chatmeg_reply)
    Button btn_chatmeg_reply;//拦截
    @BindView(R.id.btn_chatmeg_send)
    Button btn_chatmeg_send;//发送
    @BindView(R.id.ll_chatmeg_bottom)
    LinearLayout ll_chatmeg_bottom;//底部菜单
    @BindView(R.id.et_chatmeg_edit)
    EditText et_chatmeg_edit;//发送文字显示
    @BindView(R.id.ll_chatmeg_add_function)
    LinearLayout ll_chatmeg_add_function;
    @BindView(R.id.tv_chatmeg_photo)
    TextView tv_chatmeg_photo;//照片
    @BindView(R.id.tv_chatmeg_truth)
    TextView tv_chatmeg_truth;//我的真实情况
    @BindView(R.id.tv_chatmeg_about)
    TextView tv_chatmeg_about;//愛的问答
    //聊天列表中图片的位置，和别的消息区分
    private int imageCurrentPosition = 0;
    private Image image;
    public HashMap<Integer, Integer> imagePosition = new HashMap<Integer, Integer>();//图片下标位置
    //本地原图
    private File phototFile;
    //拍照文件大小
    private long fileLength;
    //发送语音的长按按钮
    @BindView(R.id.bu_chatmsg_say)
    Button bu_chatmsg_say;
    //图片结合
    private List<Image> imageList = new ArrayList<>();
    private  String uid;
    private String nicename;
    private String userAvatar;
    private ChatMegAdapter mChatMegAdapter;
    private List<Message> chatMess=new ArrayList<>();
    private long serveiceTime;
    private PermissionHelper permissionHelper;
    private LinearLayoutManager mLayoutManager;
    private static final int PERMISSION_CODE_READ_EXTERNAL_STORAGE = 0;
    private static final int MSG_TYPE_TIMER = 1;
    private AnimationDrawable  animationDrawable;
    private static final int UP_PHOTO_LOCAL = 1;
    private static final int UP_PHOTO_CAMERA = 2;
    private static final int UP_PHOTO_CROP = 3;
    //ios风格的对话框
    private ActionSheetDialog dialog;
    private String[] stringItems;
    // 录音框
    private PopupWindow mRecordPopupWindow;
    // 录音文件路径
    private String mOutputPath;
    // 录音文件名
    private String mRecordFileName;
    // 是否正在录音
    private boolean mIsRecording;
    // 录音是否被取消
    private boolean mIsRecordCanceled;
    // 录音时长（毫秒）
    private long mRecordDuration;
    // 录音计时刷新间隔（毫秒）
    private final long mRefreshInterval = 250;
    private boolean isPermission=false;
    private boolean isOpenEditText=true;
    private boolean isShowSendVipByPraise = false;
    // 计算录音时长的Handler
    private Handler mTimerHandler = new Handler() {

        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_TYPE_TIMER:
                    if (mIsRecording) {
                        // 计时
                        mRecordDuration += mRefreshInterval;
                        // 录音超过1分钟自动发送
                        if (mRecordDuration > 60 * 1000) {
                            //stopRecordAndSend();
                        } else {
                            mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_TIMER, mRefreshInterval);
                        }
                    }
                    break;
            }
        }
    };
    private boolean isFresh;


    // 录音状态常量
    private enum STATE {
        /**
         * 默认值
         */
        IDLE,
        /**
         * 录音中
         */
        RECORDING,
        /**
         * 取消
         */
        CANCELED,
        /**
         * 录音时间太短
         */
        TOO_SHORT
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatmeg);
        ButterKnife.bind(this);
        initView();
        initData();
    }
    public void initView() {
        //在聊天界面，不推送 好友聊天消息
        RxBus.getInstance().post(ConfigConstant.ISCHAT,true);
        tv_message_back.setOnClickListener(this);
        iv_report.setOnClickListener(this);
        iv_add.setOnClickListener(this);
        tv_chatmeg_photo.setOnClickListener(this);
        tv_chatmeg_truth.setOnClickListener(this);
        tv_chatmeg_about.setOnClickListener(this);
        //发送
        btn_chatmeg_send.setOnClickListener(this);
        btn_chatmeg_reply.setOnClickListener(this);
        // 键盘切换
        iv_record.setOnClickListener(this);
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rl.setLayoutManager(mLayoutManager);
        rl.setHasFixedSize(true);
        mChatMegAdapter=new ChatMegAdapter(this);
        rl.setAdapter(mChatMegAdapter);
        //初始化语音对话框
        et_chatmeg_edit.setFocusable(false);
        et_chatmeg_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                btn_chatmeg_send.setEnabled(false);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!TextUtils.isEmpty(et_chatmeg_edit.getText().toString().trim())){
                    btn_chatmeg_send.setVisibility(View.VISIBLE);
                    btn_chatmeg_send.setEnabled(true);
                }else {
                    btn_chatmeg_send.setVisibility(View.GONE);
                    btn_chatmeg_send.setEnabled(false);
                }
            }
        });
        et_chatmeg_edit.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                et_chatmeg_edit.setFocusable(true);
                et_chatmeg_edit.setFocusableInTouchMode(true);
                et_chatmeg_edit.requestFocus();
                ll_chatmeg_add_function.setVisibility(View.GONE);
                KeyBoardUtils.openKeybord(et_chatmeg_edit, mContext);
                return false;
            }
        });
        refresh_layout.setColorSchemeResources(R.color.main_color,R.color.main_color,R.color.colorPrimary,R.color.colorPrimary);
        refresh_layout.setOnRefreshListener(this);
        // 发送语音消息
        bu_chatmsg_say.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // 检查权限
                if (isPermission) {// 请求权限时不录音
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:// 按下时开始录音
                            showRecordPopupWindow(STATE.RECORDING);
                            startRecord();
                            mIsRecordCanceled = false;
                            break;
                        case MotionEvent.ACTION_MOVE:
                            if (event.getY() < -100) { // 上滑取消发送
                                mIsRecordCanceled = true;
                                showRecordPopupWindow(STATE.CANCELED);
                            } else {
                                mIsRecordCanceled = false;
                                showRecordPopupWindow(STATE.RECORDING);
                            }
                            break;

                        case MotionEvent.ACTION_UP:// 松开时停止录音并发送
                            if (mIsRecordCanceled) {
                                cancelRecord();
                            } else {
                                stopRecordAndSend();
                            }
                            break;
                    }
                }
                return true;
            }
        });

    }

    public void initData() {
        Intent intent=getIntent();
        if(intent!=null){
            uid=intent.getStringExtra(ConfigConstant.USERID);
            nicename=intent.getStringExtra(ConfigConstant.NICKNAME);
            userAvatar=intent.getStringExtra(ConfigConstant.USERAVATAR);
            tv_message_username.setText(nicename);
        }
       showProgress();
        isFresh=true;
       ChatMessageList(uid,isFresh);
   }

    @Override
    protected void onResume() {
        super.onResume();
        RxManager rxManager=new RxManager();
        rxManager.on("changePay", new Action1<String>() {
            @Override
            public void call(String s) {
                showProgress();
                isFresh=true;
                ChatMessageList(uid,isFresh);
            }
        });
//
//        InputMethodManager  manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//
//
//        @Override
//        public boolean onTouchEvent(MotionEvent event) {
//            // TODO Auto-generated method stub
//            if(event.getAction() == MotionEvent.ACTION_DOWN){
//                if(getCurrentFocus()!=null && getCurrentFocus().getWindowToken()!=null){
//                    manager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//                }
//            }
//            return super.onTouchEvent(event);
//        }
//        getWindow().getDecorView().addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
//            @Override
//            public void onLayoutChange(View v, int left, int top, int right, int bottom,
//                                       int oldLeft, int oldTop, int oldRight, int oldBottom) {
//                //获取View可见区域的bottom
//                Rect rect = new Rect();
//                getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
//                if (bottom != 0 && oldBottom != 0 && bottom - rect.bottom <= 0) {
//                    //隐藏
//                } else {
//                    //弹出键盘
//                    if (null != chatMess && chatMess.size() > 0) {
//                        rl.scrollToPosition(chatMess.size() - 1);
//                    }
//                }
//            }
//        });
        // 处理滑动冲突
        rl.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // 触摸聊天列表时若键盘是打开的，则隐藏键盘
                KeyBoardUtils.closeKeybord(et_chatmeg_edit, mContext);
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        // 禁用下拉刷新
                        rl.requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                        // 恢复下拉刷新
                        rl.requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });
        // 处理滑动冲突
        rl.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                // 判断是否滑动到顶部
                int scrolledPosition = ( rl == null ||  rl.getChildCount() == 0) ? 0 :  rl.getChildAt(0).getTop();
                refresh_layout.setEnabled(scrolledPosition >= 0);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_chatmeg_send:
             ChatMessageWirte(uid,"1",et_chatmeg_edit.getText().toString());
                break;
            case R.id.tv_message_back:
                finish();
                KeyBoardUtils.closeKeybord(et_chatmeg_edit, mContext);
                break;
            case R.id.iv_report:
                break;
            case R.id.iv_chatmeg_change://键盘切换
                showOrHideSayButton();
                break;
            case R.id.iv_chatmeg_add:
//                if(isHide){
//                    ll_chatmeg_add_function.setVisibility(View.VISIBLE);
//                    isHide=false;
//                }else{
//                    ll_chatmeg_add_function.setVisibility(View.GONE);
//                    isHide=true;
//                    KeyBoardUtils.closeKeybord(et_chatmeg_edit, mContext);
//                }
                 showChoseDialog();
                break;
            case R.id.tv_chatmeg_photo:
               // showChoseDialog();
                break;
            case R.id.tv_chatmeg_truth:
                break;
            case R.id.tv_chatmeg_about:
                break;
            case R.id.btn_chatmeg_reply:
                KeyBoardUtils.closeKeybord(et_chatmeg_edit, mContext);
                if(isShowSendVipByPraise){
                    actionSheetDialogNoTitle();
                }else{
                    startActivity(new Intent(ChatMegActivity.this, PayActivity.class));
                }
                break;

        }
    }

    //切换语音按钮显示隐藏
    private void showOrHideSayButton() {
        if(isOpenEditText){
            getPermissions();
            et_chatmeg_edit.clearFocus();
            bu_chatmsg_say.setVisibility(View.VISIBLE);
            et_chatmeg_edit.setVisibility(View.GONE);
            KeyBoardUtils.closeKeybord(et_chatmeg_edit, this);
            isOpenEditText=false;
        }else {
            bu_chatmsg_say.setVisibility(View.GONE);
            et_chatmeg_edit.setVisibility(View.VISIBLE);
            et_chatmeg_edit.requestFocus();
            et_chatmeg_edit.setFocusable(true);
            et_chatmeg_edit.setFocusableInTouchMode(true);
            KeyBoardUtils.openKeybord(et_chatmeg_edit, this);
            isOpenEditText=true;
        }
        //修改图标
        iv_record.setImageResource(bu_chatmsg_say.getVisibility() == View.VISIBLE ? R.drawable.msg_keyboard_icon : R.drawable.msg_siri_icon);
    }



    public void getMessageListSuccess(MessageList messageList,boolean isfresh){
        hideProgress();
        if(messageList!=null){
            serveiceTime=messageList.getSystemTime();
            String showWriteMsgIntercept = messageList.getShowWriteMsgIntercept();
            chatMess = messageList.getListMsg();
            if(chatMess!=null&&chatMess.size()>0){
                setChatMegAadapterDate(messageList.getListMsg(),serveiceTime);
//                 rl.scrollToPosition(chatMess.size()-1);
                rl.scrollToPosition(0);
            }
            if(!TextUtils.isEmpty(showWriteMsgIntercept)&&"1".equals(showWriteMsgIntercept)){
                  btn_chatmeg_reply.setVisibility(View.VISIBLE);
            }
            String isShowSendVipByPraise2 = messageList.getIsShowSendVipByPraise();
            if(!isfresh){
                // 滚动到底部
                mLayoutManager.scrollToPosition(mLayoutManager.getItemCount() - 1);
            }

            if(!TextUtils.isEmpty(isShowSendVipByPraise2)){
                // 设置五星好评
                if (isShowSendVipByPraise2.equals("1")) {
                    isShowSendVipByPraise = true;
                } else {
                    isShowSendVipByPraise = false;
                }
            }
        }
    }

    public void getPermissions(){
        permissionHelper = new PermissionHelper(this);
        permissionHelper.requestPermissions(this.getString(R.string.recod_tips),
                new PermissionHelper.PermissionListener() {
                    @Override
                    public void doAfterGrand(String... permission) {
                             isPermission=true;
                    }
                    @Override
                    public void doAfterDenied(String... permission) {
                        ToastUtil.showLongToast(mContext, mContext.getString(R.string.no_record_tips));
                    }
                }, Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CODE_READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePhoto();
            }
        }
        permissionHelper.handleRequestPermissionsResult(requestCode, permissions, grantResults);

    }
    private void setChatMegAadapterDate(List<Message> msg, long serveiceTime) {
        //获取消息列表的长度，并且遍历
        int liarSize = msg.size();
        for (int i = 0; i < liarSize; i++) {
            if (msg.get(i).isHavePhoto()) {
                image = msg.get(i).getChatImage();
                if (!imageList.contains(image)) {
                    imageList.add(image);
                }
                //图片的角标从0 一值累加，与其他消息position无关
                imagePosition.put(i, imageCurrentPosition);
                imageCurrentPosition++;
            }
        }
        mChatMegAdapter.setDate(serveiceTime,userAvatar,msg, imagePosition, imageList);
        mChatMegAdapter.notifyDataSetChanged();
        // 滚动到底部
        mLayoutManager.scrollToPosition(mLayoutManager.getItemCount() - 1);
    }

    //发送文字消息成功

    public void sendMessageSuccess() {
        et_chatmeg_edit.setText("");
        isFresh=false;
        ChatMessageList(uid,isFresh);
    }
    //发送文字消息失败
    public void sendMessageFail() {
        ToastUtil.showShortToast(ChatMegActivity.this,getString(R.string.chatge_send_message_fail));
        hideProgress();
    }

    //发送语音成功

    public void sendRecordSuccess() {
        showProgress();
        isFresh=false;
      ChatMessageList(uid,isFresh);
    }


    //刷新
    @Override
    public void onRefresh() {
        isFresh=true;
       ChatMessageList(uid,isFresh);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxBus.getInstance().post(ConfigConstant.ISCHAT,false);

    }

    /**
     * 开始录音
     */
    private void startRecord() {
        // 录音开始前震动一下
        Vibrator vibrator = (Vibrator) ChatMegActivity.this.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(88);
        // 根据系统时间生成文件名
        mRecordFileName = "onenight-" + FileUtil.createFileNameByTime() + ".mp3";
        // 录音文件保存路径: 根目录/Amor/record/用户id/xxx.mp3
        mOutputPath = FileUtil.RECORD_DIRECTORY_PATH + File.separator + UserInfoXml.getUID() + File.separator + mRecordFileName;
        // 开始录音
        RecordUtil.getInstance().startRecord(mOutputPath);
        mIsRecording = true;
        mRecordDuration = 0;
        // 计时，间隔250毫秒刷新
        mTimerHandler.sendEmptyMessageDelayed(MSG_TYPE_TIMER, mRefreshInterval);
    }

    /**
     * 停止录音并发送
     */
    private void stopRecordAndSend() {
        if (mIsRecording) {
            mIsRecording = false;
            if (mRecordDuration < 1 * 1000) {// 录音时长小于1秒的不发送
                showRecordPopupWindow(STATE.TOO_SHORT);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // 延时500毫秒调用MediaRecorder#stop()，防止出现start()之后立马调用stop()的异常
                        RecordUtil.getInstance().stopRecord();
                        // 删除小于1秒的文件
                        FileUtil.deleteFile(mOutputPath);
                    }
                }, 500);
            } else {// 发送语音
                RecordUtil.getInstance().stopRecord();
               sendRecordMsg(uid,mRecordDuration,mOutputPath);
            }
            // 隐藏录音框
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mRecordPopupWindow != null && mRecordPopupWindow.isShowing()) {
                        mRecordPopupWindow.dismiss();
                    }
                }
            }, 500);
        }
    }

    /**
     * 录音取消，删除已经录制的文件
     */
    private void cancelRecord() {
        if (mIsRecordCanceled) {
            RecordUtil.getInstance().stopRecord();
            mIsRecordCanceled = false;
            FileUtil.deleteFile(mOutputPath);

            // 隐藏录音框
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mRecordPopupWindow != null && mRecordPopupWindow.isShowing()) {
                        mRecordPopupWindow.dismiss();
                    }
                }
            }, 500);
        }
    }


    /**
     * 弹出录音框
     */
    private void showRecordPopupWindow(STATE state) {
        if (mRecordPopupWindow == null) {
            View contentView = LayoutInflater.from(ChatMegActivity.this).inflate(R.layout.popup_chat_record, null);
            mRecordPopupWindow = new PopupWindow(contentView, WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT, true);
        }

        View view = mRecordPopupWindow.getContentView();
        if (view != null) {
            RelativeLayout rlRecording = (RelativeLayout) view.findViewById(R.id.popup_recording_container);
            ImageView ivRecording = (ImageView) view.findViewById(R.id.popup_record_anim);
            List<Drawable> drawableList = new ArrayList<Drawable>();
            drawableList.add(getResources().getDrawable(R.mipmap.v1));
            drawableList.add(getResources().getDrawable(R.mipmap.v2));
            drawableList.add(getResources().getDrawable(R.mipmap.v3));
            drawableList.add(getResources().getDrawable(R.mipmap.v4));
            drawableList.add(getResources().getDrawable(R.mipmap.v5));
            drawableList.add(getResources().getDrawable(R.mipmap.v6));
            drawableList.add(getResources().getDrawable(R.mipmap.v7));
           AnimationDrawable animationDrawable = Utils.getFrameAnim(drawableList, true, 150);
             ivRecording.setImageDrawable(animationDrawable);
            ImageView ivCancel = (ImageView) view.findViewById(R.id.popup_cancel_record);
            ImageView ivTooShort = (ImageView) view.findViewById(R.id.popup_record_too_short);
            TextView tvState = (TextView) view.findViewById(R.id.popup_record_state);

            switch (state) {
                case RECORDING: // 正在录音
                   rlRecording.setVisibility(View.VISIBLE);
//                    // 播放动画
                     animationDrawable.start();
                    ivCancel.setVisibility(View.GONE);
                    ivTooShort.setVisibility(View.GONE);
                    tvState.setText(getString(R.string.up_for_cancel));
                    break;

                case CANCELED: // 取消录音
                    rlRecording.setVisibility(View.GONE);
                    // 停止动画
                    if (animationDrawable.isRunning()) {
                        animationDrawable.stop();
                    }
                    ivCancel.setVisibility(View.VISIBLE);
                    ivTooShort.setVisibility(View.GONE);
                    tvState.setText(getString(R.string.want_to_cancle));
                    break;

                case TOO_SHORT:// 录音时间太短
                    rlRecording.setVisibility(View.GONE);
                    // 停止动画
                    if (animationDrawable.isRunning()) {
                        animationDrawable.stop();
                    }
                    ivCancel.setVisibility(View.GONE);
                    ivTooShort.setVisibility(View.VISIBLE);
                    tvState.setText(getString(R.string.time_too_short));
                    break;
            }
        }
        mRecordPopupWindow.showAtLocation(rl, Gravity.CENTER, 0, 0);
    }


    /**
     * 获取消息列表
     * @param uid
     * @param isFresh
     */
    public void ChatMessageList(String uid,boolean isFresh){
        OkGo.post(IUrlConstant.URL_GET_CHAT_MSG)
                .headers("token", PlatformInfoXml.getToken())
                .params("platformInfo",PlatformInfoXml.getPlatformJsonString())
                .params("uid",uid)
                .execute(new ChatMessageCallBack());
    }
    public class ChatMessageCallBack extends JsonCallback<MessageList> {

        @Override
        public void onSuccess(MessageList messageList, Call call, Response response) {
           if(messageList!=null){
               if(!TextUtils.isEmpty(messageList.getIsSucceed())&&messageList.getIsSucceed().equals("1")){
                getMessageListSuccess(messageList,isFresh);
               }
           }
        }
        @Override
        public void onError(Call call, Response response, Exception e) {
            hideProgress();
            e.printStackTrace();
        }
    }

    public void showProgress() {
        LoadingDialog.showDialogForLoading(ChatMegActivity.this);
    }


    public void hideProgress() {
        refresh_layout.setRefreshing(false);
        LoadingDialog.cancelDialogForLoading(ChatMegActivity.this);
    }

    /**
     * 发送文字消息
     * @param uid
     * @param type
     * @param content
     */
    public void ChatMessageWirte(String uid,String type,String content){
        OkGo.post(IUrlConstant.URL_SEND_TEXT_MSG)
                .params("platformInfo",PlatformInfoXml.getPlatformJsonString())
                .params("uid",uid)
                .params("writeMsgType",type)
                .params("content",content)
                .execute(new ChatMessageWirteCallBack(this));
    }
    private class ChatMessageWirteCallBack extends DialogCallback<WriteMsg> {
        public ChatMessageWirteCallBack(Activity activity) {
            super(activity);
        }

        @Override
        public void onSuccess(WriteMsg writeMsg, Call call, Response response) {
            if(writeMsg!=null){
                if(!TextUtils.isEmpty(writeMsg.getIsSucceed())&&"1".equals(writeMsg.getIsSucceed())){
                     sendMessageSuccess();
                    hideProgress();

                }else {
                    sendMessageFail();
                 hideProgress();
                }
            }else {
                  sendMessageFail();
                     hideProgress();
            }
        }

        @Override
        public void onError(Call call, Response response, Exception e) {
            super.onError(call, response, e);
               sendMessageFail();
        }
    }
    /**
     * 发送语音消息
     * @param uid
     * @param seconds
     * @param filePath
     */
    public  void sendRecordMsg(String uid,float seconds,String filePath){
        String secondString = String.valueOf(Math.round(seconds));
        OkGo.post(IUrlConstant.URL_SEND_VOICE_MSG)
                .headers("uid", uid)
                .headers("writeMsgType", "1")
                .headers("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .headers("audioSecond",  String.valueOf(Math.round(mRecordDuration / 1000)))
                .params("file",new File(filePath))
                .execute(    new JsonCallback<BaseModel>() {
                    @Override
                    public void onSuccess(BaseModel baseModel, Call call, Response response) {
                        if(null!=baseModel&&baseModel.getIsSucceed().equals("1")){
                             sendRecordSuccess();
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                         hideProgress();
                    }
                });
    }
    //拍照对话框
    private void showChoseDialog() {
        stringItems = new String[]{getString(R.string.profile_benditupian), getString(R.string.profile_paizhao)};
        dialog = new ActionSheetDialog(ChatMegActivity.this, stringItems, null);
        dialog.isTitleShow(false).show();
        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                   selectPhoto();
                } else if (position == 1) {
                    takePhoto();
                    dialog.dismiss();
                }
            }
        });
    }
    public void takePhoto(){
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            // 表示对目标应用临时授权该Uri所代表的文件，7.0及以上
            takePhotoIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        //设置照片的临时保存路径
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, getUriFromFile(mContext, new File(IConfigConstant.LOCAL_PIC_PATH)));
        startActivityForResult(takePhotoIntent, UP_PHOTO_CAMERA);
    }
private  void selectPhoto(){
    Intent localIntent = new Intent(Intent.ACTION_PICK);
    localIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
    localIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
    localIntent.setType("image/*");
    startActivityForResult(localIntent, UP_PHOTO_LOCAL);
}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case UP_PHOTO_LOCAL:
                if (data != null) {
                    Uri localuri = data.getData();
                    cropImage(localuri);
                }
                break;
            case UP_PHOTO_CAMERA:
                if (data != null) {
                    Uri uri = data.getData();
                    if (uri != null) {
                        cropImage(uri);
                    } else {
                        // 拍照后，7.0以上系统读取图片时需要转换路径读取方式
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            cropImage(getUriFromFile(mContext, new File(IConfigConstant.LOCAL_PIC_PATH)));
                        } else {
                            cropImage(Uri.fromFile(new File(IConfigConstant.LOCAL_PIC_PATH)));
                        }
                    }
                } else {
                    // 拍照后，7.0以上系统读取图片时需要转换路径读取方式
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        cropImage(getUriFromFile(mContext, new File(IConfigConstant.LOCAL_PIC_PATH)));
                    } else {
                        cropImage(Uri.fromFile(new File(IConfigConstant.LOCAL_PIC_PATH)));
                    }
                }
                break;
            case UP_PHOTO_CROP:
                phototFile = new File(IConfigConstant.LOCAL_PIC_PATH);
                compressPhoto(phototFile);
                break;
        }
    }

    //裁剪图片
    private void cropImage(Uri localuri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(localuri, "image/*");
        intent.putExtra("crop", "true");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        // aspectX aspectY 是裁剪框宽高的比例
        intent.putExtra("aspectX", 3);
        intent.putExtra("aspectY", 4);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(IConfigConstant.LOCAL_PIC_PATH)));
        intent.putExtra("return-data", false);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        startActivityForResult(intent, UP_PHOTO_CROP);
    }
    /**
     * 根据file获得Uri（适配7.0及以上）
     *
     * @param file
     */
    private Uri getUriFromFile(Context context, File file) {
        Uri imageUri = null;
        if (context != null && file != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                // 7.0及以上因为安全问题，需要通过Content Provider封装Uri对象
                imageUri = FileProvider.getUriForFile(mContext, "com.online.onenight.provider", file);
            } else {
                imageUri = Uri.fromFile(file);
            }
        }
        return imageUri;
    }
    //图片压缩
    private void compressPhoto(File data) {
        Luban.get(this)
                .load(data)   //传入要压缩的图片
                .putGear(Luban.THIRD_GEAR)      //设定压缩档次，默认三挡
                .setCompressListener(new OnCompressListener() { //设置回调
                    @Override
                    public void onStart() {
                        showProgress();
                    }
                    @Override
                    public void onSuccess(File file) {
                        upPhotoToTalk(uid, file);
                        dialog.dismiss();
                    }
                    @Override
                    public void onError(Throwable e) {
                          dialog.dismiss();
                            upPhotoToTalk(uid, phototFile);
                    }
                }).launch();//启动压缩
    }
private void upPhotoToTalk(String uid,File file){
    OkGo.post(IUrlConstant.URL_SEND_PHOTO)
            .headers("token", PlatformInfoXml.getToken())
            .headers("platformInfo", PlatformInfoXml.getPlatformJsonString())
            .headers("recevUserId",  uid)
            .headers("writeMsgType", "1")
            .params("file" ,file)
            .execute(new UpPhotoCallBack());
}
private class UpPhotoCallBack extends JsonCallback<SendPhoto>{

    @Override
    public void onSuccess(SendPhoto sendPhoto, Call call, Response response) {
        if(null!=response){
            String isSucceed = sendPhoto.getIsSucceed();
            if(!TextUtils.isEmpty(isSucceed)&&isSucceed.equals("1")){
                //发送图片成功的回调
                 sendPhotoSuccess(sendPhoto);
            }else{
                //发送图片失败的回调
                sendWriteFail();
            }
        }
    }

    @Override
    public void onError(Call call, Response response, Exception e) {
        super.onError(call, response, e);
        sendWriteFail();
    }
}

    public void sendPhotoSuccess(SendPhoto data) {
        isFresh=false;
         ChatMessageList(uid,isFresh);
        hideProgress();
    }
    //发送文本消息 失败
    public void sendWriteFail() {
        ToastUtil.showShortToast(mContext, mContext.getString(R.string.send_fail));
        hideProgress();
    }
    //五星好评
    private void actionSheetDialogNoTitle() {
        final String[] stringItems = {getString(R.string.free_use), getString(R.string.buy_vip_service)};
        final ActionSheetDialog dialog = new ActionSheetDialog(ChatMegActivity.this, stringItems, null);
        dialog.isTitleShow(false).show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    DialogUtil.showDoubleBtnDialog(getSupportFragmentManager(), getString(R.string.hour12_vip), getString(R.string.send_vip), getString(R.string.go_good), getString(R.string.change_cancel), false, new OnDoubleDialogClickListener() {
                        @Override
                        public void onPositiveClick(View view) {
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("market://details?id=" + VersionInfoUtil.getPackageName(ChatMegActivity.this)));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            if (VersionInfoUtil.checkAPKExist(ChatMegActivity.this, "com.android.vending")) {
                                // 直接跳转Google商店
                                intent.setPackage("com.android.vending");
                            }
                            startActivity(intent);
                        }

                        @Override
                        public void onNegativeClick(View view) {

                        }
                    });
                } else if (position == 1) {
                    startActivity(new Intent(mContext, PayActivity.class));
                }
                dialog.dismiss();
            }
        });
    }
}