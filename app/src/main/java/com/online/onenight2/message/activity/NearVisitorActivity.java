package com.online.onenight2.message.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.lzy.okgo.OkGo;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseActivity;
import com.online.onenight2.bean.SeeMe;
import com.online.onenight2.bean.SeeMeList;
import com.online.onenight2.bean.UserBase;
import com.online.onenight2.callback.JsonCallback;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.hot.activity.UserInfoActivity;
import com.online.onenight2.math.adapter.PraisedListAdapter;
import com.online.onenight2.message.adapter.NearVisitorAdapter;
import com.online.onenight2.message.presenter.NearVisitorPresenter;
import com.online.onenight2.view.MyDecoration;
import com.online.onenight2.view.refresh.NormalRefreshViewHolder;
import com.online.onenight2.view.refresh.RefreshLayout;
import com.online.onenight2.xml.PlatformInfoXml;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Response;

/**
 * 描述：最近访客界面
 * Created by qyh on 2017/5/3.
 */

public class NearVisitorActivity extends BaseActivity<NearVisitorPresenter> implements RefreshLayout.RefreshLayoutDelegate {
    private static int PAGE = 1;
    //获取数据多少
    private static final String PAGESIZE = "10";
    // 正常请求
    public static final int NORMAL = 1;
    // 刷新请求
    public static final int REFRESH = 2;
    // 加载更多请求
    public static final int LOADMORE = 3;
    @BindView(R.id.rc_hot_content)
    RecyclerView rcHotContent;
    @BindView(R.id.refresh)
    RefreshLayout refresh;
    @BindView(R.id.tv_appbar_back)
    TextView tvAppbarBack;
    @BindView(R.id.tv_appbar_title)
    TextView tvAppbarTitle;
    private LinearLayoutManager mLinearLayoutManager;
    private NearVisitorAdapter mAdapter;
    private List<SeeMe> seeMeListData;

    @Override
    public int getLayoutId() {
        return R.layout.activity_praised_list;
    }

    @Override
    public NearVisitorPresenter newPresenter() {
        return new NearVisitorPresenter();
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        tvAppbarBack.setText(R.string.back);
        tvAppbarTitle.setText(R.string.near_visitor);
        mAdapter = new NearVisitorAdapter(mContext);
        mLinearLayoutManager = new LinearLayoutManager(mContext);
        rcHotContent.setLayoutManager(mLinearLayoutManager);
        rcHotContent.addItemDecoration(new MyDecoration(mContext, MyDecoration.VERTICAL_LIST));
        rcHotContent.setHasFixedSize(true);
        rcHotContent.setAdapter(mAdapter);
        //设置下拉、上拉
        refresh.setDelegate(this);
        refresh.setRefreshViewHolder(new NormalRefreshViewHolder(mContext, true));
        rcHotContent.addOnScrollListener(mOnScrollListener);
        mAdapter.setOnItemClickListener(new PraisedListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                UserBase userBaseEnglish = seeMeListData.get(position).getUserBaseEnglish();
                Intent intent=new Intent(mContext, UserInfoActivity.class);
                intent.putExtra(ConfigConstant.USERID, String.valueOf(userBaseEnglish.getId()));
                intent.putExtra(ConfigConstant.ISRECORD,"0");
                intent.putExtra(ConfigConstant.SOURCETAG,"1");
                intent.putExtra("image",userBaseEnglish.getImage().getImageUrl());
                mContext.startActivity(intent);
                overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
            }
        });
    }

    @Override
    public void initData() {
        Intent intent = getIntent();
        if(null!=intent){
            SeeMeList seeMeList = (SeeMeList) intent.getSerializableExtra("visitor");
            seeMeListData = seeMeList.getSeeMeList();
            mAdapter.setData(seeMeListData);
        }
    }

    //访客列表
    public void showVisitorList(List<SeeMe> list) {
         mAdapter.setData(list);
        refresh.endRefreshing();
        System.out.println("時間========="+list.get(0).getTime());
        String lastTime = list.get(0).getTime();
        if(null!=lastTime){
           String substring = lastTime.substring(5, 16);
            String substring1 = lastTime.substring(4, 7);
            String substring2 = lastTime.substring(8, 10);
            String substring3 = lastTime.substring(11, 16);
            System.out.println("substring===="+substring);
        }
    }

    public void loadMoreData(List<SeeMe> list){
        seeMeListData.addAll(list);
        mAdapter.setData(seeMeListData);
    }
    // 监听适配器的滑动，完成上拉加载的功能
    public RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
        //最后一个角标位置
        private int lastVisibleItemPosition;

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            lastVisibleItemPosition = mLinearLayoutManager.findLastVisibleItemPosition();
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mAdapter.getItemCount()) {
                PAGE++;
              getVisitor(PAGE,PAGESIZE,LOADMORE);
            }
        }
    };

    @Override
    public void onRefreshLayoutBeginRefreshing(RefreshLayout refreshLayout) {
        PAGE=1;
       getVisitor(PAGE,PAGESIZE,REFRESH);
    }

    @Override
    public boolean onRefreshLayoutBeginLoadingMore(RefreshLayout refreshLayout) {
        return false;
    }
    @OnClick({R.id.tv_appbar_back})
    public void  click(TextView v){
        switch (v.getId()){
            case R.id.tv_appbar_back:
                finish();
                break;
        }
    }
    /**
     * 获取最近访客列表
     *
     * @param pageNum
     * @param pageSize
     */
    public void getVisitor(int pageNum, String pageSize, final int type){
        OkGo.post(IUrlConstant.URL_GET_GUEST_LIST)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("pageNum",String.valueOf(pageNum))
                .params("pageSize",pageSize)
                .execute(new JsonCallback<SeeMeList>() {

                    @Override
                    public void onSuccess(SeeMeList seeMeList, Call call, Response response) {
                        if(null!=seeMeList){
                            if(seeMeList.getTotalCount()>0) {
                                List<SeeMe> seeMeListData = seeMeList.getSeeMeList();
                                switch (type){
                                    case NearVisitorActivity.REFRESH:
                                        showVisitorList(seeMeListData);
                                        break;
                                    case NearVisitorActivity.LOADMORE:
                                        loadMoreData(seeMeListData);
                                        break;
                                }

                            }
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                    }
                });
    }
}
