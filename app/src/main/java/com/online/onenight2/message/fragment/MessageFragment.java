package com.online.onenight2.message.fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.library.dialog.OnDoubleDialogClickListener;
import com.library.utils.DialogUtil;
import com.library.utils.LogUtil;
import com.library.utils.ToastUtil;
import com.lzy.okgo.OkGo;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseFragment;
import com.online.onenight2.base.RxBus;
import com.online.onenight2.base.RxManager;
import com.online.onenight2.base.adapter.BaseQuickAdapter;
import com.online.onenight2.base.adapter.OnItemClickListener;
import com.online.onenight2.bean.BaseModel;
import com.online.onenight2.bean.Chat;
import com.online.onenight2.bean.ChatListBean;
import com.online.onenight2.bean.Image;
import com.online.onenight2.bean.PeiDuiBean;
import com.online.onenight2.bean.SeeMe;
import com.online.onenight2.bean.SeeMeList;
import com.online.onenight2.bean.UserBaseEnglish;
import com.online.onenight2.callback.JsonCallback;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.hot.fragment.NearListFragment;
import com.online.onenight2.hot.fragment.RecommendListFragment;
import com.online.onenight2.message.activity.ChatMegActivity;
import com.online.onenight2.message.activity.NearVisitorActivity;
import com.online.onenight2.message.activity.PeiDuiActivity;
import com.online.onenight2.message.adapter.MessageAdapter;
import com.online.onenight2.message.presenter.MessagePresenter;
import com.online.onenight2.utils.ImageLoaderUtils;
import com.online.onenight2.view.MyDecoration;
import com.online.onenight2.view.refresh.NormalRefreshViewHolder;
import com.online.onenight2.view.refresh.RefreshLayout;
import com.online.onenight2.xml.PlatformInfoXml;

//import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.Unbinder;
import okhttp3.Call;
import okhttp3.Response;
import rx.functions.Action1;

/**
 * 描述：
 * Created by qyh on 2017/3/24.
 */

public class MessageFragment extends BaseFragment<MessagePresenter> implements RefreshLayout.RefreshLayoutDelegate, View.OnClickListener {
    @BindView(R.id.tv_appbar_title)
    TextView tvAppbarTitle;
    @BindView(R.id.rv_message_list)
    RecyclerView rvMessageList;
    @BindView(R.id.refresh)
    RefreshLayout refresh;
    // 正常请求
    public static final int NORMAL = 1;
    // 刷新请求
    public static final int REFRESH = 2;
    // 加载更多请求
    public static final int LOADMORE = 3;
    Unbinder unbinder;
    private int pageNum = 1;
    private static final int MSGREQUESTCODE = 100;
    private static final int LIKEREQUESTCODE = 200;
    private static final int VISITORREQUESTCODE = 300;
    private static final String PAGESIZE = "100";
    private MessageAdapter mMessageAdapter;
    //未读消息总数
    private int totalUnread;
    private List<ChatListBean.ListChat> listChat;
    private View headView;
    private RelativeLayout rl_message_look_me;
    private RelativeLayout rl_message_item_like;
    private ImageView iv_visitor_one;
    private ImageView iv_visitor_two;
    private ImageView iv_visitor_three;
    private LinearLayoutManager mLinearLayoutManager;
    private PeiDuiBean peiDuiData;
    private TextView tv_message_like_likenum;
    private TextView tv_message_lookmenum;
    private List<SeeMe> seeMeList;
    private SeeMeList seeMeListBean;
    private boolean isCanLoad = true;
    private boolean isRefresh = true;
    private Context context;
    private List list;
    private List num;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_message;
    }

    @Override
    protected void initView() {
         num=new ArrayList();
        num.add(1);
        num.add(2);
        num.add(3);
        num.add(4);
        tvAppbarTitle.setText(mContext.getString(R.string.message_title));
        list=new ArrayList();
        mMessageAdapter = new MessageAdapter(R.layout.item_message_list, null);
        mLinearLayoutManager = new LinearLayoutManager(mContext);
        rvMessageList.setLayoutManager(mLinearLayoutManager);
        rvMessageList.addItemDecoration(new MyDecoration(mContext, MyDecoration.VERTICAL_LIST));
        rvMessageList.setHasFixedSize(true);
        rvMessageList.setAdapter(mMessageAdapter);
        addHeaderView();
        initHeaderView();
        //设置下拉、上拉
        refresh.setDelegate(this);
        refresh.setRefreshViewHolder(new NormalRefreshViewHolder(mContext, true));
//        refresh.setRefreshViewHolder(new RefreshViewHolder(mContext, true) );
        rvMessageList.addOnScrollListener(mOnScrollListener);
        rvMessageList.addOnItemTouchListener(new OnItemClickListener() {
            private UserBaseEnglish userBaseEnglish;

            @Override
            public void SimpleOnItemClick(BaseQuickAdapter adapter, View view, int position) {
                int size = MessageFragment.this.listChat.size();

                if(position>size ){

                }
                if (MessageFragment.this.listChat.get(position) != null) {
                    userBaseEnglish = MessageFragment.this.listChat.get(position).getUserBaseEnglish();
                    Intent messgeIntent = new Intent(mContext, ChatMegActivity.class);
                    messgeIntent.putExtra(ConfigConstant.USERID, String.valueOf(userBaseEnglish.getId()));
                    messgeIntent.putExtra(ConfigConstant.NICKNAME, userBaseEnglish.getNickName());
                    Image image = userBaseEnglish.getImage();
                    if (image != null) {
                        if (!TextUtils.isEmpty(image.getThumbnailUrl())) {
                            messgeIntent.putExtra(ConfigConstant.USERAVATAR, userBaseEnglish.getImage().getThumbnailUrl());
                        }
                    }
                    Chat chat = listChat.get(position).getChat();
                    chat.setUnreadCount(0);
                    startActivityForResult(messgeIntent, 100);

                }
            }
        });
/**长按删除监听**/
        mMessageAdapter.setOnItemLongClickListener(new BaseQuickAdapter.OnItemLongClickListener() {
            @Override
            public void onItemLongClick(View view, final int position, RecyclerView.ViewHolder viewHolder) {

                final String[] items = {getString(R.string.delete)};
                Dialog alertDialog = new AlertDialog.Builder(getActivity()).
                        setItems(items, new DialogInterface.OnClickListener() {
                            private UserBaseEnglish userBaseEnglish;

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                MessageFragment.this.listChat= (List<ChatListBean.ListChat>) list;
                                userBaseEnglish = MessageFragment.this.listChat.get(position - 1).getUserBaseEnglish();
                                //选择清除聊天记录
//                                userBaseEnglish= mMessageAdapter.getItemByPosition(position);
                                DialogUtil.showDoubleBtnDialog(getChildFragmentManager(), getString(R.string.msg_delect_content), getString(R.string.msg_pre_delect_content, userBaseEnglish.getNickName()),
                                        null, null, true, new OnDoubleDialogClickListener() {
                                            @Override
                                            public void onPositiveClick(View view) {

                                                LogUtil.e(LogUtil.TAG_LMF, "聊天删除用户id " + userBaseEnglish.getId());
                                                deleteChatHistory(String.valueOf(userBaseEnglish.getId()), true, new OnDelChatHistoryListener() {

                                                    @Override
                                                    public void onSuccess() {
                                                        // 更新聊天记录数目
                                                        if (mMessageAdapter != null) {
                                                            mMessageAdapter.removeItem(position - 1);
//                                                            RxBus.getInstance().post("refresh_msg","refresh");
                                                            mMessageAdapter.notifyDataSetChanged();
                                                            getMsgList(1, PAGESIZE, REFRESH);
//                                                            String deleteUserId = String.valueOf(userBaseEnglish.getId());
//                                                            boolean readed = SharedPreferenceUtil.getBooleanValue(Utils.getContext(), deleteUserId, "READED", true);
//                                                            if(readed){
//                                                                int unRead = SharedPreferenceUtil.getIntValue(Utils.getContext(), "MSGNUM", "GET_UNREAD", totalUnread - 1);
//                                                                RxBus.getInstance().post(ConfigConstant.TOTALUNREAD, unRead-1);
//                                                            }

                                                        }
                                                    }

                                                    @Override
                                                    public void onFail() {

                                                    }
                                                });
                                            }

                                            @Override
                                            public void onNegativeClick(View view) {

                                            }
                                        });
                            }
                        }).create();
                alertDialog.show();
            }


        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            this.context = (Activity) context;
        }
    }

    private void addHeaderView() {

        headView = mContext.getLayoutInflater().inflate(R.layout.activity_message_head, (ViewGroup) rvMessageList.getParent(), false);
        mMessageAdapter.addHeaderView(headView);
    }

    private void initHeaderView() {
        rl_message_look_me = (RelativeLayout) headView.findViewById(R.id.rl_message_look_me);
        rl_message_item_like = (RelativeLayout) headView.findViewById(R.id.rl_message_like);
        tv_message_like_likenum = (TextView) headView.findViewById(R.id.tv_message_like_likenum);
        tv_message_lookmenum = (TextView) headView.findViewById(R.id.tv_message_lookmenum);
        iv_visitor_one = (ImageView) headView.findViewById(R.id.iv_visitor_one);
        iv_visitor_two = (ImageView) headView.findViewById(R.id.iv_visitor_two);
        iv_visitor_three = (ImageView) headView.findViewById(R.id.iv_visitor_three);
        rl_message_item_like.setOnClickListener(this);
        rl_message_look_me.setOnClickListener(this);
    }

    @Override
    protected void initData() {
        showLoadingPress();
        getMsgList(pageNum, PAGESIZE, NORMAL);
        getVisitor(0, PAGESIZE);
        getMathers(pageNum, PAGESIZE);
    }

    @Override
    public MessagePresenter newPresenter() {
        return new MessagePresenter();
    }

    public void ShowMsgListData(ChatListBean list) {
        hideLoadingPress();
        refresh.endRefreshing();
        listChat = list.getListChat();
        mMessageAdapter.setNewData(listChat);
        mMessageAdapter.notifyDataSetChanged();
        totalUnread = list.getTotalUnread();
        RxBus.getInstance().post(ConfigConstant.TOTALUNREAD, totalUnread);
    }

    public void addMsgListData(ChatListBean list) {
        listChat = list.getListChat();
        mMessageAdapter.addData(listChat);
        mMessageAdapter.notifyDataSetChanged();
    }

    //互相喜欢列表
    public void showPeiDuiList(PeiDuiBean peiDuiBean) {
        rl_message_item_like.setVisibility(View.VISIBLE);
        peiDuiData = peiDuiBean;
        int unreadCount = peiDuiBean.getUnreadCount();
        if (unreadCount > 0) {
            System.out.println("un===" + unreadCount);
            tv_message_like_likenum.setVisibility(View.VISIBLE);
            tv_message_like_likenum.setText(String.valueOf(unreadCount));
        }
    }

    //访客列表
    public void showVisitorList(SeeMeList lsit) {
        seeMeListBean = lsit;
        refresh.endRefreshing();
        seeMeList = lsit.getSeeMeList();
        if (seeMeList.size() > 0) {
            rl_message_look_me.setVisibility(View.VISIBLE);
            refresh.endRefreshing();
            if (seeMeList.size() == 1) {
                iv_visitor_three.setVisibility(View.VISIBLE);
                ImageLoaderUtils.display(mContext, iv_visitor_three, seeMeList.get(0).getUserBaseEnglish().getImage().getThumbnailUrl());
            } else if (seeMeList.size() == 2) {
                iv_visitor_three.setVisibility(View.VISIBLE);
                ImageLoaderUtils.display(mContext, iv_visitor_three, seeMeList.get(1).getUserBaseEnglish().getImage().getThumbnailUrl());
                iv_visitor_two.setVisibility(View.VISIBLE);
                ImageLoaderUtils.display(mContext, iv_visitor_two, seeMeList.get(0).getUserBaseEnglish().getImage().getThumbnailUrl());
            } else {
                iv_visitor_one.setVisibility(View.VISIBLE);
                iv_visitor_two.setVisibility(View.VISIBLE);
                iv_visitor_three.setVisibility(View.VISIBLE);
                ImageLoaderUtils.display(mContext, iv_visitor_one, seeMeList.get(0).getUserBaseEnglish().getImage().getThumbnailUrl());
                ImageLoaderUtils.display(mContext, iv_visitor_two, seeMeList.get(1).getUserBaseEnglish().getImage().getThumbnailUrl());
                ImageLoaderUtils.display(mContext, iv_visitor_three, seeMeList.get(2).getUserBaseEnglish().getImage().getThumbnailUrl());
            }
        }
    }

    // 监听适配器的滑动，完成上拉加载的功能
    public RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
        //最后一个角标位置
        private int lastVisibleItemPosition;

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            lastVisibleItemPosition = mLinearLayoutManager.findLastVisibleItemPosition();
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mMessageAdapter.getItemCount()) {
                pageNum++;
                getMsgList(pageNum, PAGESIZE, LOADMORE);
            }
        }
    };

    @Override
    public void onRefreshLayoutBeginRefreshing(RefreshLayout refreshLayout) {
        pageNum = 1;
        getMsgList(1, PAGESIZE, REFRESH);
        //后期加上互相喜欢和访客接口请求
        getVisitor(0, PAGESIZE);
    }

    //  没有用到 返回false
    @Override
    public boolean onRefreshLayoutBeginLoadingMore(RefreshLayout refreshLayout) {

        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case MSGREQUESTCODE:
                getMsgList(1, PAGESIZE, NORMAL);
                break;
            case LIKEREQUESTCODE:
                tv_message_like_likenum.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_message_like:
                if (null != peiDuiData && null != peiDuiData.getMatcherList() && peiDuiData.getMatcherList().size() > 0) {
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("peidui", peiDuiData);
                    intent.setClass(mContext, PeiDuiActivity.class);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, LIKEREQUESTCODE);
                }
                break;
            case R.id.rl_message_look_me:
                if (null != seeMeList && seeMeList.size() > 0) {
                    Intent visitorIntent = new Intent();
                    Bundle visitorBundle = new Bundle();
                    visitorBundle.putSerializable("visitor", seeMeListBean);
                    visitorIntent.setClass(mContext, NearVisitorActivity.class);
                    visitorIntent.putExtras(visitorBundle);
                    startActivityForResult(visitorIntent, VISITORREQUESTCODE);
                }
                break;
        }
    }

    public void hideLoading() {
        hideLoadingPress();
        refresh.endRefreshing();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        //当前fragment可见
        if (!hidden) {
            getMsgList(1, PAGESIZE, NORMAL);
        }
    }


    /**
     * 获取消息列表
     *
     * @param pageNum
     * @param pageSize
     * @param type
     */
    public void getMsgList(final int pageNum, String pageSize, final int type) {
        OkGo.post(IUrlConstant.URL_GET_ALL_CHAT_LIST)
                .tag(getView())
                .params("pageNum", String.valueOf(pageNum))
                .params("pageSize", pageSize)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .execute(new JsonCallback<ChatListBean>() {

                    @Override
                    public void onSuccess(ChatListBean chatListBean, Call call, Response response) {
                        if (null != chatListBean) {
                            hideLoading();
                            if (null != chatListBean && chatListBean.getIsSucceed().equals("1")) {
                                if (chatListBean.getListChat().size() > 0 ) {
                                    if(geiPageNum(pageNum)){
                                        for(int i=0;i<chatListBean.getListChat().size();i++){
                                            list.add(chatListBean.getListChat().get(i));
                                        }
                                    }
                                    hideLoadingPress();
                                    switch (type) {
                                        case RecommendListFragment.NORMAL:
                                            ShowMsgListData(chatListBean);
                                            break;
                                        case NearListFragment.REFRESH:
                                            ShowMsgListData(chatListBean);
                                            break;
                                        case NearListFragment.LOADMORE:
                                            addMsgListData(chatListBean);
                                            break;
                                    }
                                } else {
                                    //getView().showNoData();
                                    hideLoadingPress();
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        hideLoadingPress();
                    }
                });
    }

    /**
     * 获取最近访客列表
     *
     * @param pageNum
     * @param pageSize
     */
    public void getVisitor(int pageNum, String pageSize) {
        OkGo.post(IUrlConstant.URL_GET_GUEST_LIST)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("pageNum", String.valueOf(pageNum))
                .params("pageSize", pageSize)
                .execute(
//                        new StringCallback() {
//                             @Override
//                             public void onSuccess(String s, Call call, Response response) {
//                                 Log.e("AAAAAAA", "onSuccess: "+call+"------"+response+"---"+s );
//                             }
//
//                             @Override
//                             public void onError(Call call, Response response, Exception e) {
//                                 super.onError(call, response, e);
//                                 Log.e("AAAAAAA", "onError: "+call+"------"+response+"---"+e.toString());
//                             }
//                         }
                        new JsonCallback<SeeMeList>() {
                            @Override
                            public void onSuccess(SeeMeList seeMeList, Call call, Response response) {
                                if (null != seeMeList) {
                                    if (seeMeList.getTotalCount() > 0) {
                                        showVisitorList(seeMeList);
                                    }
                                }
                            }

                            @Override
                            public void onError(Call call, Response response, Exception e) {
                                super.onError(call, response, e);
                            }
                        }
                );
    }

    /**
     * 互相喜歡
     *
     * @param pageNum
     * @param pageSize
     */
    public void getMathers(int pageNum, String pageSize) {
        OkGo.post(IUrlConstant.URL_GET_MATCHER_LIST)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("pageNum", String.valueOf(pageNum))
                .params("pageSize", pageSize)
                .execute(new JsonCallback<PeiDuiBean>() {
                    @Override
                    public void onSuccess(PeiDuiBean peiDuiBean, Call call, Response response) {
                        if (null != peiDuiBean) {
                            if (peiDuiBean.getIsSucceed().equals("1")) {
                                List<PeiDuiBean.MatcherList> matcherList = peiDuiBean.getMatcherList();
                                if (matcherList.size() > 0) {
                                    showPeiDuiList(peiDuiBean);
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();

        RxManager rxManager = new RxManager();
        rxManager.on("login_change", new Action1<String>() {
            @Override
            public void call(String s) {
                if (isCanLoad) {
                    showLoadingPress();
                    getMsgList(pageNum, PAGESIZE, NORMAL);
                    getVisitor(pageNum, PAGESIZE);
                    getMathers(pageNum, PAGESIZE);
                    isCanLoad = false;
                }
            }
        });
        rxManager.on("refresh_msg", new Action1<String>() {

            @Override
            public void call(String s) {
                getMsgList(pageNum, PAGESIZE, NORMAL);
                getVisitor(pageNum, PAGESIZE);
                getMathers(pageNum, PAGESIZE);
                isRefresh = false;
            }
        });
    }

    /**
     * 删除聊天记录
     *
     * @param userId    用户id
     * @param showToast 是否显示Toast
     * @param listener  请求结果回调
     */
    public void deleteChatHistory(String userId, final boolean showToast, final OnDelChatHistoryListener listener) {
        OkGo.post(IUrlConstant.URL_DEL_CHAT_HISOTRY)
                .params("token", PlatformInfoXml.getToken())
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("remoteUserIds", userId)
                .execute(new JsonCallback<BaseModel>() {
                    @Override
                    public void onSuccess(BaseModel baseModel, Call call, Response response) {
                        if (response != null) {
                            String isSucceed = baseModel.getIsSucceed();
                            if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                if (showToast) {
                                    ToastUtil.showShortToast(mContext, mContext.getString(R.string.del_chat_success));
                                }
                                if (listener != null) {
                                    listener.onSuccess();
                                }
                            }else{
                                if (showToast) {
                                    ToastUtil.showShortToast(mContext, mContext.getString(R.string.del_chat_fail));
                                }
                            }
                        }
                    }
                });
    }

    public interface OnDelChatHistoryListener {

        /**
         * 删除聊天记录成功
         */
        void onSuccess();

        /**
         * 删除聊天记录失败
         */
        void onFail();
    }

    public boolean geiPageNum(int pageNum){
        if(num.contains(pageNum)){
            num.remove(num.indexOf(pageNum));
            return true;
        }else{
            return false;
        }


    }

}
