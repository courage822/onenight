package com.online.onenight2.message.View;

import com.online.onenight2.bean.MessageList;

/**
 * Created by foxmanman on 2017/4/18.
 * 聊天界面的回调数据
 */

public interface ChatMessInfView {
    void getMessageListSuccess(MessageList messageList);
    void getMessageFile();
    void sendMessageSuccess();
    void sendMessageFail();
    void showProgress();
    void hideProgress();
    void sendRecordSuccess();
}
