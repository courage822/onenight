package com.online.onenight2.impl;


public interface IPresent<V> {
    void attachV(V view);

    void detachV();
}
