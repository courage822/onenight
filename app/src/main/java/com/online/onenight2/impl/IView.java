package com.online.onenight2.impl;

public interface IView<P> {

    int getLayoutId();
    P newPresenter();

}
