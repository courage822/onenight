package com.online.onenight2.base;


import com.online.onenight2.impl.IPresent;
import com.online.onenight2.impl.IView;

/**
 * 描述：基类presenter
 * Created by qyh on 2017/3/23.
 */
public class BasePresenter<V extends IView> implements IPresent<V> {
    IView vv;
    private V v;

    @Override
    public void attachV(V view) {
        v = view;
    }

    @Override
    public void detachV() {
        v = null;
    }

    protected V getView() {
        try{
            if (v == null) {
                throw new IllegalStateException("view can not be null");
            }
        }catch (Exception e){

        }

        return v;
    }
}
