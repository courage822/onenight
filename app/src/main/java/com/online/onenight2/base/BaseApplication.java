package com.online.onenight2.base;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.library.utils.CrashHandler;
import com.lzy.okgo.OkGo;
import com.online.onenight2.utils.CrashNotes;


/**
 * 描述：MultiDexApplication防止方法数过多
 * Created by qyh on 2017/3/23.
 */

public class BaseApplication  extends MultiDexApplication {
    private static BaseApplication baseApplication;
    private static Context context;

    /** 获取Application类型的上下文 */
    public static Context getContext() {
        return context;
    }

    /** 当MyApp创建的时候这个方法会被调用 */
    @Override
    public void onCreate() {
        baseApplication = this;
        // 初始化异常收集
        CrashHandler.getInstance().init(this);
        initData();
        super.onCreate();
        context = this;

        CrashNotes crashNotes = CrashNotes.getInstance();
        crashNotes.init(getApplicationContext());
    }

    private void initData() {
        OkGo.init(this);
    }

    public static Context getAppContext() {
        return baseApplication;
    }
}
