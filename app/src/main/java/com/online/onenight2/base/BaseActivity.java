package com.online.onenight2.base;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;


import com.online.onenight2.impl.IPresent;
import com.online.onenight2.impl.IView;
import com.online.onenight2.view.LoadingDialog;

import butterknife.ButterKnife;

/**
 * 描述：
 * Created by qyh on 2017/3/23.
 */

    public abstract class BaseActivity<P extends IPresent> extends AppCompatActivity implements IView<P> {

    public Activity mContext;
    public RxManager mRxManager;
    private P p;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        doBeforeSetcontentView();
        setContentView(getLayoutId());
        mContext=BaseActivity.this;
        mRxManager=new RxManager();
        this.initView(savedInstanceState);
        this.initData();
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    /**子类实现*/
    //获取布局文件
    public abstract int getLayoutId();
    //初始化view
    public abstract void initView(Bundle savedInstanceState);
    public abstract void initData();
    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
    }

    private void doBeforeSetcontentView() {
        // 把actvity放到application栈中管理
        AppManager.getAppManager().addActivity(this);
        // 无标题
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // 设置竖屏
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    //获取presenter
    protected P getPresenter() {
        if (p == null) {
            p = newPresenter();
            if (p != null) {
                p.attachV(this);
            }
        }
        return p;
    }
    /**显示加载对抗框*/
    public void showLoadingPress(){
        LoadingDialog.showDialogForLoading(mContext);
    }

    /**隐藏加载对话框*/
    public void hideLoadingPress() {
        LoadingDialog.cancelDialogForLoading(mContext);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (getPresenter() != null) {
            getPresenter().detachV();
        }
        p = null;
        mRxManager.clear();
        AppManager.getAppManager().finishActivity(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
