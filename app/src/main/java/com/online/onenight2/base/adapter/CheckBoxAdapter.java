package com.online.onenight2.base.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.library.R;
import com.library.utils.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jzrh on 2016/8/11.
 */
public class CheckBoxAdapter extends BaseAdapter {
    // 用来控制CheckBox的选中状况
    private HashMap<Integer, Boolean> isSelected;
    private Context context;
    private List<String> mDataList;
    private List<String> selectedList;
    private List<String> oldSelectedList = new ArrayList<>();
    private boolean isSingle;

    class ViewHolder {
        TextView tvName;
        CheckBox cb;
        LinearLayout linear_layout_up;
    }

    public CheckBoxAdapter() {
        isSelected = new HashMap<Integer, Boolean>();
        // 初始化数据
        initDate();
    }

    public CheckBoxAdapter(Context context, List<String> list, boolean isSingle, List<String> selectedList) {
        this.context = context;
        this.isSingle = isSingle;
        this.selectedList = selectedList;
        isSelected = new HashMap<Integer, Boolean>();
        mDataList = list;
        // 初始化数据
        initDate();
    }

    public CheckBoxAdapter(Context context, List<String> list, boolean isSingle, List<String> selectedList, String oldSelected) {
        this.context = context;
        this.isSingle = isSingle;
        this.selectedList = selectedList;
        isSelected = new HashMap<Integer, Boolean>();
        mDataList = list;
        // 设置之前选择的数据项
        if (!TextUtils.isEmpty(oldSelected)) {
            String[] spit = oldSelected.split("\\|");
            for (String item : spit) {
                oldSelectedList.add(item);
                selectedList.add(item);
            }
        }
        // 初始化数据
        initDate();
    }

    // 初始化isSelected的数据
    private void initDate() {
        for (int i = 0; i < mDataList.size(); i++) {
            if (oldSelectedList != null && oldSelectedList.contains(mDataList.get(i))) {
                getIsSelected().put(i, true);
            } else {
                getIsSelected().put(i, false);
            }
        }
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = LayoutInflater.from(context);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_check_box_single, null);
            holder = new ViewHolder();
            holder.cb = (CheckBox) convertView.findViewById(R.id.checkBox1);
            holder.linear_layout_up = (LinearLayout) convertView.findViewById(R.id.linear_layout_up);
            holder.tvName = (TextView) convertView.findViewById(R.id.tv_device_name);
            convertView.setTag(holder);
        } else {
            // 取出holder
            holder = (ViewHolder) convertView.getTag();
        }

        final String bean = Util.convertText(mDataList.get(position));
        holder.tvName.setText(bean);
        // 监听checkBox并根据原来的状态来设置新的状态
        holder.cb.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (isSingle) {
                    for (int i = 0; i < mDataList.size(); i++) {
                        selectedList.removeAll(selectedList);
                        if (i != position) {
                            isSelected.put(i, false);
                        }
                    }
                    setIsSelected(isSelected);
                    CheckBoxAdapter.this.notifyDataSetChanged();
                }
                if (isSelected.get(position)) {
                    isSelected.put(position, false);
                    setIsSelected(isSelected);
                    selectedList.remove(mDataList.get(position));
                } else {
                    isSelected.put(position, true);
                    setIsSelected(isSelected);
                    selectedList.add(mDataList.get(position));
                }

            }
        });

        // 根据isSelected来设置checkbox的选中状况
        holder.cb.setChecked(getIsSelected().get(position));
        return convertView;
    }

    public HashMap<Integer, Boolean> getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(HashMap<Integer, Boolean> isSelect) {
        isSelected = isSelect;
    }
}
