package com.online.onenight2.base;
/**
 * 描述：
 * Created by qyh on 2017/3/23.
 */
public interface BaseView {
    /*******内嵌加载*******/
    void showLoading(String title);
    void stopLoading();
    void showErrorTip(String msg);
}
