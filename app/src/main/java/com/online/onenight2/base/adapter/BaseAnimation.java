package com.online.onenight2.base.adapter;

import android.animation.Animator;
import android.view.View;


public interface BaseAnimation {

    Animator[] getAnimators(View view);

}
