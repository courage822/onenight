package com.online.onenight2.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.online.onenight2.impl.IPresent;
import com.online.onenight2.impl.IView;
import com.online.onenight2.view.LoadingDialog;

import butterknife.ButterKnife;


/**
 * 描述：
 * Created by qyh on 2017/3/25.
 */

public abstract class BaseFragment<P extends IPresent> extends Fragment implements IView<P> {
    private P p;
    protected Activity mContext;
    public View rootView;
    protected LayoutInflater layoutInflater;
    public RxManager mRxManager;
    private boolean isVisible;                  //是否可见状态
    private boolean isPrepared;                 //标志位，View已经初始化完成。
    private boolean isFirstLoad = true;         //是否第一次加载
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layoutInflater = inflater;
        isFirstLoad = true;
        isPrepared = true;
        if (rootView == null) {
            if (getLayoutId() > 0) {
                rootView = inflater.inflate(getLayoutId(), container, false);
                ButterKnife.bind(this, rootView);
                initView();
            } else {
                return null;
            }
        }
        mRxManager=new RxManager();
        lazyLoad();
        return rootView;
    }

    protected P getPresenter() {
        if (p == null) {
            p = newPresenter();
            if (p != null) {
                p.attachV(this);
            }
        }
        return p;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            this.mContext = (Activity) context;
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(null!=mRxManager){
            mRxManager.clear();
        }
        if(null!=getPresenter()){
            getPresenter().detachV();
        }
        p = null;
    }

    /**
     * 如果是通过FragmentTransaction的show和hide的方法来控制显示，调用onHiddenChanged.
     * 若是初始就show的Fragment 为了触发该事件 需要先hide再show
     */
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            isVisible = true;
            lazyLoad();
        } else {
            isVisible = false;
        }
    }
    /** 如果是与ViewPager一起使用，调用的是setUserVisibleHint */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            isVisible = true;
            lazyLoad();
        } else {
            isVisible = false;
        }
    }
    protected void lazyLoad() {
        if (!isPrepared || !isVisible || !isFirstLoad) {
            return;
        }
        isFirstLoad = false;
        initData();
    }
    /**显示加载对抗框*/
    public void showLoadingPress(){
        LoadingDialog.showDialogForLoading(mContext);
    }

    /**隐藏加载对话框*/
    public void hideLoadingPress() {
        LoadingDialog.cancelDialogForLoading(mContext);
    }
    public abstract int getLayoutId();
    protected abstract void initView();
    protected abstract void initData();
}
