package com.online.onenight2.profile.fragment;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.library.dialog.OnListDoubleDialogClickListener;
import com.library.utils.DialogUtil;
import com.library.utils.SharedPreferenceUtil;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseFragment;
import com.online.onenight2.base.RxManager;
import com.online.onenight2.bean.Image;
import com.online.onenight2.bean.MyInfo;
import com.online.onenight2.bean.UpImage;
import com.online.onenight2.bean.User;
import com.online.onenight2.bean.UserBase;
import com.online.onenight2.callback.JsonCallback;
import com.online.onenight2.constant.IConfigConstant;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.profile.activity.PayActivity;
import com.online.onenight2.profile.activity.PhotoViewActivity;
import com.online.onenight2.profile.activity.ProfileActivity;
import com.online.onenight2.profile.activity.SettingActivity;
import com.online.onenight2.profile.presenter.UpPhotoPresenter;
import com.online.onenight2.utils.FileSizeUtils;
import com.online.onenight2.utils.ImageLoaderUtils;
import com.online.onenight2.view.CircularImage;
import com.online.onenight2.view.DampView;
import com.online.onenight2.xml.PlatformInfoXml;
import com.online.onenight2.xml.UserInfoXml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.Call;
import okhttp3.Response;
import rx.functions.Action1;

import static android.app.Activity.RESULT_OK;

/**
 * 描述：
 * Created by wangyong on 2017/3/24.
 */

public class ProfileFragment extends BaseFragment<UpPhotoPresenter> implements View.OnClickListener {
    @BindView(R.id.profile_rl_center_pay)
    CardView rl_pay;
    @BindView(R.id.profile_rl_base_data)
    CardView rl_data;
    @BindView(R.id.profile_rl_set)
    CardView rl_set;
    @BindView(R.id.profile_rl_photo)
    CardView rl_photo;
//    @BindView(R.id.profile_fragment_title_iv)
    ImageView iv_title;
    @BindView(R.id.iv_background)
    CircularImage ivBackground;
    Unbinder unbinder;
    private List<String> photo_list;
    @BindView(R.id.dampview)
    DampView damp;
    @BindView(R.id.scrollview_imageview)
    ImageView imageview;
//    @BindView(R.id.profile_tv_id_big)
    TextView tv_id_big;
//    @BindView(R.id.profile_tv_id_small)
    TextView tv_id_small;
    @BindView(R.id.profile_fragment_tv_base_ziliao)
    TextView tv_ziliao;
    @BindView(R.id.profile_fragment_tv_photo_num)
    TextView tv_num;
    @BindView(R.id.iv_vip1)
    ImageView iv_vip1;
    @BindView(R.id.iv_vip2)
    ImageView iv_vip2;
    private static final int UP_PHOTO_LOCAL = 1;
    private static final int UP_PHOTO_CAMERA = 2;
    private static final int UP_PHOTO_CROP = 3;
    private static final int UP_FROM_CAMERA = 4;
    private static final int CROP = 5;
    private static final int UP_FROM_LOCAL = 6;
    private String isMain = "1";
    private static final int PERMISSION_CODE_READ_EXTERNAL_STORAGE = 0;
    private boolean isCanLoad = true;
    private boolean isPay = true;
    public double fileOrFilesSize;
    private String photo_num;

    @Override
    protected void initView() {
        tv_id_big = (TextView) rootView.findViewById(R.id.profile_tv_id_big);
        iv_title = (ImageView) rootView.findViewById(R.id.profile_fragment_title_iv);
         tv_id_small = (TextView) rootView.findViewById(R.id.profile_tv_id_small);
        photo_list = new ArrayList<>();
        photo_list.add(getString(R.string.profile_benditupian));
        photo_list.add(getString(R.string.profile_paizhao));
        photo_list.add(getString(R.string.profile_quxiao));
        imageview.setImageDrawable(getResources().getDrawable(R.mipmap.default_me_head_bg));
        damp.setImageView(imageview);
        final String imageUrl = SharedPreferenceUtil.getStringValue(mContext, "PHOTO_URL", "url", "");

        if (!imageUrl.equals("")) {
//            ImageLoaderUtil.getInstance().loadImage(getContext(), new ImageLoader.Builder().url(imageUrl).imageView(iv_title).build());
            ImageLoaderUtils.display(mContext,iv_title, imageUrl);
        }

        //获取照片的数量
        getPhotoNum();
        getUerInfo();
    }


    private void getPhotoNum() {
        RxManager manager = new RxManager();
        manager.on("rx_photo_num", new Action1<String>() {
            @Override
            public void call(String s) {
                SharedPreferenceUtil.setStringValue(mContext, "PHOTO_NUM", "num", s);
                photo_num=s;
                tv_num.setText(s);
            }
        });
//        SharedPreferences sp=mContext.getSharedPreferences("photo_num", Context.MODE_APPEND);
//        String photo_num = sp.getString("photo_num", "0");
//        tv_num.setText(photo_num);
        photo_num = SharedPreferenceUtil.getStringValue(mContext, "PHOTO_NUM", "num", "0");
        tv_num.setText(photo_num);
    }

    @Override
    protected void initData() {
        rl_pay.setOnClickListener(this);
        rl_data.setOnClickListener(this);
        rl_set.setOnClickListener(this);
        iv_title.setOnClickListener(this);
        rl_photo.setOnClickListener(this);
    }

    @Override
    public int getLayoutId() {

        return R.layout.fragment_profile;
    }

    @Override
    public UpPhotoPresenter newPresenter() {
        return new UpPhotoPresenter();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //跳转到支付界面
            case R.id.profile_rl_center_pay:
                Intent intent = new Intent(mContext, PayActivity.class);
                startActivity(intent);
                break;
            //基本资料
            case R.id.profile_rl_base_data:
                Intent edit_intent = new Intent(mContext, ProfileActivity.class);
                startActivity(edit_intent);
                break;
            //跳转设置界面
            case R.id.profile_rl_set:
                Intent caintent = new Intent(mContext, SettingActivity.class);
                startActivity(caintent);
                break;
            case R.id.profile_rl_photo:
                Intent photo_intent = new Intent(mContext, PhotoViewActivity.class);
                startActivity(photo_intent);
                break;
            case R.id.profile_fragment_title_iv:
                isMain = "1";
                diaupphoto();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CODE_READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePhoto();
            }
        }
    }

    //选择照片或者直接拍照
    private void diaupphoto() {
        DialogUtil.showListDialog(getFragmentManager(), null, null, null, false, photo_list, new OnListDoubleDialogClickListener() {
            @Override
            public void onPositiveClick(View view) {
            }

            @Override
            public void onNegativeClick(View view) {
            }

            @Override
            public void onItemClick(View view, String item) {
                if (item.equals(photo_list.get(0))) {
                    /**gai**/
                    Intent mIntent = new Intent(Intent.ACTION_PICK);
                    mIntent.setType("image/*");
//                    mIntent.putExtra(MediaStore.EXTRA_OUTPUT, getUriFromFile(mContext, new File(IConfigConstant.LOCAL_AVATAR_PATH)));
                    startActivityForResult(mIntent, UP_FROM_LOCAL);
//                    Intent localIntent = new Intent(Intent.ACTION_PICK);
//                    localIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                    localIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//                    localIntent.setType("image/*");
//                    startActivityForResult(localIntent, UP_PHOTO_LOCAL);
                } else if (item.equals(photo_list.get(1))) {
                    /**gai**/
//                    Intent cIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    // 下面这句指定调用相机拍照后的照片存储的路径
//                    cIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(IConfigConstant.LOCAL_AVATAR_PATH)));
//                    startActivityForResult(cIntent, UP_FROM_CAMERA);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                            // 请求android.permission.READ_EXTERNAL_STORAGE权限
                            ActivityCompat.requestPermissions(mContext, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_CODE_READ_EXTERNAL_STORAGE);
                            return;
                        }
                    }
                    takePhoto();
                } else if (item.equals(photo_list.get(2))) {
                }
            }
        });
    }

    public void takePhoto() {

        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            // 表示对目标应用临时授权该Uri所代表的文件，7.0及以上
            takePhotoIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        //设置照片的临时保存路径
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, getUriFromFile(mContext, new File(IConfigConstant.LOCAL_AVATAR_PATH)));
        startActivityForResult(takePhotoIntent, UP_FROM_CAMERA);
        getActivity().overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
//        startActivityForResult(takePhotoIntent, CROP);
    }

    /**
     * 根据file获得Uri（适配7.0及以上）
     *
     * @param file
     */
    private Uri getUriFromFile(Context context, File file) {
        Uri imageUri = null;
        if (context != null && file != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                // 7.0及以上因为安全问题，需要通过Content Provider封装Uri对象
                imageUri = FileProvider.getUriForFile(mContext, "com.online.onenight.provider", file);
            } else {
                imageUri = Uri.fromFile(file);
            }
        }
        return imageUri;
    }

    Uri originalUri1;
    Bitmap upBp;
    int readPgree;
    File file;
    double fileOrFSize;

    /**
     * gai
     **/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        ContentResolver resolver = getContentResolver();
        Bitmap bm = null;

        switch (requestCode) {
            // 如果是直接从相册获取
            case UP_FROM_LOCAL:
                if (data != null) {
                    Uri originalUri = data.getData(); // 获得图片的uri
                    cropImage(originalUri,"1");
                }
                break;
            case UP_FROM_CAMERA:
                if (data != null) {
                    Uri uri = data.getData();
                    if (uri != null) {
                        cropImage(uri,"0");
                    } else {
                        // 拍照后，7.0以上系统读取图片时需要转换路径读取方式
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            cropImage(getUriFromFile(mContext, new File(IConfigConstant.LOCAL_AVATAR_PATH)),"0");
                        } else {
                            cropImage(Uri.fromFile(new File(IConfigConstant.LOCAL_AVATAR_PATH)),"0");
                        }
                    }
                } else {
                    // 拍照后，7.0以上系统读取图片时需要转换路径读取方式
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        cropImage(getUriFromFile(mContext, new File(IConfigConstant.LOCAL_AVATAR_PATH)),"0");
                    } else {
                        cropImage(Uri.fromFile(new File(IConfigConstant.LOCAL_AVATAR_PATH)),"0");
                    }
                }
                break;
            case CROP:
                if (resultCode == RESULT_OK && data != null) {
                    final BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    options.inSampleSize = calculateInSampleSize(options, 480, 800);
                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                    options.inJustDecodeBounds = false;
                    Bitmap upBitmap = BitmapFactory.decodeFile(IConfigConstant.LOCAL_AVATAR_PATH, options);
                    iv_title.setImageBitmap(upBitmap);
                    saveMyBitmap(upBitmap, 100);
                    fileOrFilesSize = FileSizeUtils.getFileOrFilesSize(IConfigConstant.LOCAL_AVATAR_PATH,
                            FileSizeUtils.SIZETYPE_KB);
                    if (fileOrFilesSize > 200 && fileOrFilesSize <= 9000) {
                        int compress = (int) (120 / fileOrFilesSize * 100);
                        if (compress < 20) {
                            compress = 20;
                        }
                        saveMyBitmap(upBitmap, compress);
                    }

                    int readPicDegree = readPicDegree(IConfigConstant.LOCAL_AVATAR_PATH);
                    if (readPicDegree != 0) {
                        Bitmap rotatedBitmap = rotateBitmap(readPicDegree, upBitmap);
                        if (fileOrFilesSize > 200 && fileOrFilesSize <= 9000) {
                            int compress = (int) (120 / fileOrFilesSize * 100);
                            if (compress < 20) {
                                compress = 20;
                            }
                            saveMyBitmap(upBitmap, compress);
                        }
                    }

                    file = new File(IConfigConstant.LOCAL_AVATAR_PATH);
                    if (fileOrFilesSize > 9000) {
                        file = null;
//                        ToastUtil.showShortToast(this, getString(R.string.img_too_big));
                    }
                }
                if (file != null && file.exists()) {
                    upPhoto(isMain, file);
                }
                break;
            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    //    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        switch (requestCode) {
//            case UP_PHOTO_LOCAL:
//                if (data != null) {
//                    Uri localuri = data.getData();
//                    cropImage(localuri);
//                }
//                break;
//            case UP_PHOTO_CAMERA:
//                if (data != null) {
//                    Uri uri = data.getData();
//                    if (uri != null) {
//                        cropImage(uri);
//                    } else {
//                        // 拍照后，7.0以上系统读取图片时需要转换路径读取方式
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                            cropImage(getUriFromFile(mContext, new File(IConfigConstant.LOCAL_AVATAR_PATH)));
//                        } else {
//                            cropImage(Uri.fromFile(new File(IConfigConstant.LOCAL_AVATAR_PATH)));
//                        }
//                    }
//                } else {
//                    // 拍照后，7.0以上系统读取图片时需要转换路径读取方式
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                        cropImage(getUriFromFile(mContext, new File(IConfigConstant.LOCAL_AVATAR_PATH)));
//                    } else {
//                        cropImage(Uri.fromFile(new File(IConfigConstant.LOCAL_AVATAR_PATH)));
//                    }
//                }
//                break;
//            case UP_PHOTO_CROP:
//                if (resultCode != mContext.RESULT_CANCELED) {
//                    final BitmapFactory.Options options = new BitmapFactory.Options();
//                    options.inJustDecodeBounds = true;
//                    // Calculate inSampleSize
//                    options.inSampleSize = calculateInSampleSize(options, 480, 800);
//                    // Decode bitmap with inSampleSize set
//                    options.inJustDecodeBounds = false;
//                  if (isMain.equals("1")) {
//                        upBp = BitmapFactory.decodeFile(IConfigConstant.LOCAL_AVATAR_PATH, options);
//                        saveMyBitmap(upBp, 100);
//                        fileOrFSize = FileSizeUtils.getFileOrFilesSize(IConfigConstant.LOCAL_AVATAR_PATH,
//                                FileSizeUtils.SIZETYPE_KB);
//                    }
//                    if (fileOrFSize > 200 && fileOrFSize <= 10000) {
//                        int compress = (int) (120 / fileOrFSize * 100);
//                        if (compress < 20) {
//                            compress = 20;
//                        }
//                        saveMyBitmap(upBp, compress);
//                    }
//                    if (isMain.equals("1")) {
//                        readPgree = readPicDegree(IConfigConstant.LOCAL_AVATAR_PATH);
//                    }
//                    if (readPgree != 0) {
//                        if (fileOrFSize > 200 && fileOrFSize <= 10000) {
//                            int compress = (int) (120 / fileOrFSize * 100);
//                            if (compress < 20) {
//                                compress = 20;
//                            }
//                            saveMyBitmap(upBp, compress);
//                        }
//                    }
//                   if (isMain.equals("1")) {
//                        file = new File(IConfigConstant.LOCAL_AVATAR_PATH);
//                    }
//                    if (fileOrFSize > 10000) {
//                        file = null;
//                        ToastUtil.showShortToast(mContext,getString(R.string.profile_bunengshiyong));
//                    }
//                }
//                if (UserInfoXml.getAvatarUrl() != null) {
//                    ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(UserInfoXml.getAvatarUrl()).imageView(iv_title).build());
//                }
//                if (file != null && file.exists()) {
//                 upPhoto(isMain,file);
//                }
//                break;
//                default:
//                break;
//        }
//        super.onActivityResult(requestCode, resultCode, data);
//    }
    //裁剪图片
    private void cropImage(Uri uri,String isPhoto) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        // aspectX aspectY 是裁剪框宽高的比例
        intent.putExtra("aspectX", 3);
        intent.putExtra("aspectY", 4);
        if (isMain.equals("1")) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(IConfigConstant.LOCAL_AVATAR_PATH)));
        }
        intent.putExtra("return-data", false);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());

        if(isPhoto.equals("0")){
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            options.inSampleSize = calculateInSampleSize(options, 480, 800);
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            options.inJustDecodeBounds = false;
            Bitmap upBitmap = BitmapFactory.decodeFile(IConfigConstant.LOCAL_AVATAR_PATH, options);
            iv_title.setImageBitmap(upBitmap);
            String num = Integer.parseInt(photo_num) + 1 + "";
            tv_num.setText(num);
        }
        startActivityForResult(intent, CROP);
        mContext.overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
    }

    // 将图片纠正到正确方向
    public static Bitmap rotateBitmap(int degree, Bitmap bitmap) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap bm = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                bitmap.getHeight(), matrix, true);
        return bm;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? widthRatio : heightRatio;
        }
        return inSampleSize;
    }

    /**
     * 通过ExifInterface类读取图片文件的被旋转角度
     *
     * @param path ： 图片文件的路径
     * @return 图片文件的被旋转角度
     */
    public static int readPicDegree(String path) {
        int degree = 0;
        // 读取图片文件信息的类ExifInterface
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (exif != null) {
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
            }
        }
        return degree;
    }

    File fs;

    public void saveMyBitmap(Bitmap mBitmap, int compress) {
        if (isMain.equals("1")) {
            fs = new File(IConfigConstant.LOCAL_AVATAR_PATH);
        }
        if (!fs.exists()) {
            fs.mkdir();
        }
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(fs);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        mBitmap.compress(Bitmap.CompressFormat.JPEG, compress, fOut);
        try {
            fOut.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        RxManager rxManager = new RxManager();
        rxManager.on("login_change", new Action1<String>() {
            @Override
            public void call(String s) {
                if (isCanLoad) {
                    getUerInfo();
                    isCanLoad = false;
                }
            }
        });
        rxManager.on("changePay", new Action1<String>() {
            @Override
            public void call(String s) {
                getUerInfo();
            }
        });
        rxManager.on("change_name", new Action1<String>() {
            @Override
            public void call(String s) {
                if (!TextUtils.isEmpty(UserInfoXml.getNickName())) {
                    tv_id_big.setText(UserInfoXml.getNickName());
                }
            }
        });

    }

    //上传图片
    public void upPhoto(String isMain, File file) {
        OkGo.post(IUrlConstant.URL_UPLOAD_PHOTO)
                .tag(this)
                .isMultipart(true)
                .connTimeOut(60 * 1000)
                .readTimeOut(60 * 1000)
                .writeTimeOut(60 * 1000)
                .headers("isMain", isMain)
                .headers("productId", PlatformInfoXml.getPlatfromInfo().getProduct())
                .headers("fid", PlatformInfoXml.getPlatfromInfo().getFid())
                .headers("pid", PlatformInfoXml.getPlatfromInfo().getPid())
                .headers("country", PlatformInfoXml.getPlatfromInfo().getCountry())
                .headers("language", PlatformInfoXml.getPlatfromInfo().getLanguage())
                .headers("version", PlatformInfoXml.getPlatfromInfo().getVersion())
                .headers("platform", PlatformInfoXml.getPlatfromInfo().getPlatform())
                .params("file", file)
                .execute(new JsonCallback<UpImage>() {
                    @Override
                    public void onSuccess(final UpImage image, Call call, Response response) {
                        if (image != null) {
                            if (image.getIsSucceed() != null && image.getIsSucceed().equals("1")) {
                                if (image != null) {
                                    Image image1 = image.getImage();
                                    if (image1 != null) {
//                                        ImageLoaderUtil.getInstance().loadImage(getContext(), new ImageLoader.Builder().url(image1.thumbnailUrl).imageView(iv_title).build());
                                        UserInfoXml.setAvatarUrl(image1.thumbnailUrl);
                                        photo_num = (Integer.parseInt(photo_num) + 1) + "";
                                        SharedPreferenceUtil.setStringValue(mContext, "PHOTO_NUM", "num", photo_num);
                                        SharedPreferenceUtil.setStringValue(mContext, "PHOTO_URL", "url", image1.thumbnailUrl);
                                        tv_num.setText(photo_num);
                                    }
                                }
                            }
                        }
                    }
                });

    }

    private void getUerInfo() {
        OkGo.post(IUrlConstant.URL_USER_INFO)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .execute(
                        new StringCallback() {
                            @Override
                            public void onSuccess(String s, Call call, Response response) {
                                MyInfo myInfo = JSON.parseObject(s, MyInfo.class);
                                if (null != myInfo) {
                                    String isSucceed = myInfo.getIsSucceed();
                                    if (!TextUtils.isEmpty(isSucceed) && isSucceed.equals("1")) {
                                        UserInfoXml.setUserInfo(myInfo.getUserEnglish());
                                        User userEnglish = myInfo.getUserEnglish();
                                        if (userEnglish != null) {
                                            UserBase userBaseEnglish = userEnglish.getUserBaseEnglish();
                                            if (userBaseEnglish != null) {
                                                int isMonthly = userBaseEnglish.getIsMonthly();
                                                if (isMonthly == 1) {//开通包月服务
                                                    iv_vip1.setImageResource(R.mipmap.vip_1_big);
                                                }
                                                int isVip = userBaseEnglish.getIsVip();
                                                if (isVip == 1) {//开通vip
                                                    iv_vip2.setImageResource(R.mipmap.vip_2_big);
                                                }
                                                if (!TextUtils.isEmpty(userBaseEnglish.getId())) {

                                                    if(!TextUtils.isEmpty(userBaseEnglish.getNickName())){
                                                        String nickName = userBaseEnglish.getNickName();
                                                        tv_id_big.setText(nickName);
                                                    }
                                                }
                                                tv_id_small.setText("id:" + userBaseEnglish.getId());
                                                Image image = userBaseEnglish.getImage();
                                                if (image != null) {
                                                    String imageUrl = image.getImageUrl();
                                                    ImageLoaderUtils.display(mContext,iv_title, imageUrl);
//                                                    ImageLoaderUtil.getInstance().loadImage(getContext(), new ImageLoader.Builder().url(image.getImageUrl()).imageView(iv_title).build());
                                                    SharedPreferenceUtil.setStringValue(mContext, "PHOTO_URL", "url", image.getImageUrl());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                );
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
