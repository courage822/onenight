package com.online.onenight2.profile.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bm.library.PhotoView;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.library.utils.Util;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseActivity;
import com.online.onenight2.bean.Image;
import com.online.onenight2.profile.adapter.PicturePagerAdapter;
import com.online.onenight2.profile.anim.CommPageTransformer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class PictureLookActivity extends BaseActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {
    @BindView(R.id.pic_viewpager)
    ViewPager mViewPager;
    @BindView(R.id.pic_activity_tv_back)
    TextView tv_back;
    @BindView(R.id.pic_tv_num)
    TextView tv_num;
    private static final String INTENT_KEY_INDEX = "intent_key_INDEX";
    private static final String INTENT_KEY_IMG_URL_LIST = "intent_key_IMG_URL_LIST";

    private int totalCount;


    /**
     * 跳转图片浏览器
     *
     * @param context    上下文对象
     * @param currIndex  当前显示的图片的索引，从0开始
     * @param imgUrlList 所有图片url集合
     */
    public static void toPictureLookActivity(Context context, int currIndex, List<Image> imgUrlList) {
        System.out.println("currIndex==="+currIndex);
        Intent intent = new Intent(context, PictureLookActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(INTENT_KEY_INDEX, currIndex);
        intent.putExtra(INTENT_KEY_IMG_URL_LIST, (Serializable) imgUrlList);
        context.startActivity(intent);
    }


    @Override
    public int getLayoutId() {
        return R.layout.activity_picture_look;
    }

    @Override
    public Object newPresenter() {
        return null;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        tv_back.setOnClickListener(this);
        mViewPager.addOnPageChangeListener(this);
    }

    @Override
    public void initData() {
        Intent intent = getIntent();
        if (intent != null) {
            int currIndex = intent.getIntExtra(INTENT_KEY_INDEX, 0);
            List<Image> list= (List<Image>) intent.getSerializableExtra(INTENT_KEY_IMG_URL_LIST);
            totalCount = list.size();
            List<PhotoView> photoViewList = new ArrayList<PhotoView>();
            if (!Util.isListEmpty(list)) {
                for (Image image : list) {
                    PhotoView photoView = new PhotoView(PictureLookActivity.this);
                    photoView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));
                    photoView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    // 开启缩放功能
                    photoView.enable();
                    if (image != null) {
                        ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder()
                                .url(image.getImageUrl()).placeHolder(R.mipmap.unload).imageView(photoView).build());
                    } else {
                        photoView.setImageResource(R.mipmap.unload);
                    }
                    photoViewList.add(photoView);
                }
            }
            PicturePagerAdapter mAdapter=new PicturePagerAdapter(photoViewList);
            // 设置切换动画
            // mViewPager.setPageTransformer(false, new CubeTransformer());
            mViewPager.setAdapter(mAdapter);
            mViewPager.setPageTransformer(true, new CommPageTransformer());
            mViewPager.setCurrentItem(currIndex);
           // setTitle((currIndex + 1) + "/" + totalCount);
//           tv_num.setText((mViewPager.getCurrentItem() + 1) + "/" + totalCount);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.pic_activity_tv_back:
                finish();
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        tv_num.setText((position + 1) + "/" + totalCount);
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
}
