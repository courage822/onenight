package com.online.onenight2.profile.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.library.utils.DialogUtil;
import com.library.utils.PayUtils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.online.onenight2.R;
import com.online.onenight2.base.RxBus;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.constant.IConfigConstant;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.payutil.IabHelper;
import com.online.onenight2.payutil.IabResult;
import com.online.onenight2.payutil.Purchase;
import com.online.onenight2.utils.AppsflyerUtils;
import com.online.onenight2.utils.TimeUtils;
import com.online.onenight2.xml.PlatformInfoXml;
import com.online.onenight2.xml.UserInfoXml;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Response;

public class PayActivity extends AppCompatActivity implements View.OnClickListener {
    //vip12个月
    RelativeLayout rl_vip_12months;
    //vip3个月
    RelativeLayout rl_vip_3months;
    //vip1个月
    RelativeLayout rl_vip_1months;
    //standed12个月
    RelativeLayout rl_standed_12months;
    //standed3个月
    RelativeLayout rl_standed_3months;
    //standed1个月
    RelativeLayout rl_standed_1months;
    //返回键
    TextView tv_back;
    IabHelper mHelper;
    static final int RC_REQUEST = 10001;
    @BindView(R.id.tv_write_12month)
    TextView tvWrite12month;
    @BindView(R.id.tv_write_3month)
    TextView tvWrite3month;
    @BindView(R.id.tv_write_1month)
    TextView tvWrite1month;
    @BindView(R.id.tv_vip_12month)
    TextView tvVip12month;
    @BindView(R.id.tv_vip_3month)
    TextView tvVip3month;
    @BindView(R.id.tv_vip_1month)
    TextView tvVip1month;
    private boolean iap_is_ok = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);
        ButterKnife.bind(this);
        initView();
        initData();
    }


    public void initView() {
        rl_vip_12months = (RelativeLayout) findViewById(R.id.pay_vip_month12);
        rl_vip_3months = (RelativeLayout) findViewById(R.id.pay_rl_vip_month3);
        rl_vip_1months = (RelativeLayout) findViewById(R.id.pay_rl_vip_month1);
        rl_standed_12months = (RelativeLayout) findViewById(R.id.pay_standed_month12);
        rl_standed_3months = (RelativeLayout) findViewById(R.id.pay_rl_standed_month3);
        rl_standed_1months = (RelativeLayout) findViewById(R.id.pay_rl_standed_month1);
        tv_back = (TextView) findViewById(R.id.pay_activity_tv_back);
        rl_vip_12months.setOnClickListener(this);
        rl_vip_3months.setOnClickListener(this);
        rl_vip_1months.setOnClickListener(this);
        rl_standed_12months.setOnClickListener(this);
        rl_standed_3months.setOnClickListener(this);
        rl_standed_1months.setOnClickListener(this);
        tv_back.setOnClickListener(this);
        //价格本地化
        getPricefromCountry();
    }

    private void getPricefromCountry() {
//        switch (PlatformInfoXml.getCountry()) {
        switch ( PlatformInfoXml.getPlatfromInfo().getCountry()) {
            case "America":
                tvWrite1month.setText("$9.99");
                tvWrite3month.setText("$19.99");
                tvWrite12month.setText("$49.99");
                tvVip1month.setText("$14.99");
                tvVip3month.setText("$29.99");
                tvVip12month.setText("$74.99");
                break;
            case "Australia":
                tvWrite1month.setText("$14.99");
                tvWrite3month.setText("$26.99");
                tvWrite12month.setText("$65.99");
                tvVip1month.setText("$18.99");
                tvVip3month.setText("$39.99");
                tvVip12month.setText("$98.99");
                break;
            case "India":
                tvWrite1month.setText("Rs620");
                tvWrite3month.setText("Rs1200");
                tvWrite12month.setText("Rs3100");
                tvVip1month.setText("Rs920");
                tvVip3month.setText("Rs1850");
                tvVip12month.setText("Rs4600");
                break;
            case "Indonesia":
                tvWrite1month.getLayoutParams().width=300;
                tvWrite3month.getLayoutParams().width=300;
                tvWrite12month.getLayoutParams().width=300;
                tvVip1month.getLayoutParams().width=300;
                tvVip3month.getLayoutParams().width=300;
                tvVip12month.getLayoutParams().width=300;
                tvWrite1month.setText("Rp149000");
                tvWrite3month.setText("Rp266000");
                tvWrite12month.setText("Rp666000");
                tvVip1month.setText("Rp219000");
                tvVip3month.setText("Rp439000");
                tvVip12month.setText("Rp1099000");
                break;
            case "UnitedKingdom":
                tvWrite1month.setText("£7.99");
                tvWrite3month.setText("£17.49");
                tvWrite12month.setText("£44.99");
                tvVip1month.setText("£12.99");
                tvVip3month.setText("£27.99");
                tvVip12month.setText("£69.99");
                break;
            case "Canada":
                tvWrite1month.setText("$13.99");
                tvWrite3month.setText("$27.99");
                tvWrite12month.setText("$69.99");
                tvVip1month.setText("$20.99");
                tvVip3month.setText("$39.99");
                tvVip12month.setText("$104.99");
                break;
            case "NewZealand":
                tvWrite1month.setText("$14.99");
                tvWrite3month.setText("$29.99");
                tvWrite12month.setText("$74.99");
                tvVip1month.setText("$21.99");
                tvVip3month.setText("$44.99");
                tvVip12month.setText("$109.99");
                break;
            case "Ireland":
                tvWrite1month.setText("9.99€");
                tvWrite3month.setText("19.99€");
                tvWrite12month.setText("49.99€");
                tvVip1month.setText("14.99€");
                tvVip3month.setText("29.99€");
                tvVip12month.setText("74.99€");
                break;
            case "SouthAfrica":
                tvVip12month.getLayoutParams().width=300;
                tvWrite1month.setText("R149.99");
                tvWrite3month.setText("R299.99");
                tvWrite12month.setText("R799.99");
                tvVip1month.setText("R229.99");
                tvVip3month.setText("R479.99");
                tvVip12month.setText("R1199.99");
                break;
            case "Singapore":
                tvVip12month.getLayoutParams().width=300;
                tvWrite1month.setText("S$14.99");
                tvWrite3month.setText("S$28.99");
                tvWrite12month.setText("S$68.99");
                tvVip1month.setText("S$21.99");
                tvVip3month.setText("S$44.99");
                tvVip12month.setText("S$104.99");
                break;
            case "Pakistan":
                tvWrite1month.setText("Rs1000");
                tvWrite3month.setText("Rs2000");
                tvWrite12month.setText("Rs5000");
                tvVip1month.setText("Rs1500");
                tvVip3month.setText("Rs3000");
                tvVip12month.setText("Rs7500");
                break;
            case "Philippines":
                tvWrite1month.setText("₱499");
                tvWrite3month.setText("₱999");
                tvWrite12month.setText("₱2490");
                tvVip1month.setText("₱749");
                tvVip3month.setText("₱1490");
                tvVip12month.setText("₱3790");
                break;
            case "HongKong":
                tvWrite1month.setText("HK$78");
                tvWrite3month.setText("HK$158");
                tvWrite12month.setText("HK$398");
                tvVip1month.setText("HK$118");
                tvVip3month.setText("HK$238");
                tvVip12month.setText("HK$588");
                break;
            case "Taiwan":
                tvWrite1month.setText("310TWD");
                tvWrite3month.setText("620TWD");
                tvWrite12month.setText("1550TWD");
                tvVip1month.setText("460TWD");
                tvVip3month.setText("930TWD");
                tvVip12month.setText("2320TWD");
                break;
            default:
                tvWrite1month.setText("$9.99");
                tvWrite3month.setText("$19.99");
                tvWrite12month.setText("$49.99");
                tvVip1month.setText("$14.99");
                tvVip3month.setText("$29.99");
                tvVip12month.setText("$74.99");
                break;
        }
    }


    public void initData() {
        // 支付统计和支付方式
        getPayWay();
        //初始化谷歌支付
        initGooglePay();
    }

    private void initGooglePay() {
        mHelper = new IabHelper(PayActivity.this, IConfigConstant.GOOGLE_APP_KEY);
        mHelper.enableDebugLogging(false);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    return;
                }
                iap_is_ok = true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pay_standed_month12:
                if (isAppInstalled(PayActivity.this, "com.android.vending")) {
                    mHelper.handleActivityResult(mHelper.mRequestCode, Activity.RESULT_CANCELED, null);
                    iapHandler.sendEmptyMessage(0);
                } else {
                    showMessage(getString(R.string.pay_tip_message));
                }
                break;
            case R.id.pay_rl_standed_month3:
                if (isAppInstalled(PayActivity.this, "com.android.vending")) {
                    mHelper.handleActivityResult(mHelper.mRequestCode, Activity.RESULT_CANCELED, null);
                    iapHandler.sendEmptyMessage(1);
                } else {
                    showMessage(getString(R.string.pay_tip_message));
                }
                break;
            case R.id.pay_rl_standed_month1:
                if (isAppInstalled(PayActivity.this, "com.android.vending")) {
                    mHelper.handleActivityResult(mHelper.mRequestCode, Activity.RESULT_CANCELED, null);
                    iapHandler.sendEmptyMessage(2);
                } else {
                    showMessage(getString(R.string.pay_tip_message));
                }
                break;
            case R.id.pay_vip_month12:
                if (isAppInstalled(PayActivity.this, "com.android.vending")) {
                    mHelper.handleActivityResult(mHelper.mRequestCode, Activity.RESULT_CANCELED, null);
                    iapHandler.sendEmptyMessage(3);
                } else {
                    showMessage(getString(R.string.pay_tip_message));
                }
                break;
            case R.id.pay_rl_vip_month3:
                if (isAppInstalled(PayActivity.this, "com.android.vending")) {
                    mHelper.handleActivityResult(mHelper.mRequestCode, Activity.RESULT_CANCELED, null);
                    iapHandler.sendEmptyMessage(4);
                } else {
                    showMessage(getString(R.string.pay_tip_message));
                }
                break;
            case R.id.pay_rl_vip_month1:
                if (isAppInstalled(PayActivity.this, "com.android.vending")) {
                    mHelper.handleActivityResult(mHelper.mRequestCode, Activity.RESULT_CANCELED, null);
                    iapHandler.sendEmptyMessage(5);
                } else {
                    showMessage(getString(R.string.pay_tip_message));
                }
                break;
            case R.id.pay_activity_tv_back:
                finish();
                break;
        }
    }

    //开启子线程
    Handler iapHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    if (iap_is_ok && mHelper != null) {
                        mHelper.launchPurchaseFlow(PayActivity.this, ConfigConstant.SKU_STANDARD12,
                                RC_REQUEST, mPurchaseFinishedListener);
                    } else {
                        showMessage(getString(R.string.pay_tip_message));
                    }
                    break;
                case 1:
                    if (iap_is_ok && mHelper != null) {
                        mHelper.launchPurchaseFlow(PayActivity.this, ConfigConstant.SKU_STANDARD3,
                                RC_REQUEST, mPurchaseFinishedListener);
                    } else {
                        showMessage(getString(R.string.pay_tip_message));
                    }
                    break;
                case 2:
                    if (iap_is_ok && mHelper != null) {
                        mHelper.launchPurchaseFlow(PayActivity.this, ConfigConstant.SKU_STANDARD1,
                                RC_REQUEST, mPurchaseFinishedListener);
                    } else {
                        showMessage(getString(R.string.pay_tip_message));
                    }
                    break;
                case 3:
                    if (iap_is_ok && mHelper != null) {
                        mHelper.launchPurchaseFlow(PayActivity.this, ConfigConstant.SKU_VIP12,
                                RC_REQUEST, mPurchaseFinishedListener);
                    } else {
                        showMessage(getString(R.string.pay_tip_message));
                    }
                    break;
                case 4:
                    if (iap_is_ok && mHelper != null) {
                        mHelper.launchPurchaseFlow(PayActivity.this, ConfigConstant.SKU_VIP3,
                                RC_REQUEST, mPurchaseFinishedListener);
                    } else {
                        showMessage(getString(R.string.pay_tip_message));
                    }
                    break;
                case 5:
                    if (iap_is_ok && mHelper != null) {
                        mHelper.launchPurchaseFlow(PayActivity.this, ConfigConstant.SKU_VIP1,
                                RC_REQUEST, mPurchaseFinishedListener);
                    } else {
                        showMessage(getString(R.string.pay_tip_message));
                    }
                    break;
                default:

                    break;
            }
        }

        ;
    };
    //完成消费的监听
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if (result.isFailure()) {
                Log.e("AAAAAAA", "onIabPurchaseFinished: " + result.getMessage());
//                Toast.makeText(PayActivity.this, "onIabPurchaseFinished:--"+result.getMessage(), Toast.LENGTH_SHORT).show();
                return;
            }
            if (purchase != null && purchase.getSku() != null) {
                Log.e("AAAAAAA", "onIabPurchaseFinished: " + purchase.getSku());
                if (purchase.getSku().equals(ConfigConstant.SKU_STANDARD12)) {
                    pay(purchase, PlatformInfoXml.getPlatfromInfo().getFid() + "003", "1");
                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                }
                if (purchase.getSku().equals(ConfigConstant.SKU_STANDARD3)) {
                    pay(purchase, PlatformInfoXml.getPlatfromInfo().getFid() + "001", "1");
                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                }
                if (purchase.getSku().equals(ConfigConstant.SKU_STANDARD1)) {
                    pay(purchase, PlatformInfoXml.getPlatfromInfo().getFid() + "002", "1");
                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                }
                if (purchase.getSku().equals(ConfigConstant.SKU_VIP12)) {
                    pay(purchase, PlatformInfoXml.getPlatfromInfo().getFid() + "203", "6");
                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                }
                if (purchase.getSku().equals(ConfigConstant.SKU_VIP3)) {
                    pay(purchase, PlatformInfoXml.getPlatfromInfo().getFid() + "201", "6");
                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                }
                if (purchase.getSku().equals(ConfigConstant.SKU_VIP1)) {
                    pay(purchase, PlatformInfoXml.getPlatfromInfo().getFid() + "202", "6");
                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                }
            }
        }
    };

    //消费成功的回调
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            if (purchase != null && purchase.getSku() != null) {
                if (purchase.getSku().equals(ConfigConstant.SKU_STANDARD12)) {
                    AppsflyerUtils.appsPurchase("1550");
                    showMessage(getString(R.string.pay_success_months12));
                }
                if (purchase.getSku().equals(ConfigConstant.SKU_STANDARD3)) {
                    AppsflyerUtils.appsPurchase("620");
                    showMessage(getString(R.string.pay_success_months3));
                }
                if (purchase.getSku().equals(ConfigConstant.SKU_STANDARD1)) {
                    AppsflyerUtils.appsPurchase("310");
                    showMessage(getString(R.string.pay_success_months1));
                }
                if (purchase.getSku().equals(ConfigConstant.SKU_VIP12)) {
                    AppsflyerUtils.appsPurchase("2320");
                    showMessage(getString(R.string.pay_success_vip12));
                }
                if (purchase.getSku().equals(ConfigConstant.SKU_VIP3)) {
                    AppsflyerUtils.appsPurchase("930");
                    showMessage(getString(R.string.pay_success_vip3));
                }
                if (purchase.getSku().equals(ConfigConstant.SKU_VIP1)) {
                    AppsflyerUtils.appsPurchase("460");
                    showMessage(getString(R.string.pay_success_vip1));
                }
            } else {

            }
        }
    };

    //提示信息
    private void showMessage(String message) {
        DialogUtil.showSingleBtnDialog(getSupportFragmentManager(), getString(R.string.pay_tip), message, getString(R.string.change_sure), true, null);
    }

    //判断应用是否在谷歌市场上存在
    private boolean isAppInstalled(Context context, String packagename) {
        PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(packagename, 0);
        } catch (PackageManager.NameNotFoundException e) {
            packageInfo = null;
            e.printStackTrace();
        }
        if (packageInfo == null) {
            return false;
        } else {
            return true;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        } else {

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null)
            mHelper.dispose();
        mHelper = null;
    }

    public void pay(final Purchase purchase, final String serviceid, final String payType) {
        if (purchase != null) {
            String transactionToken = PayUtils.encryptPay(PlatformInfoXml.getPlatfromInfo().getPid(), purchase.getOrderId());

            OkGo.post(IUrlConstant.GOOGLE_PAY)
                    .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                    .params("token", PlatformInfoXml.getToken())
                    .params("transactionId", purchase.getOrderId())
                    .params("payTime", purchase.getPurchaseTime() + "")
                    .params("clientTime", TimeUtils.getCurrentTime())
                    .params("userGoogleId", " ")
                    .params("payType", payType)
                    .params("googleId", purchase.getPackageName())
                    .params("serviceId", serviceid)
                    .params("userId", UserInfoXml.getUID())
                    .params("transactionToken", transactionToken)
                    .params("packageName", purchase.getPackageName())
                    .params("productId", purchase.getSku())
                    .params("purchaseToken", purchase.getToken())
                    .execute(new StringCallback() {
                                 @Override
                                 public void onSuccess(String response, Call call, Response s) {
                                     Log.e("AAAAAAA", "onSuccess: " + response + "--" + call + "--" + s);
                                     if (!TextUtils.isEmpty(response) && "success".equals(response)) {
                                         // 更新用户支付信息
                                         RxBus.getInstance().post("changePay", "flush");
                                         Toast.makeText(PayActivity.this, getString(R.string.pay_success), Toast.LENGTH_SHORT).show();
                                     } else {
                                         Toast.makeText(PayActivity.this, getString(R.string.pay_fail), Toast.LENGTH_SHORT).show();
                                     }
                                 }

                                 @Override
                                 public void onError(Call call, Response response, Exception e) {
                                     super.onError(call, response, e);
                                     Log.e("AAAAAAA", "onError: " + call + "---" + response + "--" + e.toString());
                                     Toast.makeText(PayActivity.this, getString(R.string.pay_fail), Toast.LENGTH_SHORT).show();
                                 }
                             }
                    );
        }
    }

    private void getPayWay() {
        OkGo.post(IUrlConstant.URL_PAY_WAY)
                .headers("token", PlatformInfoXml.getToken())
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("fromTag", "3")
                .params("serviceWay", "-1")
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        if (!TextUtils.isEmpty(s)) {
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                    }
                });
    }


}
