package com.online.onenight2.profile.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.bm.library.PhotoView;

import java.util.List;

/**
 * 描述：查看图片适配器
 * Created by qyh on 2017/2/22.
 */

public class PicturePagerAdapter extends PagerAdapter {
    private List<PhotoView> mPhotoViewList;

    public PicturePagerAdapter(List<PhotoView> list) {
        this.mPhotoViewList = list;
    }
    @Override
    public int getCount() {
        return mPhotoViewList == null ? 0 : mPhotoViewList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        PhotoView photoView = mPhotoViewList.get(position);
        container.addView(photoView, photoView.getLayoutParams());
        return photoView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

}
