package com.online.onenight2.profile.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.library.dialog.OnSingleDialogClickListener;
import com.library.utils.DialogUtil;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseActivity;
import com.online.onenight2.base.RxBus;
import com.online.onenight2.constant.IConfigConstant;
import com.online.onenight2.good.ActionSheetDialog;
import com.online.onenight2.good.OnOperItemClickL;
import com.online.onenight2.loginregs.activity.ChangePwdActivity;
import com.online.onenight2.loginregs.activity.LoginAcitivity;
import com.online.onenight2.utils.YeMeiShow;
import com.online.onenight2.xml.UserInfoXml;
import com.online.onenight2.yemei.model.MyPrestener;
import com.online.onenight2.yemei.view.IVContract;

import butterknife.BindView;

public class SettingActivity extends BaseActivity implements View.OnClickListener,IVContract{
    @BindView(R.id.setting_tv_change_back)
    TextView tv_back;
    @BindView(R.id.setting_rl_xiugaimima)
    RelativeLayout rl_xiugaimima;
    @BindView(R.id.setting_rl_about)
    RelativeLayout rl_about;
    @BindView(R.id.setting_activity_tv_banben)
    TextView tv_banben;
    @BindView(R.id.setting_activity_change_sure)
    Button btn_change;
    private MyPrestener myPrestener;
    @Override
    public int getLayoutId() {
        return R.layout.activity_setting;
    }

    @Override
    public Object newPresenter() {
        return null;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        tv_back.setOnClickListener(this);
        rl_xiugaimima.setOnClickListener(this);
        rl_about.setOnClickListener(this);
        btn_change.setOnClickListener(this);
        myPrestener=new MyPrestener(this);
    }

    @Override
    public void initData() {
        PackageManager packageManager=getPackageManager();
        try {
            PackageInfo info= packageManager.getPackageInfo(getPackageName(),0);
            String versions=info.versionName;
            tv_banben.setText("v"+versions);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.setting_tv_change_back:
                finish();
                break;
            case R.id.setting_rl_xiugaimima:
                Intent intent = new Intent(SettingActivity.this, ChangePwdActivity.class);
                startActivity(intent);
                break;
            case R.id.setting_rl_about:
                DialogUtil.showSingleBtnDialog(getSupportFragmentManager(),getString(R.string.profile_about_us), IConfigConstant.Mail,getString(R.string.change_sure),false, new OnSingleDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view) {
                    }
                });
                break;
            case R.id.setting_activity_change_sure:
                actionSheetDialogNoTitle();
                break;
        }
    }
        //自定义弹出对话框
    private void actionSheetDialogNoTitle() {
        final ActionSheetDialog dialog;
        final String[] stringItems1 = {getString(R.string.profile_tuichutishi)};
        dialog = new ActionSheetDialog(SettingActivity.this, stringItems1, null);
        dialog.isTitleShow(false).show();
        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
//                    LogUtil.e(LogUtil.TAG_LMF,"username"+ UserInfoXml.getUserName() );
                    UserInfoXml.setIsExitLogin(false);
                    Intent intent_change=new Intent(SettingActivity.this, LoginAcitivity.class);
                    startActivity(intent_change);
//                    RxBus.getInstance().post("change_user",true);
                    RxBus.getInstance().post("activity_finish",true);
                    finish();
                    overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
                }
                dialog.dismiss();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        myPrestener.isCanSee(true);
        myPrestener.loadData();
    }

    @Override
    public Context getContext() {
        return SettingActivity.this;
    }

    @Override
    public void setYeMeiData(String thumbnailUrl, int userId, String nickName, int age, int sex, String height, String msg, int type, int noticeType) {
        YeMeiShow.initNotification(SettingActivity.this,getSupportFragmentManager(),thumbnailUrl, userId,  nickName, age, sex,  height,  msg,  type,  noticeType);
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        myPrestener.isCanSee(true);
    }
    @Override
    protected void onPause() {
        super.onPause();
        myPrestener.isCanSee(false);
    }
}