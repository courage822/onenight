package com.online.onenight2.profile.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.library.dialog.OnListDoubleDialogClickListener;
import com.library.utils.DialogUtil;
import com.library.utils.ToastUtil;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseActivity;
import com.online.onenight2.base.RxBus;
import com.online.onenight2.bean.Image;
import com.online.onenight2.bean.MyInfo;
import com.online.onenight2.bean.UpImage;
import com.online.onenight2.callback.JsonCallback;
import com.online.onenight2.constant.IConfigConstant;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.profile.adapter.MyPhotoAdapter;
import com.online.onenight2.profile.presenter.PhotoPresenter;
import com.online.onenight2.utils.FileSizeUtils;
import com.online.onenight2.xml.PlatformInfoXml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import okhttp3.Call;
import okhttp3.Response;

public class PhotoViewActivity extends BaseActivity<PhotoPresenter> implements AdapterView.OnItemClickListener, View.OnClickListener {
    @BindView(R.id.photo_activity_tv_back)
    TextView tv_back;
    @BindView(R.id.photo_activity_photo_gv)
    GridView gv;
    private List<String> photo_list;
    private static List<Image> listImagedate = new ArrayList<>();
    private static final int UP_FROM_LOCAL = 1;
    private static final int UP_FROM_CAMERA = 2;
    private static final int CROP = 3;
    private String isMain="2";
    private static MyPhotoAdapter adapter;
    private static final int PERMISSION_CODE_READ_EXTERNAL_STORAGE = 0;
    @Override
    public int getLayoutId() {
        return R.layout.activity_photo_view;
    }
    @Override
    public PhotoPresenter newPresenter() {
        return new PhotoPresenter();
    }
    @Override
    public void initView(Bundle savedInstanceState) {
    }
    @Override
    public void initData() {
        photo_list=new ArrayList<>();
        photo_list.add(getString(R.string.profile_benditupian));
        photo_list.add(getString(R.string.profile_paizhao));
        photo_list.add(getString(R.string.profile_quxiao));
        getUerInfo();
        tv_back.setOnClickListener(this);
        gv.setOnItemClickListener(this);
        adapter=new MyPhotoAdapter(PhotoViewActivity.this,listImagedate,getSupportFragmentManager());
        gv.setAdapter(adapter);
    }



    private Dialog dialog;
    Uri originalUri1;
    Bitmap upBitmap;
    int readPicDegree;
    File file;
    double fileOrFilesSize;
    private String getImagePath(Uri uri, String selection) {
        String path = null;
        //通过URI和selection来获取真是的图片路径
        Cursor cursor = getContentResolver().query(uri,null,selection,null,null);
        if (cursor != null){
            if (cursor.moveToFirst()){
                path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            }
            cursor.close();
        }
        return path;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bm = null;
        switch (requestCode) {
            // 如果是直接从相册获取
            case UP_FROM_LOCAL:
                if (data != null) {
                    Uri originalUri = data.getData(); // 获得图片的uri
                    cropImage(originalUri);
                }
                break;
            // 如果是调用相机拍照时
            case UP_FROM_CAMERA:

                if (data != null) {
                    Uri uri = data.getData();
                    if (uri != null) {
                        cropImage(uri);
                    } else {
                        // 拍照后，7.0以上系统读取图片时需要转换路径读取方式
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            cropImage(getUriFromFile(mContext, new File(IConfigConstant.LOCAL_PIC_PATH)));
                        } else {
                            cropImage(Uri.fromFile(new File(IConfigConstant.LOCAL_PIC_PATH)));
                        }
                    }
                } else {
                    // 拍照后，7.0以上系统读取图片时需要转换路径读取方式
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        cropImage(getUriFromFile(mContext, new File(IConfigConstant.LOCAL_PIC_PATH)));
                    } else {
                        cropImage(Uri.fromFile(new File(IConfigConstant.LOCAL_PIC_PATH)));
                    }
                }
                break;
            // 取得裁剪后的图片
            case CROP:
                if (resultCode != RESULT_CANCELED) {

                    final BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    options.inSampleSize = calculateInSampleSize(options, 480, 800);
                    // Decode bitmap with inSampleSize set
                    options.inJustDecodeBounds = false;
                    if (isMain.equals("2")) {
                        upBitmap = BitmapFactory.decodeFile(IConfigConstant.LOCAL_PIC_PATH, options);
                        saveMyBitmap(upBitmap, 100);
                        fileOrFilesSize = FileSizeUtils.getFileOrFilesSize(IConfigConstant.LOCAL_PIC_PATH,
                                FileSizeUtils.SIZETYPE_KB);
                    }
                    if (fileOrFilesSize > 200 && fileOrFilesSize <= 10000) {
                        int compress = (int) (120 / fileOrFilesSize * 100);
                        if (compress < 20) {
                            compress = 20;
                        }
                        saveMyBitmap(upBitmap, compress);
                    }
                    if (isMain.equals("2")) {
                        readPicDegree = readPicDegree(IConfigConstant.LOCAL_PIC_PATH);
                    }
                    if (readPicDegree != 0) {
                        Bitmap rotatedBitmap = rotateBitmap(readPicDegree, upBitmap);
                        if (fileOrFilesSize > 200 && fileOrFilesSize <= 10000) {
                            int compress = (int) (120 / fileOrFilesSize * 100);
                            if (compress < 20) {
                                compress = 20;
                            }
                            saveMyBitmap(upBitmap, compress);
                        }
                    }
                    if (isMain.equals("2")) {
                        file = new File(IConfigConstant.LOCAL_PIC_PATH);
                    }
                    if (fileOrFilesSize > 10000) {
                        file = null;
                        ToastUtil.showShortToast(this, getString(R.string.profile_bunengshiyong));
                    }
                }
                if (file != null && file.exists()) {
                  upPhotoView(isMain,file);
                }
                break;
            default:
                break;

        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? widthRatio : heightRatio;
        }
        return inSampleSize;
    }

    /**
     * 通过ExifInterface类读取图片文件的被旋转角度
     *
     * @param path ： 图片文件的路径
     * @return 图片文件的被旋转角度
     */
    public static int readPicDegree(String path) {
        int degree = 0;
        // 读取图片文件信息的类ExifInterface
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (exif != null) {
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
            }
        }
        return degree;
    }
    /**
     * 将图片纠正到正确方向
     *
     * @param degree ： 图片被系统旋转的角度
     * @param bitmap ： 需纠正方向的图片
     * @return 纠向后的图片
     */
    public static Bitmap rotateBitmap(int degree, Bitmap bitmap) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap bm = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                bitmap.getHeight(), matrix, true);
        return bm;
    }
    //裁剪图片
    private void cropImage(Uri localuri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(localuri, "image/*");
        intent.putExtra("crop", "true");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        // aspectX aspectY 是裁剪框宽高的比例
        intent.putExtra("aspectX", 3);
        intent.putExtra("aspectY", 4);
        if (isMain.equals("2")) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(IConfigConstant.LOCAL_PIC_PATH)));
        }
        intent.putExtra("return-data", false);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        startActivityForResult(intent, CROP);
    }
    File f;
    public void saveMyBitmap(Bitmap mBitmap, int compress) {
        if (isMain.equals("2")) {
            f = new File(IConfigConstant.LOCAL_PIC_PATH);
        }
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        mBitmap.compress(Bitmap.CompressFormat.JPEG, compress, fOut);
        try {
            fOut.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(position==0){
            DialogUtil.showListDialog(getSupportFragmentManager(), null, null, null, true, photo_list, new OnListDoubleDialogClickListener() {
                @Override
                public void onPositiveClick(View view) {
                }
                @Override
                public void onNegativeClick(View view) {
                }
                @Override
                public void onItemClick(View view, String item) {
                    if(item.equals(photo_list.get(0))){
                        Intent mIntent = new Intent(Intent.ACTION_PICK);
                        mIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        mIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        mIntent.setType("image/*");
                        startActivityForResult(mIntent, UP_FROM_LOCAL);
                    }else if(item.equals(photo_list.get(1))){
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                                // 请求android.permission.READ_EXTERNAL_STORAGE权限
                                ActivityCompat.requestPermissions(mContext, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_CODE_READ_EXTERNAL_STORAGE);
                                return;
                            }
                        }
                        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            // 表示对目标应用临时授权该Uri所代表的文件，7.0及以上
                            takePhotoIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        }
                        //设置照片的临时保存路径
                        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, getUriFromFile(mContext,new File(IConfigConstant.LOCAL_PIC_PATH)));
                        startActivityForResult(takePhotoIntent, UP_FROM_CAMERA);
                    }
                }
            });
        }else{
            PictureLookActivity.toPictureLookActivity(mContext, position-1, listImagedate);
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.photo_activity_tv_back:
                finish();
                break;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CODE_READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    // 表示对目标应用临时授权该Uri所代表的文件，7.0及以上
                    takePhotoIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                //设置照片的临时保存路径
                takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, getUriFromFile(mContext,new File(IConfigConstant.LOCAL_PIC_PATH)));
                startActivityForResult(takePhotoIntent, UP_FROM_CAMERA);

            }
        }
    }
    /**
     * 根据file获得Uri（适配7.0及以上）
     *
     * @param file
     */
    private Uri getUriFromFile(Context context, File file) {
        Uri imageUri = null;
        if (context != null && file != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                // 7.0及以上因为安全问题，需要通过Content Provider封装Uri对象
                imageUri = FileProvider.getUriForFile(mContext, "com.online.onenight.provider", file);
            } else {
                imageUri = Uri.fromFile(file);
            }
        }
        return imageUri;
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }
    //上传多张图片
    public void upPhotoView(String isMain,File file){
        OkGo.post(IUrlConstant.URL_UPLOAD_PHOTO)
                .tag(this)
                .isMultipart(true)
                .connTimeOut(60*1000)
                .readTimeOut(60*1000)
                .writeTimeOut(60*1000)
                .headers("isMain", isMain)
                .headers("productId", PlatformInfoXml.getPlatfromInfo().getProduct())
                .headers("fid", PlatformInfoXml.getPlatfromInfo().getFid())
                .headers("pid", PlatformInfoXml.getPlatfromInfo().getPid())
                .headers("country", PlatformInfoXml.getPlatfromInfo().getCountry())
                .headers("language", PlatformInfoXml.getPlatfromInfo().getLanguage())
                .headers("version", PlatformInfoXml.getPlatfromInfo().getVersion())
                .headers("platform", PlatformInfoXml.getPlatfromInfo().getPlatform())
                .params("file",file)
                .execute(new JsonCallback<UpImage>() {
                    @Override
                    public void onSuccess(final UpImage image, Call call, Response response) {
                        if (image != null) {
                            if (image.getIsSucceed() != null && image.getIsSucceed().equals("1")) {
                                getUerInfo();
                            }
                        }
                    }
                });

    }
    public static void getUerInfo() {
        OkGo.post(IUrlConstant.URL_USER_INFO)
                .headers("token", PlatformInfoXml.getToken())
                .params("platformInfo",PlatformInfoXml.getPlatformJsonString())
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String myInfo, Call call, Response response) {
                        if (!TextUtils.isEmpty(myInfo)) {
                            MyInfo myInfo1 = JSON.parseObject(myInfo, MyInfo.class);
                            if(myInfo1!=null){
                                listImagedate.clear();
                                listImagedate.addAll(myInfo1.getUserEnglish().getListImage());
                                adapter.notifyDataSetChanged();
                                RxBus.getInstance().post("rx_photo_num",listImagedate.size()+"");
//                                SharedPreferenceUtil.setStringValue(, "PHOTO_NUM", "num", String.valueOf(listImagedate.size()));
                            }
                        }
                    }
                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                    }
                });
    }
    //删除照片
    public static void  deletePhoto(String photoUid){
        OkGo.post(IUrlConstant.URL_DELETE_PHOTO)
                .headers("token", PlatformInfoXml.getToken())
                .params("platformInfo",PlatformInfoXml.getPlatformJsonString())
                .params("imgId",photoUid)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String myInfo, Call call, Response response) {
                        getUerInfo();
                    }
                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                    }
                });
    }
}
