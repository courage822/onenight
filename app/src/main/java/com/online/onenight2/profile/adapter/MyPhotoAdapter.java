package com.online.onenight2.profile.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.library.dialog.OnDoubleDialogClickListener;
import com.library.imageloader.ImageLoader;
import com.library.imageloader.ImageLoaderUtil;
import com.library.utils.DialogUtil;
import com.online.onenight2.R;
import com.online.onenight2.bean.Image;
import com.online.onenight2.profile.activity.PhotoViewActivity;

import java.util.List;

public class MyPhotoAdapter extends BaseAdapter {

	private Context context;
	private List<Image> listImage;
    private FragmentManager getSupportFragmentManager;

	public MyPhotoAdapter(Context context, List<Image> listImage,FragmentManager supportFragmentManager) {
		this.context = context;
		this.listImage = listImage;
        this.getSupportFragmentManager=supportFragmentManager;
	}


    @Override
	public int getCount() {
		return listImage.size() + 1;
	}

	@Override
	public Object getItem(int position) {
		if (position < listImage.size()) {
			return listImage.get(position-1);
		} else {
			return null;
		}
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = View.inflate(context, R.layout.item_photo, null);
			viewHolder.image = (ImageView) convertView
					.findViewById(R.id.item_photo_iv);
			viewHolder.image_cancle = (ImageView) convertView
					.findViewById(R.id.item_photo_cancle);
			viewHolder.item_tv = (TextView) convertView
					.findViewById(R.id.item_photo_tv);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		if(position==0){
			viewHolder.image.setVisibility(View.GONE);
			viewHolder.item_tv.setVisibility(View.VISIBLE);
			viewHolder.image_cancle.setVisibility(View.GONE);
		}else {
            if (position < listImage.size() + 1) {
                if (listImage.get(position - 1).thumbnailUrl != null) {
                    if (!listImage.get(position - 1).thumbnailUrl
                            .equals(viewHolder.image.getTag())) {
                        viewHolder.image.setVisibility(View.VISIBLE);
                        viewHolder.item_tv.setVisibility(View.GONE);
                        ImageLoaderUtil.getInstance().loadImage(context, new ImageLoader.Builder().url(listImage.get(position - 1).thumbnailUrl).imageView(viewHolder.image).build());
                        viewHolder.image_cancle.setVisibility(View.VISIBLE);
                        viewHolder.image
                                .setTag(listImage.get(position - 1).thumbnailUrl);
                    } else {
                        viewHolder.image.setVisibility(View.VISIBLE);
                        viewHolder.item_tv.setVisibility(View.GONE);
                        ImageLoaderUtil.getInstance().loadImage(context, new ImageLoader.Builder().url(listImage.get(position - 1).thumbnailUrl).imageView(viewHolder.image).build());
                        viewHolder.image_cancle.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (!listImage.get(position - 1).imageUrl.equals(viewHolder.image
                            .getTag())) {
                        viewHolder.image.setVisibility(View.VISIBLE);
                        viewHolder.item_tv.setVisibility(View.GONE);
                        ImageLoaderUtil.getInstance().loadImage(context, new ImageLoader.Builder().url(listImage.get(position - 1).thumbnailUrl).imageView(viewHolder.image).build());
                        viewHolder.image.setTag(listImage.get(position - 1).imageUrl);
                        viewHolder.image_cancle.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.image.setVisibility(View.VISIBLE);
                        viewHolder.item_tv.setVisibility(View.GONE);
                        ImageLoaderUtil.getInstance().loadImage(context, new ImageLoader.Builder().url(listImage.get(position - 1).thumbnailUrl).imageView(viewHolder.image).build());
                        viewHolder.image_cancle.setVisibility(View.VISIBLE);
                    }
                }

                viewHolder.image_cancle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Image image = listImage.get(position - 1);
                        if (image != null) {
                            String cancel = context.getResources().getString(R.string.cancel);
                            String sure = context.getResources().getString(R.string.sure);
                            String delet_photo = context.getResources().getString(R.string.delet_photo);
                            DialogUtil.showDoubleBtnDialog(getSupportFragmentManager, null, delet_photo,sure ,cancel, true, new OnDoubleDialogClickListener() {
                                @Override
                                public void onPositiveClick(View view) {
                                    PhotoViewActivity.deletePhoto(image.getId());
                                }

                                @Override
                                public void onNegativeClick(View view) {
                                }
                            });
                        }
                    }
                });
            }
        }

		return convertView;
	}

	private static class ViewHolder {
		private ImageView image;
		private ImageView image_cancle;
		private TextView item_tv;
	}


}
