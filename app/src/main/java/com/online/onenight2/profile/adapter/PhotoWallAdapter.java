package com.online.onenight2.profile.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.online.onenight2.R;
import com.online.onenight2.bean.Image;
import com.online.onenight2.utils.ImageLoaderUtils;

import java.util.List;

/**
 * 我的页面 照片墙 列表
 * Created  wangyong on 2017/4/18.
 * 本地图片
 */

public class PhotoWallAdapter extends RecyclerView.Adapter<PhotoWallAdapter.ItemViewHolder> {
    private Context mContext;
    private List<Image> mData;
    private OnItemClickListener mOnItemClickListener;
    private int i;
    public PhotoWallAdapter(Context mContext, List<Image> listImagedate) {
        this.mContext = mContext;
        this.mData=listImagedate;
        this.i=i;
    }
    public void setData(List<Image> mData) {
        this.mData = mData;
        this.notifyDataSetChanged();
    }
    public Image getData(int position){
        return mData.get(position);

    }
    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_view_show, null);
         ItemViewHolder vh = new ItemViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        if(mData!=null&&mData.get(position)!=null&&mData.get(position).thumbnailUrl!=null){
            String thumImage=mData.get(position).thumbnailUrl;
            ImageLoaderUtils.display(mContext, holder.item_user_pic,thumImage);
        }

    }

    @Override
    public int getItemCount() {
        return mData==null?0:mData.size();
    }
    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView item_user_pic;
        public ItemViewHolder(View itemView) {
            super(itemView);
       item_user_pic = (ImageView) itemView.findViewById(R.id.photo_view_show_iv);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            if(null!=mOnItemClickListener){
                mOnItemClickListener.onItemClick(v,this.getPosition());
            }

        }
    }

    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.mOnItemClickListener=onItemClickListener;
    }
}
