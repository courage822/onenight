package com.online.onenight2.profile.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.library.dialog.OnCheckBoxDoubleDialogClickListener;
import com.library.dialog.OnEditDoubleDialogClickListener;
import com.library.dialog.OneWheelDialog;
import com.library.dialog.ThreeWheelDialog;
import com.library.utils.DialogUtil;
import com.library.utils.HeightUtils;
import com.library.utils.ToastUtil;
import com.library.utils.Util;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseActivity;
import com.online.onenight2.base.RxBus;
import com.online.onenight2.bean.Area;
import com.online.onenight2.bean.ChangeMyInfoBean;
import com.online.onenight2.bean.MatcherInfo;
import com.online.onenight2.bean.MyInfo;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.profile.presenter.ProfilePresenter;
import com.online.onenight2.utils.ParamsUtils;
import com.online.onenight2.utils.ProfileSpUtils;
import com.online.onenight2.utils.ProvinceUtils;
import com.online.onenight2.xml.PlatformInfoXml;
import com.online.onenight2.xml.UserInfoXml;
import com.online.onenight2.yemei.model.MyPrestener;
import com.online.onenight2.yemei.view.IVContract;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import okhttp3.Call;
import okhttp3.Response;
import rx.functions.Action1;

public class ProfileActivity extends BaseActivity<ProfilePresenter> implements View.OnClickListener,IVContract {
    @BindView(R.id.profile_activity_rl_introduce)
    RelativeLayout rl_introduce;
    @BindView(R.id.profile_activity_tv_introduce)
    TextView tv_introduce;
    @BindView(R.id.profile_activity_rl_nickname)
    RelativeLayout rl_nickname;
    @BindView(R.id.profile_activity_tv_nickname)
    TextView tv_nickname;
    @BindView(R.id.profile_activity_rl_birthday)
    RelativeLayout rl_birthday;
    @BindView(R.id.profile_activity_tv_birthday)
    TextView tv_birthday;
    @BindView(R.id.profile_activity_rl_age)
    RelativeLayout rl_age;
    @BindView(R.id.profile_activity_tv_age)
    TextView tv_age;
    @BindView(R.id.profile_activity_rl_xingzuo)
    RelativeLayout rl_xingzuo;
    @BindView(R.id.profile_activity_tv_xingzuo)
    TextView tv_xingzuo;
    @BindView(R.id.profile_activity_rl_juzhudi)
    RelativeLayout rl_juzhudi;
    @BindView(R.id.profile_activity_tv_juzhudi)
    TextView tv_juzhudi;
    @BindView(R.id.profile_activity_rl_shengao)
    RelativeLayout rl_shengao;
    @BindView(R.id.profile_activity_tv_shengao)
    TextView tv_shengao;
    @BindView(R.id.profile_activity_rl_xueli)
    RelativeLayout rl_xueli;
    @BindView(R.id.profile_activity_tv_xueli)
    TextView tv_xueli;
    @BindView(R.id.profile_activity_rl_shouru)
    RelativeLayout rl_shouru;
    @BindView(R.id.profile_activity_tv_shouru)
    TextView tv_shouru;
    @BindView(R.id.profile_activity_rl_hunyin)
    RelativeLayout rl_hunyin;
    @BindView(R.id.profile_activity_tv_hunyin)
    TextView tv_hunyin;
    @BindView(R.id.profile_activity_rl_yundong)
    RelativeLayout rl_yundong;
    @BindView(R.id.profile_activity_tv_yundong)
    TextView tv_yundong;
    @BindView(R.id.profile_activity_rl_zhiye)
    RelativeLayout rl_zhiye;
    @BindView(R.id.profile_activity_tv_zhiye)
    TextView tv_zhiye;
    @BindView(R.id.profile_activity_save_data)
    Button btn_save;
    @BindView(R.id.profile_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.profile_activity_rl_hobit)
    RelativeLayout rl_hobit;
    @BindView(R.id.profile_activity_tv_hobit)
    TextView tv_hobit;
    private MatcherInfo matcherInfo;
    private String introduce_content;
    private int num=3;
    private ChangeMyInfoBean mchangeMBeandate=new ChangeMyInfoBean();
    private MyPrestener myPrestener;
    private String hobit_string=null;
    @Override
    public int getLayoutId() {
        return R.layout.activity_profile;
    }
    @Override
    public ProfilePresenter newPresenter() {
        return new ProfilePresenter();
    }
    @Override
    public void initView(Bundle savedInstanceState) {
        rl_introduce.setOnClickListener(this);
        rl_nickname.setOnClickListener(this);
        rl_xingzuo.setOnClickListener(this);
        rl_juzhudi.setOnClickListener(this);
        rl_shengao.setOnClickListener(this);
        rl_xueli.setOnClickListener(this);
        rl_shouru.setOnClickListener(this);
        rl_yundong.setOnClickListener(this);
        rl_hunyin.setOnClickListener(this);
        rl_zhiye.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        btn_save.setOnClickListener(this);
        rl_hobit.setOnClickListener(this);
        rl_birthday.setOnClickListener(this);
        myPrestener=new MyPrestener(this);
    }
    @Override
    public void initData() {
        if (UserInfoXml.getMatchInfo() != null) {
       matcherInfo = JSON.parseObject(UserInfoXml.getMatchInfo(), MatcherInfo.class);
        } else {
            matcherInfo = new MatcherInfo();
        }
        initSetData();
    }
    public void initSetData() {
//个人介绍
        if(UserInfoXml.getMonologue()!=null&&!UserInfoXml.getMonologue().equals(Util.convertText("沒有內心獨白。"))){
            tv_introduce.setText(UserInfoXml.getMonologue());
        }
//昵称
        if(!TextUtils.isEmpty(UserInfoXml.getNickName())){
            tv_nickname.setText(UserInfoXml.getNickName());
        }
//生日
        if(!TextUtils.isEmpty(UserInfoXml.getBirthday())){
            tv_birthday.setText(UserInfoXml.getBirthday());
        }
//年龄
        if(!TextUtils.isEmpty(UserInfoXml.getAge())){
            tv_age.setText(UserInfoXml.getAge());
        }
//星座
        if(!TextUtils.isEmpty(UserInfoXml.getConstellation())){
             tv_xingzuo.setText(ParamsUtils.getConstellationMap().get(UserInfoXml.getConstellation()));
        }
//地区
        if(!TextUtils.isEmpty(UserInfoXml.getProvinceName())){
            tv_juzhudi.setText(UserInfoXml.getProvinceName());
        }
//身高
        if(!TextUtils.isEmpty(UserInfoXml.getHeight())){
           tv_shengao.setText(HeightUtils.getInchCmByCm(UserInfoXml.getHeight()));
        }
//学历

        if(!TextUtils.isEmpty(UserInfoXml.getEducation())&&!UserInfoXml.getEducation().equals("0")){
           tv_xueli.setText(ParamsUtils.getEducationMap().get(UserInfoXml.getEducation()));
        }
//职业
        if(!TextUtils.isEmpty(UserInfoXml.getWork())&&!UserInfoXml.getWork().equals("0")){
            tv_zhiye.setText(ParamsUtils.getWorkMap().get(UserInfoXml.getWork()));

        }
//收入
        if(!TextUtils.isEmpty(UserInfoXml.getIncome())&&!UserInfoXml.getIncome().equals("0") && ParamsUtils.getIncomeMap()!=null){
            tv_shouru.setText(ParamsUtils.getIncomeMap().get(UserInfoXml.getIncome()));
        }
//婚姻状况
        if(!TextUtils.isEmpty(UserInfoXml.getMarriage())&&!UserInfoXml.getMarriage().equals("0")){
            tv_hunyin.setText(ParamsUtils.getMarriageMap().get(UserInfoXml.getMarriage()));
        }
//运动
        if(!TextUtils.isEmpty(UserInfoXml.getSport())&&!UserInfoXml.getSport().equals("0")){
           tv_yundong.setText(ParamsUtils.getSportMap().get(UserInfoXml.getSport()));
        }
//兴趣爱好
        if(!TextUtils.isEmpty(ProfileSpUtils.getHobit(ProfileActivity.this))){
                tv_hobit.setText(ProfileSpUtils.getHobit(ProfileActivity.this));
        }else{
            tv_hobit.setText(R.string.profile_bianji);
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.profile_activity_rl_introduce:
               DialogUtil.showEditDoubleBtnDialog(getSupportFragmentManager(), getString(R.string.profile_introduce),getString(R.string.profile_shuruxinxi),getString(R.string.change_sure), getString(R.string.change_cancel), true, new OnEditDoubleDialogClickListener() {
                   @Override
                   public void onPositiveClick(View view, String content) {
                       if(!TextUtils.isEmpty(content.trim())){
                           introduce_content=content.trim();
                           mchangeMBeandate.setHeart(content.trim());
                           tv_introduce.setText(content.trim());
                       }
                   }
                   @Override
                   public void onNegativeClick(View view) {
                   }
               });
                break;
            case R.id.profile_activity_rl_nickname:
                DialogUtil.showEditDoubleBtnDialog(getSupportFragmentManager(),getString(R.string.profile_no_nickname),getString(R.string.profile_qingshurunicheng),getString(R.string.change_sure), getString(R.string.change_cancel), true, new OnEditDoubleDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String content) {
                        mchangeMBeandate.setNickName(content.trim());
                        tv_nickname.setText(content.trim());
                    }
                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;
            case R.id.profile_activity_rl_birthday:
                DialogUtil.showDateSelectDialog(getSupportFragmentManager(), getString(R.string.profile_birthday),getString(R.string.change_sure), getString(R.string.change_cancel), false, new ThreeWheelDialog.OnThreeWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText1, String selectedText2, String selectedText3) {
                        tv_birthday.setText(selectedText1+"-"+selectedText2+"-"+selectedText3);
                        mchangeMBeandate.setBirthday(selectedText1+"-"+selectedText2+"-"+selectedText3);

                    }

                    @Override
                    public void onNegativeClick(View view) {

                    }
                });

                break;
            case R.id.profile_activity_rl_juzhudi:
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), ProvinceUtils.getProvinceList(), getString(R.string.long_location),getString(R.string.change_sure), getString(R.string.change_cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                         tv_juzhudi.setText(selectedText);
                        mchangeMBeandate.setArea(selectedText);
                    }
                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;
            case R.id.profile_activity_rl_shengao:
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), HeightUtils.getInchCmList(),getString(R.string.height),getString(R.string.change_sure), getString(R.string.change_cancel), true, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                        tv_shengao.setText(selectedText);
                        mchangeMBeandate.setHeight(HeightUtils.getCmByInchCm(selectedText));
                    }

                    @Override
                    public void onNegativeClick(View view) {

                    }
                });
                break;
            case R.id.profile_activity_rl_xueli:
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), ParamsUtils.getEducationMapValue(), getString(R.string.education),getString(R.string.change_sure), getString(R.string.change_cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                    tv_xueli.setText(selectedText);
                    mchangeMBeandate.setEducation(ParamsUtils.getEducationMapKeyByValue(selectedText));
                    }
                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;
            case R.id.profile_activity_rl_shouru:
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), ParamsUtils.getIncomeMapValue(), getString(R.string.profile_no_shouru),getString(R.string.change_sure), getString(R.string.change_cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                       tv_shouru.setText(selectedText);
                        mchangeMBeandate.setIncome(ParamsUtils.getIncomeMapKeyByValue(selectedText));
                    }
                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;
            case R.id.profile_activity_rl_hunyin:
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), ParamsUtils.getMarriageMapValue(),getString(R.string.profile_no_hunyin),getString(R.string.change_sure), getString(R.string.change_cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                        tv_hunyin.setText(selectedText);
                        mchangeMBeandate.setMarriage(ParamsUtils.getMarriageMapKeyByValue(selectedText));
                    }
                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;
            case R.id.profile_activity_rl_yundong:
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), ParamsUtils.getSportMapValue(),getString(R.string.profile_no_yundong),getString(R.string.change_sure), getString(R.string.change_cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                        tv_yundong.setText(selectedText);
                        mchangeMBeandate.setSport(ParamsUtils.getSportMapKeyByValue(selectedText));
                    }
                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;
            case R.id.profile_activity_rl_zhiye:
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), ParamsUtils.getWorkMapValue(),getString(R.string.profile_no_jobs),getString(R.string.change_sure), getString(R.string.change_cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                        tv_zhiye.setText(selectedText);
                        mchangeMBeandate.setWork(ParamsUtils.getWorkMapKeyByValue(selectedText));
                    }
                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;
            case R.id.profile_activity_rl_hobit:
                DialogUtil.showCheckBoxDoubleBtnDialog(getSupportFragmentManager(),getString(R.string.profile_no_xingquaihao),getString(R.string.change_sure), getString(R.string.change_cancel), false, ParamsUtils.getInterestMapValue(), new OnCheckBoxDoubleDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, List<String> list) {
                        if(list!=null&&list.size()>0){
                            tv_hobit.setText(list.toString().substring(1,list.toString().length()-1));
                            hobit_string=list.toString().substring(1,list.toString().length()-1);
                            mchangeMBeandate.setInterest(getDateKey(list,"interest"));
                        }
                    }
                    @Override
                    public void onNegativeClick(View view) {

                    }
                });
                break;
            case R.id.profile_activity_save_data:
                RxBus.getInstance().post("change_name","change");
                if (!TextUtils.isEmpty(hobit_string)) {
                    ProfileSpUtils.saveHobit(hobit_string,ProfileActivity.this);
                }
                //保存数据
                saveData();
                break;
            case R.id.profile_activity_rl_back:
                finish();
                break;
            case R.id.profile_activity_rl_xingzuo:
                break;
        }
    }
    public String getDateValue(String key, Map<String, String> map) {
        if (key != null) {
            String[] spit = key.split("\\|");
            StringBuffer value = new StringBuffer();
            for (int i = 0; i < spit.length; i++) {
                if (i < spit.length - 1) {
                    value.append(map.get(spit[i]) + ",");
                } else {
                    value.append(map.get(spit[i]) + "");
                }
            }
            return value.toString();
        }
        return null;
    }
    public String getDateKey(List<String> list, String type) {
        if (list != null && list.size() > 0) {
            StringBuffer keys = new StringBuffer();
            for (int i = 0; i < list.size(); i++) {
                if (i < list.size() - 1) {

                    if (type.equals("spokenLanguage")) {
                        keys.append(ParamsUtils.getLanguageMapKeyByValue(list.get(i)) + "|");
                    } else if (type.equals("sport")) {
                        keys.append(ParamsUtils.getSportMapKeyByValue(list.get(i)) + "|");
                    } else if (type.equals("wantToGo")) {
                        keys.append(ParamsUtils.getTravelMapKeyByValue(list.get(i)) + "|");
                    } else if (type.equals("book")) {
//                        keys.append(ParamsUtils.getBookMapKeyByValue(list.get(i)) + "|");
                    }else if (type.equals("foods")) {
                        keys.append(ParamsUtils.getFoodsMapKeyByValue(list.get(i)) + "|");
                    }else if (type.equals("music")) {
                        keys.append(ParamsUtils.getMusicMapKeyByValue(list.get(i)) + "|");
                    } else if (type.equals("movies")) {
                        keys.append(ParamsUtils.getMoviesMapKeyByValue(list.get(i)) + "|");
                    } else if (type.equals("interest")) {
                        keys.append(ParamsUtils.getInterestMapKeyByValue(list.get(i)) + "|");
                    }
                } else {
                    if (type.equals("spokenLanguage")) {
                        keys.append(ParamsUtils.getLanguageMapKeyByValue(list.get(i)));
                    } else if (type.equals("sport")) {
                        keys.append(ParamsUtils.getSportMapKeyByValue(list.get(i)));
                    } else if (type.equals("wantToGo")) {
                        keys.append(ParamsUtils.getTravelMapKeyByValue(list.get(i)));
                    } else if (type.equals("book")) {
//                        keys.append(ParamsUtils.getBookMapKeyByValue(list.get(i)));
                    }else if (type.equals("foods")) {
                        keys.append(ParamsUtils.getFoodsMapKeyByValue(list.get(i)));
                    }else if (type.equals("music")) {
                        keys.append(ParamsUtils.getMusicMapKeyByValue(list.get(i)));
                    }else if (type.equals("movies")) {
                        keys.append(ParamsUtils.getMoviesMapKeyByValue(list.get(i)));
                    }else if (type.equals("interest")) {
                        keys.append(ParamsUtils.getInterestMapKeyByValue(list.get(i)));
                    }
                }
            }
            return new String(keys);
        }
        return null;
    }
    private void saveData() {
        if(!TextUtils.isEmpty(introduce_content)){
            mchangeMBeandate.setHeart(introduce_content);
        }
        Map<String,String> mchangeInfoParam=new HashMap<>();
        mchangeInfoParam.put("platformInfo", PlatformInfoXml.getPlatformJsonString());
        mchangeInfoParam.put("token",PlatformInfoXml.getToken());
//个人介绍
        if(!TextUtils.isEmpty(mchangeMBeandate.getHeart())){
            UserInfoXml.setMonologue(mchangeMBeandate.getHeart());
            mchangeInfoParam.put("monologue",mchangeMBeandate.getHeart());
            ++num;
        }
//昵称
        if(!TextUtils.isEmpty(mchangeMBeandate.getNickName())){
            UserInfoXml.setNickName(mchangeMBeandate.getNickName());
            mchangeInfoParam.put("nickName",mchangeMBeandate.getNickName());
            ++num;
        }
//生日
        if(!TextUtils.isEmpty(mchangeMBeandate.getBirthday())){
            UserInfoXml.setBirthday(mchangeMBeandate.getBirthday());
            mchangeInfoParam.put("birthday",mchangeMBeandate.getBirthday());
        }
//地区
        if(!TextUtils.isEmpty(mchangeMBeandate.getArea())){

            Map<String, Area> areamap = new HashMap<>();
            UserInfoXml.setProvinceName(mchangeMBeandate.getArea());
            Area areabean=new Area();
            areabean.setProvinceName(mchangeMBeandate.getArea());
            areamap.put("area",areabean);
            mchangeInfoParam.put("area",JSON.toJSONString(areamap));
            ++num;
        }
//身高
        if(!TextUtils.isEmpty(mchangeMBeandate.getHeight())){
            UserInfoXml.setHeight(mchangeMBeandate.getHeight());
            mchangeInfoParam.put("height",mchangeMBeandate.getHeight());
            ++num;
        }
//教育
        if(!TextUtils.isEmpty(mchangeMBeandate.getEducation())){
            UserInfoXml.setEducation(mchangeMBeandate.getEducation());
            mchangeInfoParam.put("education",mchangeMBeandate.getEducation());
            ++num;
        }
//职业
        if(!TextUtils.isEmpty(mchangeMBeandate.getWork())){
            UserInfoXml.setWork(mchangeMBeandate.getWork());
            mchangeInfoParam.put("occupation",mchangeMBeandate.getWork());
            ++num;
        }
//收入
        if(!TextUtils.isEmpty(mchangeMBeandate.getIncome())){
            UserInfoXml.setIncome(mchangeMBeandate.getIncome());
            mchangeInfoParam.put("income",mchangeMBeandate.getIncome());
            ++num;
        }
//婚姻状况
        if(!TextUtils.isEmpty(mchangeMBeandate.getMarriage())){
            UserInfoXml.setMarriage(mchangeMBeandate.getMarriage());
            mchangeInfoParam.put("marriage",mchangeMBeandate.getMarriage());
            ++num;
        }
//兴趣爱好
        if(!TextUtils.isEmpty(mchangeMBeandate.getInterest())){
            UserInfoXml.setInterest(mchangeMBeandate.getInterest());
            mchangeInfoParam.put("interest",mchangeMBeandate.getInterest());
            ++num;
        }
//运动
        if(!TextUtils.isEmpty(mchangeMBeandate.getSport())){
            UserInfoXml.setSport(mchangeMBeandate.getSport());
            mchangeInfoParam.put("sport",mchangeMBeandate.getSport());
            ++num;
        }
        int progress=(int)(((double)(num)/(double)13)*100);
        RxBus.getInstance().post("change",progress);
        RxBus.getInstance().post("nickname",mchangeMBeandate.getNickName());
        SharedPreferences sp=getSharedPreferences("save_progress", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sp.edit();
        edit.putInt("progress",progress);
        edit.putString("nickname",mchangeMBeandate.getNickName());
        edit.commit();
        UserInfoXml.setMatchInfo(JSON.toJSONString(matcherInfo));
         upLoaderMyInfo(mchangeInfoParam);
    }

    @Override
    protected void onResume() {
        super.onResume();
        myPrestener.isCanSee(true);
        myPrestener.loadData();
        mRxManager.on("finish", new Action1<String>() {
            @Override
            public void call(String s) {
                finish();
            }
        });
    }
    public void upLoaderMyInfo(Map<String,String> paramInfo) {
        OkGo.post(IUrlConstant.URL_UPLOAD_USER_INFO)
                .params(paramInfo)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String response, Call call, Response r) {
                        if(!TextUtils.isEmpty(response)){
                            MyInfo myInfo = JSON.parseObject(response, MyInfo.class);
                            if(myInfo!=null){
                                UserInfoXml.setUserInfo(myInfo.getUserEnglish());
                                    RxBus.getInstance().post("finish","完成上传");
                                ToastUtil.showShortToast(mContext, getString(R.string.message_modify_success));
                                finish();
                            }
                        }
                    }
                });

    }

    @Override
    public Context getContext() {
        return ProfileActivity.this;
    }

    @Override
    public void setYeMeiData(String thumbnailUrl, int userId, String nickName, int age, int sex, String height, String msg, int type, int noticeType) {
       // YeMeiShow.initNotification(ProfileActivity.this,getSupportFragmentManager(),thumbnailUrl, userId,  nickName, age, sex,  height,  msg,  type,  noticeType);
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        myPrestener.isCanSee(true);
    }
    @Override
    protected void onPause() {
        super.onPause();
        myPrestener.isCanSee(false);
    }
}
