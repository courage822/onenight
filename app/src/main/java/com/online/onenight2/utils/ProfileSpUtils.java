package com.online.onenight2.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Administrator on 2017/7/15.
 */

public class ProfileSpUtils {
    public static void saveHobit(String hobit,Context context){
        SharedPreferences sp=context.getSharedPreferences("hobit",Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString("hobit_string",hobit);
        edit.commit();
    };
    public static String getHobit(Context context){
        SharedPreferences sp=context.getSharedPreferences("hobit",Context.MODE_PRIVATE);
        String hobit_string = sp.getString("hobit_string", "");
       return hobit_string;
    }
    public static void clearHobit(Context context){
        SharedPreferences sp=context.getSharedPreferences("hobit",Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sp.edit();
        edit.clear();
        edit.commit();
    }
}
