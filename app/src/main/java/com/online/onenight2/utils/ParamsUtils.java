package com.online.onenight2.utils;


import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.library.utils.LogUtil;
import com.online.onenight2.bean.ParamsInit;
import com.online.onenight2.xml.UserInfoXml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 数据字典工具类
 * Created by jzrh on 2016/6/29.
 */
public class ParamsUtils {

    private static Gson gson;

    public static ParamsInit getParamsInit() {
        if(UserInfoXml.getParamsInit()!=null){
            gson = new Gson();
            return JSON.parseObject(UserInfoXml.getParamsInit(), ParamsInit.class);
        }else{
            return new ParamsInit();
        }
    }

    /**
     * 将map的key排序后，返回map的value生成的list
     *
     * @param map
     */
    private static List<String> getMapValue(Map<String, String> map) {
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        // 对key排序
        List<String> keyList = new ArrayList<String>();
        while (iterator.hasNext()) {
            keyList.add(iterator.next().getKey());
        }
        Collections.sort(keyList);
        List<String> list = new ArrayList<String>();
        for (int i = 0; i < keyList.size(); i++) {
            list.add(map.get(keyList.get(i)));
        }
        return list;
    }

    /**
     * 根据map和value获得对应的key
     *
     * @param map
     * @param value
     */
    private static String getMapKeyByValue(Map<String, String> map, String value) {
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * @return 获得星座Map
     */
    public static Map<String, String> getConstellationMap() {
        return getParamsInit().getConstellationMap2();
    }

    /**
     * @return 获得星座List
     */
    public static List<String> getConstellationMapValue() {
        return getMapValue(getConstellationMap());
    }

    /**
     * 根据value获得key(星座)
     */
    public static String getConstellationMapKeyByValue(String value) {
        return getMapKeyByValue(getConstellationMap(), value);
    }

    /**
     * @return 获得星座Map
     */
    public static Map<String, String> getPoliticalViewMap() {
        return getParamsInit().getPoliticalViewsMap2();
    }

    /**
     * @return 获得星座List
     */
    public static List<String> getPoliticalViewMapValue() {
        return getMapValue(getPoliticalViewMap());
    }


    /**
     * 根据value获得key(星座)
     */
    public static String getPoliticalViewMapKeyByValue(String value) {
        return getMapKeyByValue(getPoliticalViewMap(), value);
    }


    /**
     * @return 获得种族Map
     */
    public static Map<String, String> getEthnicityMap() {
        return getParamsInit().getEthnicityMap2();
    }

    /**
     * @return 获得种族List
     */
    public static List<String> getEthnicityMapValue() {
        return getMapValue(getEthnicityMap());
    }

    public static Map<String,String> getBodyType(){
        return getParamsInit().getBodyTypeRemoteMap2();

    }


    /**
     * 根据value获得key(种族)
     */
    public static String getEthnicityMapKeyByValue(String value) {
        return getMapKeyByValue(getEthnicityMap(), value);
    }

    /**
     * @return 获得语言Map
     */
    public static Map<String, String> getLanguageMap() {
        return getParamsInit().getLanguageMap2();
    }

    /**
     * @return 获得语言List
     */
    public static List<String> getLanguageMapValue() {
        return getMapValue(getLanguageMap());
    }

    /**
     * 根据value获得key(语言)
     */
    public static String getLanguageMapKeyByValue(String value) {
        return getMapKeyByValue(getLanguageMap(), value);
    }

    /**
     * @return 获得收入Map
     */
    public static Map<String, String> getIncomeMap() {
        return getParamsInit().getWorkMoneyMap2();
    }


    /**
     * @return 获得收入List
     */
    public static List<String> getIncomeMapValue() {
        return getMapValue(getIncomeMap());
    }

    /**
     * 根据value获得key(收入)
     */
    public static String getIncomeMapKeyByValue(String value) {
        return getMapKeyByValue(getIncomeMap(), value);
    }

    /**
     * @return 获得工作Map
     */
    public static Map<String, String> getWorkMap() {
        return getParamsInit().getWorkMap2();
    }

    /**
     * @return 获得工作List
     */
    public static List<String> getWorkMapValue() {
        return getMapValue(getWorkMap());
    }

    /**
     * 根据value获得key(工作)
     */
    public static String getWorkMapKeyByValue(String value) {//获取兴趣爱好
        return getMapKeyByValue(getWorkMap(), value);
    }

    /**
     * @return 获得学历Map
     */
    public static Map<String, String> getEducationMap() {
        return getParamsInit().getEducationMap2();
    }

    /**
     * @return 获得学历List
     */
    public static List<String> getEducationMapValue() {
        return getMapValue(getEducationMap());
    }

    /**
     * 根据value获得key(学历)
     */
    public static String getEducationMapKeyByValue(String value) {
        return getMapKeyByValue(getEducationMap(), value);
    }

    /**
     * @return 获得婚姻Map
     */
    public static Map<String, String> getMarriageMap() {
        return getParamsInit().getMarriageMap2();
    }

    /**
     * @return 获得婚姻List
     */
    public static List<String> getMarriageMapValue() {
        return getMapValue(getMarriageMap());
    }

    /**
     * 根据value获得key(婚姻)
     */
    public static String getMarriageMapKeyByValue(String value) {
        return getMapKeyByValue(getMarriageMap(), value);
    }
    /**
     * 根据value获得key(婚姻)
     */


    /**
     * @return 获得有无孩子Map
     */
    public static Map<String, String> getHaveKidsMap() {
        return getParamsInit().getHaveKidsMap2();
    }

    /**
     * @return 获得有无孩子List
     */
    public static List<String> getHaveKidsMapValue() {
        return getMapValue(getHaveKidsMap());
    }

    /**
     * 根据value获得key(有无孩子)
     */
    public static String getHaveKidsMapKeyByValue(String value) {
        return getMapKeyByValue(getHaveKidsMap(), value);
    }

    /**
     * @return 想要几个孩子Map
     */
    public static Map<String, String> getHowManyKidsMap() {
        return getParamsInit().getHowManyKidsMap2();
    }

    /**
     * @return 想要几个孩子List
     */
    public static List<String> getHowManyKidsMapValue() {
        return getMapValue(getHowManyKidsMap());
    }

    /**
     * 根据value获得key(想要几个孩子)
     */
    public static String getHowManyKidsMapKeyByValue(String value) {
        return getMapKeyByValue(getHowManyKidsMap(), value);
    }




    /**
     * @return 获得抽烟Map
     */
    public static Map<String, String> getSmokeMap() {
        return getParamsInit().getSmokeMap2();
    }

    /**
     * @return 获得抽烟List
     */
    public static List<String> getSmokeMapValue() {
        return getMapValue(getSmokeMap());
    }

    /**
     * 根据value获得key(抽烟)
     */
    public static String getSmokeMapKeyByValue(String value) {
        return getMapKeyByValue(getSmokeMap(), value);
    }

    /**
     * @return 获得喝酒Map
     */
    public static Map<String, String> getDrinkMap() {
        return getParamsInit().getDrinkMap2();
    }

    /**
     * @return 获得喝酒List
     */
    public static List<String> getDrinkMapValue() {
        return getMapValue(getDrinkMap());
    }

    /**
     * 根据value获得key(喝酒)
     */
    public static String getDrinkMapKeyByValue(String value) {
        return getMapKeyByValue(getDrinkMap(), value);
    }

    /**
     * @return 获得信仰Map
     */
    public static Map<String, String> getFaithMap() {
        return getParamsInit().getFaithMap2();
    }

    /**
     * @return 获得信仰List
     */
    public static List<String> getFaithMapValue() {
        return getMapValue(getFaithMap());
    }

    /**
     * 根据value获得key(信仰)
     */
    public static String getFaithMapKeyByValue(String value) {
        return getMapKeyByValue(getFaithMap(), value);
    }

    /**
     * @return 获得是否想要小孩Map
     */
    public static Map<String, String> getWantBabyMap() {
        return getParamsInit().getNeedBabeMap2();
    }

    /**
     * @return 获得是否想要小孩List
     */
    public static List<String> getWantBabyMapValue() {
        return getMapValue(getWantBabyMap());
    }

    /**
     * 根据value获得key(是否想要小孩)
     */
    public static String getWantBabyMapKeyByValue(String value) {
        return getMapKeyByValue(getWantBabyMap(), value);
    }
/*头发的颜色*/
    /**
     * @return Map
     */
    public  static  Map<String,String> getHairColorMap(){
        return getParamsInit().getHairMap2();
    }
    /**
     * @return List
     */
    public static List<String> getHairColorMapValue() {
        return getMapValue(getHairColorMap());
    }

    /**
     * 根据value获得key()
     */
    public static String getHairColorMapKeyByValue(String value) {
        return getMapKeyByValue(getHairColorMap(), value);
    }
/*眼睛的颜色*/
    /**
     * @return Map
     */
    public static Map<String, String> getEyesColorMap() {
        return getParamsInit().getEyesMap2();
    }

    /**
     * @return List
     */
    public static List<String> getEyesColorMapValue() {
        return getMapValue(getEyesColorMap());
    }

    /**
     * 根据value获得key()
     */
    public static String getEyesColorMapKeyByValue(String value) {
        return getMapKeyByValue(getEyesColorMap(), value);
    }

    /*动态体型特征*/
    public static Map<String, String> getBodyTypeDTMap() {
        return getParamsInit().getBodyTypeMap2();
    }

    /**
     * @return List
     */
    public static List<String> getBodyTypeDTMapValue() {
        return getMapValue(getBodyTypeDTMap());
    }

    /**
     * 根据value获得key()
     */
    public static String getBodyTypeDTMapKeyByValue(String value) {
        return getMapKeyByValue(getBodyTypeDTMap(), value);
    }


    /**
     * @return 获得喜爱的运动Map
     */
    public static Map<String, String> getSportMap() {
        return getParamsInit().getSportMap2();
    }

    /**
     * @return 获得喜爱的运动List
     */
    public static List<String> getSportMapValue() {
        return getMapValue(getSportMap());
    }

    /**
     * 根据value获得key(喜爱的运动)
     */
    public static String getSportMapKeyByValue(String value) {
        return getMapKeyByValue(getSportMap(), value);
    }

    /**
     * @return 获得宠物Map
     */
    public static Map<String, String> getPetsMap() {
        return getParamsInit().getPetsMap2();
    }

    /**
     * @return 获得宠物List
     */
    public static List<String> getPetsMapValue() {
        return getMapValue(getPetsMap());
    }

    /**
     * 根据value获得key(宠物)
     */
    public static String getPetsMapKeyByValue(String value) {
        return getMapKeyByValue(getPetsMap(), value);
    }

    /**
     * @return 获得锻炼习惯Map
     */
    public static Map<String, String> getExerciseHabitsMap() {
        return getParamsInit().getExerciseHabitsMap2();
    }

    /**
     * @return 获得锻炼习惯List
     */
    public static List<String> getExerciseHabitsMapValue() {
        return getMapValue(getExerciseHabitsMap());
    }

    /**
     * 根据value获得key(锻炼习惯)
     */
    public static String getExerciseHabitsMapKeyByValue(String value) {
        return getMapKeyByValue(getExerciseHabitsMap(), value);
    }

    /**
     * @return 获得兴趣Map
     */
    public static Map<String, String> getInterestMap() {
        return getParamsInit().getInterestMap2();
    }

    /**
     * @return 获得兴趣List
     */
    public static List<String> getInterestMapValue() {
        return getMapValue(getInterestMap());
    }

    /**
     * 根据value获得key(兴趣)
     */
    public static String getInterestMapKeyByValue(String value) {
        return getMapKeyByValue(getWorkMap(),
                value);
    }

    /**
     * @return 获得个性Map
     */
    public static Map<String, String> getPersonalMap() {
        return getParamsInit().getCharacteristicsMap2();
    }

    /**
     * @return 获得个性List
     */
    public static List<String> getPersonalMapValue() {
        return getMapValue(getPersonalMap());
    }

    /**
     * 根据value获得key(个性)
     */
    public static String getPersonalMapKeyByValue(String value) {
        return getMapKeyByValue(getPersonalMap(), value);
    }

    /**
     * @return 获得书和动漫Map
     */
    public static Map<String, String> getBookCartoonMap() {
        return getParamsInit().getBooksMap2();
    }

    /**
     * @return 获得书和动漫List
     */
    public static List<String> getBookCartoonMapValue() {
        return getMapValue(getBookCartoonMap());
    }

    /**
     * 根据value获得key(书和动漫)
     */
    public static String getBookCartoonMapKeyByValue(String value) {
        return getMapKeyByValue(getBookCartoonMap(), value);
    }

    /**
     * @return 获得旅行Map
     */
    public static Map<String, String> getTravelMap() {
        return getParamsInit().getTravelMap2();
    }

    /**
     * @return 获得旅行List
     */
    public static List<String> getTravelMapValue() {
        return getMapValue(getTravelMap());
    }

    /**
     * 根据value获得key(旅行)
     */
    public static String getTravelMapKeyByValue(String value) {
        return getMapKeyByValue(getTravelMap(), value);
    }

    /**
     * @return 系统推荐招呼信（男）
     */
    public static List<String> getMaleSayHelloList() {
        return getParamsInit().getMaleSayHelloList();
    }

    /**
     * @return 系统推荐招呼信（女）
     */
    public static List<String> getFemaleSayHelloList() {
        return getParamsInit().getFemaleSayHelloList();
    }

    /**
     * @return 系统推荐QA信（男）
     */
    public static List<String> getMaleQuestionTagList() {
        return getParamsInit().getMaleQuestionTagList();
    }

    /**
     * @return 系统推荐QA信（女）
     */
    public static List<String> getFemaleQuestionTagList() {
        return getParamsInit().getFemaleQuestionTagList();
    }

    /**
     * 获得血型Map
     *
     * @return
     */
    public static Map<String, String> getBloodMap() {
        return getParamsInit().getBloodMap2();
    }

    /**
     * @return 获得血型List
     */
    public static List<String> getBloodMapValue() {
        Map<String, String> map = getBloodMap();
        List<String> list = new ArrayList<String>();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getValue());
        }
        return list;
    }


    /**
     * 根据value获得key(血型)
     *
     * @param value
     * @return
     */
    public static String getBloodMapKeyByValue(String value) {
        Map<String, String> map = getBloodMap();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * 获得魅力Map
     *
     * @return
     */
    public static Map<String, String> getCharmMap() {
        return getParamsInit().getCharmMap2();
    }

    /**
     * 获得魅力List
     *
     * @return
     */
    public static List<String> getCharmMapValue() {
        Map<String, String> map = getCharmMap();
        List<String> list = new ArrayList<String>();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getValue());
        }
        return list;
    }

    /**
     * 根据value获得key(魅力)
     *
     * @param value
     * @return
     */

    public static String getCharmMapKeyByValue(String value) {
        Map<String, String> map = getCharmMap();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * 获得异地恋Map
     *
     * @return
     */
    public static Map<String, String> getDiffAreaLoveMap() {
        return getParamsInit().getDiffAreaLoveIdMap2();
    }

    /**
     * 获得异地恋List
     *
     * @return
     */
    public static List<String> getDiffAreaLoveMapValue() {
        Map<String, String> map = getDiffAreaLoveMap();
        List<String> list = new ArrayList<String>();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getValue());
        }
        return list;
    }

    /**
     * 根据value获得key(异地恋)
     *
     * @param value
     * @return
     */

    public static String getDiffAreaLoveMapKeyByValue(String value) {
        Map<String, String> map = getDiffAreaLoveMap();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * 获得喜欢的食物Map
     *
     * @return
     */
    public static Map<String, String> getFoodsMap() {
        return getParamsInit().getFoodsMap2();
    }

    /**
     * 获得喜欢的食物List
     *
     * @return
     */
    public static List<String> getFoodsMapValue() {
        Map<String, String> map = getFoodsMap();
        List<String> list = new ArrayList<String>();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getValue());
        }
        return list;
    }

    /**
     * 根据value获得key(喜欢的食物)
     *
     * @param value
     * @return
     */

    public static String getFoodsMapKeyByValue(String value) {
        Map<String, String> map = getFoodsMap();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * 获得住房状况Map
     *
     * @return
     */
    public static Map<String, String> getHouseMap() {
        return getParamsInit().getHouseMap2();
    }

    /**
     * 获得住房状况List
     *
     * @return
     */
    public static List<String> getHouseMapValue() {
        Map<String, String> map = getHouseMap();
        List<String> list = new ArrayList<String>();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getValue());
        }
        return list;
    }

    /**
     * 根据value获得key(住房状况)
     *
     * @param value
     * @return
     */

    public static String getHouseMapKeyByValue(String value) {
        Map<String, String> map = getHouseMap();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * 获得与父母同住Map
     *
     * @return
     */
    public static Map<String, String> getLiveWithParentMap() {
        return getParamsInit().getLiveWithParentMap2();
    }

    /**
     * 获得与父母同住List
     *
     * @return
     */
    public static List<String> getLiveWithParentMapValue() {
        Map<String, String> map = getLiveWithParentMap();
        List<String> list = new ArrayList<String>();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getValue());
        }
        return list;
    }

    /**
     * 根据value获得key(与父母同住)
     *
     * @param value
     * @return
     */
    public static String getLiveWithParentMapKeyByValue(String value) {
        Map<String, String> map = getLiveWithParentMap();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * 喜欢的类型Map
     *
     * @param isMale 根据性别返回不同的Map
     * @return
     */
    public static Map<String, String> getLoverTypeMap(boolean isMale) {
        return isMale ? getParamsInit().getLoverTypeMap2M() : getParamsInit().getLoverTypeMap2F();
    }

    /**
     * 获得喜欢的类型List
     *
     * @param isMale 根据性别返回不同的List
     * @return
     */
    public static List<String> getLoverTypeMapValue(boolean isMale) {
        Map<String, String> map = getLoverTypeMap(isMale);
        List<String> list = new ArrayList<String>();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getValue());
        }
        return list;
    }

    /**
     * 根据value获得key(喜欢的类型)
     *
     * @param value
     * @param isMale 根据性别返回不同的key
     * @return
     */
    public static String getLoverTypeMapKeyByValue(String value, boolean isMale) {
        Map<String, String> map = getLoverTypeMap(isMale);
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * 获得喜欢的音乐Map
     *
     * @return
     */
    public static Map<String, String> getMusicMap() {
        return getParamsInit().getMusicMap2();
    }

    /**
     * 获得喜欢的音乐List
     *
     * @return
     */
    public static List<String> getMusicMapValue() {
        Map<String, String> map = getMusicMap();
        List<String> list = new ArrayList<String>();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getValue());
        }
        return list;
    }

    /**
     * 根据value获得key(喜欢的音乐)
     *
     * @param value
     * @return
     */
    public static String getMusicMapKeyByValue(String value) {
        Map<String, String> map = getMusicMap();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * 获得喜欢的电影Map
     *
     * @return
     */
    public static Map<String, String> getMoviesMap() {
        return getParamsInit().getMoviesMap2();
    }

    public static Map<String,String >getManyKids(){
        return  getParamsInit().getHowManyKidsMap2();
    }

    /**
     * 获取眼镜颜色
     * qyh
     * @return
     */
    public static Map<String,String> getEyeColor(){
        return  getParamsInit().getEyesMap2();
    }
    /**
     * 获得喜欢的电影List
     *
     * @return
     */
    public static List<String> getMoviesMapValue() {
        Map<String, String> map = getMoviesMap();
        List<String> list = new ArrayList<String>();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getValue());
        }
        return list;
    }

    /**
     * 根据value获得key(喜欢的电影)
     *
     * @param value
     * @return
     */
    public static String getMoviesMapKeyByValue(String value) {
        Map<String, String> map = getMoviesMap();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * 获得亲密行为Map
     *
     * @return
     */
    public static Map<String, String> getSexBefMarriedMap() {
        return getParamsInit().getSexBefMarriedMap2();
    }

    /**
     * 获得亲密行为List
     *
     * @return
     */
    public static List<String> getSexBefMarriedMapValue() {
        Map<String, String> map = getSexBefMarriedMap();
        List<String> list = new ArrayList<String>();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getValue());
        }
        return list;
    }

    /**
     * 根据value获得key(亲密行为)
     *
     * @param value
     * @return
     */
    public static String getSexBefMarriedMapKeyByValue(String value) {
        Map<String, String> map = getSexBefMarriedMap();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * 体型Map
     *
     * @param isMale 根据性别返回不同的Map
     * @return
     */
    public static Map<String, String> getBodyTypeMap(boolean isMale) {
        LogUtil.e(LogUtil.TAG_LMF,"getParamsInit().getBodyTypeMap2M() "+getParamsInit().getBodyTypeMap2M());
        LogUtil.e(LogUtil.TAG_LMF,"getParamsInit().getBodyTypeMap2F() "+getParamsInit().getBodyTypeMap2F());
        return isMale ? getParamsInit().getBodyTypeMap2M() : getParamsInit().getBodyTypeMap2F();
    }

    /**
     * 获得体型List
     *
     * @param isMale 根据性别返回不同的List
     * @return
     */
    public static List<String> getBodyTypeMapValue(boolean isMale) {
        Map<String, String> map = getBodyTypeMap(isMale);
        List<String> list = new ArrayList<String>();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getValue());
        }
        return list;
    }

    /**
     * 根据value获得key(体型)
     *
     * @param value
     * @param isMale 根据性别返回不同的key
     * @return
     */
    public static String getBodyTypeMapKeyByValue(String value, boolean isMale) {
        Map<String, String> map = getBodyTypeMap(isMale);
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }
    /**
     * 获得地区list
     *
     * @return
     */
    public static Map<String, String> getDistractMap() {
        //ParamsInit paramsInit = JSON.parseObject(UserInfoXml.getParamsInit(), ParamsInit.class);
        ParamsInit paramsInit = gson.fromJson(UserInfoXml.getParamsInit(), ParamsInit.class);
        if(paramsInit!=null){
            return paramsInit.getDistractMap2();
        }else{
            return null;
        }
    }
    /**
     * 获得地区list
     *
     * @return
     */
    public static List<String> getDistractMapValue() {
        Map<String, String> map = getDistractMap();
        List<String> list = new ArrayList<String>();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getValue());
        }
        return list;
    }
}
