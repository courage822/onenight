package com.online.onenight2.utils;

import android.content.Context;
import android.text.TextUtils;
import android.text.format.Time;

import com.online.onenight2.R;
import com.online.onenight2.base.BaseApplication;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TimeUtils {
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    public static SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
    public static Context mContext = BaseApplication.getAppContext();

    public static String getCurrentDate() {
        return dateFormat.format(new Date(System.currentTimeMillis()));
    }

    public static String getCurrentTime() {
        return timeFormat.format(new Date(System.currentTimeMillis()));
    }

    /**
     * 显示创建时间
     *
     * @param date   需要显示的日期
     * @param format 需要显示的格式，为空时显示默认格式
     * @return 返回指定格式的时间（默认为yyyy年MM月dd日 HH:mm），当前年不显示年份
     */
    public static String showCreatedTime(String date, String format) {
        if (TextUtils.isEmpty(format)) {
            format = "yyyy-MM-dd HH:mm";
        }
        String time = null;
        try {
            SimpleDateFormat mFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date old = mFormat.parse(date);
            time = new SimpleDateFormat(format).format(old);
            Date currentDate = new Date();
            if (old.getYear() == currentDate.getYear()) {// 当前年
                long result = currentDate.getTime() - old.getTime();
                if (result < 60 * 1000) {// 一分钟内
                    time = mContext.getString(R.string.moment_ago);
                } else if (result >= 60 * 1000 && result < 60 * 60 * 1000) {// 一小时内
                    long mins = result / (60 * 1000);
                    time = mins+String.format(mContext.getResources().getQuantityString(R.plurals.minutes_ago, Integer.parseInt(String.valueOf(mins))));
                } else if (result >= 60 * 60 * 1000 && result <= 24 * 60 * 60 * 1000) {// 24小时内
                    long hours = result / (60 * 60 * 1000);
                    time =hours+String.format(mContext.getResources().getQuantityString(R.plurals.hours_ago,Integer.parseInt(String.valueOf(hours))));
                } else {// 第二日及以后
                    time = pad(old.getMonth() + 1) + "-" + pad(old.getDate()) + " " + time.substring(11);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return time;
    }

    /**
     * 显示距离现在已经过了多长时间
     *
     * @param date 要显示的时间
     * @return
     */
    public static String showTimeAgo(String date) {
        String time = null;
        try {
            SimpleDateFormat mFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date old = mFormat.parse(date);
            time = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(old);
            Date currentDate = new Date();
            if (old.getYear() == currentDate.getYear()) {// 当前年
                long result = currentDate.getTime() - old.getTime();
                if (result < 60 * 1000) {// 一分钟内
                    time = mContext.getString(R.string.moment_ago);
                } else if (result >= 60 * 1000 && result < 60 * 60 * 1000) {// 一小时内
                    long mins = result / (60 * 1000);
                    time = String.format(mContext.getResources().getQuantityString(R.plurals.minutes_ago, Integer.parseInt(String.valueOf(mins))));
                } else if (result >= 60 * 60 * 1000 && result < 24 * 60 * 60 * 1000) {// 24小时内
                    long hours = result / (60 * 60 * 1000);
                    time =String.format(mContext.getResources().getQuantityString(R.plurals.hours_ago,Integer.parseInt(String.valueOf(hours))));
                } else if (result >= 24 * 60 * 60 * 1000 && result < 7 * 24 * 60 * 60 * 1000) {// 一周内
                    long days = result / (24 * 60 * 60 * 1000);
                    time =String.format(mContext.getResources().getQuantityString(R.plurals.days_ago, Integer.parseInt(String.valueOf(days))));
                } else {// 一周以后
                    time = pad(old.getMonth() + 1) + "-" + pad(old.getDate()) + " " + time.substring(11);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return time;
    }

    /**
     * 日期/时间小于10时格式补0
     */
    public static String pad(int c) {
        if (c >= 10) {
            return String.valueOf(c);
        } else {
            return "0" + String.valueOf(c);
        }
    }
    /**
     * 转换为手机本地时间
     *
     * @param serverSysTime 当前系统时间戳
     * @param time          需要显示的时间
     * @return 转换后的本地时间
     */
  /*  public static String getLocalTime(long serverSysTime, String time) {
        long localTime = 0;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.s", Locale.US);
        try {
            localTime = System.currentTimeMillis() - (serverSysTime - simpleDateFormat.parse(time).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (localTime == 0) {
            return time;
        }
        return timeFormat.format(new Date(localTime));
    }*/

    /**
     * 根据手机时间显示不同的时间
     *
     * @param serverSysTime 当前服务器时间戳
     * @param date          需要显示的时间
     * @return 转换后的时间
     */
    public static String getLocalTime(long serverSysTime, long date) {
       /* LogUtil.e(LogUtil.TAG_ZL, "date = " + date);
       SimpleDateFormat showFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        Date targetDate = new Date(date);
        String result = showFormat.format(targetDate);
      if (targetDate.getYear() == new Date().getYear()) {// 当前年
            // 计算距离现在已经过了多长时间
            LogUtil.e(LogUtil.TAG_ZL, "serverSysTime = " + serverSysTime);
            long timeDiff = serverSysTime - date;
            LogUtil.e(LogUtil.TAG_ZL, "timeDiff = " + timeDiff);
            if (timeDiff < 60 * 1000) {// 一分钟内
                result = mContext.getString(R.string.moment_ago);
            } else if (timeDiff >= 60 * 1000 && timeDiff < 60 * 60 * 1000) {// 一小时内
                int mins = (int) (timeDiff / (60 * 1000));
                result = mins + mContext.getResources().getQuantityString(R.plurals.minutes_ago, mins);
            } else if (timeDiff >= 60 * 60 * 1000 && timeDiff < 24 * 60 * 60 * 1000) {// 24小时内
                int hours = (int) (timeDiff / (60 * 60 * 1000));
                result = hours + mContext.getResources().getQuantityString(R.plurals.hours_ago, hours);
            } else if (timeDiff >= 24 * 60 * 60 * 1000 && timeDiff < 7 * 24 * 60 * 60 * 1000) {// 一周内
                int days = (int) (timeDiff / (24 * 60 * 60 * 1000));
                result = days + mContext.getResources().getQuantityString(R.plurals.days_ago, days);
            } else {// 一周以后
                // 显示手机时间
                long localTime = System.currentTimeMillis() - timeDiff;
                Date localDate = new Date(localTime);
                result = pad(localDate.getMonth() + 1) + "-" + pad(localDate.getDate()) + " "
                        + pad(localDate.getHours()) + ":" + pad(localDate.getMinutes());
            }
        }
         return result;*/
        Date date1=new Date();
        long localMIllis= date1.getTime();
        Time time = new Time();
        time.setToNow();
//        long localMIllis= time.toMillis(false);
        int year = time.year;
        int month = time.month;
        int day = time.monthDay;
        int minute = time.minute;
        int hour = time.hour;
        int sec = time.second;
//        long localMIllis=getLongTime(year+"-"+(month+1)+"-"+day+" "+hour+":"+minute+":"+sec);
        //LogUtil.e(LogUtil.TAG_WHC,"当前时间=="+year+"-"+(month+1)+"-"+day+"-"+hour+"-"+minute+"-"+sec+"时间戳"+localMIllis+"Sys时间戳"+System.currentTimeMillis());
       String currentTime=getStringTime(String.valueOf(System.currentTimeMillis()));
        //LogUtil.e(LogUtil.TAG_WHC,"当前时间串=="+currentTime);
        int chaHour=hour;
        long chaTime = serverSysTime - date;
        long resultTime=System.currentTimeMillis()-chaTime;
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date dates = new Date(resultTime);
        res = simpleDateFormat.format(dates);
    //   LogUtil.e(LogUtil.TAG_WHC,"時間差=="+chaTime+"系统时间"+serverSysTime+"当前时间"+System.currentTimeMillis()+"转换时间"+res);
        return res;
    }

    public static long getLongTime(String date_str){
        try {
            SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            return timeFormat.parse(date_str).getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     *  把long 类型的时间戳转化成String 时间串
     * @param time
     * @return
     */
    public static String getStringTime(String time) {
        SimpleDateFormat sdr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        @SuppressWarnings("unused")
        long lcc = Long.valueOf(time);
//        int i = Integer.parseInt(time);
        String times = sdr.format(new Date(lcc));
        return times;

    }

}
