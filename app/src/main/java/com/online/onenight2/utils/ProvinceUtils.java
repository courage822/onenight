package com.online.onenight2.utils;

import com.online.onenight2.xml.PlatformInfoXml;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/4/15.
 */

public class ProvinceUtils {
    /**
     * 获得省份列表
     *
     * @return
     */
    public static List<String> getProvinceList() {
        List<String> list = new ArrayList<String>();

        if(PlatformInfoXml.getCountry().equals("Australia")){
            list.add("New South Wales");
            list.add("Victoria");
            list.add("Queensland");
            list.add("South Aoustralia");
            list.add("Northern Territory");
            list.add("West Australia");
            list.add("Tasmania");
            list.add("Australian National Territory");

        }else if(PlatformInfoXml.getCountry().equals("America")){
            list.add("Alaska");
            list.add("Alabama");
            list.add("Arkansas");
            list.add("Arizona");
            list.add("California");
            list.add("Colorado");
            list.add("Connecticut");
            list.add("District of Columbia");
            list.add("Delaware");
            list.add("Florida");
            list.add("Georgia");
            list.add("Guam");
            list.add("Hawaii");
            list.add("Iowa");
            list.add("Idaho");
            list.add("Illinois");
            list.add("Indiana");
            list.add("Kansas");
            list.add("Kentucky");
            list.add("Louisiana");
            list.add("Massachusetts");
            list.add("Maryland");
            list.add("Maine");
            list.add("Michigan");
            list.add("Minnesota");
            list.add("Missouri");
            list.add("Mississippi");
            list.add("Montana");
            list.add("North Carolina");
            list.add("North Dakota");
            list.add("Nebraska");
            list.add("New Hampshire");
            list.add("New Jersey");
            list.add("New Mexico");
            list.add("Nevada");
            list.add("New York");
            list.add("Ohio");
            list.add("Oklahoma");
            list.add("Oregon");
            list.add("Pennsylvania");
            list.add("Puerto Rico");
            list.add("Rhode Island");
            list.add("South Carolina");
            list.add("South Dakota");
            list.add("Tennessee");
            list.add("Texas");
            list.add("Utah");
            list.add("Virginia");
            list.add("Vermont");
            list.add("Washington");
            list.add("Wisconsin");
            list.add("West Virginia");
            list.add("Wyoming");
        }else if(PlatformInfoXml.getCountry().equals("Mexico")){
            list.add("Ciudad de México");
            list.add("Aguascalientes");
            list.add("Baja California");
            list.add("Baja California Sur");
            list.add("Campeche");
            list.add("Coahuila");
            list.add("Colima");
            list.add("Chiapas");
            list.add("Chihuahua");
            list.add("Durango");
            list.add("Guanajuato");
            list.add("Guerrero");
            list.add("Hidalgo");
            list.add("Jalisco");
            list.add("México");
            list.add("Michoacán");
            list.add("Morelos");
            list.add("Nayarit");
            list.add("Nuevo León");
            list.add("Oaxaca");
            list.add("Puebla");
            list.add("Querétaro");
            list.add("Quintana Roo");
            list.add("San Luisí Potos");
            list.add("Sinaloa");
            list.add("Sonora");
            list.add("Tabasco");
            list.add("Tamaulipas");
            list.add("Tlaxcala");
            list.add("Veracruzkeating");
            list.add("Yucatán");
            list.add("Zacatecas。");
        }else if(PlatformInfoXml.getCountry().equals("India")) {
            list.add("Andhra Pradesh");
            list.add("Andaman and Nicobar Islands");
            list.add("Assam");
            list.add("Bihar");
            list.add("Chhattisgarh");
            list.add("Chandigarh");
            list.add("Daman and Diu");
            list.add("Dadra and Nagar Haveli");
            list.add("Delhi");
            list.add("Goa");
            list.add("Gujarat");
            list.add("Haryana");
            list.add("Himachal Pradesh");
            list.add("Jammu-Kashmir");
            list.add("Jharkand");
            list.add("Karnataka");
            list.add("Kerala");
            list.add("Lakshadweep");
            list.add("Madhya Pradesh");
            list.add("Maharastra");
            list.add("Manipur");
            list.add("Meghalaya");
            list.add("Mizoram");
            list.add("Nagaland");
            list.add("Orrisa");
            list.add("Panjab");
            list.add("Pondicherry");
            list.add("Rajasthan");
            list.add("Sikkim");
            list.add("Tamil Nadu");
            list.add("Tripura");
            list.add("Uttaranchal");
            list.add("Uttar Pradesh");
            list.add("West Bengal");
        }
        return ParamsUtils.getDistractMapValue();
    }

}
