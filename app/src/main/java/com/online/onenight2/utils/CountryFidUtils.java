package com.online.onenight2.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 国家对应渠道号工具类
 * Created by zhangdroid on 2016/11/23.
 */
public class CountryFidUtils {

    /**
     * @return 国家和渠道号对应map
     */
    public static Map<String, String> getCountryFidMap() {
        Map<String, String> map = new HashMap<>();
        // 添加14个英语国家
        map.put("America", "11001");
        map.put("Australia", "11011");
        map.put("India", "11012");
        map.put("Indonesia", "11013");
        map.put("UnitedKingdom", "11014");
        map.put("Canada", "11015");
        map.put("NewZealand", "11016");
        map.put("Ireland", "11017");
        map.put("SouthAfrica", "11018");
        map.put("Singapore", "11020");
        map.put("Pakistan", "11024");
        map.put("Philippines", "11025");
        map.put("HongKong", "11033");
        map.put("Taiwan", "11008");



        //        map.put("America", "1080101");
//        map.put("America", "1080101");
//        map.put("Australia", "1081101");
//        map.put("India", "1081201");
//        map.put("Indonesia", "1081301");
//        map.put("UnitedKingdom", "1081401");
//        map.put("Canada", "1081501");
//        map.put("NewZealand", "1081601");
//        map.put("Ireland", "1081701");
//        map.put("SouthAfrica", "1081801");
//        map.put("Singapore", "1082001");
//        map.put("Pakistan", "1082401");
//        map.put("Philippines", "1082501");
//        map.put("HongKong", "1083301");

        return map;
    }

}
