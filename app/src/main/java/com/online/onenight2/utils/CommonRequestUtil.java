package com.online.onenight2.utils;

import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.library.utils.LogUtil;
import com.library.utils.ToastUtil;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.online.onenight2.bean.Area;
import com.online.onenight2.bean.AreaBean;
import com.online.onenight2.bean.MyInfo;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.xml.PlatformInfoXml;
import com.online.onenight2.xml.UserInfoXml;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 公用接口请求工具类
 * 1、初始化数据字典
 * 2、打招呼
 * 3、拉黑
 * 4、取消拉黑
 * 5、关注
 * 6、取消关注
 * 7、删除聊天记录
 * 8、上传/加载用户资料项信息
 * // 图片相关
 * 9、上传图片
 * 10、删除图片
 * 11、设置为头像
 * Created by zhangdroid on 2016/6/30.
 */
public class CommonRequestUtil {

    /**
     * 缘分打招呼
     */
    public static final String SAY_HELLO_TYPE_FATE = "1";
    /**
     * 搜索打招呼
     */
    public static final String SAY_HELLO_TYPE_SEARCH = "2";
    /**
     * 用户详情页打招呼
     */
    public static final String SAY_HELLO_TYPE_USER_INFO = "3";
    /**
     * 大图浏览打招呼
     */
    public static final String SAY_HELLO_TYPE_PHOTO = "4";
    /**
     * 附近的人打招呼
     */
    public static final String SAY_HELLO_TYPE_NEARBY = "9";
    /**
     * 最近访客打招呼
     */
    public static final String SAY_HELLO_TYPE_GUEST = "10";

    /**
     * 初始化数据字典
     */
    public static void initParamsDict() {
        OkGo.post(IUrlConstant.URL_INIT)
                .headers("token", PlatformInfoXml.getToken())
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        LogUtil.e(LogUtil.TAG_LMF,"初始化字典数据"+s);
                            UserInfoXml.setParamsInit(s);
                        }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                      initParamsDict();
                    }
                });
    }
    /**
     * 上传用户个人资料项
     *
     * @param showToast 是否显示Toast
     * @param listener  请求结果回调
     */
    public static void uploadUserInfo(final boolean showToast, final OnCommonListener listener) {
        Map<String, String> mapParams = new HashMap<String, String>();
        mapParams.put("platformInfo", PlatformInfoXml.getPlatformJsonString());
        String nickname = UserInfoXml.getNickName();
        if (!TextUtils.isEmpty(nickname)) {
            mapParams.put("nickName", nickname);
        }
        String monologue = UserInfoXml.getMonologue();
        if (!TextUtils.isEmpty(monologue)) {
            mapParams.put("monologue", monologue);
        }
        String birthday = UserInfoXml.getBirthday();
        if (!TextUtils.isEmpty(birthday)) {
            mapParams.put("birthday", birthday);
        }
        String constellation = UserInfoXml.getConstellation();
        if (!TextUtils.isEmpty(constellation)) {
            mapParams.put("constellationId", constellation);
        }
        String provinceName = UserInfoXml.getProvinceName();
        if (!TextUtils.isEmpty(provinceName)) {
            Area area = new Area();
            area.setProvinceName(provinceName);
            AreaBean areaBean = new AreaBean();
            areaBean.setArea(area);
            mapParams.put("area", JSON.toJSONString(areaBean));
        }
        String income = UserInfoXml.getIncome();
        if (!TextUtils.isEmpty(income)) {
            mapParams.put("income", income);
        }
        String height = UserInfoXml.getHeight();
        if (!TextUtils.isEmpty(height)) {
            mapParams.put("height", height);
        }
        String occupation = UserInfoXml.getWork();
        if (!TextUtils.isEmpty(occupation)) {
            mapParams.put("occupation", occupation);
        }
        String education = UserInfoXml.getEducation();
        if (!TextUtils.isEmpty(education)) {
            mapParams.put("education", education);
        }
        String marriage = UserInfoXml.getMarriage();
        if (!TextUtils.isEmpty(marriage)) {
            mapParams.put("maritalStatus", marriage);
        }
        String wantBaby = UserInfoXml.getWantBaby();
        if (!TextUtils.isEmpty(wantBaby)) {
            mapParams.put("childStatus", wantBaby);
        }
        String sport = UserInfoXml.getSport();
        if (!TextUtils.isEmpty(sport)) {
            mapParams.put("sports", sport);
        }
        String personal = UserInfoXml.getPersonal();
        if (!TextUtils.isEmpty(personal)) {
            mapParams.put("characteristics", personal);
        }
        String travel = UserInfoXml.getTravel();
        if (!TextUtils.isEmpty(travel)) {
            mapParams.put("travel", travel);
        }
        String bookCartoon = UserInfoXml.getBookCartoon();
        if (!TextUtils.isEmpty(bookCartoon)) {
            mapParams.put("books", bookCartoon);
        }
        String interest = UserInfoXml.getInterest();
        if (!TextUtils.isEmpty(interest)) {
            mapParams.put("listHobby", interest);
        }
        OkGo.post(IUrlConstant.URL_UPLOAD_USER_INFO)
                .params(mapParams)
                .execute(
                        new StringCallback() {
                            @Override
                            public void onSuccess(String s, Call call, Response response) {
                        if (!TextUtils.isEmpty(s)) {
                            MyInfo myInfo = JSON.parseObject(s, MyInfo.class);
                            if(myInfo!=null){
                                String isSucceed = myInfo.getIsSucceed();
                                if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                // 上传成功后更新本地信息
                                UserInfoXml.setUserInfo(myInfo.getUserEnglish());
                                if (showToast) {
                                    ToastUtil.showShortToast(Utils.getContext(), myInfo.getMsg());
                                }
                                if (listener != null) {
                                    listener.onSuccess();
                                }
                            } else {
                                if (listener != null) {
                                    listener.onFail();
                                }
                            }
                        } else {
                            if (listener != null) {
                                listener.onFail();
                            }
                        }

                        }
                            }

                            @Override
                            public void onError(Call call, Response response, Exception e) {
                                super.onError(call, response, e);

                            }
                        }
                );
    }
    /**
     * 通用监听器
     */
    public interface OnCommonListener {

        /**
         * 请求成功
         */
        void onSuccess();

        /**
         * 请求失败
         */
        void onFail();
    }
}