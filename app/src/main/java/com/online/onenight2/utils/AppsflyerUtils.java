package com.online.onenight2.utils;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.online.onenight2.base.BaseApplication;

import java.util.HashMap;
import java.util.Map;

import static com.online.onenight2.utils.TimeUtils.mContext;

/**
 * Appsflyer辅助类
 * Created by wangyong on 2017/3/17.
 */
public class AppsflyerUtils {

    private static void trackEvent(String eventName, Map<String, Object> eventValues) {
        AppsFlyerLib.getInstance().trackEvent(BaseApplication.getAppContext(), eventName, eventValues);
    }
    /**
     * 激活
     */
    public static void activate() {
        Map<String, Object> eventValues = new HashMap<String, Object>();
        eventValues.put("activate", 1);
        trackEvent("activation", eventValues);
    }
    /*
    * 正常注册
    * */
    public static void appsFlyerRegister() {
        Map<String, Object> eventValue = new HashMap<String, Object>();
        eventValue.put(AFInAppEventParameterName.LEVEL, 9);
        eventValue.put(AFInAppEventParameterName.SCORE, 100);
        AppsFlyerLib.getInstance().trackEvent(mContext, AFInAppEventType.COMPLETE_REGISTRATION, eventValue);
    }
    public static void appsPurchase(String price){
        Map<String, Object> eventValue = new HashMap<String, Object>();
        eventValue.put(AFInAppEventParameterName.REVENUE, price);//收益
        eventValue.put(AFInAppEventParameterName.CONTENT_TYPE, "category_a");//类型
        eventValue.put(AFInAppEventParameterName.CONTENT_ID, "1234567");//id
        eventValue.put(AFInAppEventParameterName.CURRENCY, "TWD");//货币
        trackEvent(AFInAppEventType.PURCHASE, eventValue);
    }
    public static void appsFlyerLogin() {
        Map<String, Object> eventValue = new HashMap<String, Object>();
        eventValue.put(AFInAppEventParameterName.LEVEL, 9);
        eventValue.put(AFInAppEventParameterName.SCORE, 100);
        trackEvent(AFInAppEventType.LOGIN, eventValue);
    }
}
