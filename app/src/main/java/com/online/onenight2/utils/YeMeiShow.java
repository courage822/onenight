package com.online.onenight2.utils;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.online.onenight2.view.MessageNotification;

/**
 * Created by Administrator on 2017/6/30.
 * //控制页眉显示和隐藏的工具类
 */

public class YeMeiShow {
    public static void initNotification(Context context, FragmentManager fragmentManager,String thumbnailUrl,
                                        int userId, String nickName, int age, int sex, String height, String msg, int type, int noticeType){
        MessageNotification  mMessageNotification = new MessageNotification.Builder()
                .setContext(context)
                .setFragmentManager(fragmentManager)
                .setUserId(userId)
                .setSex(sex)
                .setImgRes(thumbnailUrl)
                .setHeight(height)
                .setNickName(nickName)
                .setAge(String.valueOf(age))
                .setMsg(msg)
                .build(String.valueOf(type), noticeType);
                 mMessageNotification.show();
    }
}
