package com.online.onenight2.utils;

import com.lzy.okgo.OkGo;
import com.online.onenight2.bean.SendPraise;
import com.online.onenight2.callback.JsonCallback;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.xml.PlatformInfoXml;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 描述：
 * Created by qyh on 2017/4/27.
 */

public class MathDataUtils {
    /**
     * 点赞/喜欢
     * @param uid
     * @param listener
     */
    public  static void sendPraise(String uid, final onRequestDataListener listener){
        OkGo.post(IUrlConstant.SEND_PRAISE)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("uid",uid)
                .execute(new JsonCallback<SendPraise>() {

                    @Override
                    public void onSuccess(SendPraise sendPraise, Call call, Response response) {
                      if(null!=sendPraise){
                          if(sendPraise.getIsSucceed().equals("1")){
                              listener.onSuccess();
                          }
                      }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                            listener.onFailed();
                    }

                });
    }

    /**
     * 取消点赞
     * @param uid
     * @param listener
     */
    public static void  canclePraise(String uid, final onRequestDataListener listener){
        OkGo.post(IUrlConstant.CANCLE_PRAISE)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("uid",uid)
                .execute(new JsonCallback<SendPraise>() {

                    @Override
                    public void onSuccess(SendPraise sendPraise, Call call, Response response) {
                        if(null!=sendPraise &&sendPraise.getIsSucceed().equals("1")){
                            listener.onSuccess();
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        listener.onFailed();
                    }
                });
    }

    public interface onRequestDataListener{
        void onSuccess();
        void onFailed();
    }
}
