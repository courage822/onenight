package com.online.onenight2.utils;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * 描述：文件管理类
 * Created by qyh on 2017/5/11.
 */

public class FileUtil  {
    private static File file;
    //获取录音存放路径
    public static File getAppRecordDir(Context context) {
        File appDir = getAppDir(context);
        File recordDir = new File(appDir.getAbsolutePath(), FileUtil.RECORD_DIRECTORY_PATH);
        if (!recordDir.exists()) {
            recordDir.mkdir();
        }
        return recordDir;
    }
    //获取文件存放根路径
    public static File getAppDir(Context context) {
        String dirPath = "";
        //SD卡是否存在
        boolean isSdCardExists = Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
        boolean isRootDirExists = Environment.getExternalStorageDirectory().exists();
        if (isSdCardExists && isRootDirExists) {
            dirPath = String.format("%s/%s/", Environment.getExternalStorageDirectory().getAbsolutePath(), FileUtil.RECORD_DIRECTORY_PATH);
        } else {
            dirPath = String.format("%s/%s/", context.getApplicationContext().getFilesDir().getAbsolutePath(), FileUtil.RECORD_DIRECTORY_PATH);
        }

        File appDir = new File(dirPath);
        if (!appDir.exists()) {
            appDir.mkdirs();
        }
        return appDir;
    }

    /**
     * 录音文件存放的文件夹路径（根目录/Amor/record）
     */
    public static final String RECORD_DIRECTORY_PATH = FileUtil.getExternalStorageDirectory() + File.separator + "record";

    /**
     * 获得SD卡目录路径，应用内所有数据均存放在该目录下
     *
     * @return 根目录下Amor文件夹
     */
    public static String getExternalStorageDirectory() {
        if (isSDCardExist()) {
            return Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Amor";
        }
        return null;
    }
    /**
     * 判断是否有SD卡
     *
     * @return
     */
    public static boolean isSDCardExist() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }
    /**
     * 写入文件
     * @param camPicPath
     * @return
     */
    public static File inputStream(String camPicPath) {
        FileInputStream is = null;
        try
        {
            is = new FileInputStream(camPicPath);
            File camFile = new File(camPicPath); // 图片文件路径
            if (camFile.exists()) {
                int size = ImageCheckoutUtil.getImageSize(ImageCheckoutUtil.getLoacalBitmap(camPicPath));
                file= new File(camPicPath);
            } else {
                //文件不存在
            }
        } catch (
                FileNotFoundException e
                ) {
            e.printStackTrace();
        } finally {
            // 关闭流
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }
}
