package com.online.onenight2.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.widget.TextView;

import com.library.utils.MessageDigestUtil;
import com.library.utils.SharedPreferenceUtil;
import com.online.onenight2.base.BaseApplication;

import java.io.File;
import java.util.List;

/**
 * Created by zhangdroid on 2016/8/9.
 */
public class Utils {
private static File file;
    public static Context getContext() {
        return BaseApplication.getAppContext();
    }

    private static Drawable getDrawable(Context context, int resId) {
        return context.getResources().getDrawable(resId);
    }

    public static void setDrawableLeft(TextView textView, int leftDrawableResId) {
        Drawable leftDrawable = getDrawable(textView.getContext(), leftDrawableResId);
        leftDrawable.setBounds(0, 0, leftDrawable.getMinimumWidth(), leftDrawable.getMinimumHeight());
        textView.setCompoundDrawables(leftDrawable, null, null, null);
    }

    /**
     * 清空XML数据
     *
     * @param context
     * @param xmlName
     */
    public static void clearXml(Context context, String xmlName) {
        SharedPreferenceUtil.getSharePreferences(context, xmlName).edit().clear().apply();
    }

    public static boolean isListEmpty(List<?> list) {
        return list == null || list.size() == 0;
    }

    /**
     * 获得AnimationDrawable，图片动画
     *
     * @param list         动画需要播放的图片集合
     * @param isRepeatable 是否可以重复
     * @param duration     帧间隔（毫秒）
     * @return
     */
    public static AnimationDrawable getFrameAnim(List<Drawable> list, boolean isRepeatable, int duration) {
        AnimationDrawable animationDrawable = new AnimationDrawable();
        animationDrawable.setOneShot(!isRepeatable);
        if (!isListEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                animationDrawable.addFrame(list.get(i), duration);
            }
        }
        return animationDrawable;
    }

    public static boolean isAppInstalled(Context context, String packagename) {
        PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(packagename, 0);
        } catch (PackageManager.NameNotFoundException e) {
            packageInfo = null;
            e.printStackTrace();
        }
        if (packageInfo == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 功能描述：加密算法
     *
     * @param pid
     * @param transactionId
     * @return
     * @author lxl
     * @update:[变更日期YYYY-MM-DD][更改人姓名][变更描述]
     * @since 2016-10-28
     */
    public static String encryptPay(String pid, String transactionId) {
        String key = pid + transactionId;
        String md5Resault = MessageDigestUtil.encryptMD5(key.getBytes());
        return md5Resault;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? widthRatio : heightRatio;
        }

        return inSampleSize;
    }

    /**
     * 根据时间长短计算语音条宽度:220dp
     * @param context
     * @param seconds
     * @return
     */
    public synchronized static int getVoiceLineWight(Context context, int seconds) {
        //1-2s是最短的。2-10s每秒增加一个单位。10-60s每10s增加一个单位。
        if (seconds <= 2) {
            return dip2px(context, 60);
        } else if (seconds <= 10) {
            //90~170
            return dip2px(context, 60 + 8 * seconds);
        } else {
            //170~220
            return dip2px(context, 120 + 10 * (seconds / 10));

        }
    }
        /**
         * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
         */
        public static int dip2px(Context context, float dpValue) {
            final float scale = context.getResources().getDisplayMetrics().density;
            return (int) (dpValue * scale + 0.5f);
        }
}
