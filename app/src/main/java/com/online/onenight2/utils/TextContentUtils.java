package com.online.onenight2.utils;

import java.util.ArrayList;

/**
 * 描述：
 * Created by qyh on 2017/4/15.
 */

public class TextContentUtils  {
    private static  ArrayList<String> optionsHeightLeft = new ArrayList<>();
    private static ArrayList<String> optionsKm=new ArrayList<>();
    private static ArrayList<ArrayList<String>> optionsHeightRight = new ArrayList<>();
    private static  ArrayList<String> optionsAgeLeft = new ArrayList<>();
    private static  ArrayList<ArrayList<String>>optionsAgeRigt = new ArrayList<>();


    public static  ArrayList<String> getAgeLeft(){
        optionsAgeLeft.add("18");
        optionsAgeLeft.add("19");
        optionsAgeLeft.add("20");
        optionsAgeLeft.add("21");
        optionsAgeLeft.add("22");
        optionsAgeLeft.add("23");
        optionsAgeLeft.add("24");
        optionsAgeLeft.add("25");
        optionsAgeLeft.add("26");
        optionsAgeLeft.add("27");
        optionsAgeLeft.add("28");
        optionsAgeLeft.add("29");
        optionsAgeLeft.add("30");
        optionsAgeLeft.add("31");
        optionsAgeLeft.add("32");
        optionsAgeLeft.add("33");
        optionsAgeLeft.add("34");
        optionsAgeLeft.add("35");

        return optionsAgeLeft;
    }
    public static  ArrayList<String> getKMData(){
        optionsKm.add("5KM");
        optionsKm.add("10KM");
        optionsKm.add("20KM");
        optionsKm.add("30KM");
        optionsKm.add("40KM");
        optionsKm.add("50KM");
        optionsKm.add("60KM");
        optionsKm.add("100KM+");
        return optionsKm;
    }
    public  static ArrayList<ArrayList<String>> getAgeRight(){
        ArrayList<String> options2Items_01 = new ArrayList<>();
        ArrayList<String> options2Items_02 = new ArrayList<>();
        ArrayList<String> options2Items_03 = new ArrayList<>();
        ArrayList<String> options2Items_04 = new ArrayList<>();
        ArrayList<String> options2Items_05 = new ArrayList<>();
        ArrayList<String> options2Items_06 = new ArrayList<>();
        ArrayList<String> options2Items_07 = new ArrayList<>();
        ArrayList<String> options2Items_08 = new ArrayList<>();
        ArrayList<String> options2Items_09 = new ArrayList<>();
        ArrayList<String> options2Items_10 = new ArrayList<>();
        ArrayList<String> options2Items_11 = new ArrayList<>();
        ArrayList<String> options2Items_12 = new ArrayList<>();
        ArrayList<String> options2Items_13 = new ArrayList<>();
        ArrayList<String> options2Items_14 = new ArrayList<>();
        ArrayList<String> options2Items_15 = new ArrayList<>();
        ArrayList<String> options2Items_16 = new ArrayList<>();
        ArrayList<String> options2Items_17 = new ArrayList<>();
        ArrayList<String> options2Items_18 = new ArrayList<>();

        optionsAgeRigt.add(getOptions(options2Items_01,19,81));
        optionsAgeRigt.add(getOptions(options2Items_02,20,80));
        optionsAgeRigt.add(getOptions(options2Items_03,21,79));
        optionsAgeRigt.add(getOptions(options2Items_04,22,78));
        optionsAgeRigt.add(getOptions(options2Items_05,23,77));
        optionsAgeRigt.add(getOptions(options2Items_06,24,76));
        optionsAgeRigt.add(getOptions(options2Items_07,25,75));
        optionsAgeRigt.add(getOptions(options2Items_08,26,74));
        optionsAgeRigt.add(getOptions(options2Items_09,27,74));
        optionsAgeRigt.add(getOptions(options2Items_10,28,73));
        optionsAgeRigt.add(getOptions(options2Items_11,29,72));
        optionsAgeRigt.add(getOptions(options2Items_12,30,71));
        optionsAgeRigt.add(getOptions(options2Items_13,31,70));
        optionsAgeRigt.add(getOptions(options2Items_14,32,69));
        optionsAgeRigt.add(getOptions(options2Items_15,33,68));
        optionsAgeRigt.add(getOptions(options2Items_16,34,67));
        optionsAgeRigt.add(getOptions(options2Items_17,35,66));
        optionsAgeRigt.add(getOptions(options2Items_18,36,65));
        return  optionsAgeRigt;
    }

    private static ArrayList<String> getOptions(ArrayList<String> options2Items, int start, int num){
        for(int i=0;i<=num;i++){
            options2Items.add(String.valueOf(start++));
        }
        return options2Items;
    }
    public  static ArrayList<String> getHeightLeft(){
        optionsHeightLeft.add("150cm 4'11\"");
        optionsHeightLeft.add("155cm 5'0\"");
        optionsHeightLeft.add("160cm 5'2\"");
        optionsHeightLeft.add("165cm 5'4\"");
        optionsHeightLeft.add("170cm 5'5\"");
        optionsHeightLeft.add("175cm 5'7\"");
        optionsHeightLeft.add("180cm 5'9\"");
        optionsHeightLeft.add("185cm 5'10\"");
        optionsHeightLeft.add("190cm 6'0\"");
        optionsHeightLeft.add("195cm 6'2\"");
        optionsHeightLeft.add("200cm 6'3\"");


        return optionsHeightLeft;
    }
    public  static ArrayList<ArrayList<String>> getHeightRight(){
        ArrayList<String> options2Items_03 = new ArrayList<>();
        ArrayList<String> options2Items_04 = new ArrayList<>();
        ArrayList<String> options2Items_05 = new ArrayList<>();
        ArrayList<String> options2Items_06 = new ArrayList<>();
        ArrayList<String> options2Items_07 = new ArrayList<>();
        ArrayList<String> options2Items_08 = new ArrayList<>();
        ArrayList<String> options2Items_09 = new ArrayList<>();
        ArrayList<String> options2Items_10 = new ArrayList<>();
        ArrayList<String> options2Items_11 = new ArrayList<>();
        ArrayList<String> options2Items_12 = new ArrayList<>();
        ArrayList<String> options2Items_13 = new ArrayList<>();


        options2Items_03.add("155cm 5'0\"");
        options2Items_03.add("160cm 5'2\"");
        options2Items_03.add("165cm 5'4\"");
        options2Items_03.add("170cm 5'5\"");
        options2Items_03.add("175cm 5'7\"");
        options2Items_03.add("180cm 5'9\"");
        options2Items_03.add("185cm 5'10\"");
        options2Items_03.add("190cm 6'0\"");
        options2Items_03.add("195cm 6'2\"");
        options2Items_03.add("200cm 6'3\"");
        options2Items_03.add("220cm 6'10\"");

        options2Items_04.add("160cm 5'2\"");
        options2Items_04.add("165cm 5'4\"");
        options2Items_04.add("170cm 5'5\"");
        options2Items_04.add("175cm 5'7\"");
        options2Items_04.add("180cm 5'9\"");
        options2Items_04.add("185cm 5'10\"");
        options2Items_04.add("190cm 6'0\"");
        options2Items_04.add("195cm 6'2\"");
        options2Items_04.add("200cm 6'3\"");
        options2Items_04.add("220cm 6'10\"");

        options2Items_05.add("165cm 5'4\"");
        options2Items_05.add("170cm 5'5\"");
        options2Items_05.add("175cm 5'7\"");
        options2Items_05.add("180cm 5'9\"");
        options2Items_05.add("185cm 5'10\"");
        options2Items_05.add("190cm 6'0\"");
        options2Items_05.add("195cm 6'2\"");
        options2Items_05.add("200cm 6'3\"");
        options2Items_05.add("220cm 6'10\"");

        options2Items_06.add("170cm 5'5\"");
        options2Items_06.add("175cm 5'7\"");
        options2Items_06.add("180cm 5'9\"");
        options2Items_06.add("185cm 5'10\"");
        options2Items_06.add("190cm 6'0\"");
        options2Items_06.add("195cm 6'2\"");
        options2Items_06.add("200cm 6'3\"");
        options2Items_06.add("220cm 6'10\"");

        options2Items_07.add("175cm 5'7\"");
        options2Items_07.add("180cm 5'9\"");
        options2Items_07.add("185cm 5'10\"");
        options2Items_07.add("190cm 6'0\"");
        options2Items_07.add("195cm 6'2\"");
        options2Items_07.add("200cm 6'3\"");
        options2Items_07.add("220cm 6'10\"");

        options2Items_08.add("180cm 5'9\"");
        options2Items_08.add("185cm 5'10\"");
        options2Items_08.add("190cm 6'0\"");
        options2Items_08.add("195cm 6'2\"");
        options2Items_08.add("200cm 6'3\"");
        options2Items_08.add("220cm 6'10\"");

        options2Items_09.add("185cm 5'10\"");
        options2Items_09.add("190cm 6'0\"");
        options2Items_09.add("195cm 6'2\"");
        options2Items_09.add("200cm 6'3\"");
        options2Items_09.add("220cm 6'10\"");

        options2Items_10.add("190cm 6'0\"");
        options2Items_10.add("195cm 6'2\"");
        options2Items_10.add("200cm 6'3\"");
        options2Items_10.add("220cm 6'10\"");

        options2Items_11.add("195cm 6'2\"");
        options2Items_11.add("200cm 6'3\"");
        options2Items_11.add("220cm 6'10\"");

        options2Items_12.add("200cm 6'3\"");
        options2Items_12.add("220cm 6'10\"");

        options2Items_13.add("220cm 6'10\"");

         optionsHeightRight.add(options2Items_03);
         optionsHeightRight.add(options2Items_04);
         optionsHeightRight.add(options2Items_05);
         optionsHeightRight.add(options2Items_06);
         optionsHeightRight.add(options2Items_07);
         optionsHeightRight.add(options2Items_08);
         optionsHeightRight.add(options2Items_09);
         optionsHeightRight.add(options2Items_10);
         optionsHeightRight.add(options2Items_11);
         optionsHeightRight.add(options2Items_12);
         optionsHeightRight.add(options2Items_13);


        return optionsHeightRight;
    }
}
