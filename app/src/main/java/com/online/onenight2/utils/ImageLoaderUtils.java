package com.online.onenight2.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.online.onenight2.R;
import com.online.onenight2.base.BaseApplication;


/**
 * 描述：图片加载工具类
 * Created by qyh on 2016/10/28.
 */
public class ImageLoaderUtils {

    public static void display(Context context, ImageView imageView, String url, int placeholder, int error) {
        if(imageView == null) {
            throw new IllegalArgumentException("argument error");
        }
        Glide.with(BaseApplication.getAppContext()).load(url).placeholder(placeholder)
                .error(error).crossFade().into(imageView);
    }

    public static void display(Context context, ImageView imageView, String url) {
        if(imageView == null) {
            throw new IllegalArgumentException("argument error");
        }
        Glide.with(BaseApplication.getAppContext()).load(url).placeholder(R.mipmap.unload)
                .error(R.mipmap.unload).crossFade().into(imageView);
    }
}
