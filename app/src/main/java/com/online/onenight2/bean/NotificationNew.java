package com.online.onenight2.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 描述：
 * Created by qyh on 2017/4/6.
 *
 * 问题：混淆 default constructor not found
 * 解决：内部类加 static
 */

public class NotificationNew implements Serializable {

    private int displaySecond  ;
    private int noticeType  ;
    private String lastMsgTime  ;
    private String isSucceed  ;
    private List<UnreadMsgBoxList> unreadMsgBoxList;
    private RemoteYuanfenUserBase remoteYuanfenUserBase;

    public RemoteYuanfenUserBase getRemoteYuanfenUserBase() {
        return remoteYuanfenUserBase;
    }

    public void setRemoteYuanfenUserBase(RemoteYuanfenUserBase remoteYuanfenUserBase) {
        this.remoteYuanfenUserBase = remoteYuanfenUserBase;
    }

    public NotificationNew(){
        super();
    }

    public  class RemoteYuanfenUserBase implements Serializable {

        private String country;
        private int isSayHello;
        private String language;
        private int isRecUser;
        private int resultType;
        private UserBaseEnglish userBaseEnglish;

        @Override
        public String toString() {
            return "RemoteYuanfenUserBase{" +
                    "country='" + country + '\'' +
                    ", isSayHello=" + isSayHello +
                    ", language='" + language + '\'' +
                    ", isRecUser=" + isRecUser +
                    ", resultType=" + resultType +
                    ", userBaseEnglish=" + userBaseEnglish +
                    '}';
        }

        public RemoteYuanfenUserBase(){
            super();
        }
        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public int getIsSayHello() {
            return isSayHello;
        }

        public void setIsSayHello(int isSayHello) {
            this.isSayHello = isSayHello;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public int getIsRecUser() {
            return isRecUser;
        }

        public void setIsRecUser(int isRecUser) {
            this.isRecUser = isRecUser;
        }

        public int getResultType() {
            return resultType;
        }

        public void setResultType(int resultType) {
            this.resultType = resultType;
        }

        public UserBaseEnglish getUserBaseEnglish() {
            return userBaseEnglish;
        }

        public void setUserBaseEnglish(UserBaseEnglish userBaseEnglish) {
            this.userBaseEnglish = userBaseEnglish;
        }

        public  class UserBaseEnglish implements Serializable {

            private int income;
            private int bodyType;
            private String country;
            private int education;
            private int occupation;
            private int sign;
            private String language;
            private int isAuthentication;
            private String monologue;
            private int isBeanUser;
            private int id;
            private String height;
            private String stat;
            private int isMonthly;
            private int level;
            private String nickName;
            private int sex;
            private String heightCm;
            private int isVip;
            private String regTime;
            private String district;
            private String spokenLanguage;
            private int age;
            private int maritalStatus;
            private Area area;

            @Override
            public String toString() {
                return "UserBaseEnglish{" +
                        "income=" + income +
                        ", bodyType=" + bodyType +
                        ", country='" + country + '\'' +
                        ", education=" + education +
                        ", occupation=" + occupation +
                        ", sign=" + sign +
                        ", language='" + language + '\'' +
                        ", isAuthentication=" + isAuthentication +
                        ", monologue='" + monologue + '\'' +
                        ", isBeanUser=" + isBeanUser +
                        ", id=" + id +
                        ", height='" + height + '\'' +
                        ", stat='" + stat + '\'' +
                        ", isMonthly=" + isMonthly +
                        ", level=" + level +
                        ", nickName='" + nickName + '\'' +
                        ", sex=" + sex +
                        ", heightCm='" + heightCm + '\'' +
                        ", isVip=" + isVip +
                        ", regTime='" + regTime + '\'' +
                        ", district='" + district + '\'' +
                        ", spokenLanguage='" + spokenLanguage + '\'' +
                        ", age=" + age +
                        ", maritalStatus=" + maritalStatus +
                        ", area=" + area +
                        ", image=" + image +
                        '}';
            }

            public UserBaseEnglish(){
                super();
            }
            public int getIncome() {
                return income;
            }

            public void setIncome(int income) {
                this.income = income;
            }

            public int getBodyType() {
                return bodyType;
            }

            public void setBodyType(int bodyType) {
                this.bodyType = bodyType;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public int getEducation() {
                return education;
            }

            public void setEducation(int education) {
                this.education = education;
            }

            public int getOccupation() {
                return occupation;
            }

            public void setOccupation(int occupation) {
                this.occupation = occupation;
            }

            public int getSign() {
                return sign;
            }

            public void setSign(int sign) {
                this.sign = sign;
            }

            public String getLanguage() {
                return language;
            }

            public void setLanguage(String language) {
                this.language = language;
            }

            public int getIsAuthentication() {
                return isAuthentication;
            }

            public void setIsAuthentication(int isAuthentication) {
                this.isAuthentication = isAuthentication;
            }

            public String getMonologue() {
                return monologue;
            }

            public void setMonologue(String monologue) {
                this.monologue = monologue;
            }

            public int getIsBeanUser() {
                return isBeanUser;
            }

            public void setIsBeanUser(int isBeanUser) {
                this.isBeanUser = isBeanUser;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getHeight() {
                return height;
            }

            public void setHeight(String height) {
                this.height = height;
            }

            public String getStat() {
                return stat;
            }

            public void setStat(String stat) {
                this.stat = stat;
            }

            public int getIsMonthly() {
                return isMonthly;
            }

            public void setIsMonthly(int isMonthly) {
                this.isMonthly = isMonthly;
            }

            public int getLevel() {
                return level;
            }

            public void setLevel(int level) {
                this.level = level;
            }

            public String getNickName() {
                return nickName;
            }

            public void setNickName(String nickName) {
                this.nickName = nickName;
            }

            public int getSex() {
                return sex;
            }

            public void setSex(int sex) {
                this.sex = sex;
            }

            public String getHeightCm() {
                return heightCm;
            }

            public void setHeightCm(String heightCm) {
                this.heightCm = heightCm;
            }

            public int getIsVip() {
                return isVip;
            }

            public void setIsVip(int isVip) {
                this.isVip = isVip;
            }

            public String getRegTime() {
                return regTime;
            }

            public void setRegTime(String regTime) {
                this.regTime = regTime;
            }

            public String getDistrict() {
                return district;
            }

            public void setDistrict(String district) {
                this.district = district;
            }

            public String getSpokenLanguage() {
                return spokenLanguage;
            }

            public void setSpokenLanguage(String spokenLanguage) {
                this.spokenLanguage = spokenLanguage;
            }

            public int getAge() {
                return age;
            }

            public void setAge(int age) {
                this.age = age;
            }

            public int getMaritalStatus() {
                return maritalStatus;
            }

            public void setMaritalStatus(int maritalStatus) {
                this.maritalStatus = maritalStatus;
            }

            public Area getArea() {
                return area;
            }

            public void setArea(Area area) {
                this.area = area;
            }

            public Image getImage() {
                return image;
            }

            public void setImage(Image image) {
                this.image = image;
            }

            public class Area implements Serializable {

                public int cityIndex;
                public String country;
                public int areaIndex;
                public int areaId;
                public String cityName;
                public String areaName;
                public String provinceName;
                public int cityId;
                public int provinceId;
            }

            public Image image;

            public   class Image implements Serializable {

                public String country;
                public int isMain;
                public String thumbnailUrlM;
                public String language;
                public int label;
                public Double rate;
                public String imageUrl;
                public String localPath;
                public int id;
                public int state;
                public int pornStatus;
                public String thumbnailUrl;
                public int status;
            }

        }
    }
    public int getDisplaySecond() {
        return displaySecond;
    }

    public int getNoticeType() {
        return noticeType;
    }

    public String getLastMsgTime() {
        return lastMsgTime;
    }

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setDisplaySecond(int displaySecond) {
        this.displaySecond = displaySecond;
    }

    public void setNoticeType(int noticeType) {
        this.noticeType = noticeType;
    }

    public void setLastMsgTime(String lastMsgTime) {
        this.lastMsgTime = lastMsgTime;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public void setUnreadMsgBoxList(List<UnreadMsgBoxList> unreadMsgBoxList) {
        this.unreadMsgBoxList = unreadMsgBoxList;
    }

    public List<UnreadMsgBoxList> getUnreadMsgBoxList() {
        return unreadMsgBoxList;
    }

    public class UnreadMsgBoxList implements Serializable {

        private String msg;
        private String country;
        private String timeMills;
        private int isRead;
        private String language;
        private String noHeadImg;
        private int type;
        private int ownedUid;
        private String audioUrl;
        private String noVerifyIdentity;
        private String audioTime;
        private String noConformAge;
        private int id;
        private String time;
        private String diffProvinces;

        @Override
        public String toString() {
            return "UnreadMsgBoxList{" +
                    "msg='" + msg + '\'' +
                    ", country='" + country + '\'' +
                    ", timeMills='" + timeMills + '\'' +
                    ", isRead=" + isRead +
                    ", language='" + language + '\'' +
                    ", noHeadImg='" + noHeadImg + '\'' +
                    ", type=" + type +
                    ", ownedUid=" + ownedUid +
                    ", audioUrl='" + audioUrl + '\'' +
                    ", noVerifyIdentity='" + noVerifyIdentity + '\'' +
                    ", audioTime='" + audioTime + '\'' +
                    ", noConformAge='" + noConformAge + '\'' +
                    ", id=" + id +
                    ", time='" + time + '\'' +
                    ", diffProvinces='" + diffProvinces + '\'' +
                    ", userBaseEnglish=" + userBaseEnglish +
                    '}';
        }

        public UnreadMsgBoxList(){
            super();
        }

        public UnreadMsgBoxList(String msg, String country, String timeMills, int isRead, String language,
                                String noHeadImg, int type, int ownedUid, String audioUrl,
                                String noVerifyIdentity, String audioTime, String noConformAge, int id,
                                String time, String diffProvinces, UserBaseEnglish userBaseEnglish) {
            this.msg = msg;
            this.country = country;
            this.timeMills = timeMills;
            this.isRead = isRead;
            this.language = language;
            this.noHeadImg = noHeadImg;
            this.type = type;
            this.ownedUid = ownedUid;
            this.audioUrl = audioUrl;
            this.noVerifyIdentity = noVerifyIdentity;
            this.audioTime = audioTime;
            this.noConformAge = noConformAge;
            this.id = id;
            this.time = time;
            this.diffProvinces = diffProvinces;
            this.userBaseEnglish = userBaseEnglish;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public void setTimeMills(String timeMills) {
            this.timeMills = timeMills;
        }

        public void setIsRead(int isRead) {
            this.isRead = isRead;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public void setNoHeadImg(String noHeadImg) {
            this.noHeadImg = noHeadImg;
        }

        public void setType(int type) {
            this.type = type;
        }

        public void setOwnedUid(int ownedUid) {
            this.ownedUid = ownedUid;
        }

        public void setAudioUrl(String audioUrl) {
            this.audioUrl = audioUrl;
        }

        public void setNoVerifyIdentity(String noVerifyIdentity) {
            this.noVerifyIdentity = noVerifyIdentity;
        }

        public void setAudioTime(String audioTime) {
            this.audioTime = audioTime;
        }

        public void setNoConformAge(String noConformAge) {
            this.noConformAge = noConformAge;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public void setDiffProvinces(String diffProvinces) {
            this.diffProvinces = diffProvinces;
        }

        public void setUserBaseEnglish(UserBaseEnglish userBaseEnglish) {
            this.userBaseEnglish = userBaseEnglish;
        }

        public String getMsg() {
            return msg;
        }

        public String getCountry() {
            return country;
        }

        public String getTimeMills() {
            return timeMills;
        }

        public int getIsRead() {
            return isRead;
        }

        public String getLanguage() {
            return language;
        }

        public String getNoHeadImg() {
            return noHeadImg;
        }

        public int getType() {
            return type;
        }

        public int getOwnedUid() {
            return ownedUid;
        }

        public String getAudioUrl() {
            return audioUrl;
        }

        public String getNoVerifyIdentity() {
            return noVerifyIdentity;
        }

        public String getAudioTime() {
            return audioTime;
        }

        public String getNoConformAge() {
            return noConformAge;
        }

        public int getId() {
            return id;
        }

        public String getTime() {
            return time;
        }

        public String getDiffProvinces() {
            return diffProvinces;
        }

        public UserBaseEnglish getUserBaseEnglish() {
            return userBaseEnglish;
        }

        private UserBaseEnglish userBaseEnglish;

        public  class UserBaseEnglish implements Serializable {

            private int income;
            private int bodyType;
            private String country;
            private int education;
            private int occupation;
            private int sign;
            private String language;
            private int isAuthentication;
            private String monologue;
            private int isBeanUser;
            private int id;
            private String height;
            private String stat;
            private int isMonthly;
            private int level;
            private String nickName;
            private int sex;
            private String heightCm;
            private int isVip;
            private String regTime;
            private String district;
            private String spokenLanguage;
            private int age;
            private int maritalStatus;
            private Area area;

            public UserBaseEnglish(){
                super();
            }
            public int getIncome() {
                return income;
            }

            public int getBodyType() {
                return bodyType;
            }

            public String getCountry() {
                return country;
            }

            public int getEducation() {
                return education;
            }

            public int getOccupation() {
                return occupation;
            }

            public int getSign() {
                return sign;
            }

            public String getLanguage() {
                return language;
            }

            public int getIsAuthentication() {
                return isAuthentication;
            }

            public String getMonologue() {
                return monologue;
            }

            public int getIsBeanUser() {
                return isBeanUser;
            }

            public int getId() {
                return id;
            }

            public String getHeight() {
                return height;
            }

            public String getStat() {
                return stat;
            }

            public int getIsMonthly() {
                return isMonthly;
            }

            public int getLevel() {
                return level;
            }

            public String getNickName() {
                return nickName;
            }

            public int getSex() {
                return sex;
            }

            public String getHeightCm() {
                return heightCm;
            }

            public int getIsVip() {
                return isVip;
            }

            public String getRegTime() {
                return regTime;
            }

            public String getDistrict() {
                return district;
            }

            public String getSpokenLanguage() {
                return spokenLanguage;
            }

            public int getAge() {
                return age;
            }

            public int getMaritalStatus() {
                return maritalStatus;
            }

            public Area getArea() {
                return area;
            }

            public Image getImage() {
                return image;
            }

            public void setIncome(int income) {
                this.income = income;
            }

            public void setBodyType(int bodyType) {
                this.bodyType = bodyType;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public void setEducation(int education) {
                this.education = education;
            }

            public void setOccupation(int occupation) {
                this.occupation = occupation;
            }

            public void setSign(int sign) {
                this.sign = sign;
            }

            public void setLanguage(String language) {
                this.language = language;
            }

            public void setIsAuthentication(int isAuthentication) {
                this.isAuthentication = isAuthentication;
            }

            public void setMonologue(String monologue) {
                this.monologue = monologue;
            }

            public void setIsBeanUser(int isBeanUser) {
                this.isBeanUser = isBeanUser;
            }

            public void setId(int id) {
                this.id = id;
            }

            public void setHeight(String height) {
                this.height = height;
            }

            public void setStat(String stat) {
                this.stat = stat;
            }

            public void setIsMonthly(int isMonthly) {
                this.isMonthly = isMonthly;
            }

            public void setLevel(int level) {
                this.level = level;
            }

            public void setNickName(String nickName) {
                this.nickName = nickName;
            }

            public void setSex(int sex) {
                this.sex = sex;
            }

            public void setHeightCm(String heightCm) {
                this.heightCm = heightCm;
            }

            public void setIsVip(int isVip) {
                this.isVip = isVip;
            }

            public void setRegTime(String regTime) {
                this.regTime = regTime;
            }

            public void setDistrict(String district) {
                this.district = district;
            }

            public void setSpokenLanguage(String spokenLanguage) {
                this.spokenLanguage = spokenLanguage;
            }

            public void setAge(int age) {
                this.age = age;
            }

            public void setMaritalStatus(int maritalStatus) {
                this.maritalStatus = maritalStatus;
            }

            public void setArea(Area area) {
                this.area = area;
            }

            public void setImage(Image image) {
                this.image = image;
            }

            private  class Area implements Serializable {

                private int cityIndex;
                private String country;
                private int areaIndex;
                private int areaId;
                private String cityName;
                private String areaName;
                private String provinceName;
                private int cityId;
                private int provinceId;

                public Area(){
                    super();
                }
                public int getCityIndex() {
                    return cityIndex;
                }

                public void setCityIndex(int cityIndex) {
                    this.cityIndex = cityIndex;
                }

                public String getCountry() {
                    return country;
                }

                public void setCountry(String country) {
                    this.country = country;
                }

                public int getAreaIndex() {
                    return areaIndex;
                }

                public void setAreaIndex(int areaIndex) {
                    this.areaIndex = areaIndex;
                }

                public int getAreaId() {
                    return areaId;
                }

                public void setAreaId(int areaId) {
                    this.areaId = areaId;
                }

                public String getCityName() {
                    return cityName;
                }

                public void setCityName(String cityName) {
                    this.cityName = cityName;
                }

                public String getAreaName() {
                    return areaName;
                }

                public void setAreaName(String areaName) {
                    this.areaName = areaName;
                }

                public String getProvinceName() {
                    return provinceName;
                }

                public void setProvinceName(String provinceName) {
                    this.provinceName = provinceName;
                }

                public int getCityId() {
                    return cityId;
                }

                public void setCityId(int cityId) {
                    this.cityId = cityId;
                }

                public int getProvinceId() {
                    return provinceId;
                }

                public void setProvinceId(int provinceId) {
                    this.provinceId = provinceId;
                }
            }

            private Image image;

            public  class Image implements Serializable {

                private String country;
                private int isMain;
                private String thumbnailUrlM;
                private String language;
                private int label;
                private Double rate;
                private String imageUrl;
                private String localPath;
                private int id;
                private int state;
                private int pornStatus;
                private String thumbnailUrl;
                private int status;
                public Image(){
                    super();
                }
                public String getCountry() {
                    return country;
                }

                public void setCountry(String country) {
                    this.country = country;
                }

                public int getIsMain() {
                    return isMain;
                }

                public void setIsMain(int isMain) {
                    this.isMain = isMain;
                }

                public String getThumbnailUrlM() {
                    return thumbnailUrlM;
                }

                public void setThumbnailUrlM(String thumbnailUrlM) {
                    this.thumbnailUrlM = thumbnailUrlM;
                }

                public String getLanguage() {
                    return language;
                }

                public void setLanguage(String language) {
                    this.language = language;
                }

                public int getLabel() {
                    return label;
                }

                public void setLabel(int label) {
                    this.label = label;
                }

                public Double getRate() {
                    return rate;
                }

                public void setRate(Double rate) {
                    this.rate = rate;
                }

                public String getImageUrl() {
                    return imageUrl;
                }

                public void setImageUrl(String imageUrl) {
                    this.imageUrl = imageUrl;
                }

                public String getLocalPath() {
                    return localPath;
                }

                public void setLocalPath(String localPath) {
                    this.localPath = localPath;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getState() {
                    return state;
                }

                public void setState(int state) {
                    this.state = state;
                }

                public int getPornStatus() {
                    return pornStatus;
                }

                public void setPornStatus(int pornStatus) {
                    this.pornStatus = pornStatus;
                }

                public String getThumbnailUrl() {
                    return thumbnailUrl;
                }

                public void setThumbnailUrl(String thumbnailUrl) {
                    this.thumbnailUrl = thumbnailUrl;
                }

                public int getStatus() {
                    return status;
                }

                public void setStatus(int status) {
                    this.status = status;
                }
            }
        }
    }

    @Override
    public String toString() {
        return "NotificationNew{" +
                "displaySecond=" + displaySecond +
                ", noticeType=" + noticeType +
                ", lastMsgTime='" + lastMsgTime + '\'' +
                ", isSucceed='" + isSucceed + '\'' +
                ", unreadMsgBoxList=" + unreadMsgBoxList +
                ", remoteYuanfenUserBase=" + remoteYuanfenUserBase +
                '}';
    }
}