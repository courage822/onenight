package com.online.onenight2.bean;

import java.util.List;

/**
 * 搜索用户对象
 * Created by zhangdroid on 2016/8/12.
 */
public class SearchUser {
    String isRecUser;// 是否为推荐用户，1->为推荐用户,
    String isSayHello;// 是否打过招呼，1->已打招呼, 0->未打招呼,
    String isFollow;// 是否关注
    List<String> listLabel;// 推荐标签
    String imgCount;// 照片数量
    String constellation;// 星座
    String distance;// 距离
    UserBase userBaseEnglish;// 用户对象基础信息

    public String getIsRecUser() {
        return isRecUser;
    }

    public void setIsRecUser(String isRecUser) {
        this.isRecUser = isRecUser;
    }

    public String getIsSayHello() {
        return isSayHello;
    }

    public void setIsSayHello(String isSayHello) {
        this.isSayHello = isSayHello;
    }

    public String getIsFollow() {
        return isFollow;
    }

    public void setIsFollow(String isFollow) {
        this.isFollow = isFollow;
    }

    public List<String> getListLabel() {
        return listLabel;
    }

    public void setListLabel(List<String> listLabel) {
        this.listLabel = listLabel;
    }

    public String getImgCount() {
        return imgCount;
    }

    public void setImgCount(String imgCount) {
        this.imgCount = imgCount;
    }

    public String getConstellation() {
        return constellation;
    }

    public void setConstellation(String constellation) {
        this.constellation = constellation;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public UserBase getUserBaseEnglish() {
        return userBaseEnglish;
    }

    public void setUserBaseEnglish(UserBase userBaseEnglish) {
        this.userBaseEnglish = userBaseEnglish;
    }

    @Override
    public String toString() {
        return "SearchUser{" +
                "isRecUser='" + isRecUser + '\'' +
                ", isSayHello='" + isSayHello + '\'' +
                ", isFollow='" + isFollow + '\'' +
                ", listLabel='" + listLabel + '\'' +
                ", imgCount='" + imgCount + '\'' +
                ", sign='" + constellation + '\'' +
                ", distance='" + distance + '\'' +
                ", userBaseEnglish=" + userBaseEnglish +
                '}';
    }

}
