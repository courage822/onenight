package com.online.onenight2.bean;

/**
 * Created by foxmanman on 2016/7/4.
 * 修改密码
 */
public class ChangePwdBean  {
    private  String isSucceed;
    private String msg;

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "ChangePwdBean{" +
                "isSucceed='" + isSucceed + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}
