package com.online.onenight2.bean;

/**
 * 聊天信息对象
 * Created by zhangdroid on 2016/8/23.
 */
public class ChatMsg {
    long id;
    long localUserId;// 己方id
    long remoteUserId;// 对方id
    int chatStatus;// 聊天状态类型 1、开始写信 2、发信 3、收信 4、配对
    int unreadCount;// 未读消息数
    String lastTime;// 最新消息的时间
    String lastContent;// 最新消息
    String addTime;// 收到新消息的时间
    int fromChannel;// 渠道号
    String country;
    String language;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getLocalUserId() {
        return localUserId;
    }

    public void setLocalUserId(long localUserId) {
        this.localUserId = localUserId;
    }

    public long getRemoteUserId() {
        return remoteUserId;
    }

    public void setRemoteUserId(long remoteUserId) {
        this.remoteUserId = remoteUserId;
    }

    public int getChatStatus() {
        return chatStatus;
    }

    public void setChatStatus(int chatStatus) {
        this.chatStatus = chatStatus;
    }

    public int getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }

    public String getLastTime() {
        return lastTime;
    }

    public void setLastTime(String lastTime) {
        this.lastTime = lastTime;
    }

    public String getLastContent() {
        return lastContent;
    }

    public void setLastContent(String lastContent) {
        this.lastContent = lastContent;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public int getFromChannel() {
        return fromChannel;
    }

    public void setFromChannel(int fromChannel) {
        this.fromChannel = fromChannel;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "ChatMsg{" +
                "id=" + id +
                ", localUserId=" + localUserId +
                ", remoteUserId=" + remoteUserId +
                ", chatStatus=" + chatStatus +
                ", unreadCount=" + unreadCount +
                ", lastTime='" + lastTime + '\'' +
                ", lastContent='" + lastContent + '\'' +
                ", addTime='" + addTime + '\'' +
                ", fromChannel=" + fromChannel +
                ", country='" + country + '\'' +
                ", language='" + language + '\'' +
                '}';
    }

}
