package com.online.onenight2.bean;

import java.util.List;
import java.util.Map;

/**
 * Created by jzrh on 2016/6/29.
 */
public class ParamsInit {
    public List<String> bodyTypeList;
    public Map<String, String> bodyTypeMap;
    public Map<String, String> bodyTypeMap2;
    public Map<String, String> distractMap;
    public Map<String, String> distractMap2;
    public Map<String, String> booksMap;
    public Map<String, String> booksMap2;
    public List<String> constellationList;
    public Map<String, String> constellationMap;
    public Map<String, String> constellationMap2;
    public DictIndex dictIndex;
    public List<String> drinkList;
    public Map<String, String> drinkMap;
    public Map<String, String> drinkMap2;
    public List<String> educationList;
    public Map<String, String> educationMap;
    public Map<String, String> educationMap2;
    public List<String> ethnicityList;
    public Map<String, String> ethnicityMap;
    public Map<String, String> ethnicityMap2;
    public List<String> exerciseHabitsList;
    public Map<String, String> exerciseHabitsMap;
    public Map<String, String> exerciseHabitsMap2;
    public List<String> eyesList;
    public Map<String, String> eyesMap;
    public Map<String, String> eyesMap2;
    public List<String> faithList;
    public Map<String, String> faithMap;
    public Map<String, String> faithMap2;
    public List<String> hairList;
    public Map<String, String> hairMap;
    public Map<String, String> hairMap2;
    public List<String> haveKidsList;
    public Map<String, String> haveKidsMap;
    public Map<String, String> haveKidsMap2;
    public List<String> heightList;
    public List<String> howManyKidsList;
    public Map<String, String> howManyKidsMap;
    public Map<String, String> howManyKidsMap2;
    public List<String> interestList;
    public Map<String, String> interestMap;
    public Map<String, String> interestMap2;
    public List<String> languageList;
    public Map<String, String> languageMap;
    public Map<String, String> languageMap2;
    public List<String> marriageList;
    public Map<String, String> marriageMap;
    public Map<String, String> marriageMap2;
    public List<String> needBabeList;
    public Map<String, String> needBabeMap;
    public Map<String, String> needBabeMap2;
    public List<String> petsList;
    public Map<String, String> petsMap;
    public Map<String, String> petsMap2;
    public List<String> politicalViewsList;
    public Map<String, String> politicalViewsMap;
    public Map<String, String> politicalViewsMap2;
    public List<String> smokeList;
    public Map<String, String> smokeMap;
    public Map<String, String> smokeMap2;
    public List<String> sportList;
    public Map<String, String> sportMap;
    public Map<String, String> sportMap2;
    public List<String> workList;
    public Map<String, String> workMap;
    public Map<String, String> workMap2;
    public List<String> workMoneyList;
    public Map<String, String> workMoneyMap;
    public Map<String, String> workMoneyMap2;
    public Map<String, String> travelMap;
    public Map<String, String> travelMap2;
    public Map<String, String> characteristicsMap;
    public Map<String, String> characteristicsMap2;
    public List<String> maleSayHelloList;// 系统推荐招呼信（男）
    public List<String> femaleSayHelloList;// 系统推荐招呼信（女）
    public List<String> maleQuestionTagList;// 系统推荐QA信（男）
    public List<String> femaleQuestionTagList;// 系统推荐QA信（女）

    public Map<String,String> bloodMap2;
    public Map<String,String> charmMap2;
    public Map<String,String> diffAreaLoveIdMap2;
    public Map<String,String> foodsMap2;
    public Map<String,String> houseMap2;
    public Map<String,String> liveWithParentMap2;
    public Map<String,String> loverTypeMap2;
    public Map<String,String> loverTypeRemoteMap2;
    private Map<String, String> loverTypeMap2M;// 喜欢的类型（男）
    private Map<String, String> loverTypeMap2F;// 喜欢的类型（女）
    public Map<String,String> musicMap2;
    public Map<String,String> moviesMap2;
    public Map<String,String> sexBefMarriedMap2;

    public Map<String,String> bloodMap;
    public Map<String,String> charmMap;
    public Map<String,String> diffAreaLoveIdMap;
    public Map<String,String> foodsMap;
    public Map<String,String> houseMap;
    public Map<String,String> liveWithParentMap;
    public Map<String,String> loverTypeMap;
    public Map<String,String> loverTypeRemoteMap;
    public Map<String,String> musicMap;
    public Map<String,String> moviesMap;
    public Map<String,String> sexBefMarriedMap;
    public String defaultCity;
    private Map<String, String> bodyTypeMap2M;// 体型（男）
    private Map<String, String> bodyTypeMap2F;// 体型（女）
    private Map<String, String> bodyTypeRemoteMap;// 对方详情界面体型
    private Map<String, String> bodyTypeRemoteMap2;//

    public Map<String, String> getBodyTypeRemoteMap() {
        return bodyTypeRemoteMap;
    }

    public void setBodyTypeRemoteMap(Map<String, String> bodyTypeRemoteMap) {
        this.bodyTypeRemoteMap = bodyTypeRemoteMap;
    }

    public Map<String, String> getBodyTypeRemoteMap2() {
        return bodyTypeRemoteMap2;
    }

    public void setBodyTypeRemoteMap2(Map<String, String> bodyTypeRemoteMap2) {
        this.bodyTypeRemoteMap2 = bodyTypeRemoteMap2;
    }

    public Map<String, String> getLoverTypeMap2M() {
        return loverTypeMap2M;
    }

    public void setLoverTypeMap2M(Map<String, String> loverTypeMap2M) {
        this.loverTypeMap2M = loverTypeMap2M;
    }

    public Map<String, String> getLoverTypeMap2F() {
        return loverTypeMap2F;
    }

    public void setLoverTypeMap2F(Map<String, String> loverTypeMap2F) {
        this.loverTypeMap2F = loverTypeMap2F;
    }

    public Map<String, String> getBodyTypeMap2M() {
        return bodyTypeMap2M;
    }

    public void setBodyTypeMap2M(Map<String, String> bodyTypeMap2M) {
        this.bodyTypeMap2M = bodyTypeMap2M;
    }

    public Map<String, String> getBodyTypeMap2F() {
        return bodyTypeMap2F;
    }

    public void setBodyTypeMap2F(Map<String, String> bodyTypeMap2F) {
        this.bodyTypeMap2F = bodyTypeMap2F;
    }

    public Map<String, String> getDistractMap2() {
        return distractMap2;
    }

    public void setDistractMap2(Map<String, String> distractMap2) {
        this.distractMap2 = distractMap2;
    }

    public Map<String, String> getDistractMap() {
        return distractMap;
    }

    public void setDistractMap(Map<String, String> distractMap) {
        this.distractMap = distractMap;
    }

    public String getDefaultCity() {
        return defaultCity;
    }

    public void setDefaultCity(String defaultCity) {
        this.defaultCity = defaultCity;
    }

    public Map<String, String> getBloodMap() {
        return bloodMap;
    }

    public void setBloodMap(Map<String, String> bloodMap) {
        this.bloodMap = bloodMap;
    }

    public Map<String, String> getCharmMap() {
        return charmMap;
    }

    public void setCharmMap(Map<String, String> charmMap) {
        this.charmMap = charmMap;
    }

    public Map<String, String> getDiffAreaLoveIdMap() {
        return diffAreaLoveIdMap;
    }

    public void setDiffAreaLoveIdMap(Map<String, String> diffAreaLoveIdMap) {
        this.diffAreaLoveIdMap = diffAreaLoveIdMap;
    }

    public Map<String, String> getFoodsMap() {
        return foodsMap;
    }

    public void setFoodsMap(Map<String, String> foodsMap) {
        this.foodsMap = foodsMap;
    }

    public Map<String, String> getHouseMap() {
        return houseMap;
    }

    public void setHouseMap(Map<String, String> houseMap) {
        this.houseMap = houseMap;
    }

    public Map<String, String> getLiveWithParentMap() {
        return liveWithParentMap;
    }

    public void setLiveWithParentMap(Map<String, String> liveWithParentMap) {
        this.liveWithParentMap = liveWithParentMap;
    }

    public Map<String, String> getLoverTypeMap() {
        return loverTypeMap;
    }

    public void setLoverTypeMap(Map<String, String> loverTypeMap) {
        this.loverTypeMap = loverTypeMap;
    }

    public Map<String, String> getLoverTypeRemoteMap() {
        return loverTypeRemoteMap;
    }

    public void setLoverTypeRemoteMap(Map<String, String> loverTypeRemoteMap) {
        this.loverTypeRemoteMap = loverTypeRemoteMap;
    }

    public Map<String, String> getMusicMap() {
        return musicMap;
    }

    public void setMusicMap(Map<String, String> musicMap) {
        this.musicMap = musicMap;
    }

    public Map<String, String> getMoviesMap() {
        return moviesMap;
    }

    public void setMoviesMap(Map<String, String> moviesMap) {
        this.moviesMap = moviesMap;
    }

    public Map<String, String> getSexBefMarriedMap() {
        return sexBefMarriedMap;
    }

    public void setSexBefMarriedMap(Map<String, String> sexBefMarriedMap) {
        this.sexBefMarriedMap = sexBefMarriedMap;
    }

    public Map<String, String> getBloodMap2() {
        return bloodMap2;
    }

    public void setBloodMap2(Map<String, String> bloodMap2) {
        this.bloodMap2 = bloodMap2;
    }

    public Map<String, String> getCharmMap2() {
        return charmMap2;
    }

    public void setCharmMap2(Map<String, String> charmMap2) {
        this.charmMap2 = charmMap2;
    }

    public Map<String, String> getDiffAreaLoveIdMap2() {
        return diffAreaLoveIdMap2;
    }

    public void setDiffAreaLoveIdMap2(Map<String, String> diffAreaLoveIdMap2) {
        this.diffAreaLoveIdMap2 = diffAreaLoveIdMap2;
    }

    public Map<String, String> getFoodsMap2() {
        return foodsMap2;
    }

    public void setFoodsMap2(Map<String, String> foodsMap2) {
        this.foodsMap2 = foodsMap2;
    }

    public Map<String, String> getHouseMap2() {
        return houseMap2;
    }

    public void setHouseMap2(Map<String, String> houseMap2) {
        this.houseMap2 = houseMap2;
    }

    public Map<String, String> getLiveWithParentMap2() {
        return liveWithParentMap2;
    }

    public void setLiveWithParentMap2(Map<String, String> liveWithParentMap2) {
        this.liveWithParentMap2 = liveWithParentMap2;
    }

    public Map<String, String> getLoverTypeMap2() {
        return loverTypeMap2;
    }

    public void setLoverTypeMap2(Map<String, String> loverTypeMap2) {
        this.loverTypeMap2 = loverTypeMap2;
    }

    public Map<String, String> getLoverTypeRemoteMap2() {
        return loverTypeRemoteMap2;
    }

    public void setLoverTypeRemoteMap2(Map<String, String> loverTypeRemoteMap2) {
        this.loverTypeRemoteMap2 = loverTypeRemoteMap2;
    }

    public Map<String, String> getMusicMap2() {
        return musicMap2;
    }

    public void setMusicMap2(Map<String, String> musicMap2) {
        this.musicMap2 = musicMap2;
    }

    public Map<String, String> getMoviesMap2() {
        return moviesMap2;
    }

    public void setMoviesMap2(Map<String, String> moviesMap2) {
        this.moviesMap2 = moviesMap2;
    }

    public Map<String, String> getSexBefMarriedMap2() {
        return sexBefMarriedMap2;
    }

    public void setSexBefMarriedMap2(Map<String, String> sexBefMarriedMap2) {
        this.sexBefMarriedMap2 = sexBefMarriedMap2;
    }

    public List<String> getBodyTypeList() {
        return bodyTypeList;
    }

    public void setBodyTypeList(List<String> bodyTypeList) {
        this.bodyTypeList = bodyTypeList;
    }

    public Map<String, String> getBodyTypeMap() {
        return bodyTypeMap;
    }

    public void setBodyTypeMap(Map<String, String> bodyTypeMap) {
        this.bodyTypeMap = bodyTypeMap;
    }

    public Map<String, String> getBodyTypeMap2() {
        return bodyTypeMap2;
    }

    public void setBodyTypeMap2(Map<String, String> bodyTypeMap2) {
        this.bodyTypeMap2 = bodyTypeMap2;
    }

    public Map<String, String> getBooksMap() {
        return booksMap;
    }

    public void setBooksMap(Map<String, String> booksMap) {
        this.booksMap = booksMap;
    }

    public Map<String, String> getBooksMap2() {
        return booksMap2;
    }

    public void setBooksMap2(Map<String, String> booksMap2) {
        this.booksMap2 = booksMap2;
    }

    public List<String> getConstellationList() {
        return constellationList;
    }

    public void setConstellationList(List<String> constellationList) {
        this.constellationList = constellationList;
    }

    public Map<String, String> getConstellationMap() {
        return constellationMap;
    }

    public void setConstellationMap(Map<String, String> constellationMap) {
        this.constellationMap = constellationMap;
    }

    public Map<String, String> getConstellationMap2() {
        return constellationMap2;
    }

    public void setConstellationMap2(Map<String, String> constellationMap2) {
        this.constellationMap2 = constellationMap2;
    }

    public DictIndex getDictIndex() {
        return dictIndex;
    }

    public void setDictIndex(DictIndex dictIndex) {
        this.dictIndex = dictIndex;
    }

    public List<String> getDrinkList() {
        return drinkList;
    }

    public void setDrinkList(List<String> drinkList) {
        this.drinkList = drinkList;
    }

    public Map<String, String> getDrinkMap() {
        return drinkMap;
    }

    public void setDrinkMap(Map<String, String> drinkMap) {
        this.drinkMap = drinkMap;
    }

    public Map<String, String> getDrinkMap2() {
        return drinkMap2;
    }

    public void setDrinkMap2(Map<String, String> drinkMap2) {
        this.drinkMap2 = drinkMap2;
    }

    public List<String> getEducationList() {
        return educationList;
    }

    public void setEducationList(List<String> educationList) {
        this.educationList = educationList;
    }

    public Map<String, String> getEducationMap() {
        return educationMap;
    }

    public void setEducationMap(Map<String, String> educationMap) {
        this.educationMap = educationMap;
    }

    public Map<String, String> getEducationMap2() {
        return educationMap2;
    }

    public void setEducationMap2(Map<String, String> educationMap2) {
        this.educationMap2 = educationMap2;
    }

    public List<String> getEthnicityList() {
        return ethnicityList;
    }

    public void setEthnicityList(List<String> ethnicityList) {
        this.ethnicityList = ethnicityList;
    }

    public Map<String, String> getEthnicityMap() {
        return ethnicityMap;
    }

    public void setEthnicityMap(Map<String, String> ethnicityMap) {
        this.ethnicityMap = ethnicityMap;
    }

    public Map<String, String> getEthnicityMap2() {
        return ethnicityMap2;
    }

    public void setEthnicityMap2(Map<String, String> ethnicityMap2) {
        this.ethnicityMap2 = ethnicityMap2;
    }

    public List<String> getExerciseHabitsList() {
        return exerciseHabitsList;
    }

    public void setExerciseHabitsList(List<String> exerciseHabitsList) {
        this.exerciseHabitsList = exerciseHabitsList;
    }

    public Map<String, String> getExerciseHabitsMap() {
        return exerciseHabitsMap;
    }

    public void setExerciseHabitsMap(Map<String, String> exerciseHabitsMap) {
        this.exerciseHabitsMap = exerciseHabitsMap;
    }

    public Map<String, String> getExerciseHabitsMap2() {
        return exerciseHabitsMap2;
    }

    public void setExerciseHabitsMap2(Map<String, String> exerciseHabitsMap2) {
        this.exerciseHabitsMap2 = exerciseHabitsMap2;
    }

    public List<String> getEyesList() {
        return eyesList;
    }

    public void setEyesList(List<String> eyesList) {
        this.eyesList = eyesList;
    }

    public Map<String, String> getEyesMap() {
        return eyesMap;
    }

    public void setEyesMap(Map<String, String> eyesMap) {
        this.eyesMap = eyesMap;
    }

    public Map<String, String> getEyesMap2() {
        return eyesMap2;
    }

    public void setEyesMap2(Map<String, String> eyesMap2) {
        this.eyesMap2 = eyesMap2;
    }

    public List<String> getFaithList() {
        return faithList;
    }

    public void setFaithList(List<String> faithList) {
        this.faithList = faithList;
    }

    public Map<String, String> getFaithMap() {
        return faithMap;
    }

    public void setFaithMap(Map<String, String> faithMap) {
        this.faithMap = faithMap;
    }

    public Map<String, String> getFaithMap2() {
        return faithMap2;
    }

    public void setFaithMap2(Map<String, String> faithMap2) {
        this.faithMap2 = faithMap2;
    }

    public List<String> getHairList() {
        return hairList;
    }

    public void setHairList(List<String> hairList) {
        this.hairList = hairList;
    }

    public Map<String, String> getHairMap() {
        return hairMap;
    }

    public void setHairMap(Map<String, String> hairMap) {
        this.hairMap = hairMap;
    }

    public Map<String, String> getHairMap2() {
        return hairMap2;
    }

    public void setHairMap2(Map<String, String> hairMap2) {
        this.hairMap2 = hairMap2;
    }

    public List<String> getHaveKidsList() {
        return haveKidsList;
    }

    public void setHaveKidsList(List<String> haveKidsList) {
        this.haveKidsList = haveKidsList;
    }

    public Map<String, String> getHaveKidsMap() {
        return haveKidsMap;
    }

    public void setHaveKidsMap(Map<String, String> haveKidsMap) {
        this.haveKidsMap = haveKidsMap;
    }

    public Map<String, String> getHaveKidsMap2() {
        return haveKidsMap2;
    }

    public void setHaveKidsMap2(Map<String, String> haveKidsMap2) {
        this.haveKidsMap2 = haveKidsMap2;
    }

    public List<String> getHeightList() {
        return heightList;
    }

    public void setHeightList(List<String> heightList) {
        this.heightList = heightList;
    }

    public List<String> getHowManyKidsList() {
        return howManyKidsList;
    }

    public void setHowManyKidsList(List<String> howManyKidsList) {
        this.howManyKidsList = howManyKidsList;
    }

    public Map<String, String> getHowManyKidsMap() {
        return howManyKidsMap;
    }

    public void setHowManyKidsMap(Map<String, String> howManyKidsMap) {
        this.howManyKidsMap = howManyKidsMap;
    }

    public Map<String, String> getHowManyKidsMap2() {
        return howManyKidsMap2;
    }

    public void setHowManyKidsMap2(Map<String, String> howManyKidsMap2) {
        this.howManyKidsMap2 = howManyKidsMap2;
    }

    public List<String> getInterestList() {
        return interestList;
    }

    public void setInterestList(List<String> interestList) {
        this.interestList = interestList;
    }

    public Map<String, String> getInterestMap() {
        return interestMap;
    }

    public void setInterestMap(Map<String, String> interestMap) {
        this.interestMap = interestMap;
    }

    public Map<String, String> getInterestMap2() {
        return interestMap2;
    }

    public void setInterestMap2(Map<String, String> interestMap2) {
        this.interestMap2 = interestMap2;
    }

    public List<String> getLanguageList() {
        return languageList;
    }

    public void setLanguageList(List<String> languageList) {
        this.languageList = languageList;
    }

    public Map<String, String> getLanguageMap() {
        return languageMap;
    }

    public void setLanguageMap(Map<String, String> languageMap) {
        this.languageMap = languageMap;
    }

    public Map<String, String> getLanguageMap2() {
        return languageMap2;
    }

    public void setLanguageMap2(Map<String, String> languageMap2) {
        this.languageMap2 = languageMap2;
    }

    public List<String> getMarriageList() {
        return marriageList;
    }

    public void setMarriageList(List<String> marriageList) {
        this.marriageList = marriageList;
    }

    public Map<String, String> getMarriageMap() {
        return marriageMap;
    }

    public void setMarriageMap(Map<String, String> marriageMap) {
        this.marriageMap = marriageMap;
    }

    public Map<String, String> getMarriageMap2() {
        return marriageMap2;
    }

    public void setMarriageMap2(Map<String, String> marriageMap2) {
        this.marriageMap2 = marriageMap2;
    }

    public List<String> getNeedBabeList() {
        return needBabeList;
    }

    public void setNeedBabeList(List<String> needBabeList) {
        this.needBabeList = needBabeList;
    }

    public Map<String, String> getNeedBabeMap() {
        return needBabeMap;
    }

    public void setNeedBabeMap(Map<String, String> needBabeMap) {
        this.needBabeMap = needBabeMap;
    }

    public Map<String, String> getNeedBabeMap2() {
        return needBabeMap2;
    }

    public void setNeedBabeMap2(Map<String, String> needBabeMap2) {
        this.needBabeMap2 = needBabeMap2;
    }

    public List<String> getPetsList() {
        return petsList;
    }

    public void setPetsList(List<String> petsList) {
        this.petsList = petsList;
    }

    public Map<String, String> getPetsMap() {
        return petsMap;
    }

    public void setPetsMap(Map<String, String> petsMap) {
        this.petsMap = petsMap;
    }

    public Map<String, String> getPetsMap2() {
        return petsMap2;
    }

    public void setPetsMap2(Map<String, String> petsMap2) {
        this.petsMap2 = petsMap2;
    }

    public List<String> getPoliticalViewsList() {
        return politicalViewsList;
    }

    public void setPoliticalViewsList(List<String> politicalViewsList) {
        this.politicalViewsList = politicalViewsList;
    }

    public Map<String, String> getPoliticalViewsMap() {
        return politicalViewsMap;
    }

    public void setPoliticalViewsMap(Map<String, String> politicalViewsMap) {
        this.politicalViewsMap = politicalViewsMap;
    }

    public Map<String, String> getPoliticalViewsMap2() {
        return politicalViewsMap2;
    }

    public void setPoliticalViewsMap2(Map<String, String> politicalViewsMap2) {
        this.politicalViewsMap2 = politicalViewsMap2;
    }

    public List<String> getSmokeList() {
        return smokeList;
    }

    public void setSmokeList(List<String> smokeList) {
        this.smokeList = smokeList;
    }

    public Map<String, String> getSmokeMap() {
        return smokeMap;
    }

    public void setSmokeMap(Map<String, String> smokeMap) {
        this.smokeMap = smokeMap;
    }

    public Map<String, String> getSmokeMap2() {
        return smokeMap2;
    }

    public void setSmokeMap2(Map<String, String> smokeMap2) {
        this.smokeMap2 = smokeMap2;
    }

    public List<String> getSportList() {
        return sportList;
    }

    public void setSportList(List<String> sportList) {
        this.sportList = sportList;
    }

    public Map<String, String> getSportMap() {
        return sportMap;
    }

    public void setSportMap(Map<String, String> sportMap) {
        this.sportMap = sportMap;
    }

    public Map<String, String> getSportMap2() {
        return sportMap2;
    }

    public void setSportMap2(Map<String, String> sportMap2) {
        this.sportMap2 = sportMap2;
    }

    public List<String> getWorkList() {
        return workList;
    }

    public void setWorkList(List<String> workList) {
        this.workList = workList;
    }

    public Map<String, String> getWorkMap() {
        return workMap;
    }

    public void setWorkMap(Map<String, String> workMap) {
        this.workMap = workMap;
    }

    public Map<String, String> getWorkMap2() {
        return workMap2;
    }

    public void setWorkMap2(Map<String, String> workMap2) {
        this.workMap2 = workMap2;
    }

    public List<String> getWorkMoneyList() {
        return workMoneyList;
    }

    public void setWorkMoneyList(List<String> workMoneyList) {
        this.workMoneyList = workMoneyList;
    }

    public Map<String, String> getWorkMoneyMap() {
        return workMoneyMap;
    }

    public void setWorkMoneyMap(Map<String, String> workMoneyMap) {
        this.workMoneyMap = workMoneyMap;
    }

    public Map<String, String> getWorkMoneyMap2() {
        return workMoneyMap2;
    }

    public void setWorkMoneyMap2(Map<String, String> workMoneyMap2) {
        this.workMoneyMap2 = workMoneyMap2;
    }

    public Map<String, String> getTravelMap() {
        return travelMap;
    }

    public void setTravelMap(Map<String, String> travelMap) {
        this.travelMap = travelMap;
    }

    public Map<String, String> getTravelMap2() {
        return travelMap2;
    }

    public void setTravelMap2(Map<String, String> travelMap2) {
        this.travelMap2 = travelMap2;
    }

    public Map<String, String> getCharacteristicsMap() {
        return characteristicsMap;
    }

    public void setCharacteristicsMap(Map<String, String> characteristicsMap) {
        this.characteristicsMap = characteristicsMap;
    }

    public Map<String, String> getCharacteristicsMap2() {
        return characteristicsMap2;
    }

    public void setCharacteristicsMap2(Map<String, String> characteristicsMap2) {
        this.characteristicsMap2 = characteristicsMap2;
    }

    public List<String> getMaleSayHelloList() {
        return maleSayHelloList;
    }

    public void setMaleSayHelloList(List<String> maleSayHelloList) {
        this.maleSayHelloList = maleSayHelloList;
    }

    public List<String> getFemaleSayHelloList() {
        return femaleSayHelloList;
    }

    public void setFemaleSayHelloList(List<String> femaleSayHelloList) {
        this.femaleSayHelloList = femaleSayHelloList;
    }

    public List<String> getMaleQuestionTagList() {
        return maleQuestionTagList;
    }

    public void setMaleQuestionTagList(List<String> maleQuestionTagList) {
        this.maleQuestionTagList = maleQuestionTagList;
    }

    public List<String> getFemaleQuestionTagList() {
        return femaleQuestionTagList;
    }

    public void setFemaleQuestionTagList(List<String> femaleQuestionTagList) {
        this.femaleQuestionTagList = femaleQuestionTagList;
    }

    @Override
    public String toString() {
        return "ParamsInit{" +
                "bodyTypeList=" + bodyTypeList +
                ", bodyTypeMap=" + bodyTypeMap +
                ", bodyTypeMap2=" + bodyTypeMap2 +
                ", distractMap=" + distractMap +
                ", distractMap2=" + distractMap2 +
                ", booksMap=" + booksMap +
                ", booksMap2=" + booksMap2 +
                ", constellationList=" + constellationList +
                ", constellationMap=" + constellationMap +
                ", constellationMap2=" + constellationMap2 +
                ", dictIndex=" + dictIndex +
                ", drinkList=" + drinkList +
                ", drinkMap=" + drinkMap +
                ", drinkMap2=" + drinkMap2 +
                ", educationList=" + educationList +
                ", educationMap=" + educationMap +
                ", educationMap2=" + educationMap2 +
                ", ethnicityList=" + ethnicityList +
                ", ethnicityMap=" + ethnicityMap +
                ", ethnicityMap2=" + ethnicityMap2 +
                ", exerciseHabitsList=" + exerciseHabitsList +
                ", exerciseHabitsMap=" + exerciseHabitsMap +
                ", exerciseHabitsMap2=" + exerciseHabitsMap2 +
                ", eyesList=" + eyesList +
                ", eyesMap=" + eyesMap +
                ", eyesMap2=" + eyesMap2 +
                ", faithList=" + faithList +
                ", faithMap=" + faithMap +
                ", faithMap2=" + faithMap2 +
                ", hairList=" + hairList +
                ", hairMap=" + hairMap +
                ", hairMap2=" + hairMap2 +
                ", haveKidsList=" + haveKidsList +
                ", haveKidsMap=" + haveKidsMap +
                ", haveKidsMap2=" + haveKidsMap2 +
                ", heightList=" + heightList +
                ", howManyKidsList=" + howManyKidsList +
                ", howManyKidsMap=" + howManyKidsMap +
                ", howManyKidsMap2=" + howManyKidsMap2 +
                ", interestList=" + interestList +
                ", interestMap=" + interestMap +
                ", interestMap2=" + interestMap2 +
                ", languageList=" + languageList +
                ", languageMap=" + languageMap +
                ", languageMap2=" + languageMap2 +
                ", marriageList=" + marriageList +
                ", marriageMap=" + marriageMap +
                ", marriageMap2=" + marriageMap2 +
                ", needBabeList=" + needBabeList +
                ", needBabeMap=" + needBabeMap +
                ", needBabeMap2=" + needBabeMap2 +
                ", petsList=" + petsList +
                ", petsMap=" + petsMap +
                ", petsMap2=" + petsMap2 +
                ", politicalViewsList=" + politicalViewsList +
                ", politicalViewsMap=" + politicalViewsMap +
                ", politicalViewsMap2=" + politicalViewsMap2 +
                ", smokeList=" + smokeList +
                ", smokeMap=" + smokeMap +
                ", smokeMap2=" + smokeMap2 +
                ", sportList=" + sportList +
                ", sportMap=" + sportMap +
                ", sportMap2=" + sportMap2 +
                ", workList=" + workList +
                ", workMap=" + workMap +
                ", workMap2=" + workMap2 +
                ", workMoneyList=" + workMoneyList +
                ", workMoneyMap=" + workMoneyMap +
                ", workMoneyMap2=" + workMoneyMap2 +
                ", travelMap=" + travelMap +
                ", travelMap2=" + travelMap2 +
                ", characteristicsMap=" + characteristicsMap +
                ", characteristicsMap2=" + characteristicsMap2 +
                ", maleSayHelloList=" + maleSayHelloList +
                ", femaleSayHelloList=" + femaleSayHelloList +
                ", maleQuestionTagList=" + maleQuestionTagList +
                ", femaleQuestionTagList=" + femaleQuestionTagList +
                ", bloodMap2=" + bloodMap2 +
                ", charmMap2=" + charmMap2 +
                ", diffAreaLoveIdMap2=" + diffAreaLoveIdMap2 +
                ", foodsMap2=" + foodsMap2 +
                ", houseMap2=" + houseMap2 +
                ", liveWithParentMap2=" + liveWithParentMap2 +
                ", loverTypeMap2=" + loverTypeMap2 +
                ", loverTypeRemoteMap2=" + loverTypeRemoteMap2 +
                ", loverTypeMap2M=" + loverTypeMap2M +
                ", loverTypeMap2F=" + loverTypeMap2F +
                ", musicMap2=" + musicMap2 +
                ", moviesMap2=" + moviesMap2 +
                ", sexBefMarriedMap2=" + sexBefMarriedMap2 +
                ", bloodMap=" + bloodMap +
                ", charmMap=" + charmMap +
                ", diffAreaLoveIdMap=" + diffAreaLoveIdMap +
                ", foodsMap=" + foodsMap +
                ", houseMap=" + houseMap +
                ", liveWithParentMap=" + liveWithParentMap +
                ", loverTypeMap=" + loverTypeMap +
                ", loverTypeRemoteMap=" + loverTypeRemoteMap +
                ", musicMap=" + musicMap +
                ", moviesMap=" + moviesMap +
                ", sexBefMarriedMap=" + sexBefMarriedMap +
                ", defaultCity='" + defaultCity + '\'' +
                ", bodyTypeMap2M=" + bodyTypeMap2M +
                ", bodyTypeMap2F=" + bodyTypeMap2F +
                ", bodyTypeRemoteMap=" + bodyTypeRemoteMap +
                ", bodyTypeRemoteMap2=" + bodyTypeRemoteMap2 +
                '}';
    }
}
