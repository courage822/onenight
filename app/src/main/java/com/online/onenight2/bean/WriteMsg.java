package com.online.onenight2.bean;

/**
 * 写信结果
 * Created by zhangdroid on 2016/7/6.
 */
public class WriteMsg extends BaseModel {
    private String errType;// 失败信息类型,参照错误码定义,
    private String contactMsg;// 联系方式语句，只有isSucceed等于1的时候返回
    private String payType;// 支付方式(支付关闭：-1；苹果内支付：1 WEB支付中心支付：0)
    private String payWay;// 支付渠道信息(苹果内支付：apple;支付关闭:-1;WEB支付中心支付：url地址)

    public String getErrType() {
        return errType;
    }

    public void setErrType(String errType) {
        this.errType = errType;
    }

    public String getContactMsg() {
        return contactMsg;
    }

    public void setContactMsg(String contactMsg) {
        this.contactMsg = contactMsg;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPayWay() {
        return payWay;
    }

    public void setPayWay(String payWay) {
        this.payWay = payWay;
    }

    @Override
    public String toString() {
        return "WriteMsg{" +
                "errType='" + errType + '\'' +
                ", contactMsg='" + contactMsg + '\'' +
                ", payType='" + payType + '\'' +
                ", payWay='" + payWay + '\'' +
                '}';
    }
}
