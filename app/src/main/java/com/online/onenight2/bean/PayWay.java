package com.online.onenight2.bean;

/**
 * 支付方式对象
 * Created by zhangdroid on 2017/3/21.
 */
public class PayWay {
    private String payType;// 支付通道关闭：-1；苹果内支付：1； WEB支付中心支付：0;
    private String payWay;// 支付通道关闭：none；苹果内支付：apple; WEB支付中心支付：url地址

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPayWay() {
        return payWay;
    }

    public void setPayWay(String payWay) {
        this.payWay = payWay;
    }

    @Override
    public String toString() {
        return "PayWay{" +
                "payType='" + payType + '\'' +
                ", payWay='" + payWay + '\'' +
                '}';
    }

}
