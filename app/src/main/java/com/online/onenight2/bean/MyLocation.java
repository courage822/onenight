package com.online.onenight2.bean;

import java.util.List;

/**
 * Created by Administrator on 2017/7/8.
 */

public class MyLocation {

    /**
     * results : [{"address_components":[{"long_name":"京广快速路","short_name":"京广快速路","types":["route"]},{"long_name":"金水区","short_name":"金水区","types":["political","sublocality","sublocality_level_1"]},{"long_name":"郑州市","short_name":"郑州市","types":["locality","political"]},{"long_name":"河南省","short_name":"河南省","types":["administrative_area_level_1","political"]},{"long_name":"中国","short_name":"CN","types":["country","political"]},{"long_name":"450000","short_name":"450000","types":["postal_code"]}],"formatted_address":"中国河南省郑州市金水区京广快速路 邮政编码: 450000","geometry":{"bounds":{"northeast":{"lat":34.7617931,"lng":113.6503632},"southwest":{"lat":34.7586776,"lng":113.6476883}},"location":{"lat":34.7602979,"lng":113.6491473},"location_type":"GEOMETRIC_CENTER","viewport":{"northeast":{"lat":34.7617931,"lng":113.6503747302915},"southwest":{"lat":34.7586776,"lng":113.6476767697085}}},"place_id":"ChIJhSzo6MFl1zURpRMjOFxZRg4","types":["route"]},{"address_components":[{"long_name":"金水区","short_name":"金水区","types":["political","sublocality","sublocality_level_1"]},{"long_name":"郑州市","short_name":"郑州市","types":["locality","political"]},{"long_name":"河南省","short_name":"河南省","types":["administrative_area_level_1","political"]},{"long_name":"中国","short_name":"CN","types":["country","political"]}],"formatted_address":"中国河南省郑州市金水区","geometry":{"bounds":{"northeast":{"lat":34.8856508,"lng":113.8701557},"southwest":{"lat":34.7348353,"lng":113.6076858}},"location":{"lat":34.800156,"lng":113.660554},"location_type":"APPROXIMATE","viewport":{"northeast":{"lat":34.8856508,"lng":113.8701557},"southwest":{"lat":34.7348353,"lng":113.6076858}}},"place_id":"ChIJdX15Zbhg1zUR3LqryiCrJZg","types":["political","sublocality","sublocality_level_1"]},{"address_components":[{"long_name":"郑州市","short_name":"郑州市","types":["locality","political"]},{"long_name":"河南省","short_name":"河南省","types":["administrative_area_level_1","political"]},{"long_name":"中国","short_name":"CN","types":["country","political"]}],"formatted_address":"中国河南省郑州市","geometry":{"bounds":{"northeast":{"lat":34.9895057,"lng":114.2209613},"southwest":{"lat":34.262109,"lng":112.7211768}},"location":{"lat":34.746611,"lng":113.625328},"location_type":"APPROXIMATE","viewport":{"northeast":{"lat":34.9338694,"lng":113.9749146},"southwest":{"lat":34.5513541,"lng":113.3514404}}},"place_id":"ChIJnWqb8JRl1zURLO3M9BnuMnk","types":["locality","political"]},{"address_components":[{"long_name":"450053","short_name":"450053","types":["postal_code"]},{"long_name":"金水区","short_name":"金水区","types":["political","sublocality","sublocality_level_1"]},{"long_name":"郑州市","short_name":"郑州市","types":["locality","political"]},{"long_name":"河南省","short_name":"河南省","types":["administrative_area_level_1","political"]},{"long_name":"中国","short_name":"CN","types":["country","political"]}],"formatted_address":"中国河南省郑州市金水区 邮政编码: 450053","geometry":{"bounds":{"northeast":{"lat":34.8070518,"lng":113.6680748},"southwest":{"lat":34.75557300000001,"lng":113.6204645}},"location":{"lat":34.7807406,"lng":113.643804},"location_type":"APPROXIMATE","viewport":{"northeast":{"lat":34.8070518,"lng":113.6680748},"southwest":{"lat":34.75557300000001,"lng":113.6204645}}},"place_id":"ChIJgax0WrVl1zURiQj_wW6QcBY","types":["postal_code"]},{"address_components":[{"long_name":"450000","short_name":"450000","types":["postal_code"]},{"long_name":"二七区","short_name":"二七区","types":["political","sublocality","sublocality_level_1"]},{"long_name":"郑州市","short_name":"郑州市","types":["locality","political"]},{"long_name":"河南省","short_name":"河南省","types":["administrative_area_level_1","political"]},{"long_name":"中国","short_name":"CN","types":["country","political"]}],"formatted_address":"中国河南省郑州市二七区 邮政编码: 450000","geometry":{"bounds":{"northeast":{"lat":34.8376535,"lng":113.7632829},"southwest":{"lat":34.7151291,"lng":113.5728502}},"location":{"lat":34.7498744,"lng":113.6678122},"location_type":"APPROXIMATE","viewport":{"northeast":{"lat":34.8376535,"lng":113.7632829},"southwest":{"lat":34.7151291,"lng":113.5728502}}},"place_id":"ChIJiRJDBuJl1zUR2358melCvgY","types":["postal_code"]},{"address_components":[{"long_name":"河南省","short_name":"河南省","types":["administrative_area_level_1","political"]},{"long_name":"中国","short_name":"CN","types":["country","political"]}],"formatted_address":"中国河南省","geometry":{"bounds":{"northeast":{"lat":36.3665602,"lng":116.6522319},"southwest":{"lat":31.38237089999999,"lng":110.3604758}},"location":{"lat":34.2904302,"lng":113.3823545},"location_type":"APPROXIMATE","viewport":{"northeast":{"lat":36.3665602,"lng":116.6522319},"southwest":{"lat":31.38237089999999,"lng":110.3604758}}},"place_id":"ChIJYd_UVOOS1jURyxqiJ3gVsoI","types":["administrative_area_level_1","political"]},{"address_components":[{"long_name":"中国","short_name":"CN","types":["country","political"]}],"formatted_address":"中国","geometry":{"bounds":{"northeast":{"lat":53.56097399999999,"lng":134.7728099},"southwest":{"lat":17.9996,"lng":73.4994136}},"location":{"lat":35.86166,"lng":104.195397},"location_type":"APPROXIMATE","viewport":{"northeast":{"lat":53.56097399999999,"lng":134.7726951},"southwest":{"lat":18.1618062,"lng":73.5034261}}},"place_id":"ChIJwULG5WSOUDERbzafNHyqHZU","types":["country","political"]}]
     * status : OK
     */

    private String status;
    private List<ResultsBean> results;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ResultsBean> getResults() {
        return results;
    }

    public void setResults(List<ResultsBean> results) {
        this.results = results;
    }

    public static class ResultsBean {
        /**
         * address_components : [{"long_name":"京广快速路","short_name":"京广快速路","types":["route"]},{"long_name":"金水区","short_name":"金水区","types":["political","sublocality","sublocality_level_1"]},{"long_name":"郑州市","short_name":"郑州市","types":["locality","political"]},{"long_name":"河南省","short_name":"河南省","types":["administrative_area_level_1","political"]},{"long_name":"中国","short_name":"CN","types":["country","political"]},{"long_name":"450000","short_name":"450000","types":["postal_code"]}]
         * formatted_address : 中国河南省郑州市金水区京广快速路 邮政编码: 450000
         * geometry : {"bounds":{"northeast":{"lat":34.7617931,"lng":113.6503632},"southwest":{"lat":34.7586776,"lng":113.6476883}},"location":{"lat":34.7602979,"lng":113.6491473},"location_type":"GEOMETRIC_CENTER","viewport":{"northeast":{"lat":34.7617931,"lng":113.6503747302915},"southwest":{"lat":34.7586776,"lng":113.6476767697085}}}
         * place_id : ChIJhSzo6MFl1zURpRMjOFxZRg4
         * types : ["route"]
         */

        private String formatted_address;
        private GeometryBean geometry;
        private String place_id;
        private List<AddressComponentsBean> address_components;
        private List<String> types;

        public String getFormatted_address() {
            return formatted_address;
        }

        public void setFormatted_address(String formatted_address) {
            this.formatted_address = formatted_address;
        }

        public GeometryBean getGeometry() {
            return geometry;
        }

        public void setGeometry(GeometryBean geometry) {
            this.geometry = geometry;
        }

        public String getPlace_id() {
            return place_id;
        }

        public void setPlace_id(String place_id) {
            this.place_id = place_id;
        }

        public List<AddressComponentsBean> getAddress_components() {
            return address_components;
        }

        public void setAddress_components(List<AddressComponentsBean> address_components) {
            this.address_components = address_components;
        }

        public List<String> getTypes() {
            return types;
        }

        public void setTypes(List<String> types) {
            this.types = types;
        }

        public static class GeometryBean {
            /**
             * bounds : {"northeast":{"lat":34.7617931,"lng":113.6503632},"southwest":{"lat":34.7586776,"lng":113.6476883}}
             * location : {"lat":34.7602979,"lng":113.6491473}
             * location_type : GEOMETRIC_CENTER
             * viewport : {"northeast":{"lat":34.7617931,"lng":113.6503747302915},"southwest":{"lat":34.7586776,"lng":113.6476767697085}}
             */

            private BoundsBean bounds;
            private LocationBean location;
            private String location_type;
            private ViewportBean viewport;

            public BoundsBean getBounds() {
                return bounds;
            }

            public void setBounds(BoundsBean bounds) {
                this.bounds = bounds;
            }

            public LocationBean getLocation() {
                return location;
            }

            public void setLocation(LocationBean location) {
                this.location = location;
            }

            public String getLocation_type() {
                return location_type;
            }

            public void setLocation_type(String location_type) {
                this.location_type = location_type;
            }

            public ViewportBean getViewport() {
                return viewport;
            }

            public void setViewport(ViewportBean viewport) {
                this.viewport = viewport;
            }

            public static class BoundsBean {
                /**
                 * northeast : {"lat":34.7617931,"lng":113.6503632}
                 * southwest : {"lat":34.7586776,"lng":113.6476883}
                 */

                private NortheastBean northeast;
                private SouthwestBean southwest;

                public NortheastBean getNortheast() {
                    return northeast;
                }

                public void setNortheast(NortheastBean northeast) {
                    this.northeast = northeast;
                }

                public SouthwestBean getSouthwest() {
                    return southwest;
                }

                public void setSouthwest(SouthwestBean southwest) {
                    this.southwest = southwest;
                }

                public static class NortheastBean {
                    /**
                     * lat : 34.7617931
                     * lng : 113.6503632
                     */

                    private double lat;
                    private double lng;

                    public double getLat() {
                        return lat;
                    }

                    public void setLat(double lat) {
                        this.lat = lat;
                    }

                    public double getLng() {
                        return lng;
                    }

                    public void setLng(double lng) {
                        this.lng = lng;
                    }
                }

                public static class SouthwestBean {
                    /**
                     * lat : 34.7586776
                     * lng : 113.6476883
                     */

                    private double lat;
                    private double lng;

                    public double getLat() {
                        return lat;
                    }

                    public void setLat(double lat) {
                        this.lat = lat;
                    }

                    public double getLng() {
                        return lng;
                    }

                    public void setLng(double lng) {
                        this.lng = lng;
                    }
                }
            }

            public static class LocationBean {
                /**
                 * lat : 34.7602979
                 * lng : 113.6491473
                 */

                private double lat;
                private double lng;

                public double getLat() {
                    return lat;
                }

                public void setLat(double lat) {
                    this.lat = lat;
                }

                public double getLng() {
                    return lng;
                }

                public void setLng(double lng) {
                    this.lng = lng;
                }
            }

            public static class ViewportBean {
                /**
                 * northeast : {"lat":34.7617931,"lng":113.6503747302915}
                 * southwest : {"lat":34.7586776,"lng":113.6476767697085}
                 */

                private NortheastBeanX northeast;
                private SouthwestBeanX southwest;

                public NortheastBeanX getNortheast() {
                    return northeast;
                }

                public void setNortheast(NortheastBeanX northeast) {
                    this.northeast = northeast;
                }

                public SouthwestBeanX getSouthwest() {
                    return southwest;
                }

                public void setSouthwest(SouthwestBeanX southwest) {
                    this.southwest = southwest;
                }

                public static class NortheastBeanX {
                    /**
                     * lat : 34.7617931
                     * lng : 113.6503747302915
                     */

                    private double lat;
                    private double lng;

                    public double getLat() {
                        return lat;
                    }

                    public void setLat(double lat) {
                        this.lat = lat;
                    }

                    public double getLng() {
                        return lng;
                    }

                    public void setLng(double lng) {
                        this.lng = lng;
                    }
                }

                public static class SouthwestBeanX {
                    /**
                     * lat : 34.7586776
                     * lng : 113.6476767697085
                     */

                    private double lat;
                    private double lng;

                    public double getLat() {
                        return lat;
                    }

                    public void setLat(double lat) {
                        this.lat = lat;
                    }

                    public double getLng() {
                        return lng;
                    }

                    public void setLng(double lng) {
                        this.lng = lng;
                    }
                }
            }
        }

        public static class AddressComponentsBean {
            /**
             * long_name : 京广快速路
             * short_name : 京广快速路
             * types : ["route"]
             */

            private String long_name;
            private String short_name;
            private List<String> types;

            public String getLong_name() {
                return long_name;
            }

            public void setLong_name(String long_name) {
                this.long_name = long_name;
            }

            public String getShort_name() {
                return short_name;
            }

            public void setShort_name(String short_name) {
                this.short_name = short_name;
            }

            public List<String> getTypes() {
                return types;
            }

            public void setTypes(List<String> types) {
                this.types = types;
            }
        }
    }
}
