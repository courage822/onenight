package com.online.onenight2.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 描述：获取下一位用户
 * Created by qyh on 2017/4/13.
 */

public class NextUsers implements Serializable {
   private List<User> listUser;
    private User user;

    public List<User> getListUser() {
        return listUser;
    }
    public User getUser(){
        return user;
    }

    public void setListUser(List<User> listUser) {
        this.listUser = listUser;
    }
    public void setUser(User user) {
        this.user = user;
    }
}
