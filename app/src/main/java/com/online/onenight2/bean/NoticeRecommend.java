package com.online.onenight2.bean;

/**
 * Created by Administrator on 2017/6/23.
 */

public class NoticeRecommend {
    private int cycle;
    private RemoteYuanfenUserBaseBean remoteYuanfenUserBase;
    private String isSucceed;
    private int displaySecond;
    private int noticeType;
    private int unreadMsg;

    public int getCycle() {
        return cycle;
    }

    public void setCycle(int cycle) {
        this.cycle = cycle;
    }

    public RemoteYuanfenUserBaseBean getRemoteYuanfenUserBase() {
        return remoteYuanfenUserBase;
    }

    public void setRemoteYuanfenUserBase(RemoteYuanfenUserBaseBean remoteYuanfenUserBase) {
        this.remoteYuanfenUserBase = remoteYuanfenUserBase;
    }

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public int getDisplaySecond() {
        return displaySecond;
    }

    public void setDisplaySecond(int displaySecond) {
        this.displaySecond = displaySecond;
    }

    public int getNoticeType() {
        return noticeType;
    }

    public void setNoticeType(int noticeType) {
        this.noticeType = noticeType;
    }

    public int getUnreadMsg() {
        return unreadMsg;
    }

    public void setUnreadMsg(int unreadMsg) {
        this.unreadMsg = unreadMsg;
    }

    public static class RemoteYuanfenUserBaseBean {

        private int isRecUser;
        private int isSayHello;
        private UserBaseEnglishBean userBaseEnglish;
        private Object language;
        private Object country;
        private int resultType;

        @Override
        public String toString() {
            return "RemoteYuanfenUserBaseBean{" +
                    "isRecUser=" + isRecUser +
                    ", isSayHello=" + isSayHello +
                    ", userBaseEnglish=" + userBaseEnglish +
                    ", language=" + language +
                    ", country=" + country +
                    ", resultType=" + resultType +
                    '}';
        }

        public int getIsRecUser() {
            return isRecUser;
        }

        public void setIsRecUser(int isRecUser) {
            this.isRecUser = isRecUser;
        }

        public int getIsSayHello() {
            return isSayHello;
        }

        public void setIsSayHello(int isSayHello) {
            this.isSayHello = isSayHello;
        }

        public UserBaseEnglishBean getUserBaseEnglish() {
            return userBaseEnglish;
        }

        public void setUserBaseEnglish(UserBaseEnglishBean userBaseEnglish) {
            this.userBaseEnglish = userBaseEnglish;
        }

        public Object getLanguage() {
            return language;
        }

        public void setLanguage(Object language) {
            this.language = language;
        }

        public Object getCountry() {
            return country;
        }

        public void setCountry(Object country) {
            this.country = country;
        }

        public int getResultType() {
            return resultType;
        }

        public void setResultType(int resultType) {
            this.resultType = resultType;
        }

        public static class UserBaseEnglishBean {

            private int id;
            private String nickName;
            private ImageBean image;
            private int age;
            private int sex;
            private String height;
            private int education;
            private int income;
            private Object regTime;
            private int maritalStatus;
            private String monologue;
            private int isBeanUser;
            private int isMonthly;
            private int isVip;
            private int level;
            private int isAuthentication;
            private String country;
            private Object stat;
            private Object district;
            private AreaBean area;
            private String language;
            private int bodyType;
            private int heightCm;
            private Object spokenLanguage;
            private int occupation;
            private int sign;

            @Override
            public String toString() {
                return "UserBaseEnglishBean{" +
                        "id=" + id +
                        ", nickName='" + nickName + '\'' +
                        ", image=" + image +
                        ", age=" + age +
                        ", sex=" + sex +
                        ", height='" + height + '\'' +
                        ", education=" + education +
                        ", income=" + income +
                        ", regTime=" + regTime +
                        ", maritalStatus=" + maritalStatus +
                        ", monologue='" + monologue + '\'' +
                        ", isBeanUser=" + isBeanUser +
                        ", isMonthly=" + isMonthly +
                        ", isVip=" + isVip +
                        ", level=" + level +
                        ", isAuthentication=" + isAuthentication +
                        ", country='" + country + '\'' +
                        ", stat=" + stat +
                        ", district=" + district +
                        ", area=" + area +
                        ", language='" + language + '\'' +
                        ", bodyType=" + bodyType +
                        ", heightCm=" + heightCm +
                        ", spokenLanguage=" + spokenLanguage +
                        ", occupation=" + occupation +
                        ", sign=" + sign +
                        '}';
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getNickName() {
                return nickName;
            }

            public void setNickName(String nickName) {
                this.nickName = nickName;
            }

            public ImageBean getImage() {
                return image;
            }

            public void setImage(ImageBean image) {
                this.image = image;
            }

            public int getAge() {
                return age;
            }

            public void setAge(int age) {
                this.age = age;
            }

            public int getSex() {
                return sex;
            }

            public void setSex(int sex) {
                this.sex = sex;
            }

            public String getHeight() {
                return height;
            }

            public void setHeight(String height) {
                this.height = height;
            }

            public int getEducation() {
                return education;
            }

            public void setEducation(int education) {
                this.education = education;
            }

            public int getIncome() {
                return income;
            }

            public void setIncome(int income) {
                this.income = income;
            }

            public Object getRegTime() {
                return regTime;
            }

            public void setRegTime(Object regTime) {
                this.regTime = regTime;
            }

            public int getMaritalStatus() {
                return maritalStatus;
            }

            public void setMaritalStatus(int maritalStatus) {
                this.maritalStatus = maritalStatus;
            }

            public String getMonologue() {
                return monologue;
            }

            public void setMonologue(String monologue) {
                this.monologue = monologue;
            }

            public int getIsBeanUser() {
                return isBeanUser;
            }

            public void setIsBeanUser(int isBeanUser) {
                this.isBeanUser = isBeanUser;
            }

            public int getIsMonthly() {
                return isMonthly;
            }

            public void setIsMonthly(int isMonthly) {
                this.isMonthly = isMonthly;
            }

            public int getIsVip() {
                return isVip;
            }

            public void setIsVip(int isVip) {
                this.isVip = isVip;
            }

            public int getLevel() {
                return level;
            }

            public void setLevel(int level) {
                this.level = level;
            }

            public int getIsAuthentication() {
                return isAuthentication;
            }

            public void setIsAuthentication(int isAuthentication) {
                this.isAuthentication = isAuthentication;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public Object getStat() {
                return stat;
            }

            public void setStat(Object stat) {
                this.stat = stat;
            }

            public Object getDistrict() {
                return district;
            }

            public void setDistrict(Object district) {
                this.district = district;
            }

            public AreaBean getArea() {
                return area;
            }

            public void setArea(AreaBean area) {
                this.area = area;
            }

            public String getLanguage() {
                return language;
            }

            public void setLanguage(String language) {
                this.language = language;
            }

            public int getBodyType() {
                return bodyType;
            }

            public void setBodyType(int bodyType) {
                this.bodyType = bodyType;
            }

            public int getHeightCm() {
                return heightCm;
            }

            public void setHeightCm(int heightCm) {
                this.heightCm = heightCm;
            }

            public Object getSpokenLanguage() {
                return spokenLanguage;
            }

            public void setSpokenLanguage(Object spokenLanguage) {
                this.spokenLanguage = spokenLanguage;
            }

            public int getOccupation() {
                return occupation;
            }

            public void setOccupation(int occupation) {
                this.occupation = occupation;
            }

            public int getSign() {
                return sign;
            }

            public void setSign(int sign) {
                this.sign = sign;
            }

            public static class ImageBean {
                /**
                 * id : 25943
                 * state : 0
                 * isMain : 0
                 * thumbnailUrl : http://imagetw.ilove.ren/imagedata/201510/05/07/25/146394_1_c.jpg
                 * thumbnailUrlM : http://imagetw.ilove.ren/imagedata/201510/05/07/25/146394_1_m.jpg?x-oss-process\=image/resize,w_640,h_640/quality,Q_50
                 * imageUrl : http://imagetw.ilove.ren/imagedata/201510/05/07/25/146394_1.jpg
                 * language : null
                 * country : null
                 * localPath : null
                 * status : 2
                 * pornStatus : 0
                 * label : 0
                 * rate : 0.0
                 */

                private int id;
                private int state;
                private int isMain;
                private String thumbnailUrl;
                private String thumbnailUrlM;
                private String imageUrl;
                private Object language;
                private Object country;
                private Object localPath;
                private int status;
                private int pornStatus;
                private int label;
                private double rate;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getState() {
                    return state;
                }

                public void setState(int state) {
                    this.state = state;
                }

                public int getIsMain() {
                    return isMain;
                }

                public void setIsMain(int isMain) {
                    this.isMain = isMain;
                }

                public String getThumbnailUrl() {
                    return thumbnailUrl;
                }

                public void setThumbnailUrl(String thumbnailUrl) {
                    this.thumbnailUrl = thumbnailUrl;
                }

                public String getThumbnailUrlM() {
                    return thumbnailUrlM;
                }

                public void setThumbnailUrlM(String thumbnailUrlM) {
                    this.thumbnailUrlM = thumbnailUrlM;
                }

                public String getImageUrl() {
                    return imageUrl;
                }

                public void setImageUrl(String imageUrl) {
                    this.imageUrl = imageUrl;
                }

                public Object getLanguage() {
                    return language;
                }

                public void setLanguage(Object language) {
                    this.language = language;
                }

                public Object getCountry() {
                    return country;
                }

                public void setCountry(Object country) {
                    this.country = country;
                }

                public Object getLocalPath() {
                    return localPath;
                }

                public void setLocalPath(Object localPath) {
                    this.localPath = localPath;
                }

                public int getStatus() {
                    return status;
                }

                public void setStatus(int status) {
                    this.status = status;
                }

                public int getPornStatus() {
                    return pornStatus;
                }

                public void setPornStatus(int pornStatus) {
                    this.pornStatus = pornStatus;
                }

                public int getLabel() {
                    return label;
                }

                public void setLabel(int label) {
                    this.label = label;
                }

                public double getRate() {
                    return rate;
                }

                public void setRate(double rate) {
                    this.rate = rate;
                }
            }

            public static class AreaBean {
                /**
                 * country : Taiwan
                 * provinceId : 130
                 * provinceName : 桃園市
                 * cityId : 0
                 * cityName : null
                 * areaId : 0
                 * areaName : null
                 * areaIndex : 0
                 * cityIndex : 0
                 */

                private String country;
                private int provinceId;
                private String provinceName;
                private int cityId;
                private Object cityName;
                private int areaId;
                private Object areaName;
                private int areaIndex;
                private int cityIndex;

                public String getCountry() {
                    return country;
                }

                public void setCountry(String country) {
                    this.country = country;
                }

                public int getProvinceId() {
                    return provinceId;
                }

                public void setProvinceId(int provinceId) {
                    this.provinceId = provinceId;
                }

                public String getProvinceName() {
                    return provinceName;
                }

                public void setProvinceName(String provinceName) {
                    this.provinceName = provinceName;
                }

                public int getCityId() {
                    return cityId;
                }

                public void setCityId(int cityId) {
                    this.cityId = cityId;
                }

                public Object getCityName() {
                    return cityName;
                }

                public void setCityName(Object cityName) {
                    this.cityName = cityName;
                }

                public int getAreaId() {
                    return areaId;
                }

                public void setAreaId(int areaId) {
                    this.areaId = areaId;
                }

                public Object getAreaName() {
                    return areaName;
                }

                public void setAreaName(Object areaName) {
                    this.areaName = areaName;
                }

                public int getAreaIndex() {
                    return areaIndex;
                }

                public void setAreaIndex(int areaIndex) {
                    this.areaIndex = areaIndex;
                }

                public int getCityIndex() {
                    return cityIndex;
                }

                public void setCityIndex(int cityIndex) {
                    this.cityIndex = cityIndex;
                }
            }
        }
    }

    @Override
    public String toString() {
        return "NoticeRecommend{" +
                "cycle=" + cycle +
                ", remoteYuanfenUserBase=" + remoteYuanfenUserBase +
                ", isSucceed='" + isSucceed + '\'' +
                ", displaySecond=" + displaySecond +
                ", noticeType=" + noticeType +
                ", unreadMsg=" + unreadMsg +
                '}';
    }
}
