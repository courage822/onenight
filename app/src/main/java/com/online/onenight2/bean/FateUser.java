package com.online.onenight2.bean;

/**
 * 缘分用户对象
 * Created by zhangdroid on 2016/8/10.
 */
public class FateUser {
    String isRecUser;// 是否为推荐用户，1->为推荐用户,客户端需要大图显示（注意每9条数据中只能有一个用户为true）,
    String isSayHello;// 是否打过招呼，1->已打招呼,0->未打招呼
    UserBase userBaseEnglish;// 用户对象基础信息

    public String getIsRecUser() {
        return isRecUser;
    }

    public void setIsRecUser(String isRecUser) {
        this.isRecUser = isRecUser;
    }

    public String getIsSayHello() {
        return isSayHello;
    }

    public void setIsSayHello(String isSayHello) {
        this.isSayHello = isSayHello;
    }

    public UserBase getUserBaseEnglish() {
        return userBaseEnglish;
    }

    public void setUserBaseEnglish(UserBase userBaseEnglish) {
        this.userBaseEnglish = userBaseEnglish;
    }

    @Override
    public String toString() {
        return "FateUser{" +
                "isRecUser='" + isRecUser + '\'' +
                ", isSayHello='" + isSayHello + '\'' +
                ", userBaseEnglish=" + userBaseEnglish +
                '}';
    }

}
