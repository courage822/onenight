package com.online.onenight2.bean;

/**
 * 用户详情
 * Created by zhangdroid on 2016/7/1.
 */
public class UserDetail extends BaseModel {
    private User userEnglish;
    private String showWriteMsgIntercept;// 1:支付拦截 2:不拦截

    public User getUserEnglish() {
        return userEnglish;
    }

    public void setUserEnglish(User userEnglish) {
        this.userEnglish = userEnglish;
    }

    public String getShowWriteMsgIntercept() {
        return showWriteMsgIntercept;
    }

    public void setShowWriteMsgIntercept(String showWriteMsgIntercept) {
        this.showWriteMsgIntercept = showWriteMsgIntercept;
    }

    @Override
    public String toString() {
        return "UserDetail{" +
                "userEnglish=" + userEnglish +
                ", showWriteMsgIntercept='" + showWriteMsgIntercept +
                '}';
    }

}
