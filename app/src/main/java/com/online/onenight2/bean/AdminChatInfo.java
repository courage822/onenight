package com.online.onenight2.bean;

/**
 * 管理员信状态消息
 * Created by zhangdroid on 2016/9/7.
 */
public class AdminChatInfo {
    private int id;
    private ChatMsg chat;
    private UserBase userBaseEnglish;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ChatMsg getChat() {
        return chat;
    }

    public void setChat(ChatMsg chat) {
        this.chat = chat;
    }

    public UserBase getUserBaseEnglish() {
        return userBaseEnglish;
    }

    public void setUserBaseEnglish(UserBase userBaseEnglish) {
        this.userBaseEnglish = userBaseEnglish;
    }

    @Override
    public String toString() {
        return "AdminChatInfo{" +
                "id=" + id +
                ", chat=" + chat +
                ", userBaseEnglish=" + userBaseEnglish +
                '}';
    }

}
