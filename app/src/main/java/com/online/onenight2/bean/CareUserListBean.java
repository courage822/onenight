package com.online.onenight2.bean;

import java.util.List;

/**
 * Created by Administrator on 2016/11/2.
 */

public class CareUserListBean extends BaseModel {
    public String pageNum;
    public List<User> userEnglishList;

    public String getPageNum() {
        return pageNum;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    public List<User> getUserEnglishList() {
        return userEnglishList;
    }

    public void setUserEnglishList(List<User> userEnglishList) {
        this.userEnglishList = userEnglishList;
    }

    @Override
    public String toString() {
        return "CareUserListBean{" +
                "pageNum='" + pageNum + '\'' +
                ", userEnglishList=" + userEnglishList +
                '}';
    }
}
