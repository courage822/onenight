package com.online.onenight2.bean;

/**
 * 区域对象
 * Created by zhangdroid on 2016/6/23.
 */
public class AreaBean {
    private Area area;

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    @Override
    public String toString() {
        return "AreaBean{" +
                "area=" + area +
                '}';
    }

}
