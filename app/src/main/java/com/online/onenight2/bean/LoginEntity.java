package com.online.onenight2.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jzrh on 2016/6/25.
 */
public class LoginEntity implements Serializable {
    public String checkInTemplate;
    public String introduceTemplate;
    public String isInterceptSteps2;
    public String isShowOneYuanDialog;
    public String isShowPraiseDialog;
    public String isShowService; //1
    public String isShowUploadImg; //判断用户是否上传了头像
    public String isShowUploadAudioInformation; //判断女用户是否上传了语音
    public String isSucceed; //2
    public String msg;
    public String sessionId;
    public String token;
    public User userEnglish;
    public List<String> leaveTemplates;
    public List<String> recallTags;


//    public String regTime;

    public String getCheckInTemplate() {
        return checkInTemplate;
    }

    public void setCheckInTemplate(String checkInTemplate) {
        this.checkInTemplate = checkInTemplate;
    }

    public String getIntroduceTemplate() {
        return introduceTemplate;
    }

    public void setIntroduceTemplate(String introduceTemplate) {
        this.introduceTemplate = introduceTemplate;
    }

    public String getIsInterceptSteps2() {
        return isInterceptSteps2;
    }

    public void setIsInterceptSteps2(String isInterceptSteps2) {
        this.isInterceptSteps2 = isInterceptSteps2;
    }

    public String getIsShowOneYuanDialog() {
        return isShowOneYuanDialog;
    }

    public void setIsShowOneYuanDialog(String isShowOneYuanDialog) {
        this.isShowOneYuanDialog = isShowOneYuanDialog;
    }

    public String getIsShowPraiseDialog() {
        return isShowPraiseDialog;
    }

    public void setIsShowPraiseDialog(String isShowPraiseDialog) {
        this.isShowPraiseDialog = isShowPraiseDialog;
    }

    public String getIsShowService() {
        return isShowService;
    }

    public void setIsShowService(String isShowService) {
        this.isShowService = isShowService;
    }

    public String getIsShowUploadImg() {
        return isShowUploadImg;
    }

    public void setIsShowUploadImg(String isShowUploadImg) {
        this.isShowUploadImg = isShowUploadImg;
    }
    public String getIsShowUploadAudioInformation() {
        return isShowUploadAudioInformation;
    }

    public void setIsShowUploadAudioInformation(String isShowUploadAudioInformation) {
        this.isShowUploadAudioInformation = isShowUploadAudioInformation;
    }


    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUserEnglish() {
        return userEnglish;
    }

    public void setUserEnglish(User userEnglish) {
        this.userEnglish = userEnglish;
    }

    public List<String> getLeaveTemplates() {
        return leaveTemplates;
    }

    public void setLeaveTemplates(List<String> leaveTemplates) {
        this.leaveTemplates = leaveTemplates;
    }

    public List<String> getRecallTags() {
        return recallTags;
    }

    public void setRecallTags(List<String> recallTags) {
        this.recallTags = recallTags;
    }

    @Override
    public String toString() {
        return "LoginEntity{" +
                "checkInTemplate='" + checkInTemplate + '\'' +
                ", introduceTemplate='" + introduceTemplate + '\'' +
                ", isInterceptSteps2='" + isInterceptSteps2 + '\'' +
                ", isShowOneYuanDialog='" + isShowOneYuanDialog + '\'' +
                ", isShowPraiseDialog='" + isShowPraiseDialog + '\'' +
                ", isShowService='" + isShowService + '\'' +
                ", isShowUploadImg='" + isShowUploadImg + '\'' +
                ", isShowUploadAudioInformation='" + isShowUploadAudioInformation + '\'' +
                ", isSucceed='" + isSucceed + '\'' +
                ", msg='" + msg + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", token='" + token + '\'' +
                ", user=" + userEnglish +
                ", leaveTemplates=" + leaveTemplates +
                ", recallTags=" + recallTags +
                '}';
    }
}
