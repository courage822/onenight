package com.online.onenight2.bean;

/**
 * http请求后返回的相同字段
 * Created by zhangdroid on 2016/6/23.
 */
public class BaseModel {
    /**
     * 1代表请求成功
     */
    public String isSucceed;
    /**
     * 提示信息
     */
    public String msg;

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "BaseModel{" +
                "isSucceed='" + isSucceed + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }

}
