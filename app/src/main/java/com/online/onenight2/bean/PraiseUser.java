package com.online.onenight2.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 描述：获取点赞列表
 * Created by qyh on 2017/4/27.
 */

public class PraiseUser implements Serializable {


    private int pageSize;
    private int pageNum;
    private String isSucceed;
    private List<PraisedList> praisedList;

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public List<PraisedList> getPraisedList() {
        return praisedList;
    }

    public void setPraisedList(List<PraisedList> praisedList) {
        this.praisedList = praisedList;
    }

    public class PraisedList implements Serializable {

        private String country;
        private String praiseTime;
        private String language;
        private UserBaseEnglish userBaseEnglish;

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getPraiseTime() {
            return praiseTime;
        }

        public void setPraiseTime(String praiseTime) {
            this.praiseTime = praiseTime;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public UserBaseEnglish getUserBaseEnglish() {
            return userBaseEnglish;
        }

        public void setUserBaseEnglish(UserBaseEnglish userBaseEnglish) {
            this.userBaseEnglish = userBaseEnglish;
        }
    }

    @Override
    public String toString() {
        return "PraiseUser{" +
                "pageSize=" + pageSize +
                ", pageNum=" + pageNum +
                ", isSucceed='" + isSucceed + '\'' +
                ", praisedList=" + praisedList +
                '}';
    }
}
