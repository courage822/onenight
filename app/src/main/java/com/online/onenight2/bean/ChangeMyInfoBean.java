package com.online.onenight2.bean;

/**
 * Created by Administrator on 2016/10/29.
 */

public class ChangeMyInfoBean {
    public String heart;
    public String personal;
    public String sport;
    public String wangToGo;
    public String book;
    public String nickName;
    public String birthday;
    public String area;
    public String income;
    public String height;
    public String work;
    public String education;
    public String marriage;
    public String wantBaby;
    /**
     * 添加
     * @return
     */
    public String foods;
    public String music;
    public String movies;
    public String interest;
    public String blood;
    public String charm;
    public String diffAreaLove;
    public String house;
    public String liveWithParent;
    public String loveType;
    public String sexBefMarried;
    /*profile 数据*/
    public String spokenLanguage;
    public String sign;
    public String provinceName;
    public String bodyType;
    public String drink;//喝酒
    public String exerciseHabits;//锻炼习惯
    public String hairColor;//头发颜色
    public String howManyKids;//有多少小孩
    public String eyeColor;//眼睛颜色
    public String wantsKids;//是否要小孩,
     public String pets;//宠物
    public String sports;//运动
    public String politicalViews;
    public String smoke;
    public String ethnicity;
    public String haveKids;
    public String childStatus;
//    征友条件
    public String zyarea;//地区
    public int minAge;// 征友最小年龄
    public int maxAge;// 征友最大年龄
    public String minHeight;// 征友最低身高（英寸）
    public String maxHeight;// 征友最高身高（英寸）
    public String minEducationEnglish;
    public String incomeEnglish;

    public String getZyarea() {
        return zyarea;
    }

    public void setZyarea(String zyarea) {
        this.zyarea = zyarea;
    }

    public int getMinAge() {
        return minAge;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    public String getMinHeight() {
        return minHeight;
    }

    public void setMinHeight(String minHeight) {
        this.minHeight = minHeight;
    }

    public String getMaxHeight() {
        return maxHeight;
    }

    public void setMaxHeight(String maxHeight) {
        this.maxHeight = maxHeight;
    }

    public String getMinEducationEnglish() {
        return minEducationEnglish;
    }

    public void setMinEducationEnglish(String minEducationEnglish) {
        this.minEducationEnglish = minEducationEnglish;
    }

    public String getIncomeEnglish() {
        return incomeEnglish;
    }

    public void setIncomeEnglish(String incomeEnglish) {
        this.incomeEnglish = incomeEnglish;
    }

    public String getChildStatus() {
        return childStatus;
    }

    public void setChildStatus(String childStatus) {
        this.childStatus = childStatus;
    }

    public String getHowManyKids() {
        return howManyKids;
    }

    public void setHowManyKids(String howManyKids) {
        this.howManyKids = howManyKids;
    }

    public String getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(String eyeColor) {
        this.eyeColor = eyeColor;
    }

    public String getWantsKids() {
        return wantsKids;
    }

    public void setWantsKids(String wantsKids) {
        this.wantsKids = wantsKids;
    }

    public String getPets() {
        return pets;
    }

    public void setPets(String pets) {
        this.pets = pets;
    }

    public String getSports() {
        return sports;
    }

    public void setSports(String sports) {
        this.sports = sports;
    }

    public String getPoliticalViews() {
        return politicalViews;
    }

    public void setPoliticalViews(String politicalViews) {
        this.politicalViews = politicalViews;
    }

    public String getSmoke() {
        return smoke;
    }

    public void setSmoke(String smoke) {
        this.smoke = smoke;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public String getHaveKids() {
        return haveKids;
    }

    public void setHaveKids(String haveKids) {
        this.haveKids = haveKids;
    }

    public String getHairColor() {
        return hairColor;
    }

    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    public String getExerciseHabits() {
        return exerciseHabits;
    }

    public void setExerciseHabits(String exerciseHabits) {
        this.exerciseHabits = exerciseHabits;
    }

    public String getDrink() {
        return drink;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getSpokenLanguage() {
        return spokenLanguage;
    }

    public void setSpokenLanguage(String spokenLanguage) {
        this.spokenLanguage = spokenLanguage;
    }

    public String getFoods() {
        return foods;
    }

    public void setFoods(String foods) {
        this.foods = foods;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public String getMovies() {
        return movies;
    }

    public void setMovies(String movies) {
        this.movies = movies;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getBlood() {
        return blood;
    }

    public void setBlood(String blood) {
        this.blood = blood;
    }

    public String getCharm() {
        return charm;
    }

    public void setCharm(String charm) {
        this.charm = charm;
    }

    public String getDiffAreaLove() {
        return diffAreaLove;
    }

    public void setDiffAreaLove(String diffAreaLove) {
        this.diffAreaLove = diffAreaLove;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getLiveWithParent() {
        return liveWithParent;
    }

    public void setLiveWithParent(String liveWithParent) {
        this.liveWithParent = liveWithParent;
    }

    public String getLoveType() {
        return loveType;
    }

    public void setLoveType(String loveType) {
        this.loveType = loveType;
    }

    public String getSexBefMarried() {
        return sexBefMarried;
    }

    public void setSexBefMarried(String sexBefMarried) {
        this.sexBefMarried = sexBefMarried;
    }

    public String getHeart() {
        return heart;
    }

    public void setHeart(String heart) {
        this.heart = heart;
    }

    public String getPersonal() {
        return personal;
    }

    public void setPersonal(String personal) {
        this.personal = personal;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public String getWangToGo() {
        return wangToGo;
    }

    public void setWangToGo(String wangToGo) {
        this.wangToGo = wangToGo;
    }

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getMarriage() {
        return marriage;
    }

    public void setMarriage(String marriage) {
        this.marriage = marriage;
    }

    public String getWantBaby() {
        return wantBaby;
    }

    public void setWantBaby(String wantBaby) {
        this.wantBaby = wantBaby;
    }

    @Override
    public String toString() {
        return "ChangeMyInfoBean{" +
                "heart='" + heart + '\'' +
                ", personal='" + personal + '\'' +
                ", sport='" + sport + '\'' +
                ", wangToGo='" + wangToGo + '\'' +
                ", book='" + book + '\'' +
                ", nickName='" + nickName + '\'' +
                ", birthday='" + birthday + '\'' +
                ", area='" + area + '\'' +
                ", income='" + income + '\'' +
                ", height='" + height + '\'' +
                ", work='" + work + '\'' +
                ", education='" + education + '\'' +
                ", marriage='" + marriage + '\'' +
                ", wantBaby='" + wantBaby + '\'' +
                ", foods='" + foods + '\'' +
                ", music='" + music + '\'' +
                ", movies='" + movies + '\'' +
                ", interest='" + interest + '\'' +
                ", blood='" + blood + '\'' +
                ", charm='" + charm + '\'' +
                ", diffAreaLove='" + diffAreaLove + '\'' +
                ", house='" + house + '\'' +
                ", liveWithParent='" + liveWithParent + '\'' +
                ", loveType='" + loveType + '\'' +
                ", sexBefMarried='" + sexBefMarried + '\'' +
                ", spokenLanguage='" + spokenLanguage + '\'' +
                ", sign='" + sign + '\'' +
                ", provinceName='" + provinceName + '\'' +
                ", bodyType='" + bodyType + '\'' +
                ", drink='" + drink + '\'' +
                ", exerciseHabits='" + exerciseHabits + '\'' +
                ", hairColor='" + hairColor + '\'' +
                ", howManyKids='" + howManyKids + '\'' +
                ", eyeColor='" + eyeColor + '\'' +
                ", wantsKids='" + wantsKids + '\'' +
                ", pets='" + pets + '\'' +
                ", sports='" + sports + '\'' +
                ", politicalViews='" + politicalViews + '\'' +
                ", smoke='" + smoke + '\'' +
                ", ethnicity='" + ethnicity + '\'' +
                ", haveKids='" + haveKids + '\'' +
                ", childStatus='" + childStatus + '\'' +
                ", zyarea='" + zyarea + '\'' +
                ", minAge=" + minAge +
                ", maxAge=" + maxAge +
                ", minHeight='" + minHeight + '\'' +
                ", maxHeight='" + maxHeight + '\'' +
                ", minEducationEnglish='" + minEducationEnglish + '\'' +
                ", incomeEnglish='" + incomeEnglish + '\'' +
                '}';
    }
}
