package com.online.onenight2.bean;

/**
 * Created by Administrator on 2016/11/25.
 */

public class QaAnswer {
    public String id;
    public String content;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "QaAnswer{" +
                "id='" + id + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
