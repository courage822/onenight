package com.online.onenight2.bean;

/**
 * Created by jzrh on 2016/7/2.
 */
public class UploadImage extends BaseModel {
    public Image image;

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "UploadImage{" +
                "image=" + image +
                '}';
    }

}
