package com.online.onenight2.bean;

import java.util.List;

/**
 * 管理员信对象
 * Created by zhangdroid on 2016/9/7.
 */
public class AdminMsg extends BaseModel {
    private AdminChatInfo adminChatInfo;
    private List<Message> adminChatMsgList;

    public AdminChatInfo getAdminChatInfo() {
        return adminChatInfo;
    }

    public void setAdminChatInfo(AdminChatInfo adminChatInfo) {
        this.adminChatInfo = adminChatInfo;
    }

    public List<Message> getAdminChatMsgList() {
        return adminChatMsgList;
    }

    public void setAdminChatMsgList(List<Message> adminChatMsgList) {
        this.adminChatMsgList = adminChatMsgList;
    }

    @Override
    public String toString() {
        return "AdminMsg{" +
                "adminChatInfo=" + adminChatInfo +
                ", adminChatMsgList=" + adminChatMsgList +
                '}';
    }

}
