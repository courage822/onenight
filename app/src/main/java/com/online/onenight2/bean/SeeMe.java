package com.online.onenight2.bean;

import java.io.Serializable;

/**
 * Created by jzrh on 2016/7/5.
 */
public class SeeMe  implements Serializable{
    String time; //访问时间
    int isSayHello;//是否打过招呼，1->已打招呼, 0->未打招呼
    UserBase userBase;
    UserBase userBaseEnglish;//用户对象基础信息
    String language;//语言
    String country;//国家


    public UserBase getUserBaseEnglish() {
        return userBaseEnglish;
    }

    public void setUserBaseEnglish(UserBase userBaseEnglish) {
        this.userBaseEnglish = userBaseEnglish;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int isSayHello() {
        return isSayHello;
    }

    public void setSayHello(int sayHello) {
        isSayHello = sayHello;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public UserBase getUserBase() {
        return userBase;
    }

    public void setUserBase(UserBase userBase) {
        this.userBase = userBase;
    }

    @Override
    public String toString() {
        return "SeeMe{" +
                "time='" + time + '\'' +
                ", isSayHello=" + isSayHello +
                ", userBase=" + userBase +
                ", language='" + language + '\'' +
                ", country='" + country + '\'' +
                ", userBaseEnglish=" + userBaseEnglish +
                '}';
    }
}
