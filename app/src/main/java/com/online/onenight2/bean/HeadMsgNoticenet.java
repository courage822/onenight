package com.online.onenight2.bean;

/**
 * Created by foxmanman on 2016/8/3.
 */
public class HeadMsgNoticenet {
    String message;
    String Msg;
    String isSucceed;
    int displaySecond;
    int unreadMsg;
    int noticeType;
    int remoteUserId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMsg() {
        return Msg;
    }

    public void setMsg(String msg) {
        Msg = msg;
    }

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public int getDisplaySecond() {
        return displaySecond;
    }

    public void setDisplaySecond(int displaySecond) {
        this.displaySecond = displaySecond;
    }

    public int getUnreadMsg() {
        return unreadMsg;
    }

    public void setUnreadMsg(int unreadMsg) {
        this.unreadMsg = unreadMsg;
    }

    public int getNoticeType() {
        return noticeType;
    }

    public void setNoticeType(int noticeType) {
        this.noticeType = noticeType;
    }

    public int getRemoteUserId() {
        return remoteUserId;
    }

    public void setRemoteUserId(int remoteUserId) {
        this.remoteUserId = remoteUserId;
    }

    @Override
    public String toString() {
        return "HeadMsgNoticenet{" +
                "message='" + message + '\'' +
                ", Msg='" + Msg + '\'' +
                ", isSucceed='" + isSucceed + '\'' +
                ", displaySecond=" + displaySecond +
                ", unreadMsg=" + unreadMsg +
                ", noticeType=" + noticeType +
                ", remoteUserId=" + remoteUserId +
                '}';
    }
}
