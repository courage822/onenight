package com.online.onenight2.bean;

/**
 * 描述：发送图片
 * Created by qyh on 2017/3/10.
 */

public class SendPhoto {
    public String isSucceed;
    public String msg;
    public String imageUrl;

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
