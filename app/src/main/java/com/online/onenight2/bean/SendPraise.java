package com.online.onenight2.bean;

/**
 * 描述：点赞/喜欢
 * Created by qyh on 2017/4/27.
 */

public class SendPraise {
    private String isMatched;
    private String isSucceed;
    private String msg;

    public String getIsMatched() {
        return isMatched;
    }

    public void setIsMatched(String isMatched) {
        this.isMatched = isMatched;
    }

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
