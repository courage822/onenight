package com.online.onenight2.bean;

/**
 * Created by Administrator on 2017/3/29.
 */

public class LocationEntity {
    String isSucceed;
    String country;
    String msg;

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "LocationEntity{" +
                "isSucceed='" + isSucceed + '\'' +
                ", country='" + country + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}
