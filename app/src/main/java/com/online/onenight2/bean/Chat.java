package com.online.onenight2.bean;

/**
 * 描述：
 * Created by qyh on 2017/4/14.
 */

public class Chat {
    /**
     * id":Id标识符
     "localUserId":{本方ID}
     "remoteUserId":{对方ID}
     "chatStatus":{聊天状态}//聊天状态类型 1、开始写信 2、发信 3、收信  4 、配对 5、管理员信
     "unreadCount":{未读信数}
     "lastTime":{最新时间}
     "lastContent":{最信消息}
     "addTime":{添加时间}
     "addTimeMills":{添加时间毫秒数}
     "fromChannel":{渠道号}
     "country":{语言}
     "language":{国家}
     */
    private String lastTime;
    private int chatStatus;
    private String country;
    private String addTime;
    private String lastContent;
    private int unreadCount;
    private String language;
    private String updateTime;
    private int remoteUserId;
    private int fromChannel;
    private String id;
    private int localUserId;
    private String addTimeMills;

    public String getLastTime() {
        return lastTime;
    }

    public void setLastTime(String lastTime) {
        this.lastTime = lastTime;
    }

    public int getChatStatus() {
        return chatStatus;
    }

    public void setChatStatus(int chatStatus) {
        this.chatStatus = chatStatus;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getLastContent() {
        return lastContent;
    }

    public void setLastContent(String lastContent) {
        this.lastContent = lastContent;
    }

    public int getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public int getRemoteUserId() {
        return remoteUserId;
    }

    public void setRemoteUserId(int remoteUserId) {
        this.remoteUserId = remoteUserId;
    }

    public int getFromChannel() {
        return fromChannel;
    }

    public void setFromChannel(int fromChannel) {
        this.fromChannel = fromChannel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getLocalUserId() {
        return localUserId;
    }

    public void setLocalUserId(int localUserId) {
        this.localUserId = localUserId;
    }

    public String getAddTimeMills() {
        return addTimeMills;
    }

    public void setAddTimeMills(String addTimeMills) {
        this.addTimeMills = addTimeMills;
    }
}
