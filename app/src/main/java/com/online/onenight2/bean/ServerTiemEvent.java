package com.online.onenight2.bean;

/**
 * Created by foxmanman on 2016/9/28.
 */
public class ServerTiemEvent {
    long serverTime;

    public long getServerTime() {
        return serverTime;
    }

    public void setServerTime(long serverTime) {
        this.serverTime = serverTime;
    }

    @Override
    public String toString() {
        return "ServerTiemEvent{" +
                "serverTime=" + serverTime +
                '}';
    }
}
