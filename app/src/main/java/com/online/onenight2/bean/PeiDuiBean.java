package com.online.onenight2.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 描述：PeiDuiBean
 * Created by qyh on 2017/5/2.
 */

public class PeiDuiBean implements Serializable {
    
        private int pageSize  ;
        private int unreadCount  ;
        private int totalCount  ;//总数,
        private int pageNum  ;
        private String isSucceed  ;
        private List<MatcherList> matcherList;

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public List<MatcherList> getMatcherList() {
        return matcherList;
    }

    public void setMatcherList(List<MatcherList> matcherList) {
        this.matcherList = matcherList;
    }

    public class MatcherList implements Serializable {

            private String country  ;
            private String lastContent  ;
            private int isRead  ;//是否已读, -1 未读 1 已读
            private int remoteUserId  ;//配对好友用户Id
            private String language  ;
            private String matchTime  ;//配对成功时间
            private int status  ;
            private  UserBaseEnglish userBaseEnglish;

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getLastContent() {
            return lastContent;
        }

        public void setLastContent(String lastContent) {
            this.lastContent = lastContent;
        }

        public int getIsRead() {
            return isRead;
        }

        public void setIsRead(int isRead) {
            this.isRead = isRead;
        }

        public int getRemoteUserId() {
            return remoteUserId;
        }

        public void setRemoteUserId(int remoteUserId) {
            this.remoteUserId = remoteUserId;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getMatchTime() {
            return matchTime;
        }

        public void setMatchTime(String matchTime) {
            this.matchTime = matchTime;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public UserBaseEnglish getUserBaseEnglish() {
            return userBaseEnglish;
        }

        public void setUserBaseEnglish(UserBaseEnglish userBaseEnglish) {
            this.userBaseEnglish = userBaseEnglish;
        }
    }
}
