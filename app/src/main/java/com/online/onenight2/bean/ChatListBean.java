package com.online.onenight2.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 描述：消息列表，，具有未读条数
 * Created by qyh on 2017/4/14.
 */

public class ChatListBean {
    
        private int showWriteMsgIntercept;
        private int pageSize;
        private Long systemTime;
        private int pageNum;
        private String isSucceed;
       //未读消息总数
        private int totalUnread;
        private List<ListChat> listChat;
    public int getShowWriteMsgIntercept() {
        return showWriteMsgIntercept;
    }

    public void setShowWriteMsgIntercept(int showWriteMsgIntercept) {
        this.showWriteMsgIntercept = showWriteMsgIntercept;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public Long getSystemTime() {
        return systemTime;
    }

    public void setSystemTime(Long systemTime) {
        this.systemTime = systemTime;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public String getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(String isSucceed) {
        this.isSucceed = isSucceed;
    }

    public int getTotalUnread() {
        return totalUnread;
    }

    public void setTotalUnread(int totalUnread) {
        this.totalUnread = totalUnread;
    }

    public List<ListChat> getListChat() {
        return listChat;
    }

    public void setListChat(List<ListChat> listChat) {
        this.listChat = listChat;
    }

    public class ListChat implements Serializable {
            private int id;
            private UserBaseEnglish userBaseEnglish;
            private Chat chat;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public UserBaseEnglish getUserBaseEnglish() {
            return userBaseEnglish;
        }

        public void setUserBaseEnglish(UserBaseEnglish userBaseEnglish) {
            this.userBaseEnglish = userBaseEnglish;
        }

        public Chat getChat() {
            return chat;
        }

        public void setChat(Chat chat) {
            this.chat = chat;
        }

    }

}
