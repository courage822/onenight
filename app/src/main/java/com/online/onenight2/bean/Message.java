package com.online.onenight2.bean;

/**
 * 聊天消息对象
 * Created by zhangdroid on 2016/6/22.
 */
public class Message {
    String id;// 消息id
    String language;// 语言
    String country;// 国家
    String uid;// 会员id
    // 3->自己文本消息，4->文本消息, 7->语音消息,8->qa问答信,9->小助手问题
    // 10->图片, 11->qa解锁信）
    String msgType;
    String content;// 消息内容
    String audioUrl;// 语音url
    String audioTime;// 语音时间
    String recevStatus;// 语音状态(0是未读，1是已读)
    String createDate;// 发送时间
    QaQuestion qaQuestion;

    String photoUrl;//聊天图片的url  msgType=10时返回
    boolean havePhoto;//是否有聊天图片
    Image chatImage;//封装聊天图片

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public boolean isHavePhoto() {
        return havePhoto;
    }

    public void setHavePhoto(boolean havePhoto) {
        this.havePhoto = havePhoto;
    }

    public Image getChatImage() {
        return chatImage;
    }

    public void setChatImage(Image chatImage) {
        this.chatImage = chatImage;
    }

    public QaQuestion getQaQuestion() {
        return qaQuestion;
    }

    public void setQaQuestion(QaQuestion qaQuestion) {
        this.qaQuestion = qaQuestion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public String getAudioTime() {
        return audioTime;
    }

    public void setAudioTime(String audioTime) {
        this.audioTime = audioTime;
    }

    public String getRecevStatus() {
        return recevStatus;
    }

    public void setRecevStatus(String recevStatus) {
        this.recevStatus = recevStatus;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String  createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id='" + id + '\'' +
                ", language='" + language + '\'' +
                ", country='" + country + '\'' +
                ", uid='" + uid + '\'' +
                ", msgType='" + msgType + '\'' +
                ", content='" + content + '\'' +
                ", audioUrl='" + audioUrl + '\'' +
                ", audioTime='" + audioTime + '\'' +
                ", recevStatus='" + recevStatus + '\'' +
                ", createDate='" + createDate + '\'' +
                ", qaQuestion=" + qaQuestion +
                '}';
    }
}
