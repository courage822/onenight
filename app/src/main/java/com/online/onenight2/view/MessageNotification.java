package com.online.onenight2.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.IntEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.library.utils.HeightUtils;
import com.online.onenight2.R;
import com.online.onenight2.base.RxBus;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.hot.activity.UserInfoActivity;
import com.online.onenight2.message.activity.ChatMegActivity;
import com.online.onenight2.utils.ImageLoaderUtils;

/**
 * 描述：悬挂式通知栏
 * Created by qyh on 2017/4/1.
 */
public class MessageNotification implements View.OnTouchListener{

    private static final int DIRECTION_LEFT = -1;
    private static final int DIRECTION_NONE = 0;
    private static final int DIRECTION_RIGHT = 1;

    public  static final int DISMISS_INTERVAL = 4000;

    private WindowManager mWindowManager;
    private WindowManager.LayoutParams mWindowParams;
    private View mContentView;
    private Context mContext;
    private int mScreenWidth = 0;
    private int mStatusBarHeight = 0;

    private boolean isShowing = false;
    private ValueAnimator restoreAnimator = null;
    private ValueAnimator dismissAnimator = null;
    private ImageView mIvIcon;
    private TextView tv_notification_age;
    private TextView tv_notification_height;
    private TextView tv_notification_msg;
    private TextView tv_notification_nickname;
    private TextView tv_notification_look;
    private ImageView iv_notification_avatar;
    private ImageView iv_avatar;
    private TextView tv_golook;
    private TextView tv_ignore;
    private TextView tv_nikename;
    private LinearLayout ll_root;
    private static final int HIDE_WINDOW = 0;
    private TextView iv_notification_sex;

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case HIDE_WINDOW:
                    dismiss();
                    break;
            }
            return false;
        }
    });
    private LinearLayout linear_root;
    private LinearLayout ll_backgroud;

    public MessageNotification(Builder builder, String type, int noticeType) {
        mContext = builder.getContext();
                mStatusBarHeight = getStatusBarHeight();
                mScreenWidth = mContext.getResources().getDisplayMetrics().widthPixels;

                mWindowManager = (WindowManager)
                        mContext.getSystemService(Context.WINDOW_SERVICE);
                mWindowParams = new WindowManager.LayoutParams();
//                 mWindowParams.type = WindowManager.LayoutParams.TYPE_TOAST;// 系统提示window
                mWindowParams.gravity = Gravity.LEFT | Gravity.TOP;
                //FLAG_LAYOUT_NO_LIMITS：让Window能全屏显示，能延伸至状态栏
                mWindowParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                mWindowParams.format= PixelFormat.TRANSLUCENT;// 支持透明
                mWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                mWindowParams.flags =
                        WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
                //设置进入和退出动画
                mWindowParams.windowAnimations = R.style.NotificationAnim;
                mWindowParams.x = 0;
                mWindowParams.y = -mStatusBarHeight;
                setContentView(mContext, builder, type, noticeType);
    }

    /***
     * 设置内容视图
     *
     * @param context
     * @param noticeType
     */
    private void setContentView(Context context, final Builder builder, String type, int noticeType) {
        if(noticeType==1) {
            mContentView = LayoutInflater.from(context).inflate(R.layout.layout_notification, null);
            View v_state_bar = mContentView.findViewById(R.id.v_state_bar);
            ViewGroup.LayoutParams layoutParameter = v_state_bar.getLayoutParams();
            layoutParameter.height = mStatusBarHeight;
            v_state_bar.setLayoutParams(layoutParameter);
            mIvIcon = (ImageView) mContentView.findViewById(R.id.iv_icon);
            tv_notification_age = (TextView) mContentView.findViewById(R.id.tv_notification_age);
            ll_root = (LinearLayout) mContentView.findViewById(R.id.ll_root);
            ll_backgroud = (LinearLayout) mContentView.findViewById(R.id.ll_backgroud);
            tv_notification_height = (TextView) mContentView.findViewById(R.id.tv_notification_height);
            tv_notification_msg = (TextView) mContentView.findViewById(R.id.tv_notification_msg);
            tv_notification_nickname = (TextView) mContentView.findViewById(R.id.tv_notification_nickname);
            tv_notification_look = (TextView) mContentView.findViewById(R.id.tv_notification_look);
            iv_notification_avatar = (ImageView) mContentView.findViewById(R.id.iv_notification_avatar);
            iv_notification_sex = (TextView) mContentView.findViewById(R.id.iv_notification_sex);

            setIcon(builder.image, noticeType);
            setAge(builder.age);
            setNickName(builder.nickName, noticeType);
            setHeight(builder.height);
            setMsg(builder.msg, type);
            setSex(builder.sex);
            ll_root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    RxBus.getInstance().post("message_change",true);
                    Bundle bundle = new Bundle();
                    bundle.putString(ConfigConstant.USERID, String.valueOf(builder.userId));
                    bundle.putString(ConfigConstant.NICKNAME, builder.nickName);
                    Intent intent = new Intent(mContext, ChatMegActivity.class);
                    if (null != builder.image) {
                        bundle.putString(ConfigConstant.USERAVATAR, builder.image);
                    }
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            });
        }else if(noticeType==2){
            mContentView = LayoutInflater.from(context).inflate(R.layout.layout_notification_look, null);
            View v_state_bar = mContentView.findViewById(R.id.v_state_bar);
            ViewGroup.LayoutParams layoutParameter = v_state_bar.getLayoutParams();
            layoutParameter.height = mStatusBarHeight;
            v_state_bar.setLayoutParams(layoutParameter);
            tv_nikename = (TextView) mContentView.findViewById(R.id.tv_nikename);
            linear_root = ((LinearLayout) mContentView.findViewById(R.id.look_root_linear));
            tv_ignore = (TextView) mContentView.findViewById(R.id.tv_ignore);
            tv_golook = (TextView) mContentView.findViewById(R.id.tv_golook);
            iv_avatar = (ImageView) mContentView.findViewById(R.id.iv_avatar);
            setNickName(builder.nickName,noticeType);
            setIcon(builder.image,noticeType);
            //忽略
            tv_ignore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            //去看看
            tv_golook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString(ConfigConstant.USERID, String.valueOf(builder.userId));
                    bundle.putString(ConfigConstant.NICKNAME, builder.nickName);
                    bundle.putString(ConfigConstant.ISRECORD,"0");
                    bundle.putString(ConfigConstant.SOURCETAG,"1");
                    Intent intent = new Intent(mContext, UserInfoActivity.class);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            });
            linear_root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString(ConfigConstant.USERID, String.valueOf(builder.userId));
                    bundle.putString(ConfigConstant.NICKNAME, builder.nickName);
                    bundle.putString(ConfigConstant.ISRECORD,"0");
                    bundle.putString(ConfigConstant.SOURCETAG,"1");
                    Intent intent = new Intent(mContext, UserInfoActivity.class);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            });
        }
    }
    private int downX = 0;

    private int direction = DIRECTION_NONE;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (isAnimatorRunning()) {
            return false;
        }
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downX = (int) event.getRawX();
                break;
            case MotionEvent.ACTION_MOVE:
                //处于滑动状态就取消自动消失
                mHandler.removeMessages(HIDE_WINDOW);
                int moveX = (int) event.getRawX() - downX;
                //判断滑动方向
                if (moveX > 0) {
                    direction = DIRECTION_RIGHT;
                } else {
                    direction = DIRECTION_LEFT;
                }

                updateWindowLocation(moveX, mWindowParams.y);

                break;
            case MotionEvent.ACTION_UP:
                if (Math.abs(mWindowParams.x) > mScreenWidth / 2) {
                    startDismissAnimator(direction);
                } else {
                    startRestoreAnimator();
                }
                break;
        }
        return true;
    }
    private void startDismissAnimator(int direction) {
        if (direction == DIRECTION_LEFT)
            dismissAnimator = new ValueAnimator().ofInt(mWindowParams.x, -mScreenWidth);
        else {
            dismissAnimator = new ValueAnimator().ofInt(mWindowParams.x, mScreenWidth);
        }
        dismissAnimator.setDuration(300);
        dismissAnimator.setEvaluator(new IntEvaluator());

        dismissAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                updateWindowLocation((Integer) animation.getAnimatedValue(), -mStatusBarHeight);
            }
        });
        dismissAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                restoreAnimator = null;
                dismiss();
            }
        });
        dismissAnimator.start();
    }

    private void startRestoreAnimator() {
        restoreAnimator = new ValueAnimator().ofInt(mWindowParams.x, 0);
        restoreAnimator.setDuration(300);
        restoreAnimator.setEvaluator(new IntEvaluator());

        restoreAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                System.out.println("onAnimationUpdate:" + animation.getAnimatedValue());
                updateWindowLocation((Integer) animation.getAnimatedValue(), -mStatusBarHeight);
            }
        });
        restoreAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                restoreAnimator = null;
                autoDismiss();
            }
        });
        restoreAnimator.start();
    }

    private boolean isAnimatorRunning() {
        return (restoreAnimator != null && restoreAnimator.isRunning()) || (dismissAnimator != null && dismissAnimator.isRunning());
    }

    public void updateWindowLocation(int x, int y) {
        if (isShowing) {
            mWindowParams.x = x;
            mWindowParams.y = y;
            mWindowManager.updateViewLayout(mContentView, mWindowParams);
        }

    }

    public void show() {
        try{
            if (!isShowing) {
                Vibrator vibrator = (Vibrator)mContext.getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(200);
                isShowing = true;
                mWindowManager.addView(mContentView, mWindowParams);
                autoDismiss();
            }
        }catch (Exception e){
            dismiss();
        }
    }

    public void dismiss() {
        if (isShowing) {
            resetState();
            try {
                mWindowManager.removeView(mContentView);
            }catch(Exception e){
            }
        }
    }

    /**
     * 重置状态
     */
    private void resetState() {
        isShowing = false;
        mWindowParams.x = 0;
        mWindowParams.y = -mStatusBarHeight;
    }

    /**
     * 自动隐藏通知
     */
    private void autoDismiss() {
        mHandler.removeMessages(HIDE_WINDOW);
        mHandler.sendEmptyMessageDelayed(HIDE_WINDOW, DISMISS_INTERVAL);
    }

    /**
     * 获取状态栏的高度
     */
    public int getStatusBarHeight() {
        int height = 0;
        int resId = mContext.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resId > 0) {
            height = mContext.getResources().getDimensionPixelSize(resId);
        }
        return height;
    }

    public void setIcon(String url, int noticeType) {
        if(noticeType==1){
            ImageLoaderUtils.display(mContext,iv_notification_avatar, url);
        }else if(noticeType==2){
            ImageLoaderUtils.display(mContext,iv_avatar, url);
        }
    }
    public void setNickName(String nickName, int noticeType) {
        if(noticeType==1) {
            if (!TextUtils.isEmpty(nickName)) {
                tv_notification_nickname.setText(nickName);
            }
        }else if(noticeType==2){
            if (!TextUtils.isEmpty(nickName)) {
                tv_nikename.setText(nickName);
            }
        }
    }
    public void setAge(String age) {
        if (!TextUtils.isEmpty(age)) {
            tv_notification_age.setVisibility(View.VISIBLE);
            tv_notification_age.setText(age);
        }
    }
    public void setSex(int sex){
        //女
        if(sex==1){
            iv_notification_sex.setText("♀");
            ll_backgroud.setBackgroundResource(R.drawable.shape_notification_age_women);
        }else  if(sex==0){
            iv_notification_sex.setText("♂");
            ll_backgroud.setBackgroundResource(R.drawable.shape_notification_age);
        }
    }
    public void setMsg(String content, String type) {
        if (!TextUtils.isEmpty(content)) {
            tv_notification_msg.setVisibility(View.VISIBLE);
            //文本消息
            if(type.equals("4")){
                tv_notification_msg.setText(content);
                //图片消息
            }else if(type.equals("10")){
                tv_notification_msg.setText("["+ mContext.getString(R.string.photo)+"]");
            }else if(type.equals("7")){
                tv_notification_msg.setText("["+ mContext.getString(R.string.voice)+"]");
            }
        }
    }

    public void setHeight(String height) {
        if (!TextUtils.isEmpty(height)) {
            String  heightCm = HeightUtils.getInchCmByCm(height);
            tv_notification_height.setVisibility(View.VISIBLE);
            tv_notification_height.setText(heightCm);
        }
    }

    public static class Builder {
        private Context context;
        private int userId;
        private String age;
        private String msg;
        private String height;
        private String nickName;
        private String image;
        private int sex;
        private FragmentManager fragmentManager;

        public Context getContext() {
            return context;
        }

        public Builder setContext(Context context) {
            this.context = context;
            return this;
        }
        public Builder setFragmentManager(FragmentManager fragmentManager){
            this.fragmentManager=fragmentManager;
            return this;
        }
        public Builder setSex(int s) {
            this.sex = s;
            return this;
        }

        public Builder setUserId(int id) {
            this.userId = id;
            return this;
        }

        public Builder setImgRes(String url) {
            this.image = url;
            return this;
        }


        public Builder setAge(String age) {
            this.age = age;
            return this;
        }

        public Builder setHeight(String height) {
            this.height = height;
            return this;
        }

        public Builder setMsg(String msg) {
            this.msg = msg;
            return this;
        }

        public Builder setNickName(String nickName) {
            this.nickName = nickName;
            return this;
        }
        public MessageNotification build(String type,int noticeType) {

            if (null == context)
                throw new IllegalArgumentException("the context is required.");
            return new MessageNotification(this,type,noticeType);
        }
    }
}
