package com.online.onenight2.view.dialog;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.library.app.BaseDialogFragment;
import com.library.utils.ToastUtil;
import com.lzy.okgo.OkGo;
import com.online.onenight2.R;
import com.online.onenight2.bean.BaseModel;
import com.online.onenight2.callback.JsonCallback;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.xml.PlatformInfoXml;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by zhangdroid on 2016/7/4.
 */
public class ReportDialog extends BaseDialogFragment {
    private String mUserId;

    public static ReportDialog newInstance(String title, String uId) {
        ReportDialog reportDialog = new ReportDialog();
        reportDialog.mUserId = uId;
        reportDialog.setArguments(getDialogBundle(title, null, null, null, true));
        return reportDialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_report;
    }

    @Override
    protected void setDialogContentView(View view) {
        TextView tvTitle = (TextView) view.findViewById(R.id.dialog_report_title);
        View dividerView = view.findViewById(R.id.dialog_report_divider);// 标题栏下分割线
        final RadioGroup radioGroupReason = (RadioGroup) view.findViewById(R.id.dialog_reason_group);
        final EditText etMore = (EditText) view.findViewById(R.id.dialog_report_reason_more);// 补充说明
        Button btnPositive = (Button) view.findViewById(R.id.dialog_report_sure);
        Button btnNegative = (Button) view.findViewById(R.id.dialog_report_cancel);

        String title = getDialogTitle();
        if (!TextUtils.isEmpty((title))) {
            tvTitle.setText(title);
            tvTitle.setVisibility(View.VISIBLE);
            dividerView.setVisibility(View.VISIBLE);
        } else {
            tvTitle.setVisibility(View.GONE);
            dividerView.setVisibility(View.GONE);
        }

        String positive = getDialogPositive();
        if (!TextUtils.isEmpty((positive))) {
            btnPositive.setText(positive);
        }

        String negative = getDialogNegative();
        if (!TextUtils.isEmpty((negative))) {
            btnNegative.setText(negative);
        }

        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton checkBtn = (RadioButton) radioGroupReason.findViewById(radioGroupReason.getCheckedRadioButtonId());
//                LogUtil.e(LogUtil.TAG_LMF,"举报原因checkBtn.getText()"+checkBtn.getText());
                if(checkBtn!=null&& checkBtn.getText()!=null){

                    String reason = checkBtn.getText().toString().trim();
                    if (TextUtils.isEmpty(reason)) {
//                        ToastUtil.showShortToast(getActivity(), getActivity().getString(R.string.report_reason_empty));
                        return;
                    }else if(!TextUtils.isEmpty(reason)){
                        reportUser(reason, etMore.getText().toString().trim());
                    }
                }else{
                    ToastUtil.showShortToast(getActivity(), getActivity().getString(R.string.report_reason_empty));
                }


            }
        });
        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void reportUser(String reason, String contentMore) {
        if (TextUtils.isEmpty(mUserId)) {
            dismiss();
            return;
        }

        if(reason!=null){
            OkGo.post(IUrlConstant.URL_REPORT)
                    .headers("token", PlatformInfoXml.getToken())
                    .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                    .params("contentId", reason)// 举报原因
                    .params("content", contentMore)// 补充说明
                    .execute(new JsonCallback<BaseModel>() {
                        @Override
                        public void onSuccess(BaseModel baseModel, Call call, Response response) {
                            if (baseModel != null) {
                                String isSucceed = baseModel.getIsSucceed();
                                if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {
                                    ToastUtil.showShortToast(getActivity(), getString(R.string.report_success));
                                    dismiss();
                                }
                            }
                        }

                        @Override
                        public void onError(Call call, Response response, Exception e) {
                            super.onError(call, response, e);
                            ToastUtil.showShortToast(getActivity(), getString(R.string.report_fail));
                            dismiss();
                        }
                    });
        }

    }
}
