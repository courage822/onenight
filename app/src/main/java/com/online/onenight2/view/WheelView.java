package com.online.onenight2.view;
import com.online.onenight2.R;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import java.util.ArrayList;
import java.util.List;
import android.annotation.SuppressLint;
import android.content.res.TypedArray;
import android.graphics.LinearGradient;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.os.Handler;
import android.os.Message;
import android.text.TextPaint;
/**
 * 滚轮控件
 * Created by zhangdroid on 2016/6/24.
 */
public class WheelView extends View {
    /**
     * 控件宽度
     */
    private float controlWidth;
    /**
     * 控件高度
     */
    private float controlHeight;
    /**
     * 是否滑动中
     */
    private boolean isScrolling = false;
    /**
     * 选择的内容
     */
    private ArrayList<ItemObject> itemList = new ArrayList<>();
    /**
     * 设置数据
     */
    private List<String> dataList = new ArrayList<>();
    /**
     * 按下的坐标
     */
    private int downY;
    /**
     * 按下的时间
     */
    private long downTime = 0;
    /**
     * 短促移动
     */
    private long goonTime = 200;
    /**
     * 短促移动距离
     */
    private int goonDistance = 100;
    /**
     * 画线画笔
     */
    private Paint linePaint;
    /**
     * 线的默认颜色
     */
    private int lineColor = 0xff000000;
    /**
     * 线的默认宽度
     */
    private float lineHeight = 2f;
    /**
     * 默认字体
     */
    private int normalFont = 14;
    /**
     * 选中的时候字体
     */
    private int selectedFont = 24;
    /**
     * 单元格高度
     */
    private int unitHeight = 40;
    /**
     * 显示多少个内容
     */
    private int itemNumber = 3;
    /**
     * 默认字体颜色
     */
    private int normalColor;
    /**
     * 选中时候的字体颜色
     */
    private int selectedColor;
    /**
     * 蒙板高度
     */
    private int maskHeight = 40;
    /**
     * 选择监听
     */
    private OnItemSelectedListener onItemSelectedListener;
    /**
     * 是否可用
     */
    private boolean isEnable = true;
    /**
     * 刷新界面
     */
    private static final int REFRESH_VIEW = 0x001;
    /**
     * 移动距离
     */
    private static final int MOVE_NUMBER = 5;
    /**
     * 是否允许选空
     */
    private boolean noEmpty = true;

    /**
     * 正在修改数据，避免ConcurrentModificationException异常
     */
    private boolean isClearing = false;

    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case REFRESH_VIEW:
                    invalidate();
                    break;
            }
        }
    };

    public WheelView(Context context) {
        this(context, null);
    }

    public WheelView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WheelView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.WheelView, defStyle, 0);
        itemNumber = typedArray.getInteger(R.styleable.WheelView_itemNumber, itemNumber);
        unitHeight = typedArray.getDimensionPixelSize(R.styleable.WheelView_unitHeight, (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics()));

        normalFont = typedArray.getDimensionPixelSize(R.styleable.WheelView_normalTextSize, (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_SP, 14, getResources().getDisplayMetrics()));
        selectedFont = typedArray.getDimensionPixelSize(R.styleable.WheelView_selectedTextSize, (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_SP, 18, getResources().getDisplayMetrics()));
//        normalColor = typedArray.getColor(R.styleable.WheelView_normalTextColor, Color.BLACK);
        normalColor = typedArray.getColor(R.styleable.WheelView_normalTextColor, Color.parseColor("#2B2B2B"));

//        selectedColor = typedArray.getColor(R.styleable.WheelView_selectedTextColor, Color.BLACK);
        selectedColor = typedArray.getColor(R.styleable.WheelView_selectedTextColor, Color.parseColor("#E61624"));

        lineColor = typedArray.getColor(R.styleable.WheelView_lineColor, lineColor);
        lineHeight = typedArray.getDimension(R.styleable.WheelView_lineHeight, lineHeight);

        maskHeight = typedArray.getDimensionPixelSize(R.styleable.WheelView_maskHeight, (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 48, getResources().getDisplayMetrics()));
        noEmpty = typedArray.getBoolean(R.styleable.WheelView_noEmpty, true);
        isEnable = typedArray.getBoolean(R.styleable.WheelView_isEnable, true);
        typedArray.recycle();

        controlHeight = itemNumber * unitHeight;

        initData();
    }

    /**
     * 初始化数据
     */
    private void initData() {
        isClearing = true;
        itemList.clear();
        for (int i = 0; i < dataList.size(); i++) {
            ItemObject itemObject = new ItemObject();
            itemObject.id = i;
            itemObject.itemText = dataList.get(i);
            itemObject.x = 0;
            itemObject.y = i * unitHeight;
            itemList.add(itemObject);
        }
        isClearing = false;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        controlWidth = getWidth();
        if (controlWidth != 0) {
            setMeasuredDimension(getWidth(), itemNumber * unitHeight);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        drawLine(canvas);
        drawList(canvas);
        drawMask(canvas);
    }

    /**
     * 绘制线条
     *
     * @param canvas
     */
    private void drawLine(Canvas canvas) {

        if (linePaint == null) {
            linePaint = new Paint();
            linePaint.setColor(lineColor);
            linePaint.setAntiAlias(true);
            linePaint.setStrokeWidth(lineHeight);
        }

        canvas.drawLine(0, controlHeight / 2 - unitHeight / 2 + lineHeight,
                controlWidth, controlHeight / 2 - unitHeight / 2 + lineHeight, linePaint);
        canvas.drawLine(0, controlHeight / 2 + unitHeight / 2 - lineHeight,
                controlWidth, controlHeight / 2 + unitHeight / 2 - lineHeight, linePaint);
    }

    private synchronized void drawList(Canvas canvas) {
        if (isClearing)
            return;
        try {
            for (ItemObject itemObject : itemList) {
                itemObject.drawSelf(canvas, getMeasuredWidth());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 绘制遮盖板
     *
     * @param canvas
     */
    private void drawMask(Canvas canvas) {
        LinearGradient lg = new LinearGradient(0, 0, 0, maskHeight, 0x00f2f2f2,
                0x00f2f2f2, TileMode.MIRROR);
        Paint paint = new Paint();
        paint.setShader(lg);
        canvas.drawRect(0, 0, controlWidth, maskHeight, paint);

        LinearGradient lg2 = new LinearGradient(0, controlHeight - maskHeight,
                0, controlHeight, 0x00f2f2f2, 0x00f2f2f2, TileMode.MIRROR);
        Paint paint2 = new Paint();
        paint2.setShader(lg2);
        canvas.drawRect(0, controlHeight - maskHeight, controlWidth,
                controlHeight, paint2);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!isEnable)
            return true;
        int y = (int) event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                isScrolling = true;
                downY = (int) event.getY();
                downTime = System.currentTimeMillis();
                break;

            case MotionEvent.ACTION_MOVE:
                actionMove(y - downY);
                break;

            case MotionEvent.ACTION_UP:
                int move = Math.abs(y - downY);
                // 判断这段时间移动的距离
                if (System.currentTimeMillis() - downTime < goonTime && move > goonDistance) {
                    goonMove(y - downY);
                } else {
                    actionUp(y - downY);
                    noEmpty();
                    isScrolling = false;
                }
                break;
        }
        return true;
    }

    /**
     * 继续移动一定距离
     */
    private synchronized void goonMove(final int move) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                int distance = 0;
                while (distance < unitHeight * MOVE_NUMBER) {
                    try {
                        Thread.sleep(5);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    actionThreadMove(move > 0 ? distance : distance * (-1));
                    distance += 10;

                }
                actionUp(move > 0 ? distance - 10 : distance * (-1) + 10);
                noEmpty();
            }
        }).start();
    }

    /**
     * 不能为空，必须有选项
     */
    private void noEmpty() {
        if (!noEmpty)
            return;
        for (ItemObject item : itemList) {
            if (item.isSelected())
                return;
        }
        int move = (int) itemList.get(0).moveToSelected();
        if (move < 0) {
            defaultMove(move);
        } else {
            defaultMove((int) itemList.get(itemList.size() - 1)
                    .moveToSelected());
        }
        for (ItemObject item : itemList) {
            if (item.isSelected()) {
                if (onItemSelectedListener != null)
                    onItemSelectedListener.onItemSelected(item.id, item.itemText);
                break;
            }
        }
    }

    /**
     * 移动的时候
     *
     * @param move
     */
    private void actionMove(int move) {
        for (ItemObject item : itemList) {
            item.move(move);
        }
        invalidate();
    }

    /**
     * 移动，线程中调用
     *
     * @param move
     */
    private void actionThreadMove(int move) {
        for (ItemObject item : itemList) {
            item.move(move);
        }
        Message rMessage = new Message();
        rMessage.what = REFRESH_VIEW;
        handler.sendMessage(rMessage);
    }

    /**
     * 松开的时候
     *
     * @param move
     */
    private void actionUp(int move) {
        int newMove = 0;
        if (move > 0) {
            for (int i = 0; i < itemList.size(); i++) {
                if (itemList.get(i).isSelected()) {
                    newMove = (int) itemList.get(i).moveToSelected();
                    if (onItemSelectedListener != null)
                        onItemSelectedListener.onItemSelected(itemList.get(i).id,
                                itemList.get(i).itemText);
                    break;
                }
            }
        } else {
            for (int i = itemList.size() - 1; i >= 0; i--) {
                if (itemList.get(i).isSelected()) {
                    newMove = (int) itemList.get(i).moveToSelected();
                    if (onItemSelectedListener != null)
                        onItemSelectedListener.onItemSelected(itemList.get(i).id,
                                itemList.get(i).itemText);
                    break;
                }
            }
        }
        for (ItemObject item : itemList) {
            item.newY(move + 0);
        }
        slowMove(newMove);
        Message rMessage = new Message();
        rMessage.what = REFRESH_VIEW;
        handler.sendMessage(rMessage);

    }

    /**
     * 缓慢移动
     *
     * @param move
     */
    private synchronized void slowMove(final int move) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                // 判断正负
                int m = move > 0 ? move : move * (-1);
                int i = move > 0 ? 1 : (-1);
                // 移动速度
                int speed = 1;
                while (true) {
                    m = m - speed;
                    if (m <= 0) {
                        for (ItemObject item : itemList) {
                            item.newY(m * i);
                        }
                        Message rMessage = new Message();
                        rMessage.what = REFRESH_VIEW;
                        handler.sendMessage(rMessage);
                        try {
                            Thread.sleep(2);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                    for (ItemObject item : itemList) {
                        item.newY(speed * i);
                    }
                    Message rMessage = new Message();
                    rMessage.what = REFRESH_VIEW;
                    handler.sendMessage(rMessage);
                    try {
                        Thread.sleep(2);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                for (ItemObject item : itemList) {
                    if (item.isSelected()) {
                        if (onItemSelectedListener != null)
                            onItemSelectedListener.onItemSelected(item.id, item.itemText);
                        break;
                    }
                }

            }
        }).start();
    }

    /**
     * 移动到默认位置
     *
     * @param move
     */
    private void defaultMove(int move) {
        for (ItemObject item : itemList) {
            item.newY(move);
        }
        Message rMessage = new Message();
        rMessage.what = REFRESH_VIEW;
        handler.sendMessage(rMessage);
    }

    /**
     * 设置数据 （第一次）
     *
     * @param data
     */
    public void setData(List<String> data) {
        this.dataList = data;
        initData();
    }

    /**
     * 重置数据
     *
     * @param data
     */
    public void refreshData(List<String> data) {
        setData(data);
        invalidate();
    }

    /**
     * 获取返回项 id
     *
     * @return
     */
    public int getSelected() {
        for (ItemObject item : itemList) {
            if (item.isSelected())
                return item.id;
        }
        return -1;
    }

    /**
     * 获取返回的内容
     *
     * @return
     */
    public String getSelectedText() {
        for (ItemObject item : itemList) {
            if (item.isSelected())
                return item.itemText;
        }
        return "";
    }

    /**
     * 是否正在滑动
     *
     * @return
     */
    public boolean isScrolling() {
        return isScrolling;
    }

    /**
     * 是否可用
     *
     * @return
     */
    public boolean isEnabled() {
        return isEnable;
    }

    /**
     * 设置是否可用
     *
     * @param isEnable
     */
    public void setEnabled(boolean isEnable) {
        this.isEnable = isEnable;
    }

    /**
     * 设置默认选项
     *
     * @param index
     */
    public void setDefaultIndex(int index) {
        if (index < 0 || index > itemList.size() - 1)
            return;
        float move = itemList.get(index).moveToSelected();
        defaultMove((int) move);
    }

    /**
     * 获取列表大小
     *
     * @return
     */
    public int getListSize() {
        if (itemList == null)
            return 0;
        return itemList.size();
    }

    /**
     * 获取某项的内容
     *
     * @param index
     * @return
     */
    public String getItemText(int index) {
        if (itemList == null)
            return "";
        return itemList.get(index).itemText;
    }

    /**
     * 监听
     *
     * @param onItemSelectedListener
     */
    public void setOnItemSelectedListener(OnItemSelectedListener onItemSelectedListener) {
        this.onItemSelectedListener = onItemSelectedListener;
    }

    /**
     * 单条内容
     *
     * @author JiangPing
     */
    private class ItemObject {
        /**
         * id
         */
        public int id = 0;
        /**
         * 内容
         */
        public String itemText = "";
        /**
         * x坐标
         */
        public int x = 0;
        /**
         * y坐标
         */
        public int y = 0;
        /**
         * 移动距离
         */
        public int move = 0;
        /**
         * 字体画笔
         */
        private TextPaint textPaint;
        /**
         * 字体范围矩形
         */
        private Rect textRect;

        public ItemObject() {
            super();
        }

        /**
         * 绘制自身
         *
         * @param canvas         画板
         * @param containerWidth 容器宽度
         */
        public void drawSelf(Canvas canvas, int containerWidth) {

            if (textPaint == null) {
                textPaint = new TextPaint();
                textPaint.setAntiAlias(true);
            }

            if (textRect == null)
                textRect = new Rect();

            // 判断是否被选择
            if (isSelected()) {
                textPaint.setColor(selectedColor);
                // 获取距离标准位置的距离
                float moveToSelect = moveToSelected();
                moveToSelect = moveToSelect > 0 ? moveToSelect : moveToSelect * (-1);
                // 计算当前字体大小
                float textSize = normalFont + ((selectedFont - normalFont) * (1.0f - moveToSelect / (float) unitHeight));
                textPaint.setTextSize(textSize);
            } else {
                textPaint.setColor(normalColor);
                textPaint.setTextSize(normalFont);
            }

            // 返回包围整个字符串的最小的一个Rect区域
//            itemText = (String) TextUtils.ellipsize(itemText, textPaint, containerWidth, TextUtils.TruncateAt.END);
            textPaint.getTextBounds(itemText, 0, itemText.length(), textRect);
            // 判断是否可视
            if (!isInView())
                return;

            // 绘制内容
            canvas.drawText(itemText, x + controlWidth / 2 - textRect.width() / 2,
                    y + move + unitHeight / 2 + textRect.height() / 2, textPaint);

        }

        /**
         * 是否在可视界面内
         *
         * @return
         */
        public boolean isInView() {
            if (y + move > controlHeight || (y + move + unitHeight / 2 + textRect.height() / 2) < 0)
                return false;
            return true;
        }

        /**
         * 移动距离
         *
         * @param _move
         */
        public void move(int _move) {
            this.move = _move;
        }

        /**
         * 设置新的坐标
         *
         * @param _move
         */
        public void newY(int _move) {
            this.move = 0;
            this.y = y + _move;
        }

        /**
         * 判断是否在选择区域内
         *
         * @return
         */
        public boolean isSelected() {
            if ((y + move) >= controlHeight / 2 - unitHeight / 2 + lineHeight
                    && (y + move) <= controlHeight / 2 + unitHeight / 2 - lineHeight) {
                return true;
            }
            if ((y + move + unitHeight) >= controlHeight / 2 - unitHeight / 2 + lineHeight
                    && (y + move + unitHeight) <= controlHeight / 2 + unitHeight / 2 - lineHeight) {
                return true;
            }
            if ((y + move) <= controlHeight / 2 - unitHeight / 2 + lineHeight
                    && (y + move + unitHeight) >= controlHeight / 2 + unitHeight / 2 - lineHeight) {
                return true;
            }
            return false;
        }

        /**
         * 获取移动到标准位置需要的距离
         */
        public float moveToSelected() {
            return (controlHeight / 2 - unitHeight / 2) - (y + move);
        }
    }

    public interface OnItemSelectedListener {

        /**
         * 当前选中的item
         *
         * @param id
         * @param text
         */
        void onItemSelected(int id, String text);
    }
}







//public class WheelView extends ScrollView {
//    public static final String TAG = WheelView.class.getSimpleName();
//
//    public static class OnWheelViewListener {
//        public void onSelected(int selectedIndex, String item) {
//        }
//    }
//
//
//    private Context context;
////    private ScrollView scrollView;
//
//    private LinearLayout views;
//
//    public WheelView(Context context) {
//        super(context);
//        init(context);
//    }
//
//    public WheelView(Context context, AttributeSet attrs) {
//        super(context, attrs);
//        init(context);
//    }
//
//    public WheelView(Context context, AttributeSet attrs, int defStyle) {
//        super(context, attrs, defStyle);
//        init(context);
//    }
//
//    //    String[] items;
//    List<String> items;
//
//    private List<String> getItems() {
//        return items;
//    }
//
//    public void setItems(List<String> list) {
//        if (null == items) {
//            items = new ArrayList<String>();
//        }
//        items.clear();
//        items.addAll(list);
//
//        // 前面和后面补全
//        for (int i = 0; i < offset; i++) {
//            items.add(0, "");
//            items.add("");
//        }
//
//        initData();
//
//    }
//
//
//    public static final int OFF_SET_DEFAULT = 1;
//    int offset = OFF_SET_DEFAULT; // 偏移量（需要在最前面和最后面补全）
//
//    public int getOffset() {
//        return offset;
//    }
//
//    public void setOffset(int offset) {
//        this.offset = offset;
//    }
//
//    int displayItemCount; // 每页显示的数量
//
//    int selectedIndex = 1;
//
//
//    private void init(Context context) {
//        this.context = context;
//
////        scrollView = ((ScrollView)this.getParent());
////        Log.d(TAG, "scrollview: " + scrollView);
//        Log.d(TAG, "parent: " + this.getParent());
////        this.setOrientation(VERTICAL);
//        this.setVerticalScrollBarEnabled(false);
//
//        views = new LinearLayout(context);
//        views.setOrientation(LinearLayout.VERTICAL);
//        this.addView(views);
//
//        scrollerTask = new Runnable() {
//
//            public void run() {
//
//                int newY = getScrollY();
//                if (initialY - newY == 0) { // stopped
//                    final int remainder = initialY % itemHeight;
//                    final int divided = initialY / itemHeight;
////                    Log.d(TAG, "initialY: " + initialY);
////                    Log.d(TAG, "remainder: " + remainder + ", divided: " + divided);
//                    if (remainder == 0) {
//                        selectedIndex = divided + offset;
//
//                        onSeletedCallBack();
//                    } else {
//                        if (remainder > itemHeight / 2) {
//                            WheelView.this.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    WheelView.this.smoothScrollTo(0, initialY - remainder + itemHeight);
//                                    selectedIndex = divided + offset + 1;
//                                    onSeletedCallBack();
//                                }
//                            });
//                        } else {
//                            WheelView.this.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    WheelView.this.smoothScrollTo(0, initialY - remainder);
//                                    selectedIndex = divided + offset;
//                                    onSeletedCallBack();
//                                }
//                            });
//                        }
//
//
//                    }
//
//
//                } else {
//                    initialY = getScrollY();
//                    WheelView.this.postDelayed(scrollerTask, newCheck);
//                }
//            }
//        };
//
//
//    }
//
//    int initialY;
//
//    Runnable scrollerTask;
//    int newCheck = 50;
//
//    public void startScrollerTask() {
//
//        initialY = getScrollY();
//        this.postDelayed(scrollerTask, newCheck);
//    }
//
//    private void initData() {
//        displayItemCount = offset * 2 + 1;
//
//        for (String item : items) {
//            views.addView(createView(item));
//        }
//
//        refreshItemView(0);
//    }
//
//    int itemHeight = 0;
//
//    private TextView createView(String item) {
//        TextView tv = new TextView(context);
//        tv.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//        tv.setSingleLine(true);
//        tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
//        tv.setText(item);
//        tv.setGravity(Gravity.CENTER);
//        int padding = dip2px(15);
//        tv.setPadding(padding, padding, padding, padding);
//        if (0 == itemHeight) {
//            itemHeight = getViewMeasuredHeight(tv);
//            Log.d(TAG, "itemHeight: " + itemHeight);
//            views.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, itemHeight * displayItemCount));
//            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) this.getLayoutParams();
//            this.setLayoutParams(new LinearLayout.LayoutParams(lp.width, itemHeight * displayItemCount));
//        }
//        return tv;
//    }
//
//
//    @Override
//    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
//        super.onScrollChanged(l, t, oldl, oldt);
//
////        Log.d(TAG, "l: " + l + ", t: " + t + ", oldl: " + oldl + ", oldt: " + oldt);
//
////        try {
////            Field field = ScrollView.class.getDeclaredField("mScroller");
////            field.setAccessible(true);
////            OverScroller mScroller = (OverScroller) field.get(this);
////
////
////            if(mScroller.isFinished()){
////                Log.d(TAG, "isFinished...");
////            }
////
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
//
//
//        refreshItemView(t);
//
//        if (t > oldt) {
////            Log.d(TAG, "向下滚动");
//            scrollDirection = SCROLL_DIRECTION_DOWN;
//        } else {
////            Log.d(TAG, "向上滚动");
//            scrollDirection = SCROLL_DIRECTION_UP;
//
//        }
//
//
//    }
//
//    private void refreshItemView(int y) {
//        int position = y / itemHeight + offset;
//        int remainder = y % itemHeight;
//        int divided = y / itemHeight;
//
//        if (remainder == 0) {
//            position = divided + offset;
//        } else {
//            if (remainder > itemHeight / 2) {
//                position = divided + offset + 1;
//            }
//
////            if(remainder > itemHeight / 2){
////                if(scrollDirection == SCROLL_DIRECTION_DOWN){
////                    position = divided + offset;
////                    Log.d(TAG, ">down...position: " + position);
////                }else if(scrollDirection == SCROLL_DIRECTION_UP){
////                    position = divided + offset + 1;
////                    Log.d(TAG, ">up...position: " + position);
////                }
////            }else{
//////                position = y / itemHeight + offset;
////                if(scrollDirection == SCROLL_DIRECTION_DOWN){
////                    position = divided + offset;
////                    Log.d(TAG, "<down...position: " + position);
////                }else if(scrollDirection == SCROLL_DIRECTION_UP){
////                    position = divided + offset + 1;
////                    Log.d(TAG, "<up...position: " + position);
////                }
////            }
////        }
//
////        if(scrollDirection == SCROLL_DIRECTION_DOWN){
////            position = divided + offset;
////        }else if(scrollDirection == SCROLL_DIRECTION_UP){
////            position = divided + offset + 1;
//        }
//
//        int childSize = views.getChildCount();
//        for (int i = 0; i < childSize; i++) {
//            TextView itemView = (TextView) views.getChildAt(i);
//            if (null == itemView) {
//                return;
//            }
//            if (position == i) {
//                itemView.setTextColor(Color.parseColor("#0288ce"));
//            } else {
//                itemView.setTextColor(Color.parseColor("#c623e3"));
//            }
//        }
//    }
//
//    /**
//     * 获取选中区域的边界
//     */
//    int[] selectedAreaBorder;
//
//    private int[] obtainSelectedAreaBorder() {
//        if (null == selectedAreaBorder) {
//            selectedAreaBorder = new int[2];
//            selectedAreaBorder[0] = itemHeight * offset;
//            selectedAreaBorder[1] = itemHeight * (offset + 1);
//        }
//        return selectedAreaBorder;
//    }
//
//
//    private int scrollDirection = -1;
//    private static final int SCROLL_DIRECTION_UP = 0;
//    private static final int SCROLL_DIRECTION_DOWN = 1;
//
//    Paint paint;
//    int viewWidth;
//
//    @Override
//    public void setBackgroundDrawable(Drawable background) {
//
//        if (viewWidth == 0) {
//            viewWidth = ((Activity) context).getWindowManager().getDefaultDisplay().getWidth();
//            Log.d(TAG, "viewWidth: " + viewWidth);
//        }
//
//        if (null == paint) {
//            paint = new Paint();
//            paint.setColor(Color.parseColor("#0288ce"));
//            paint.setStrokeWidth(dip2px(1f));
//        }
//
//        background = new Drawable() {
//            @Override
//            public void draw(Canvas canvas) {
//                canvas.drawLine(viewWidth * 1 / 6, obtainSelectedAreaBorder()[0], viewWidth * 5 / 6, obtainSelectedAreaBorder()[0], paint);
//                canvas.drawLine(viewWidth * 1 / 6, obtainSelectedAreaBorder()[1], viewWidth * 5 / 6, obtainSelectedAreaBorder()[1], paint);
//            }
//
//            @Override
//            public void setAlpha(int alpha) {
//
//            }
//
//            @Override
//            public void setColorFilter(ColorFilter cf) {
//
//            }
//
//            @Override
//            public int getOpacity() {
//                return 0;
//            }
//        };
//
//
//        super.setBackgroundDrawable(background);
//
//    }
//
//    @Override
//    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
//        super.onSizeChanged(w, h, oldw, oldh);
//        Log.d(TAG, "w: " + w + ", h: " + h + ", oldw: " + oldw + ", oldh: " + oldh);
//        viewWidth = w;
//        setBackgroundDrawable(null);
//    }
//
//    /**
//     * 选中回调
//     */
//    private void onSeletedCallBack() {
//        if (null != onWheelViewListener) {
//            onWheelViewListener.onSelected(selectedIndex, items.get(selectedIndex));
//        }
//
//    }
//
//    public void setSeletion(int position) {
//        final int p = position;
//        selectedIndex = p + offset;
//        this.post(new Runnable() {
//            @Override
//            public void run() {
//                WheelView.this.smoothScrollTo(0, p * itemHeight);
//            }
//        });
//
//    }
//
//    public String getSeletedItem() {
//        return items.get(selectedIndex);
//    }
//
//    public int getSeletedIndex() {
//        return selectedIndex - offset;
//    }
//
//
//    @Override
//    public void fling(int velocityY) {
//        super.fling(velocityY / 3);
//    }
//
//    @Override
//    public boolean onTouchEvent(MotionEvent ev) {
//        if (ev.getAction() == MotionEvent.ACTION_UP) {
//
//            startScrollerTask();
//        }
//        return super.onTouchEvent(ev);
//    }
//
//    private OnWheelViewListener onWheelViewListener;
//
//    public OnWheelViewListener getOnWheelViewListener() {
//        return onWheelViewListener;
//    }
//
//    public void setOnWheelViewListener(OnWheelViewListener onWheelViewListener) {
//        this.onWheelViewListener = onWheelViewListener;
//    }
//
//    private int dip2px(float dpValue) {
//        final float scale = context.getResources().getDisplayMetrics().density;
//        return (int) (dpValue * scale + 0.5f);
//    }
//
//    private int getViewMeasuredHeight(View view) {
//        int width = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
//        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
//        view.measure(width, expandSpec);
//        return view.getMeasuredHeight();
//    }
//
//}
