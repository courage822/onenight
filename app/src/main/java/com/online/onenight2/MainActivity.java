package com.online.onenight2;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.library.dialog.OnDoubleDialogClickListener;
import com.library.utils.DialogUtil;
import com.library.utils.LogUtil;
import com.library.utils.SharedPreferenceUtil;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.online.onenight2.base.BaseActivity;
import com.online.onenight2.base.RxBus;
import com.online.onenight2.bean.BaseModel;
import com.online.onenight2.bean.ChatListBean;
import com.online.onenight2.bean.Fate;
import com.online.onenight2.bean.FateUser;
import com.online.onenight2.bean.LocationInfo;
import com.online.onenight2.bean.RecommendUser;
import com.online.onenight2.bean.UserBase;
import com.online.onenight2.callback.JsonCallback;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.hot.activity.UserInfoActivity;
import com.online.onenight2.hot.adapter.PopuWindowsGvAdapter;
import com.online.onenight2.hot.fragment.HomeFragment;
import com.online.onenight2.hot.presenter.MainPresenter;
import com.online.onenight2.math.fragment.MathFragment;
import com.online.onenight2.message.activity.ChatMegActivity;
import com.online.onenight2.message.fragment.MessageFragment;
import com.online.onenight2.profile.fragment.ProfileFragment;
import com.online.onenight2.utils.GoogleUtil;
import com.online.onenight2.utils.ImageLoaderUtils;
import com.online.onenight2.utils.Utils;
import com.online.onenight2.utils.YeMeiShow;
import com.online.onenight2.view.CircularImage;
import com.online.onenight2.xml.PlatformInfoXml;
import com.online.onenight2.xml.UserInfoXml;
import com.online.onenight2.yemei.SaveTimeUtils;
import com.online.onenight2.yemei.model.MyPrestener;
import com.online.onenight2.yemei.view.IVContract;
import com.yinglan.alphatabs.AlphaTabsIndicator;
import com.yinglan.alphatabs.OnTabChangedListner;
//
//import org.greenrobot.eventbus.EventBus;
//import org.greenrobot.eventbus.Subscribe;
//import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Response;
import rx.functions.Action1;

public class MainActivity extends BaseActivity<MainPresenter> implements OnTabChangedListner, IVContract {
    @BindView(R.id.fl_body)
    FrameLayout flBody;
    @BindView(R.id.alphaindicator)
    AlphaTabsIndicator alphaIndicator;
    @BindView(R.id.recommenduser_layout)
    LinearLayout recommenduser_layout;
    @BindView(R.id.iv_avatar)
    CircularImage ivAvatar;
    @BindView(R.id.tv_nikename)
    TextView tvNikename;
    @BindView(R.id.tv_ignore)
    TextView tvIgnore;
    @BindView(R.id.tv_golook)
    TextView tvGolook;
    @BindView(R.id.ll_look)
    LinearLayout llLook;
    @BindView(R.id.tv_sentence)
    TextView tvSentence;
    @BindView(R.id.main_activity_rl)
    RelativeLayout root_rl;
    @BindView(R.id.main_activity_tv_showpop)
    TextView tv_shopop;
    private HomeFragment mHotFragment;
    private MathFragment mMathFragment;
    private MessageFragment mMessageFragment;
    private ProfileFragment mProfileFragment;
    private FragmentTransaction transaction;
    private int sex;
    List<FateUser> listUser = new ArrayList<>();
    //当前角标的位置
    private static final int CURRENTTABPOSITION = 0;
    //未读消息数量
    private static int MSGNUM;
    private int pageNum = 1;
    private static final String PAGESIZE = "20";
    //是否继续推荐好友
    private boolean isRemmenduser = true;
    private static final int Get_DATA = 1;
    private static final int HEAD_TIME = 20000;
    private boolean isGetMegLis;
    private boolean isCanLoad = true;
    //是否开启了悬挂通知栏权限
    private boolean isShowNotification = true;
    private MyPrestener myPrestener;
    private UserBase userBaseEnglish;
    private Bundle savedInstance;
    private boolean isHaveMessage = true;
    private LocationManager mLocationManager;
    private static final int PERMISSION_CODE_ACCESS_FINE_LOCATION = 16;
    private GridView popu_gv;
    private ImageView popu_iv_cancle;
    private PopupWindow pop_window;
    private List<String> list = new ArrayList<>();
    PopuWindowsGvAdapter popuWindowsGvAdapter;
    private Button btn_sayhello;
    private static List<String> list_userid = new ArrayList<>();
    private int offset = 120;
    private int width = 1200;
    private boolean isFirstInto=true;

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        //eventbus传参框架
//        EventBus.getDefault().register(this);
//    }
//


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        //eventbus传参框架
//        EventBus.getDefault().register(this);
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        savedInstance = savedInstanceState;
        initTab();
        //  checkMessagePermission();
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int widthPixels = dm.widthPixels;
        int heightPixels = dm.heightPixels;
        float density = dm.density;
        int screenWidth = (int) (widthPixels * density);
        int screenHeight = (int) (heightPixels * density);
        initFragment(savedInstanceState);

        //获取定位信息
        requestLocation();

        if (screenWidth > 2835) {
            offset = 60;
            width = 1300;
        } else if (screenWidth <= 2835 && screenWidth > 1440) {
            offset = 120;
            width = 1200;
        } else if (screenWidth <= 1440) {
            offset = 40;
            width = 900;
        }
        showopuWindows();
////        获取弹窗的数据
//        loadYuanFenData();
        // 检查商品
        //  checkIAP();
        // 检查是否有支付成功但未开通服务的商品
        // checkPayInfo();
        myPrestener = new MyPrestener(this);
    }


    private void showopuWindows() {
        View view = LayoutInflater.from(this).inflate(R.layout.popu_layout, null);
        CardView cd = ((CardView) view.findViewById(R.id.popu_layout_bg));
        cd.getBackground().setAlpha(245);
        popu_gv = ((GridView) view.findViewById(R.id.popu_layout_gv));
        popu_iv_cancle = ((ImageView) view.findViewById(R.id.popu_layout_iv_cancle));
        btn_sayhello = (Button) view.findViewById(R.id.popu_layout_btn);
        pop_window = new PopupWindow(view, RelativeLayout.LayoutParams.WRAP_CONTENT, width, true);
        btn_sayhello.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pop_window.dismiss();
                allSayHello();
            }
        });
        popu_iv_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pop_window.dismiss();
            }
        });
    }

    /**
     * 获取屏幕高度(px)
     */
    public static int getScreenHeight(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    /**
     * 获取屏幕宽度(px)
     */
    public static int getScreenWidth(Context context) {

        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public void loadYuanFenData() {
        if(isFirstInto){
            isFirstInto=false;
            OkGo.post(IUrlConstant.URL_FATE)
                    .headers("token", PlatformInfoXml.getToken())
                    .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                    .params("pageNum", String.valueOf((1 + (int) (Math.random() * 200))))
                    .params("pageSize", "6")
                    .execute(
                            new JsonCallback<Fate>() {
                                @Override
                                public void onSuccess(Fate fate, Call call, Response response) {
                                    if (fate != null) {
                                        if (fate.getIsSucceed().equals("1")) {
                                            List<FateUser> listUser1 = fate.getListUser();
                                            for (FateUser fateUser : listUser1) {
                                                listUser.add(fateUser);
                                                list_userid.add(fateUser.getUserBaseEnglish().getId());
                                            }
                                            popuWindowsGvAdapter = new PopuWindowsGvAdapter(listUser, mContext);
                                            popu_gv.setAdapter(popuWindowsGvAdapter);
                                            if (listUser != null && listUser.size() > 0) {
                                                new Handler().postDelayed(new Runnable() {
                                                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                                                    public void run() {
                                                        if (!isFinishing()) {
                                                            pop_window.showAsDropDown(tv_shopop, offset, 0, Gravity.CENTER);
                                                        }
                                                    }
                                                }, 3000);
                                            }
                                        }
                                    }
                                }

                                @Override
                                public void onError(Call call, Response response, Exception e) {
                                    super.onError(call, response, e);
                                }
                            }
                    );
        }

    }

    private void checkMessagePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(MainActivity.this)) {
                DialogUtil.showDoubleBtnDialog(getSupportFragmentManager(), getString(R.string.shownotititle), getString(R.string.showno_tips), getString(R.string.change_sure), getString(R.string.change_cancel), false, new OnDoubleDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view) {
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                        intent.setData(Uri.parse("package:" + getPackageName()));
                        MainActivity.this.startActivityForResult(intent, 100);
                    }

                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
            } else {
                isShowNotification = true;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            isRemmenduser = true;
            if (Build.VERSION.SDK_INT >= 23) {
                if (!Settings.canDrawOverlays(MainActivity.this)) {
                    isShowNotification = false;
                } else {
                    isShowNotification = true;
                }
            }
        }
    }

    @Override
    public MainPresenter newPresenter() {
        return new MainPresenter();
    }


    @Override
    public void initData() {
        getMsgList(pageNum, PAGESIZE, 0);
    }

    private void initTab() {
        alphaIndicator.setOnTabChangedListner(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        myPrestener.isCanSee(true);
        myPrestener.loadData();
        isHaveMessage = true;
        mRxManager.on(ConfigConstant.TOTALUNREAD, new Action1<Integer>() {
            @Override
            public void call(Integer integer) {

//                if (isHaveMessage) {
                if (MSGNUM!=integer) {
                    MSGNUM = integer;
                    //显示未读消息数
                    alphaIndicator.getTabView(2).showNumber(MSGNUM);
                    /**保存未读消息数**/
                    SharedPreferenceUtil.setIntValue(Utils.getContext(), "MSGNUM", "GET_UNREAD", MSGNUM);
//                    isHaveMessage = false;
                }
            }
        });
        mRxManager.on("activity_finish", new Action1<Boolean>() {
            @Override
            public void call(Boolean integer) {
                finish();
            }
        });

        //更换账号后
        mRxManager.on("change_user", new Action1<Boolean>() {

            @Override
            public void call(Boolean aBoolean) {
                //显示未读消息数
                if (isCanLoad) {
                    alphaIndicator.getTabView(2).showNumber(0);
                    SharedPreferenceUtil.setIntValue(Utils.getContext(), "MSGNUM", "GET_UNREAD", 0);
                    isCanLoad = false;
                }
            }
        });
    }

    //消息
    public void ShowMsgListData(ChatListBean chatListBean) {
        if (chatListBean.getTotalUnread() > MSGNUM) {
            RxBus.getInstance().post("refresh_msg", "refresh");
        }
        MSGNUM = chatListBean.getTotalUnread();
        alphaIndicator.getTabView(2).showNumber(MSGNUM);
        SharedPreferenceUtil.setIntValue(Utils.getContext(), "MSGNUM", "GET_UNREAD", MSGNUM);
    }

    //好友推荐
    public void showRecommendUser(RecommendUser recommendUser) {
        isRemmenduser = false;
        String sentence = recommendUser.getSentence();
        userBaseEnglish = recommendUser.getUser().getUserBaseEnglish();
        recommenduser_layout.setVisibility(View.VISIBLE);
        tvNikename.setText(userBaseEnglish.getNickName());
        tvSentence.setText(sentence);
        ImageLoaderUtils.display(MainActivity.this, ivAvatar, userBaseEnglish.getImage().getThumbnailUrl());
    }

    /**
     * 请求定位信息
     */
    private void requestLocation() {
         String getLatitude = SharedPreferenceUtil.getStringValue(MainActivity.this, "LOCATION", "getLatitude", "");
         String getLongitude = SharedPreferenceUtil.getStringValue(MainActivity.this, "LOCATION", "getLongitude", "");
       if(TextUtils.isEmpty(getLatitude) || TextUtils.isEmpty(getLongitude)){
           mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
           if (ContextCompat.checkSelfPermission(MainActivity.this,
                   android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
               // 请求android.permission.ACCESS_FINE_LOCATION
               ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_CODE_ACCESS_FINE_LOCATION);
           } else {
               if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {// Network定位
                   mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 5, mLocationListener);
               } else if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// GPS定位
                   uploadLocation(mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
               }
           }
       }else{
           String country = UserInfoXml.getCountry();
           Map<String, String> googleAddress = GoogleUtil.getGoogleAddress(getLatitude, getLongitude, "English", country);
//        for (String key : googleAddress.keySet()) {
//            if(key=="province")
//            Log.v("============","key= "+ key + " and value= " + googleAddress.get(key));
//        }
           if(googleAddress!=null){
               String province = googleAddress.get("province");
               if(!TextUtils.isEmpty(province)){
//            英国城市为空。。。
                   if (!province.equals(UserInfoXml.getProvinceName())) {
                       UserInfoXml.setProvinceName(province);
                   }
                   getLocation(getLatitude,getLongitude,province);
               }else {
                   mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                   if (ContextCompat.checkSelfPermission(MainActivity.this,
                           android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                       // 请求android.permission.ACCESS_FINE_LOCATION
                       ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_CODE_ACCESS_FINE_LOCATION);
                   } else {
                       if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {// Network定位
                           mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 5, mLocationListener);
                       } else if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// GPS定位
                           uploadLocation(mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
                       }
                   }
               }
           }else{
               mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
               if (ContextCompat.checkSelfPermission(MainActivity.this,
                       android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                   // 请求android.permission.ACCESS_FINE_LOCATION
                   ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_CODE_ACCESS_FINE_LOCATION);
               } else {
                   if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {// Network定位
                       mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 5, mLocationListener);
                   } else if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// GPS定位
                       uploadLocation(mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
                   }
               }
           }

       }

    }

    private LocationListener mLocationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            uploadLocation(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CODE_ACCESS_FINE_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {// 申请权限成功
                if(mLocationManager!=null){
                    if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {// Network定位
                        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 5, mLocationListener);
                    } else if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// GPS定位
                        uploadLocation(mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
                    }
                }

            }
        }
    }

    /**
     * 上传位置信息
     *
     * @param location 定位成功后的信息
     */
    private void uploadLocation(Location location) {
        if (location != null) {
            String getLatitude = String.valueOf(location.getLatitude());
            String getLongitude = String.valueOf(location.getLongitude());
            String country = UserInfoXml.getCountry();
//            if(country.equals("Taiwai"))
            Map<String, String> googleAddress = GoogleUtil.getGoogleAddress(getLatitude, getLongitude, "English", country);
           if(googleAddress!=null){
               String province = googleAddress.get("province");
//            英国城市为空。。。
               if (!TextUtils.isEmpty(province) && !province.equals(UserInfoXml.getProvinceName())) {
                   UserInfoXml.setProvinceName(province);
               }
               getLocation(getLatitude,getLongitude,province);
           }

        }
    }

    private void getLocation(String getLatitude, String getLongitude, String province) {
        LocationInfo locationInfo = new LocationInfo();
        locationInfo.setAddrStr("");
        locationInfo.setProvince(province);
        locationInfo.setCity("");
        locationInfo.setCityCode("");
        locationInfo.setDistrict("");
        locationInfo.setStreet("");
        locationInfo.setStreetNumber("");
        locationInfo.setLatitude(getLatitude);
        locationInfo.setLongitude(getLongitude);
        Map<String, LocationInfo> map = new HashMap<String, LocationInfo>();
        map.put("locationInfo", locationInfo);
        OkGo.post(IUrlConstant.URL_UPLOAD_LOCATION)
                .headers("token", PlatformInfoXml.getToken())
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("locationInfo", JSON.toJSONString(map))
                .execute(new StringCallback() {
                             @Override
                             public void onSuccess(String s, Call call, Response response1) {

                                 if (!TextUtils.isEmpty(s)) {
                                     BaseModel response = JSON.parseObject(s, BaseModel.class);
                                     if (response != null) {
                                         String isSucceed = response.getIsSucceed();
                                         if (!TextUtils.isEmpty(isSucceed) && "1".equals(isSucceed)) {// 上传成功
                                             LogUtil.i(LogUtil.TAG_ZL, "上传位置信息成功");
                                             // 发送事件，更新位置信息
//                                             if(getProvince){
//                                                 EventBus.getDefault().post(new UpdateUserProvince());
//                                                 EventBus.getDefault().post(new MatchInfoChangeEvent());
//                                             }
                                         }
                                     }
                                 }
                                 // 获取弹窗的数据
                                 loadYuanFenData();
                             }
                            @Override
                            public void onError(Call call, Response response, Exception e) {
                                super.onError(call, response, e);
                                // 获取弹窗的数据
                                loadYuanFenData();
                            }

                         }
                );
    }

    @OnClick({R.id.tv_ignore, R.id.tv_golook,R.id.ll_look})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tv_ignore:
                recommenduser_layout.setVisibility(View.GONE);
                break;
            case R.id.tv_golook:
                isRemmenduser = true;
                recommenduser_layout.setVisibility(View.GONE);
                goLook(userBaseEnglish, mContext);
                break;
            case R.id.ll_look:
                isRemmenduser = true;
                recommenduser_layout.setVisibility(View.GONE);
                goLook(userBaseEnglish, mContext);
                break;
        }
    }

    //初始化碎片
    private void initFragment(Bundle savedInstanceState) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (null == savedInstanceState) {
            mHotFragment = new HomeFragment();
            mMathFragment = new MathFragment();
            mMessageFragment = new MessageFragment();
            mProfileFragment = new ProfileFragment();

            fragmentTransaction.add(R.id.fl_body, mHotFragment, "mHotFragment");
            fragmentTransaction.add(R.id.fl_body, mMathFragment, "mMathFragment");
            fragmentTransaction.add(R.id.fl_body, mMessageFragment, "mMessageFragment");
            fragmentTransaction.add(R.id.fl_body, mProfileFragment, "mProfileFragment");
        } else {
            mHotFragment = (HomeFragment) getSupportFragmentManager().findFragmentByTag("mHotFragment");
            mMathFragment = (MathFragment) getSupportFragmentManager().findFragmentByTag("mMathFragment");
            mMessageFragment = (MessageFragment) getSupportFragmentManager().findFragmentByTag("mMessageFragment");
            mProfileFragment = (ProfileFragment) getSupportFragmentManager().findFragmentByTag("mProfileFragment");
        }
        fragmentTransaction.commit();
        SwitchTo(CURRENTTABPOSITION);
    }

    private void SwitchTo(int position) {
        transaction = getSupportFragmentManager().beginTransaction();
        switch (position) {
            case 0:
                //为了初始化后能调用onHiddenChanged
                transaction.hide(mHotFragment);
                fragmentShoworHide(mHotFragment, mMathFragment, mMessageFragment, mProfileFragment);
                break;
            case 1:
                fragmentShoworHide(mMathFragment, mHotFragment, mMessageFragment, mProfileFragment);
                break;
            case 2:
                fragmentShoworHide(mMessageFragment, mHotFragment, mMathFragment, mProfileFragment);
                break;
            case 3:
                fragmentShoworHide(mProfileFragment, mHotFragment, mMathFragment, mMessageFragment);
                break;
        }
    }

    private void fragmentShoworHide(Fragment f1, Fragment f2, Fragment f3, Fragment f4) {
        transaction.show(f1);
        transaction.hide(f2);
        transaction.hide(f3);
        transaction.hide(f4);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void onTabSelected(int tabNum) {
        recommenduser_layout.setVisibility(View.GONE);
        switch (tabNum) {
            case 0:
                SwitchTo(0);
                break;
            case 1:
                SwitchTo(1);
                break;
            case 2:
                SwitchTo(2);
                break;
            case 3:
                SwitchTo(3);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SaveTimeUtils.clearTime(MainActivity.this);
        //取消所有请求
        OkGo.getInstance().cancelAll();
//        EventBus.getDefault().unregister(this);
    }

    /**
     * 获取消息列表
     *
     * @param pageNum
     * @param pageSize
     * @param type
     */
    public void getMsgList(int pageNum, String pageSize, int type) {
        OkGo.post(IUrlConstant.URL_GET_ALL_CHAT_LIST)
                .tag(this)
                .params("pageNum", String.valueOf(pageNum))
                .params("pageSize", pageSize)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .execute(new JsonCallback<ChatListBean>() {

                    @Override
                    public void onSuccess(ChatListBean chatListBean, Call call, Response response) {
                        if (null != chatListBean) {
                            hideLoadingPress();
                            if (chatListBean.getIsSucceed() != null & chatListBean.getIsSucceed().equals("1")) {
                                if (chatListBean.getListChat().size() > 0) {
                                    ShowMsgListData(chatListBean);
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                        hideLoadingPress();
                    }
                });
    }

    /**
     * 获取好友推荐
     */
    public void getRecommendUser() {

        OkGo.post(IUrlConstant.URL_GET_RECOMMENDUSER)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("type", String.valueOf(0))
                .params("cycle", String.valueOf(20000))
                .execute(new JsonCallback<RecommendUser>() {

                    @Override
                    public void onSuccess(RecommendUser recommendUser, Call call, Response response) {
                        if (null != recommendUser) {
                            if (!TextUtils.isEmpty(recommendUser.getIsSucceed()) && recommendUser.getIsSucceed().equals("1")) {
                                showRecommendUser(recommendUser);


                            }
                        }
                    }
                });
    }

    /**
     * 群打招呼
     */
    public void allSayHello() {
        OkGo.post(IUrlConstant.URL_ALL_SAYHELLO)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("type", "1")
                .addUrlParams("listUid", list_userid)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(String s, Call call, Response response) {
                        Toast.makeText(MainActivity.this, R.string.say_hello_success, Toast.LENGTH_SHORT).show();
                        pop_window.dismiss();
                    }

                    @Override
                    public void onError(Call call, Response response, Exception e) {
                        super.onError(call, response, e);
                    }
                });
    }

    /**
     * 去看看，付费用户--->聊天界面；未付费用户--->对方空间页
     *
     * @param userBaseEnglish
     * @param mContext
     */
    public void goLook(UserBase userBaseEnglish, Context mContext) {
        //付费用户跳转到聊天界面
        Bundle bundle = new Bundle();
        if (UserInfoXml.isBeanUser() || UserInfoXml.isVip() || UserInfoXml.isMonthly() || UserInfoXml.getGender().equals("1")) {
            bundle.putString(ConfigConstant.USERID, userBaseEnglish.getId());
            bundle.putString(ConfigConstant.NICKNAME, userBaseEnglish.getNickName());
            Intent intent = new Intent(mContext, ChatMegActivity.class);
            intent.putExtras(bundle);
            if (null != userBaseEnglish.getImage()) {
                bundle.putString(ConfigConstant.USERAVATAR, userBaseEnglish.getImage().getThumbnailUrl());
            }
            mContext.startActivity(intent);
            overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
        } else {
            bundle.putString(ConfigConstant.USERID, userBaseEnglish.getId());
            bundle.putString(ConfigConstant.NICKNAME, userBaseEnglish.getNickName());
            bundle.putString(ConfigConstant.ISRECORD, "0");
            bundle.putString(ConfigConstant.SOURCETAG, "1");
            Intent intent = new Intent(mContext, UserInfoActivity.class);
            intent.putExtras(bundle);
            mContext.startActivity(intent);
            overridePendingTransition(R.anim.login_anim_in, R.anim.login_anim_out);
        }
    }

    // 用来计算返回键的点击间隔时间
    private long exitTime = 0;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getAction() == KeyEvent.ACTION_DOWN) {
            if ((System.currentTimeMillis() - exitTime) > 2000) {
                //弹出提示，可以有多种方式
                Toast.makeText(getApplicationContext(), getString(R.string.tuichu), Toast.LENGTH_SHORT).show();
                exitTime = System.currentTimeMillis();
            } else {
                finish();
            }
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public Context getContext() {
        return MainActivity.this;
    }

    @Override
    public void setYeMeiData(String thumbnailUrl, int userId, String nickName, int age, int sex, String height, String msg, int type, int noticeType) {
        getMsgList(pageNum, PAGESIZE, 0);
        if (isRemmenduser) {
            getRecommendUser();
        }
        YeMeiShow.initNotification(MainActivity.this, getSupportFragmentManager(), thumbnailUrl, userId, nickName, age, sex, height, msg, type, noticeType);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        myPrestener.isCanSee(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        myPrestener.isCanSee(false);
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onEvent(Message msg) {
//        if (msg.what == 99999) {
//            MSGNUM = (int) msg.obj;
//            alphaIndicator.getTabView(2).showNumber(MSGNUM);
//        }
//    }

}
