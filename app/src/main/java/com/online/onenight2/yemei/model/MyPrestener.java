package com.online.onenight2.yemei.model;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.online.onenight2.base.RxManager;
import com.online.onenight2.bean.NoticeRecommend;
import com.online.onenight2.bean.NotificationNew;
import com.online.onenight2.constant.ConfigConstant;
import com.online.onenight2.constant.IUrlConstant;
import com.online.onenight2.xml.PlatformInfoXml;
import com.online.onenight2.yemei.SaveTimeUtils;
import com.online.onenight2.yemei.presenter.IvPrestener;
import com.online.onenight2.yemei.view.IVContract;

import okhttp3.Call;
import okhttp3.Response;
import rx.functions.Action1;


/**
 * Created by Administrator on 2017/6/30.
 */

public class MyPrestener implements IvPrestener {
    private Context context;
    private IVContract mView;
    private boolean flag=true;
    //用户是否打开用户空间页面的标记，打开用户空间页面不推送 谁看过我的消息
    private boolean isUserinfo = false;
    //用户是否打开聊天页面的标记，打开聊天页面不推送 来信消息
    private boolean isChat = false;
    private RxManager rxManager;
     private Handler handler;
    boolean isCanShow=true;
    private static final int HEAD_TIME = 20000;
    public MyPrestener(IVContract mView) {
        this.context = mView.getContext();
        this.mView = mView;
        handler=new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if(flag){
                    getNotificationMessages();
                    handler.sendEmptyMessageDelayed(1,HEAD_TIME);
                }
            }
        };
    }

    @Override
    public void loadData() {
      // handler.sendEmptyMessageDelayed(1,3000);
    }

    @Override
    public void isCanSee(boolean falg) {
        flag=falg;
        if(falg){
            handler.sendEmptyMessageDelayed(1,HEAD_TIME);
        }else if(!falg){
            handler.removeMessages(1);
        }
    }
    private String lastMsgTime= "";
    /**
     * 获取页眉消息
     */
    public void getNotificationMessages(){
        rxManager = new RxManager();
        rxManager.on(ConfigConstant.ISUSERINFO, new Action1<Boolean>() {

            @Override
            public void call(Boolean aBoolean) {
                isUserinfo = aBoolean;
            }
        });
        rxManager.on(ConfigConstant.ISCHAT, new Action1<Boolean>() {

            @Override
            public void call(Boolean aBoolean) {
                isChat = aBoolean;
            }
        });
        isCanShow=true;
        OkGo.post(IUrlConstant.URL_GET_HEADNOTICENEW)
                .params("platformInfo", PlatformInfoXml.getPlatformJsonString())
                .params("lastMsgTime", SaveTimeUtils.getTime(context))
                .execute(new StringCallback() {
                             @Override
                             public void onSuccess(String s, Call call, Response response) {
                                 if(!TextUtils.isEmpty(s)){
                                     //未读消息
                                     NotificationNew notificationNew = JSON.parseObject(s, NotificationNew.class);
                                     if(null!=notificationNew){
                                         if(!TextUtils.isEmpty(notificationNew.getIsSucceed())&&notificationNew.getIsSucceed().equals("1")){
                                             if (null != notificationNew.getUnreadMsgBoxList()) {
                                                 //未讀信息
                                                 if (notificationNew.getNoticeType() == 1 && !isChat) {
                                                     if (null != notificationNew.getUnreadMsgBoxList()) {
                                                         int size = notificationNew.getUnreadMsgBoxList().size();
                                                         notificationNew.getUnreadMsgBoxList().size();
                                                         NotificationNew.UnreadMsgBoxList unreadMsgBoxList = notificationNew.getUnreadMsgBoxList().get(size - 1);
                                                         String msg = unreadMsgBoxList.getMsg();
                                                         int type = unreadMsgBoxList.getType();
                                                         int noticeType = notificationNew.getNoticeType();
                                                         SaveTimeUtils.saveTime(notificationNew.getLastMsgTime(),context);
                                                         NotificationNew.UnreadMsgBoxList.UserBaseEnglish userBaseEnglish =unreadMsgBoxList.getUserBaseEnglish();
                                                         if(userBaseEnglish!=null){

                                                             String  height = userBaseEnglish.getHeightCm();
                                                             String nickName = userBaseEnglish.getNickName();
                                                             int  age = userBaseEnglish.getAge();
                                                             int  userId = userBaseEnglish.getId();
                                                             int  sex = userBaseEnglish.getSex();
                                                             NotificationNew.UnreadMsgBoxList.UserBaseEnglish.Image image = userBaseEnglish.getImage();
                                                             if(image!=null){
                                                                 String thumbnailUrl = userBaseEnglish.getImage().getThumbnailUrl();
                                                                 if(isCanShow){
                                                                     mView.setYeMeiData(thumbnailUrl, userId, nickName, age, sex, height, msg, type, noticeType);
                                                                     isCanShow=false;
                                                                 }
                                                             }
                                                         }
                                                     }
                                                 }
                                             }
                                         }
                                     }
                                     //推荐用户
                                     NoticeRecommend noticeRecommend = JSON.parseObject(s, NoticeRecommend.class);
                                     if(noticeRecommend!=null){
                                         if(noticeRecommend.getIsSucceed().equals("1")&&noticeRecommend.getNoticeType()==2){
                                             NoticeRecommend.RemoteYuanfenUserBaseBean remoteYuanfenUserBase = noticeRecommend.getRemoteYuanfenUserBase();
                                             if(remoteYuanfenUserBase!=null){
                                                 NoticeRecommend.RemoteYuanfenUserBaseBean.UserBaseEnglishBean userBaseEnglish = remoteYuanfenUserBase.getUserBaseEnglish();
                                                 String nickName = userBaseEnglish.getNickName();
                                                 int id = userBaseEnglish.getId();
                                                 int age = userBaseEnglish.getAge();
                                                 NoticeRecommend.RemoteYuanfenUserBaseBean.UserBaseEnglishBean.ImageBean image = userBaseEnglish.getImage();
                                                 if(image!=null){
                                                     String imageUrl = image.getImageUrl();
                                                     if(isCanShow){
                                                         mView.setYeMeiData(imageUrl, id, nickName, age, 0, "", "", 0, 2);
                                                         isCanShow=false;
                                                     }
                                                 }

                                             }

                                         }
                                     }
                                 }
                             }
                         }
                );
    }
}
