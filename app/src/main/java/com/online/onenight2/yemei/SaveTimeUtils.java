package com.online.onenight2.yemei;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

/**
 * Created by Administrator on 2017/7/21.
 */

public class SaveTimeUtils {
    public static void saveTime(String time,Context context){
        SharedPreferences sharedPreferences=context.getSharedPreferences("save_time",Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("time", time);
        edit.commit();
    }
    public static String getTime(Context context){
        SharedPreferences sharedPreferences=context.getSharedPreferences("save_time",Context.MODE_PRIVATE);
        String time = sharedPreferences.getString("time", "");
        if(!TextUtils.isEmpty(time)){
            return time;
        }else{
            return "";
        }
    }
    public static void clearTime(Context context){
        SharedPreferences sharedPreferences=context.getSharedPreferences("save_time",Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.clear();
        edit.commit();
    }
}
