package com.online.onenight2.yemei.view;

import android.content.Context;

/**
 * Created by Administrator on 2017/6/30.
 */

public interface IVContract {
    Context getContext();
    void setYeMeiData(String thumbnailUrl, int userId, String nickName, int age, int sex, String height, String msg, int type, int noticeType);
}
